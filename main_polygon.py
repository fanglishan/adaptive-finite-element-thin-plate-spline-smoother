#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2021-10-26 23:05:40
@Last Modified time: 2022-04-28 17:31:20

This class contains modules to extrac build a polygon for non-uniformly distributed
read-world data sets.



"""

# Import libraries
from plot.DataPlot import plot_data_dist_2D

import os
import sys
import pandas as pd
import numpy as np
from descartes import PolygonPatch
import matplotlib.pyplot as plt
import alphashape
import pygmsh
import csv
from copy import deepcopy
from math import sqrt, cos, sin, atan
import random
#sys.path.insert(0, os.path.dirname(os.getcwd()))


def read_file(filename):
	""" Read data file """

	path = "dataset/" + filename + "_subset.csv"

	data = []

	with open(path) as csvfile:

		# Read numeric fields
		reader = csv.reader(csvfile, quoting=csv.QUOTE_NONNUMERIC)

		# Read each row as a list
		for row in reader:
			if len(row) > 1:
				data.append([row[0], row[1]])
	
	print("read data:", len(data))

	return data


def expand_data(data, expand):
	
	dist = expand

	new_data = []

	for point in data:
		if len(data) < 1000 or random.random() < 0.5:
			new_data.append([point[0]+dist,point[1]])
			new_data.append([point[0],point[1]+dist])
			new_data.append([point[0],point[1]-dist])
			new_data.append([point[0]-dist,point[1]])
			new_data.append([point[0]-dist,point[1]-dist])
			new_data.append([point[0]-dist,point[1]+dist])
			new_data.append([point[0]+dist,point[1]-dist])
			new_data.append([point[0]+dist,point[1]+dist])

	return new_data


def build_polygon(filename, alpha_value, expand, if_plot=False):
	""" Build a polygon containing given data using alphashape """

	data = read_file(filename)


#	fig, ax = plt.subplots()
#	ax.scatter(*zip(*points_2d))
#	plt.show()

#	expanded_data = data
	expanded_data = expand_data(data, expand)

	polygon = alphashape.alphashape(expanded_data, alpha_value)
#	polygon = alphashape.alphashape(data, alpha_value)
	
	if if_plot:
		fig, ax = plt.subplots()
		ax.scatter(*zip(*data))
		ax.add_patch(PolygonPatch(polygon, alpha=0.2))
		plt.show()
	
	return data, polygon


def process_coord(vertices, nearby_dist):
	""" Expand polygon and remove nearby points """

	new_vertices = []


	tol = nearby_dist

	for v_old in vertices:

		add = True

		for v_new in new_vertices:

			dist = sqrt((v_new[0]-v_old[0])**2+(v_new[1]-v_old[1])**2)
#			print(dist)
			if sqrt((v_new[0]-v_old[0])**2+(v_new[1]-v_old[1])**2) < tol:
				add = False
				break

		if add:
			new_vertices.append(v_old)
	"""
	vertices = []
	
	centre = [0.5, 0.5]

	ratio = 0.0

	for v_new in new_vertices:

		r = sqrt((v_new[0]-centre[0])**2+(v_new[1]-centre[1])**2)

		theta = atan((v_new[1]-centre[1])/(v_new[0]-centre[0]))

		v = [0.0,0.0]
		if v_new[0] > centre[0]:
			v[0] = v_new[0] + abs(ratio*r*cos(theta))
		else:
			v[0] = v_new[0] - abs(ratio*r*cos(theta))

		if v_new[1] > centre[1]:
			v[1] = v_new[1] + abs(ratio*r*sin(theta))
		else:
			v[1] = v_new[1] - abs(ratio*r*sin(theta))

#		v_new = [centre[0]+ratio*r*cos(theta), centre[1]+ratio*r*sin(theta)]
		vertices.append(v)
	"""
#	plot_data_dist_2D(vertices)
#	plt.show()
	

#	return vertices
	return new_vertices


def extract_coord(polygon, nearby_dist):
	""" Extract vertices from the polygon """
#	print(str(polygon))
	poly_str = str(polygon)[10:-2]

	coordinates = poly_str.replace(', ', ',')
	coordinates = coordinates.split(',')

	vertices = []

	for point in coordinates:
		coord = point.split(' ')
		vertices.append([float(coord[0]),float(coord[1])])

	vertices = process_coord(vertices, nearby_dist)

	print(len(vertices), "vertices:", vertices)

	return vertices


def build_mesh(vertices, size=0.1):
	""" Build a mesh based on given vertices """

	with pygmsh.geo.Geometry() as geom:
		geom.add_polygon(
			vertices,
			mesh_size=size,
		)
		mesh = geom.generate_mesh()
#	print(mesh)
#	print(mesh.points[0])
#	print(mesh.cells[0])
	print(len(mesh.points), "nodes")
	print(len(mesh.cells[0].data), "boundary edges")
	print(len(mesh.cells[1].data), "triangles")

	return mesh.points, mesh.cells[0].data, mesh.cells[1].data


def calc_dist(point1, point2):
	return sqrt((point1[0]-point2[0])**2+(point1[1]-point2[1])**2)


def find_min_distance(vertices, data):

	min_values = []

	for vertice in vertices:

		min_value = 10000.0
		for point in data:

			min_value = min(min_value, calc_dist(vertice, point))
		min_values.append(min_value)
	print("max and min distance: ", max(min_values), min(min_values))


def plot_line(nodes, edge):

#	print(edge)
	coord1 = nodes[edge[0]]
	coord2 = nodes[edge[1]]
#	print(coord1, coord2)
	x = [coord1[0], coord2[0]]
	y = [coord1[1], coord2[1]]
		
	line = plt.plot(x,y)
	plt.setp(line,linewidth=1, color="b")


def print_results(nodes, bnd_edges, triangles):
	""" Print info of mesh """

	nodes = nodes.tolist()
	bnd_edges = bnd_edges.tolist()
	triangles = triangles.tolist()


	text_file = open("info_mesh.txt", "a")
	text_file.write("vertices = "+ str(nodes) + "\n")
	text_file.write("bnd_edges = "+ str(bnd_edges) + "\n")
	text_file.write("triangles = "+ str(triangles) + "\n")
	text_file.close()

	
	print("vertices = ",nodes)
	print("bnd_edges = ",bnd_edges)
	print("triangles = ",triangles)


def plot_mesh(nodes, edges=[], triangles=[], data=[]):
	""" Plot the given mesh """

	fig = plt.figure("mesh")
#	print(nodes)

	for tri in triangles:

		if tri[0] > tri[1]:
			plot_line(nodes, [tri[0],tri[1]])
		if tri[1] > tri[2]:
			plot_line(nodes, [tri[1],tri[2]])
		if tri[2] > tri[0]:
			plot_line(nodes, [tri[2],tri[0]])

	for edge in edges:
		plot_line(nodes, edge)

	for i in range(0, len(nodes)):
		plt.plot(nodes[i][0],nodes[i][1],"bo",markersize=2)
		plt.text(nodes[i][0]+0.003,nodes[i][1]+0.003, str(i))
	
	for point in data:
		plt.plot(point[0], point[1],"go",markersize=0.5)

	plt.show()

def plot_mesh_data(nodes, data):
	""" Plot the given mesh """

	fig = plt.figure("mesh")
#	print(nodes)
	
	if len(data) < 1000:
		for point in data:
			plt.plot(point[0], point[1],"go",markersize=0.5)
	else:
		for point in data:
			if random.random() < 0.1:
				plt.plot(point[0], point[1],"go",markersize=0.5)

	for i in range(0, len(nodes)-1):
		plt.plot([nodes[i][0],nodes[i+1][0]],[nodes[i][1],nodes[i+1][1]], "b",linewidth=0.3)
	if len(nodes) > 1:
		plt.plot([nodes[0][0],nodes[-1][0]],[nodes[0][1],nodes[-1][1]],"b",linewidth=0.3)

	for i in range(0, len(nodes)):
		plt.plot(nodes[i][0],nodes[i][1],"bo",markersize=2)
		plt.text(nodes[i][0]+0.003,nodes[i][1]+0.003, str(i))
	plt.show()


data_list = ["ironmine", "mountain", "burney", "canyon", "river", "beach"]

dataname = data_list[5]

# Regularisation parameter for domain wrt data
alpha_value = 5

# Expand data to avoid data on boundaries
expand = 0.05

# Minimal distance allowed for defined boundary vertices
nearby_dist = 0.05

# Mesh size of mesh 
mesh_size = 0.2

# Whether to plot alphashape
if_plot = False
#if_plot = True

data, polygon = build_polygon(dataname, alpha_value, expand, if_plot)

#vertices = extract_coord(polygon, nearby_dist)

vertices = [[0.6145165933801086, 0.8260775136808633],[0.8487719350660587, 0.7033532231386682],[0.8010198294897299, 0.2072393705008187],[0.4022599262878982, 0.13],[0.3420312122308081, 0.4699428757261047],[0.1503060384749231, 0.6285388063293389],[0.15, 0.841538820468337]]


#vertices = [[0.7517484129477274+0.02, 0.7863752051073911], [0.80097665138171, 0.2812393074948542-0.0812393074948542], [0.2238646788421499, 0.2363452144006475-0.0363452144006475], [0.192899599310527, 0.8]]
#vertices = [[0.5595379768988624, 0.2051647612643242], [0.5013125656282503, 0.215413584398135], [0.3312565628281622, 0.2763416274378258], [0.2994924746237442, 0.2914324142568976], [0.2792789639482123, 0.3024075319435005], [0.2658907945397461, 0.3127370544720914], [0.2294539726986556, 0.3511499663752301], [0.2141757087854612, 0.3810087424344251], [0.2050402520126333, 0.4311230665770159], [0.2033601680084222, 0.4575117686617539], [0.2077178858942818, 0.4751849361129877], [0.2353867693385051, 0.5775924680564881], [0.2616380819041077, 0.6378749159381096], [0.2978123906195331, 0.6871015467384056], [0.4521701085053927, 0.779905850706107], [0.468235911795582, 0.7845864156018649], [0.6363493174658688, 0.7939475453933809], [0.659555477773862, 0.7866039004707379], [0.7804690234511673, 0.6510289172830885], [0.7988974448721922, 0.5938937457969181], [0.7640357017850916, 0.4340282447881839], [0.7250787539376473, 0.3458238063214603], [0.7150507525376031, 0.3291997310020244], [0.6624956247812501, 0.2722259583052996]]
# Mountain original
#old_v = [[0.2115955113701257, 0.4427266069222382], [0.2078303490203203, 0.5028881290862994], [0.2047792946135079, 0.5529904621708253], [0.2022475553403264, 0.6132240158792873], [0.1977467012782303, 0.6639651304016015], [0.1919907836454125, 0.7581691957277963], [0.2231073466357612, 0.8079469109075694], [0.2761281029327307, 0.8047609797417974], [0.3607732222284331, 0.7985192099114709], [0.4190738620127853, 0.7980210086204695], [0.475891518160069, 0.7985192099114709], [0.5340839554738912, 0.7976909900898326], [0.6186641816496999, 0.7871018814056434], [0.6955251187851794, 0.785550162940047], [0.7521379134791304, 0.7856104551869156], [0.7783857623700781, 0.7362728637044038], [0.7835574150245308, 0.631901727103634], [0.7864353738409597, 0.5771378916321979], [0.790113974214333, 0.5037852027980504], [0.7957833298707591, 0.3844998123284602], [0.7988776936281676, 0.3240685721990982], [0.8011713874662316, 0.2732744671027343], [0.803876250692237, 0.2231848196799771], [0.7342426429765163, 0.1957499155294402], [0.6815248252345583, 0.1929891911091088], [0.6221696580702494, 0.1918690346975358], [0.5499019087171012, 0.1922180941985124], [0.4977250466029525, 0.190729845634079], [0.4368176774190737, 0.1912756451917189], [0.3679789395165345, 0.1907044576736126], [0.3134275933317332, 0.1909265856907423], [0.2525418362795925, 0.191209005954735], [0.2236266617694324, 0.2456872435078783], [0.2200779043606467, 0.3051665183045049], [0.2165724279400568, 0.3606982730322374]]
#old_v.pop(0)
#vertices = [old_v[0],old_v[5],old_v[11],old_v[15],old_v[20],old_v[24],old_v[29],old_v[33]]
#print(vertices)
#plot_mesh(vertices)

find_min_distance(vertices, data)

plot_mesh_data(vertices, data)

nodes, bnd_edges, triangles = build_mesh(vertices, mesh_size)


print_results(nodes, bnd_edges, triangles)
plot_mesh(nodes, bnd_edges, triangles)
