#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-06-13 10:13:44
@Last Modified time: 2024-09-28 23:58:50

This class plots a TPSFEM solution from a stored grid in a file.

"""

# Import libraries
from build.CopyGrid import copy_grid
from function.FunctionManager import obtain_model_function
from parameter.Parameter import Parameter
from parameter.ModelProblemFunction import find_model_problem
from estimator.Error import Error
from fileio.GridFunction import read_grid
import plot.FemPlot2D as femPlot2D
import plot.TpsfemPlot1D as tpsfemPlot1D
import plot.TpsfemPlot2D as tpsfemPlot2D
import plot.TpsfemPlotly2D as tpsfemPlotly2D
import plot.IndicatorPlot as indicatorPlot
import plot.ErrorPlot as errorPlot
import plot.SurfaceTriangulation as stPlot
import plot.SurfaceTriangulation as trisurfPlot

import matplotlib.pyplot as plt


# Obtain grid for error estimation
#path = "result/" + "result_1_6_20_02_01_11_02" + "/"
#filename = "1_5677"#path[14:19]
#filename = "1_177"
path = "result/" 
#path = "result/" + "result_5_4_21_03_20_14_11" + "/"
#filename = path[14:21]


#filename_list = ["7_6948","9_6528", "10_6642"]
#filename = "10_10193"
#filename = "9_6608"
filename = "5_1707"

d = 2


grid = read_grid(filename, path)

#grid = copy_grid(grid)

#dataset = 1081200

dataset = 5#1int(filename[:7])

#data = obtain_dataset(dataset)
parameter = Parameter()
parameter.set_dataset(dataset)

# Find model problems
#model_problem = find_model_problem(dataset)
#function = obtain_model_function(model_problem, d)
#parameter.set_function(function)

#print(dataset)
print("grid node number", grid.get_no_nodes())

from utilities.calc_h import calc_h_maxmin
h_max, h_min = calc_h_maxmin(grid)
print("h max/min:", h_max, h_min)


show_plot = True

if show_plot:

	# Plot resulting grids
	if grid.get_dim() == 1:
#		indicatorPlot.plot_error_indicator(grid, parameter)
#		tpsfemPlot1D.plot_tpsfem_solution_1D(grid, None, None, False)

		pass

	elif grid.get_dim() == 2:
		stPlot.plot_surface_triangulation(grid)
		femPlot2D.plot_fem_grid_2D(grid, None, False)
#		tpsfemPlot2D.plot_tpsfem_solution_2D(grid)
#		tpsfemPlot2D.plot_contour(grid)
#		tpsfemPlotly2D.plot_contour(grid)
#		errorPlot.plot_error_contour_2D(grid, parameter)
#		trisurfPlot.plot_surface_triangulation(grid)
		pass
#	plt.xlim(0.15,0.85)
#	plt.ylim(0.15,0.85)
	plt.xlim(-0.05,1.05)
	plt.ylim(-0.05,1.05)
	plt.show()