#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-04-27 22:34:45
@Last Modified time: 2022-03-29 11:10:45

This class contains functions to calculate a smoothing parameter
for the given grid and data using GCV.

"""

# Import libraries
from tpsfem.BuildMatrix import build_A, build_L, build_G_2D, build_G_combine_2D, build_BT_y_2D
from tpsfem.BuildBasis import build_node_list
from gcv.FunctionV2D import V, V_approx

from numpy import zeros
from scipy.sparse import csc_matrix
from scipy.sparse.linalg import inv
from scipy.optimize import minimize_scalar
from random import random
import matplotlib.pylab as plt


def random_function():
	# Randomly return 1 or -1
	return 1.0 if random() < 0.5 else -1.0


def prep(grid, region, parameter, node_list):
	""" Build submatrices for the V function """

	B_T, y = build_BT_y_2D(grid, region, parameter, node_list)


	A = build_A(grid, parameter, node_list)

	L = build_L(grid, parameter, node_list)

	G = build_G_combine_2D(grid, parameter, node_list)

	K = B_T*inv(L)*G

	KT = csc_matrix(K.transpose())

	KTK = KT*K

	# Parameters for V approximation function
	KTy = KT*y

	yTK = csc_matrix(KTy.transpose())

	yTy = 0.0
	for i in range(0, len(y)):
		yTy += y[i][0]**2

	# Initialise a vector u
	u = zeros([len(y), 1])
	for i in range(0, len(y)):
		u[i,0] = random_function() 

	KTu = KT*u

	uTK = csc_matrix(KTu.transpose())

	return K, KT, KTK, L, y, KTy, yTK, yTy, KTu, uTK


def GCV_analysis_2D(grid, region, parameter, v_approx=True):
	""" This GCV function is used to validate the V function """

	# Build a list of nodes
	node_list = build_node_list(grid, parameter.get_boundary())

	K, KT, KTK, L, y, KTy, yTK, yTy, KTu, uTK = prep(grid, region, parameter, node_list)

	# Alpha values
	alpha_list = [10**(-n-1) for n in range(0,12)]

	# Initialise lists to record results
	V_list = []
	numerator_list = []
	denominator_list = []

	# Run GCV for different alpha value
	for i in range(0, len(alpha_list)):
		if v_approx:
			V_approx(alpha_list[i], K, KT, KTK, L, y, KTy, yTK, yTy, KTu, uTK, V_list, numerator_list, denominator_list)
		else:
			V(alpha_list[i], K, KT, KTK, L, y, V_list, numerator_list, denominator_list)

	print("alpha_list =", alpha_list)
	print("V_list =", V_list)
	print("numerator_list =", numerator_list)
	print("denominator_list =", denominator_list)



def GCV_multiple_2D(grid, region, parameter, v_approx=True):
	""" Find optimal alpha for the grid """

	# Build a list of nodes
	node_list = build_node_list(grid, parameter.get_boundary())
	
	# Obtain required matrices
	K, KT, KTK, L, y, KTy, yTK, yTy, KTu, uTK = prep(grid, region, parameter, node_list)

	alpha_list = []
	for i in range(0, 10):
		print(i)
		# Initialise a vector u
		u = zeros([len(y), 1])
		for i in range(0, len(y)):
			u[i,0] = random_function() 

		count_p = 0
		count_n = 0 
		for j in range(0, len(y)):
			if u[j,0] == 1:
				count_p += 1
			if u[j,0] == -1:
				count_n += 1
		print("positive:", count_p, ", netagive:", count_n)

		KTu = KT*u

		uTK = csc_matrix(KTu.transpose())

		# Minimise V function using bounded method
		if v_approx:
			result = minimize_scalar(V_approx, bounds=(1e-9, 1e-4), method='bounded', \
				args=(K, KT, KTK, L, y, KTy, yTK, yTy, KTu, uTK), \
				options={'xatol':1e-10, 'maxiter':5})
		else:
			result = minimize_scalar(V, bounds=(1e-9, 1e-4), method='bounded', \
				args=(K, KT, KTK, L, y), \
				options={'xatol':1e-10, 'maxiter':5})

#		print("GCV's alpha:", result.x, "iteration:", result.nfev)
		alpha_list.append(result.x)

	print(alpha_list)



def alternative(grid, region, parameter, node_list, v_approx, \
		max_iteration, start_bound, end_bound):

	# Build a list of nodes
	if node_list is None:
		node_list = build_node_list(grid, parameter.get_boundary())
	
	# Obtain required matrices
	K, KT, KTK, L, y, KTy, yTK, yTy, KTu, uTK = prep(grid, region, parameter, node_list)

	if parameter.get_iteration() == 0:

		# Minimise V function using bounded method
		if v_approx:
			result = minimize_scalar(V_approx, bounds=(start_bound, end_bound), method='bounded', \
				args=(K, KT, KTK, L, y, KTy, yTK, yTy, KTu, uTK), \
				options={'xatol':1e-10, 'maxiter':max_iteration})
		else:
			result = minimize_scalar(V, bounds=(start_bound, end_bound), method='bounded', \
				args=(K, KT, KTK, L, y), \
				options={'xatol':1e-10, 'maxiter':max_iteration})

	#	print result
		print("GCV's alpha:", result.x, "iteration:", result.nfev)
		return result.x		

	else:

		alpha1 = parameter.get_alpha()
		alpha2 = alpha1/2.0
		alpha3 = alpha1/5.0

		if v_approx:
			V1 = V_approx(alpha1, K, KT, KTK, L, y, KTy, yTK, yTy, KTu, uTK)
			V2 = V_approx(alpha2, K, KT, KTK, L, y, KTy, yTK, yTy, KTu, uTK)
			V3 = V_approx(alpha3, K, KT, KTK, L, y, KTy, yTK, yTy, KTu, uTK)

			print(alpha1, V1)
			print(alpha2, V2)
			print(alpha3, V3)
			print("")
			if V1 <= V2 and V1 <= V3:
				return alpha1

			if V2 <= V1 and V2 <= V3:
				return alpha2

			if V3 <= V1 and V3 <= V2:
				return alpha3

		else:
			pass

def GCV_2D(grid, region, parameter, node_list=None, v_approx=True, \
				max_iteration=5, start_bound=1e-8, end_bound=1e-5):
	""" Find optimal alpha for the grid """

#	return alternative(grid, region, parameter, node_list, v_approx, max_iteration, start_bound, end_bound)
	
	if not parameter.get_local():
		print("Start GCV")
		
	# Build a list of nodes
	if node_list is None:
		node_list = build_node_list(grid, parameter.get_boundary())
	
	# Obtain required matrices
	K, KT, KTK, L, y, KTy, yTK, yTy, KTu, uTK = prep(grid, region, parameter, node_list)

	# Minimise V function using bounded method
	if v_approx:
		result = minimize_scalar(V_approx, bounds=(start_bound, end_bound), method='bounded', \
			args=(K, KT, KTK, L, y, KTy, yTK, yTy, KTu, uTK), \
			options={'xatol':1e-10, 'maxiter':max_iteration})
	else:
		result = minimize_scalar(V, bounds=(start_bound, end_bound), method='bounded', \
			args=(K, KT, KTK, L, y), \
			options={'xatol':1e-10, 'maxiter':max_iteration})

#	print result
	if not parameter.get_local():
		print("GCV's alpha:", result.x, "iteration:", result.nfev)

	return result.x


