#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-04-27 22:26:34
@Last Modified time: 2024-04-02 22:00:51

This class contains functions to calculate the V function
for the GCV.

"""

# Import libraries
from numpy.linalg import norm
from scipy.sparse import hstack, vstack, csc_matrix, identity
import matplotlib.pylab as plt
from scipy.sparse.linalg import inv, spsolve
import matplotlib.pylab as plt


def V(alpha, K, KT, KTK, L, y, V_list=None, numerator_list=None, denominator_list=None):
	""" V function of GCV """

	# Dimension information
	no_nodes = L.get_shape()[0]
	no_data = len(y)

	I = identity(no_data)

	# Compute KTK+apha*L
	aL = alpha*L
	empty_matrix =  csc_matrix((no_nodes,no_nodes),dtype=float)
	A = csc_matrix(K*inv(KTK + vstack((hstack((aL,empty_matrix)), \
		hstack((empty_matrix, aL)))))*KT)

	# Trace of I-A
	trace_value = 1.0*no_nodes
	for i in range(no_data):
		trace_value -= A[i,i]
	
	# Compute numerator and denominator
	inside = (I-A)*y
	numerator = norm(inside)**2
	denominator = trace_value**2

	# Record results
	if numerator_list is not None:
		numerator_list.append(numerator)
	if denominator_list is not None:
		denominator_list.append(denominator)
	if V_list is not None:
		V_list.append(numerator/denominator)

	return numerator/denominator


def V_approx(alpha, K, KT, KTK, L, y, KTy, yTK, yTy, KTu, uTK, \
			 V_list=None, numerator_list=None, denominator_list=None):
	""" Approximation of the V function of GCV """

	# Dimension information
	no_nodes = L.get_shape()[0]
	no_data = len(y)	

	# Compute KTK+apha*L
	aL = alpha*L
	empty_matrix =  csc_matrix((no_nodes,no_nodes),dtype=float)
	matrix = csc_matrix(KTK + 
		vstack((hstack((aL,empty_matrix)), hstack((empty_matrix, aL)))))

	# Compute numerator
	x1 = spsolve(matrix, KTy)
#	print x1
	yTKx1 = (yTK*x1)[0]
	x1KTKx1 = (x1.dot(KTK*x1))
	numerator = yTy-2*yTKx1+x1KTKx1	


	# Compute denominator
	x2 = spsolve(matrix, KTu)

	uTKx2 = (uTK*x2)[0]
#	print "alpha:", alpha
#	print "numerator:", yTy, (yTK*x1)[0],(x1.dot(KTK*x1))
#	print "denominator", uTKx2
	denominator = (no_nodes-uTKx2)**2

#	print numerator#,denominator	
#	print yTy,2*yTKx1,x1KTKx1
#	print no_nodes,uTKx2

	# Record results
	if numerator_list is not None:
		numerator_list.append(numerator)
	if denominator_list is not None:
		denominator_list.append(denominator)
	if V_list is not None:
		V_list.append(numerator/denominator)

	return numerator/denominator


