#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-04-27 22:26:34
@Last Modified time: 2022-03-29 11:10:33

This class contains functions to calculate the V function for the GCV.
Instead of the K^T*K+alpha*L formulation described in Linda's article,
this implementation uses the original saddle point system of equations.

"""

# Import libraries
from numpy.linalg import norm
from scipy.sparse import hstack, vstack, csc_matrix, identity
import matplotlib.pylab as plt
from scipy.sparse.linalg import inv, spsolve
from copy import deepcopy
from numpy import sqrt


def matrix_with_alpha_L(matrix, alpha, L, no_nodes):
	""" Add alpha*L into the matrix """

	L_alpha = L.multiply(alpha)

	matrix[no_nodes:no_nodes*2,no_nodes:no_nodes*2] = L_alpha
	"""
	matrix_alpha = deepcopy(matrix)

	for j in range(0, no_nodes):
		for i in range(0, no_nodes):

			L_value = L[i, j]
			#if L_value > 0.00000000000001:
			matrix_alpha[i+no_nodes,j+no_nodes] = alpha*L_value
	"""
	return csc_matrix(matrix)


def V_approx(alpha, A, L, G, L_lu, yTy, KTy, KTu, matrix, vector, KTu_vector, \
			 alpha_list=None, V_list=None, numerator_list=None, denominator_list=None):
	""" Approximation of the V function of GCV """

	# Dimension information
	no_nodes = L.get_shape()[0]
	
	# Compute numerator

	# Solve first system of equation
	matrix_alpha = matrix_with_alpha_L(matrix, alpha, L, no_nodes)
	x = spsolve(matrix_alpha, vector)
	x1 = -x[no_nodes:2*no_nodes]

	# Calculate the second term of numerator
	yTKx1 = 0.0
	for i in range(0, no_nodes):
		yTKx1 += KTy[i]*x1[i]

	# Calculate the third term of numerator
	LGx1 = L_lu.solve(G*x1)

	ALGx1 = A*LGx1

	x1KTKx1 = 0.0
	for i in range(0, no_nodes):
		x1KTKx1 += LGx1[i]*ALGx1[i]

	# Sum up the numerator
	numerator = yTy-2*yTKx1+x1KTKx1

	# Compute denominator

	# Solve second system of equation
	x = spsolve(matrix_alpha, KTu_vector)
	x2 = -x[no_nodes:2*no_nodes]

	# Calculate the second term of denominator
	uTKx2 = 0.0
	for i in range(0, no_nodes):
		uTKx2 += KTu[i]*x2[i]

	# Sum up the denominator
	denominator = (no_nodes-uTKx2)**2

	# Record results
	if alpha_list is not None:
		alpha_list.append(alpha)
	if numerator_list is not None:
		numerator_list.append(numerator)
	if denominator_list is not None:
		denominator_list.append(denominator)
	if V_list is not None:
		V_list.append(numerator/denominator)

		
	return abs(numerator/denominator)



def V(alpha, K, KT, KTK, L, y, alpha_list=None, V_list=None, numerator_list=None, denominator_list=None):
	""" V function of GCV """

	# Dimension information
	no_nodes = L.get_shape()[0]
	no_data = len(y)

	I = identity(no_data)

	# Compute KTK+apha*L
	aL = alpha*L
	empty_matrix =  csc_matrix((no_nodes,no_nodes),dtype=float)

#	print K.shape, KTK.shape, aL.shape, empty_matrix, KT.shape
#	print vstack((hstack((aL,empty_matrix)), hstack((empty_matrix, aL)))).shape
#	print ""
	A = csc_matrix(K*inv(KTK + vstack((hstack((aL,empty_matrix)), \
		hstack((empty_matrix, aL)))))*KT)

	# Trace of I-A
	trace_value = 1.0*no_nodes
	for i in range(no_data):
		trace_value -= A[i,i]
	
	# Compute numerator and denominator
	numerator = norm((I-A)*y)**2
	denominator = trace_value**2

	# Record results
	if alpha_list is not None:
		alpha_list.append(alpha)
	if numerator_list is not None:
		numerator_list.append(numerator)
	if denominator_list is not None:
		denominator_list.append(denominator)
	if V_list is not None:
		V_list.append(numerator/denominator)

	return abs(numerator/denominator)