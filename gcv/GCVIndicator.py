#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2020-01-19 00:39:54
@Last Modified time: 2022-03-29 11:10:52

This class contains routines that use GCV to estimate smoothing
parameters for the error indicators. Unlike the GCV for a 
particular grid, the existing global smoothing parameters is used
as the initial guess to derive the current smoothing parameter 
with two or three computations of the V function.

"""

# Import libraries
from tpsfem.BuildMatrix import build_A, build_L, build_y, build_G_combine_2D, build_BT_y_2D
from gcv.GCVSaddle1D import build_submatrices as build_submatrices_1D
from gcv.GCVSaddle2D import build_submatrices as build_submatrices_2D
from gcv.FunctionVSaddle1D import V_approx as V_approx_1D
from gcv.FunctionVSaddle2D import V_approx as V_approx_2D
from gcv.FunctionVSaddle1D import V as V_1D
from gcv.FunctionVSaddle2D import V as V_2D
from gcv.GCV1D import prep as prep_1D
from gcv.GCV2D import prep as prep_2D
from fileio.WriteResult import write_line

from copy import deepcopy



def GCV_indicator_1D(grid, region, parameter, node_list=None, v_approx=True, \
			max_iteration=5, matrix=None, vector=None, KTu_vector=None, \
			start_bound=1e-9, end_bound=1e-4):
	""" Find optimal alpha for the grid """

	if not parameter.get_local():
		print("Warning: This GCV routine is only meant for error indicators")

	vector_copy = deepcopy(vector)
	KTu_vector_copy = deepcopy(KTu_vector)

	# Build a list of nodes
	if node_list is None:
		node_list = build_node_list(grid, parameter.get_boundary())

	# Build required matrices
	if v_approx:
		A, L, G, L_lu, yTy, KTy, KTu, y_length = build_submatrices_1D(grid, region, parameter, node_list, matrix, vector_copy, KTu_vector_copy)

	else:
		K, KT, KTK, L, y, KTy, yTK, yTy, KTu, uTK = prep_1D(grid, region, parameter, node_list)

	if L is None:
		return False

	# Alpha values
	alpha_list = [10**(-n-1) for n in range(2,6)]

	# Initialise lists to record results
	a_list = []
	V_list = []
	numerator_list = []
	denominator_list = []

	# Run GCV for different alpha value
	for i in range(0, len(alpha_list)):
		if v_approx:
			V_approx_1D(alpha_list[i], A, L, G, L_lu, yTy, KTy, KTu, matrix, vector_copy, \
				KTu_vector_copy, a_list, V_list, numerator_list, denominator_list)
		else:
			V_1D(alpha_list[i], K, KT, KTK, L, y, a_list, V_list, numerator_list, denominator_list)
	"""
	print("alpha_list =", alpha_list)
	print("V_list =", V_list)
	print("numerator_list =", numerator_list)
	print("denominator_list =", denominator_list)
	"""
	min_index = 0
	min_V_value = 10000000.0

	for i in range(0, len(alpha_list)):
		if min_V_value > V_list[i]:
			min_index = i
			min_V_value = V_list[i]

	min_alpha = alpha_list[min_index]

#	new_alpha_list = [min_alpha*6, min_alpha*3, min_alpha/3, min_alpha/6]
	new_alpha_list = [min_alpha*7, min_alpha*5, min_alpha*4, min_alpha*3, min_alpha*2, \
						min_alpha/7, min_alpha/5, min_alpha/4, min_alpha/3, min_alpha/2]

	# Initialise lists to record results
	new_V_list = []
	new_numerator_list = []
	new_denominator_list = []

	# Run GCV for different alpha value
	for i in range(0, len(new_alpha_list)):
		if v_approx:
			V_approx_1D(new_alpha_list[i], A, L, G, L_lu, yTy, KTy, KTu, matrix, vector_copy, \
						KTu_vector_copy, a_list, new_V_list, new_numerator_list, new_denominator_list)
		else:
			V_1D(new_alpha_list[i], K, KT, KTK, L, y, new_V_list, new_numerator_list, new_denominator_list)
	"""
	print("new_alpha_list =", new_alpha_list)
	print("new_V_list =", new_V_list)
	print("new_numerator_list =", new_numerator_list)
	print("new_denominator_list =", new_denominator_list)
	"""
	min_index = -1

	for i in range(0, len(new_alpha_list)):
		if min_V_value > new_V_list[i]:
			min_index = i
			min_V_value = new_V_list[i]

	if min_index != -1:
		min_alpha = new_alpha_list[min_index]

#	print("min alpha", min_alpha)

	return min_alpha



def GCV_indicator_2D(grid, region, parameter, node_list=None, v_approx=True, \
			max_iteration=5, matrix=None, vector=None, KTu_vector=None, \
			start_bound=1e-8, end_bound=1e-4):
	""" Find optimal alpha for the grid """

	if not parameter.get_local():
		print("Warning: This GCV routine is only meant for error indicators")

	vector_copy = deepcopy(vector)
	KTu_vector_copy = deepcopy(KTu_vector)

	# Build a list of nodes
	if node_list is None:
		node_list = build_node_list(grid, parameter.get_boundary())

	# Build required matrices
	if v_approx:
		A, L, G, L_lu, yTy, KTy, KTu, y_length = build_submatrices_2D(grid, region, parameter, node_list, matrix, vector_copy, KTu_vector_copy)

	else:
		K, KT, KTK, L, y, KTy, yTK, yTy, KTu, uTK = prep_2D(grid, region, parameter, node_list)

	if L is None:
		return False

	# Alpha values
	alpha_list = [10**(-n-1) for n in range(2,6)]

	# Initialise lists to record results
	a_list = []
	V_list = []
	numerator_list = []
	denominator_list = []

	# Run GCV for different alpha value
	for i in range(0, len(alpha_list)):
		if v_approx:
			V_approx_2D(alpha_list[i], A, L, G, L_lu, yTy, KTy, KTu, matrix, vector_copy, \
				KTu_vector_copy, a_list, V_list, numerator_list, denominator_list)
		else:
			V_2D(alpha_list[i], K, KT, KTK, L, y, V_list, numerator_list, denominator_list)
	print("alpha_list =", alpha_list)
	print("V_list =", V_list)
	print("numerator_list =", numerator_list)
	print("denominator_list =", denominator_list)

	min_index = 0
	min_V_value = 10000000.0

	for i in range(0, len(alpha_list)):
		if min_V_value > V_list[i]:
			min_index = i
			min_V_value = V_list[i]

	min_alpha = alpha_list[min_index]

#	new_alpha_list = [min_alpha*6, min_alpha*3, min_alpha/3, min_alpha/6]
	new_alpha_list = [min_alpha*7, min_alpha*5, min_alpha*4, min_alpha*3, min_alpha*2, \
						min_alpha/7, min_alpha/5, min_alpha/4, min_alpha/3, min_alpha/2]

	# Initialise lists to record results
	new_V_list = []
	new_numerator_list = []
	new_denominator_list = []

	# Run GCV for different alpha value
	for i in range(0, len(new_alpha_list)):
		if v_approx:
			V_approx_2D(new_alpha_list[i], A, L, G, L_lu, yTy, KTy, KTu, matrix, vector_copy, \
						KTu_vector_copy, a_list, new_V_list, new_numerator_list, new_denominator_list)
		else:
			V_2D(new_alpha_list[i], K, KT, KTK, L, y, new_V_list, new_numerator_list, new_denominator_list)

	min_index = -1

	for i in range(0, len(new_alpha_list)):
		if min_V_value > new_V_list[i]:
			min_index = i
			min_V_value = new_V_list[i]

	if min_index != -1:
		min_alpha = new_alpha_list[min_index]

	if min_alpha < 1e-4:
		min_alpha = 1e-4

	return min_alpha


