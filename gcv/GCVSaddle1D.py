#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-04-27 22:34:45
@Last Modified time: 2022-03-29 11:11:19

This class contains functions to calculate a smoothing parameter
for the given grid and data using GCV. Instead of the K^T*K+alpha*L 
formulation described in Linda's article, this implementation uses 
the original saddle point system of equations.

"""

# Import libraries
from tpsfem.BuildMatrix import build_A, build_L, build_y, build_G_1D, build_BT_y_1D
from tpsfem.BuildBasis import build_node_list
from gcv.FunctionVSaddle1D import V, V_approx
from parameter.Definition import BoundaryType

from scipy.sparse import csc_matrix
from scipy.sparse.linalg import inv, splu, spsolve
from scipy.optimize import minimize_scalar
import matplotlib.pylab as plt
from copy import deepcopy
from numpy import sqrt, zeros



def build_submatrices(grid, region, parameter, node_list, matrix, vector, KTu_vector):
	""" Build submatrices for the GCV """

	# Build y^T*y
	y = build_y(grid, region, parameter)

	yTy = 0.0
	for i in range(0, len(y)):
		yTy += y[i]**2

	# Dimension of submatrices
	no_nodes = len(node_list)
	no_data = len(y)
	sqrt_no_data = sqrt(no_data)

	for i in range(0, len(vector)):
		vector[i,0] = vector[i,0]*sqrt_no_data
		KTu_vector[i,0] = KTu_vector[i,0]*sqrt_no_data

	# subsection of vectors
	d = vector[0:no_nodes,0]
	u_vector = KTu_vector[0:no_nodes,0]

	# submatrices of the matrix
	A = build_A(grid, parameter, node_list)
	L = build_L(grid, parameter, node_list)
	G = build_G_1D(grid, parameter, node_list)

	# Sparse LU decomposition of the L matrix
	L_lu = splu(L)

	# Build K^T*y
	# The inverse of L is still symmetric
	KTy = G.transpose()*L_lu.solve(d)

	# Build K^T*u
	KTu = G.transpose()*L_lu.solve(u_vector)

	return A, L, G, L_lu, yTy, KTy, KTu, len(y)



def prep(grid, region, parameter, node_list):
	""" Build submatrices for the V function """

	B_T, y = build_BT_y_1D(grid, region, parameter, node_list)

	A = build_A(grid, parameter, node_list)

	L = build_L(grid, parameter, node_list)

	G = build_G_1D(grid, parameter, node_list)

	K = B_T*inv(L)*G

	KT = csc_matrix(K.transpose())

	KTK = KT*K

	# Parameters for V approximation function
	KTy = KT*y

	yTK = csc_matrix(KTy.transpose())

	yTy = 0.0
	for i in range(0, len(y)):
		yTy += y[i][0]**2

	return K, KT, KTK, L, y, KTy, yTK, yTy



def GCV_analysis_1D(grid, region, parameter, node_list, v_approx, max_iteration, matrix, vector, KTu_vector):
	""" This GCV function is used to validate the V function """

	# Build a list of nodes
	if node_list is None:
		node_list = build_node_list(grid, parameter.get_boundary())

	
	vector_copy = deepcopy(vector)
	KTu_vector_copy = deepcopy(KTu_vector)
	
	# Build required matrices
#	if v_approx:
	A, L, G, L_lu, yTy, KTy, KTu = build_submatrices(grid, region, parameter, node_list, matrix, vector_copy, KTu_vector_copy)
#	else:
#	K, KT, KTK, L, y, KTy, yTK, yTy = prep(grid, region, parameter, node_list)

	# Alpha values
	alpha_list = [10**(-n-1) for n in range(0,12)]

	# Initialise lists to record results
	V_list = []
	numerator_list = []
	denominator_list = []

	# Run GCV for different alpha value
	for i in range(0, len(alpha_list)):
#		if v_approx:
		V_approx(alpha_list[i], A, L, G, L_lu, yTy, KTy, KTu, matrix, vector_copy, KTu_vector_copy, V_list, numerator_list, denominator_list)

#		V(alpha_list[i], K, KT, KTK, L, y, V_list, numerator_list, denominator_list)

	print("alpha_list =", alpha_list)
	print("V_list =", V_list)
	print("numerator_list =", numerator_list)
	print("denominator_list =", denominator_list)



def GCV_1D(grid, region, parameter, node_list=None, v_approx=True, \
			max_iteration=5, matrix=None, vector=None, KTu_vector=None, \
			start_bound=1e-9, end_bound=1e-6):
	""" Find optimal alpha for the grid """

	vector_copy = deepcopy(vector)

	# Build a list of nodes
	if node_list is None:
		node_list = build_node_list(grid, parameter.get_boundary())

	# Minimise V function using bounded method
	if v_approx:
		# Obtain required matrices
		A, L, G, L_lu, yTy, KTy, KTu = build_submatrices(grid, region, parameter, node_list, matrix, vector_copy, KTu_vector)

		result = minimize_scalar(V_approx, bounds=(start_bound, end_bound), method='bounded', \
			args=(A, L, G, L_lu, yTy, KTy, KTu, matrix, vector_copy, KTu_vector), \
			options={'xatol':1e-10, 'maxiter':max_iteration})
	
	else:
		# Obtain required matrices
		K, KT, KTK, L, yy, KTy, yTK, yTy, KTu, uTK = prep(grid, region, parameter, node_list)

		result = minimize_scalar(V, bounds=(start_bound, end_bound), method='bounded', \
			args=(K, KT, KTK, L, y), \
			options={'xatol':1e-10, 'maxiter':max_iteration})

#	print result
	if not parameter.get_local():
		print("GCV's alpha:", result.x, "iteration:", result.nfev)

	return result.x


