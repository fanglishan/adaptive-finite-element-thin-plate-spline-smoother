#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2020-01-18 20:13:31
@Last Modified time: 2022-03-29 11:11:13

This class contains routines that use GCV to estimate smoothing
parameters during the adaptive refinement process. Unlike the GCV
for a particular grid, the existing smoothing parameters from the 
previous refinement iterations can be used as the initial guess and
derive the current smoothing parameter with two or three computations
of the V function.

################ This function is depreciated
"""

# Import libraries
from GCV.GCVSaddle1D import build_submatrices as build_submatrices_1D
from GCV.GCVSaddle2D import build_submatrices as  build_submatrices_2D
from GCV.FunctionVSaddle1D import V_approx as V_approx_1D
from GCV.FunctionVSaddle2D import V_approx as V_approx_2D
from GCV.FunctionV2D import V_approx as V_approx_2D
from GCV.FunctionVSaddle2D import V_approx_single as V_approx_single_2D
from GCV.GCV2D import prep
from file.WriteResult import write_line

from scipy.optimize import minimize_scalar
from copy import deepcopy



def adjust_ratios_2D(parameter, ratio1, ratio2, ratio3):
	""" Adjust ratios for alpha when it drops too fast """

	ratios = parameter.get_ratios()

	if len(ratios) < 2:
		return ratio1, ratio2, ratio3

	else:
		if ratios[-1] < ratio3 and ratios[-2] < ratio3:
			return ratio1, ratio2/5.0, ratio3/10.0

		elif ratios[-1] < ratio3 and ratios[-2] == ratio3:
			return ratio1, ratio2/3.0, ratio3/5.0

		elif ratios[-1] == ratio3 and ratios[-2] == ratio3:
			return ratio1, ratio2/2.0, ratio3/3.0


	return ratio1, ratio2, ratio3



def estimate_alpha_2D(parameter, K, KT, KTK, L, y, KTy, yTK, yTy, KTu, uTK, \
					matrix, vector_copy, KTu_vector_copy):
	""" Estimate alpha during the adaptive refinement """

	print "begin ratio routine"

	"""
	########################## Test if V approx function works correctly
	# Alpha values
	alpha_list_1 = [10**(-n-1) for n in range(0,12)]

	# Initialise lists to record results
	alpha_list = []
	V_list = []
	numerator_list = []
	denominator_list = []

	# Run GCV for different alpha value
	for i in range(0, len(alpha_list_1)):
		V_approx_2D(alpha_list_1[i], A, L, G, L_lu, yTy, KTy, KTu, matrix, vector_copy, KTu_vector_copy, alpha_list, V_list, numerator_list, denominator_list)

	print "alpha_list =", alpha_list
	print "V_list =", V_list
	print "numerator_list =", numerator_list
	print "denominator_list =", denominator_list
	"""
	global_alpha = parameter.get_alpha()

	# This ratio works for now
	ratio1 = 1.0
	ratio2 = 0.3
	ratio3 = 0.1

	ratio1, ratio2, ratio3 = adjust_ratios_2D(parameter, ratio1, ratio2, ratio3)
	print "new ratios", ratio1, ratio2, ratio3

	alpha1 = global_alpha*ratio1
	alpha2 = global_alpha*ratio2
	alpha3 = global_alpha*ratio3

	V1 = V_approx_2D(alpha1, K, KT, KTK, L, y, KTy, yTK, yTy, KTu, uTK)
	write_line(parameter, str(alpha1)+", "+str(V1), True)

	V2 = V_approx_2D(alpha2, K, KT, KTK, L, y, KTy, yTK, yTy, KTu, uTK)
	write_line(parameter, str(alpha2)+", "+str(V2), True)

	V3 = V_approx_2D(alpha3, K, KT, KTK, L, y, KTy, yTK, yTy, KTu, uTK)
	write_line(parameter, str(alpha3)+", "+str(V3), True)

	"""
	if V1 < V2 and V1 < V3:
		return global_alpha*ratio1, ratio1
	elif V2 < V1 and V2 < V3:
		return global_alpha*ratio2, ratio2
	else:
		return global_alpha*ratio3, ratio3
	"""
#	print ratio1,alpha1, V1
#	print ratio2,alpha2, V2
#	print ratio3,alpha3, V3

	if V3 < V1 and V3 < V2:
		return global_alpha*ratio3, ratio3
	elif V2 < V1 and V2 < V3:
		return global_alpha*ratio2, ratio2
	else:
		return global_alpha*ratio1, ratio1



def GCV_refine_2D_condensed(grid, region, parameter, node_list, matrix, vector, \
			KTu_vector, max_iteration, start_bound=1e-8, end_bound=1e-5):
	""" Run GCV 2D to estimate the optimal alpha """

	# 1-use the minimiser with current alpha and alpha/100 as the bound
	# 2-apply several ratios on the current alpha to find the optimal alpha 
	routine = 2

	if parameter.get_local():
		print "Warning: This GCV routine is not meant for error indicators"
		return

	# Build a list of nodes
	if node_list is None:
		node_list = build_node_list(grid, parameter.get_boundary())

	vector_copy = deepcopy(vector)
	KTu_vector_copy = deepcopy(KTu_vector)

	K, KT, KTK, L, y, KTy, yTK, yTy, KTu, uTK = prep(grid, region, parameter, node_list)

	y_length = len(y)
	if y_length == 0:
		print "Warning: no data, no GCV"
		return parameter.get_alpha()

	
	"""
	alpha, ratio = estimate_alpha_2D(parameter, A, L, G, L_lu, \
					yTy, KTy, KTu, matrix, vector_copy, KTu_vector_copy)
	parameter.add_ratio(ratio)
	print "GCV's alpha:", alpha, "ratio:", ratio

	"""
	if parameter.get_iteration() == 0:

		result = minimize_scalar(V_approx_2D, bounds=(start_bound, end_bound), method='bounded', \
			args=(K, KT, KTK, L, y, KTy, yTK, yTy, KTu, uTK), \
			options={'xatol':1e-10, 'maxiter':max_iteration})

		alpha = result.x

		print "GCV's alpha:", alpha, "iteration:", result.nfev
	else:
		if routine == 1:

			alpha = parameter.get_alpha()

			# Start bound and end bound might require further adjustments
			start_bound = alpha/100.0
			end_bound = alpha*2
			max_iteration = 6

			result = minimize_scalar(V_approx_2D, bounds=(start_bound, end_bound), method='bounded', \
				args=(K, KT, KTK, L, y, KTy, yTK, yTy, KTu, uTK), \
				options={'xatol':1e-10, 'maxiter':max_iteration})

			alpha = result.x

			print "GCV's alpha:", alpha, "iteration:", result.nfev
		
		elif routine == 2:

			alpha, ratio = estimate_alpha_2D(parameter, K, KT, KTK, L, y, KTy, yTK, yTy, KTu, uTK, matrix, vector_copy, KTu_vector_copy)

			parameter.add_ratio(ratio)

			print "GCV's alpha:", alpha, "ratio:", ratio
	
	return alpha




def adjust_ratios_1D(parameter, ratio1, ratio2, ratio3):
	""" Adjust ratios for alpha when it drops too fast """

	ratios = parameter.get_ratios()

	if len(ratios) < 2:
		return ratio1, ratio2, ratio3

	else:
		if ratios[-1] < ratio3 and ratios[-2] < ratio3:
			return ratio1, ratio2/5.0, ratio3/10.0

		elif ratios[-1] < ratio3 and ratios[-2] == ratio3:
			return ratio1, ratio2/3.0, ratio3/5.0

		elif ratios[-1] == ratio3 and ratios[-2] == ratio3:
			return ratio1, ratio2/2.0, ratio3/3.0


	return ratio1, ratio2, ratio3



def estimate_alpha_1D(parameter, A, L, G, L_lu, yTy, KTy, KTu, \
					matrix, vector_copy, KTu_vector_copy):
	""" Estimate alpha during the adaptive refinement """

	print "begin ratio routine"

	global_alpha = parameter.get_alpha()

	# This ratio works for now
	ratio1 = 1.0
	ratio2 = 0.3
	ratio3 = 0.1

	ratio1, ratio2, ratio3 = adjust_ratios_1D(parameter, ratio1, ratio2, ratio3)
	print "new ratios", ratio1, ratio2, ratio3
	
	alpha1 = global_alpha*ratio1
	alpha2 = global_alpha*ratio2
	alpha3 = global_alpha*ratio3

	V1 = V_approx_1D(alpha1, A, L, G, L_lu, yTy, KTy, KTu, matrix, vector_copy, KTu_vector_copy)
	write_line(parameter, str(alpha1)+", "+str(V1), True)

	V2 = V_approx_1D(alpha2, A, L, G, L_lu, yTy, KTy, KTu, matrix, vector_copy, KTu_vector_copy)
	write_line(parameter, str(alpha2)+", "+str(V2), True)

	V3 = V_approx_1D(alpha3, A, L, G, L_lu, yTy, KTy, KTu, matrix, vector_copy, KTu_vector_copy)
	write_line(parameter, str(alpha3)+", "+str(V3), True)

#	print ratio1, alpha1, V1
#	print ratio2, alpha2, V2
#	print ratio3, alpha3, V3
	
	# Set a brake as alpha decreases really fast for 1D smooth function
	if alpha1 < 1e-12 and V2 > V1/2:
		return alpha1, ratio1
	if alpha2 < 1e-13 and V3 > V2/2:
		return alpha2, ratio2


	if V1 < V2 and V1 < V3:
		return alpha1, ratio1
	elif V2 < V1 and V2 < V3:
		return alpha2, ratio2
	else:
		return alpha3, ratio3



def GCV_refine_1D(grid, region, parameter, node_list, matrix, vector, \
			KTu_vector, max_iteration, start_bound=1e-8, end_bound=1e-5):
	""" Run GCV 1D to estimate the optimal alpha """

	if parameter.get_local():
		print "Warning: This GCV routine is not meant for error indicators"
		return

	vector_copy = deepcopy(vector)
	KTu_vector_copy = deepcopy(KTu_vector)

	A, L, G, L_lu, yTy, KTy, KTu, no_data = build_submatrices_1D(grid, region, parameter, node_list, matrix, vector_copy, KTu_vector_copy)

	alpha = 0.0

	if parameter.get_iteration() == 0:

		result = minimize_scalar(V_approx_1D, bounds=(start_bound, end_bound), method='bounded', \
			args=(A, L, G, L_lu, yTy, KTy, KTu, matrix, vector_copy, KTu_vector_copy), \
			options={'xatol':1e-10, 'maxiter':max_iteration})

		alpha = result.x

		print "GCV's alpha:", alpha, "iteration:", result.nfev

	else:
		alpha, ratio = estimate_alpha_1D(parameter, A, L, G, L_lu, \
				yTy, KTy, KTu, matrix, vector_copy, KTu_vector_copy)

		parameter.add_ratio(ratio)
		
		print "GCV's alpha:", alpha, "ratio:", ratio
		

	return alpha