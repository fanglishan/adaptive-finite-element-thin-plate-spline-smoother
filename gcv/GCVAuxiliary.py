#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2020-06-23 17:31:38
@Last Modified time: 2022-10-09 23:43:15

This class contains routines to compute a smoothing parameter for 
auxiliary problems.

"""

# Import libraries
from adaptive.line.LocalGrid1D import build_local_grid_1D
from adaptive.triangle.LocalGrid2D import build_local_grid_2D
from adaptive.triangle.TriangleFunction import find_triangle_pairs
from tpsfem.TpsfemSolver1D import calculate_alpha_1D
from tpsfem.TpsfemSolver2D import calculate_alpha_2D
from fileio.WriteResult import write_line
from data.DataThinning import count_data_1D, count_data_2D, count_data_local_grid_2D
from data.ObtainData import obtain_data_grid
import plot.FemPlot2D as femPlot2D
from parameter.Definition import IndicatorType

from copy import deepcopy
#from numpy import median
from random import random



def choose_this(no_alpha, no_base_edge):

	prob = 1.0*no_alpha/no_base_edge

	if no_alpha == no_base_edge:
		return True

	elif random() < prob:
		return True

	else:
		return False




def compute_gcv_auxiliary_1D(grid, region, base_edges, parameter, no_alpha):
	""" Compute smoothing parameter for 1D auxiliary problems """

	alpha_list = []

	# Parameters
	if parameter.get_indicator() == IndicatorType.auxiliary:
		indicator_parameter = parameter.get_indicator_parameter()
		boundary = indicator_parameter[0]
		no_layers = indicator_parameter[1]
		refine_choice = indicator_parameter[2]
		uni_refine = indicator_parameter[3]
		form = indicator_parameter[4]
		run_gcv = indicator_parameter[5]
		subset_data = indicator_parameter[6]
		min_data = indicator_parameter[7]	
	else:
		boundary = 1
		no_layers = 2
		refine_choice = 1
		uni_refine = 0
		form = 2
		run_gcv = False
		subset_data = True
		min_data = -1

	no_base_edge = len(base_edges)

	if no_alpha > len(base_edges):
		no_alpha = len(base_edges)

	max_loop = 3
	loop_count = 0

	while len(alpha_list) < no_alpha:

		for edge in base_edges:

			if not choose_this(no_alpha, no_base_edge):
				continue

			# Warning: something wrong with the implementation that 
			# the grid will be affected by some changes in loca grids
			# remove it after the issue is fixed 
#			grid_copy = deepcopy(grid)
			
			# End nodes of the edge
			endpt1 = edge.get_endpt1()
			node1 = grid.get_node(endpt1)
			endpt2 = edge.get_endpt2()
			node2 = grid.get_node(endpt2)
			
			count_data = True

			# Build a local refined grid
			local_grid, mid_id, no_data = build_local_grid_1D(grid, region, node1, node2, \
					 parameter, no_layers, refine_choice, uni_refine, count_data)

			if local_grid.get_no_nodes() < 4:
				continue

			# Determine whether use the V approximation
			V_approx = True

			# If there is not enough data points, just switch to another auxiliary problem
			if no_data < 5:
				continue
#			elif no_data < 100:
#				V_approx = False

			data = obtain_data_grid(region, local_grid)


			# Calculate alpha for the auxiliary problem
			alpha = calculate_alpha_1D(local_grid, region, parameter, V_approx)

			if not (alpha is False):
				alpha_list.append(alpha)

			if len(alpha_list) == no_alpha:
				break

		loop_count += 1

		if loop_count >= max_loop:
			break

	return alpha_list


def compute_gcv_auxiliary_2D(grid, region, base_edges, parameter, no_alpha):
	""" Compute smoothing parameter for 2D auxiliary problems """

	alpha_list = []

	# Parameters
	if parameter.get_indicator() == IndicatorType.auxiliary:
		indicator_parameter = parameter.get_indicator_parameter()
		boundary = indicator_parameter[0]
		no_layers = indicator_parameter[1]
		refine_choice = indicator_parameter[2]
		uni_refine = indicator_parameter[3]
		form = indicator_parameter[4]
		run_gcv = indicator_parameter[5]
		subset_data = indicator_parameter[6]
		min_data = indicator_parameter[7]	
	else:
		boundary = 1
		no_layers = 2
		refine_choice = 1
		uni_refine = 0
		form = 2
		run_gcv = False
		subset_data = True
		min_data = -1

	no_base_edge = len(base_edges)

	if no_alpha > len(base_edges):
		no_alpha = len(base_edges)

	max_loop = 3
	loop_count = 0

	while len(alpha_list) < no_alpha:

		for edge in base_edges:

			if not choose_this(no_alpha, no_base_edge):
				continue

			# Warning: something wrong with the implementation that 
			# the grid will be affected by some changes in loca grids
			# remove it after the issue is fixed 
#			grid_copy = deepcopy(grid)
			
			# End nodes of the edge
			endpt1 = edge.get_endpt1()
			node1 = grid.get_node(endpt1)
			endpt2 = edge.get_endpt2()
			node2 = grid.get_node(endpt2)
			
			# Obtain other vertices of the triangle pairs
			node3, node4 = find_triangle_pairs(grid, endpt1, endpt2)
			
			count_data = True

			# Build a local refined grid
			local_grid, mid_id, no_data = build_local_grid_2D(grid, region, node1, node2, \
					node3, node4, parameter, no_layers, refine_choice, uni_refine, count_data)

			# Determine whether use the V approximation
			V_approx = True

			# If there is not enough data points, just switch to another auxiliary problem
			if no_data < 5:
				continue
			elif no_data < 100:
				V_approx = False

#			data = obtain_data_grid(region, local_grid)
#			femPlot2D.plot_fem_grid_data_2D(local_grid, data, parameter)
#			femPlot2D.plot_fem_grid_2D(local_grid, parameter, True)

			# Calculate alpha for the auxiliary problem
			alpha = calculate_alpha_2D(local_grid, region, parameter, V_approx)

			if not (alpha is False):
				# Set lower limit
				if alpha < 1e-4:
					alpha = 1e-4
				alpha_list.append(alpha)

			if len(alpha_list) == no_alpha:
				break
#		print alpha_list
		loop_count += 1

		if loop_count >= max_loop:
			break

	return alpha_list


def compute_gcv_auxiliary(grid, region, base_edges, parameter, no_alpha=None):
	""" Compute smoothing parameter for auxiliary problems """

	parameter_copy = deepcopy(parameter)

	parameter_copy.set_local(True)
	"""
	# Number of alpha values
	if no_alpha is None:
		no_alpha = 10

	if grid.get_dim() == 1:
		alpha_list = compute_gcv_auxiliary_1D(grid, region, \
						base_edges, parameter_copy, no_alpha)

	elif grid.get_dim() == 2:
		alpha_list = compute_gcv_auxiliary_2D(grid, region, \
						base_edges, parameter_copy, no_alpha)

	else:
		print("Warning: invalid dimension")
		return

	if len(alpha_list) > no_alpha/2:
		auxiliary_alpha = median(alpha_list)
	else:
		auxiliary_alpha = 1e-6

	write_line(parameter, "Auxiliary alpha list: " + str(alpha_list), True)
	write_line(parameter, "Auxiliary alpha: " + str(auxiliary_alpha), True)
	"""
	auxiliary_alpha = 1e-6
	parameter.add_auxiliary_alpha(auxiliary_alpha)

	return auxiliary_alpha
