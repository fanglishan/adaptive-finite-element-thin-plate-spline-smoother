# -*- coding: utf-8 -*-
"""
@Author: Fang
@Date:   2022-11-05 13:34:30
@Last Modified by:   Fang
@Last Modified time: 2022-11-05 13:36:45

This class contains functions to print information of the given data set.

"""


def print_data_minmax(data):


	x_max = -100000000000.0
	x_min = 100000000000.0
	y_max = -100000000000.0
	y_min = 100000000000.0
	z_max = -100000000000.0
	z_min = 100000000000.0

	for point in data:
		x_max = max(point[0], x_max)
		x_min = min(point[0], x_min)
		y_max = max(point[1], y_max)
		y_min = min(point[1], y_min)
		z_max = max(point[2], z_max)
		z_min = min(point[2], z_min)


	print("#data:", len(data))
	print("x:", x_min, "to", x_max)
	print("y:", y_min, "to", y_max)
	print("z:", z_min, "to", z_max)
	print("")
