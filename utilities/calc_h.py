# -*- coding: utf-8 -*-
"""
@Author: Fang
@Date:   2024-02-13 14:40:13
@Last Modified by:   Fang
@Last Modified time: 2024-02-13 14:44:57

This function calculates the maximum and minimum mesh size of elements
in the given grid.

"""

from grid.NodeTable import node_iterator
from grid.EdgeTable import endpt_iterator
from adaptive.triangle.TriangleFunction import find_h, cal_distance


def calc_h_maxmin(grid):

	h_max = -10000.0
	h_min = 10000.0

	for node in node_iterator(grid):
		node_id = node.get_node_id()

		for endpt in endpt_iterator(grid, node_id): 
			if node_id > endpt:

				end_node = grid.get_node(endpt)
				d = cal_distance(node, end_node)

				h_max = max(h_max, d)
				h_min = min(h_min, d)

	return h_max, h_min
