#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-04-16 10:13:44
@Last Modified time: 2024-08-07 13:57:55

The TPSFEM main program builds a uniform grid and adaptively refine to obtain
a final solution. This version uses Python 3.7 syntax

"""

# Import libraries
from tpsfem.main_init import main_init
from tpsfem.main_tpsfem import main_tpsfem
from grid.NodeTable import node_iterator
from grid.EdgeTable import endpt_iterator
from build.TrimElement import trim_elements
from parameter.ParameterInit import init_parameter
from parameter.Definition import IndicatorType
from data.ObtainData import obtain_data_all
from data.ObtainBndData import obtain_boundary_data

import plot.FemPlot2D as femPlot2D
import plot.FemPlot3D as femPlot3D
import plot.TpsfemPlot1D as tpsfemPlot1D
import plot.TpsfemPlot2D as tpsfemPlot2D
import plot.TpsfemGradientPlot2D as gradientPlot2D
import plot.RegionPlot as regionPlot
import plot.DataErrorPlot as dataErrorPlot
import plot.IndicatorPlot as indicatorPlot
import plot.EINormPlot as eiNormPlot
import plot.SurfaceTriangulation as trisurfPlot
import matplotlib.pyplot as plt



def set_indicator_param(indicator):
	""" Set Error indicator parameters """

	# Indicator parameters
	indicator_parameter = []

	# 0-boundary type, 1-number of layers, 2-refine choice, 3-number of uniform refinement, 4-error estimate form, 5-run gcv, 6-subset data, 7-min data
	# Refine choice 2 and 3 leads to the same result
	if indicator == IndicatorType.auxiliary:
		indicator_parameter = [1,2,2,0,2, False, False,-1]
	# 0-boundary type, 1-number of layers, 2-refine choice, 3-source of Dx, 4-ways to improve Dx
	elif indicator == IndicatorType.recovery:
		indicator_parameter = [1,2,0,1,1]
	# 0-number of layers, 1-refine choice, 2-source of Dx, 
	# 3-quadrature rule
	elif indicator == IndicatorType.norm:
		indicator_parameter = [2,0,1,1]
	# 0-boundary type, 1-number of layers, 2-refine choice, 3-ratio, 4-run gcv(EIRecovery1D)
	elif indicator == IndicatorType.residual:
		indicator_parameter = [1,2,1,0.0, False]

	# 0-number of layers, 1-refine choice                 
	elif indicator == IndicatorType.GCV:              ### Depreciated
		indicator_parameter = [2, 1]
	# 0-number of layers, 1-refine choice, 2-error type, 3-smoother type
	elif indicator == IndicatorType.regression:       ### Depreciated
		indicator_parameter = [2, 1, 2, 2]

	return indicator_parameter


# Basis function: 1-linear, 2-quadratic
basis = 1

# Tolerance for the solver
solver_tol = 1.0E-12

# Error tolerance
error_tol = 1e-7
error_tol = 0.0

# Refinement approach
# 1: refine all above error tol
# 2: refine high indicator iteratively
refine = 2

# Maximum number of refinement in one iteration
max_no = 1

# Norm type
# 0-max norm, 1-one norm, 2-two norm
norm = 1

# Min data points for refinement
min_data = 0

# Whether to run GCV
#run_gcv = True
run_gcv = False

# GCV parameters: 0-whether to use apprximation, 1-max iteration, 2-start bound, 3-end bound (start should be smaller)
#gcv_parameter = [True, 5, 1e-8, 1e-4]    # 1D
gcv_parameter = [True, 6, 1e-6, 1e-4]    # 2D


# Whether to store info: 0-whether to write anything, 1-write grid figure, 2-write error figure, 3-write refine figure
# 4-write contour figure, 5-write regression error figure, 6-indicator figure
write_file = [True, True, True, True, True, True, True, True]
write_file = [False, False, False, False, False, False, False]

# Smoothing parameter
alpha = 1e-11

# Whether stop refine based on RMSE
stop_criteria = False


### Dataset
# 1:dimension, 2-3:model, 4:distribution, 5:noise distribution, 6:noise level, 7:data size
### 1D Model problem
# zero=0, linear=1, quadratic=2, cubic=3, exp=4, sine=5, osci_sine=6, grad_exp=7, steep_exp=8
# exp_fluc=9, exp_mul=10, frac=11, shift=12, runge=13
### 2D Model problem
# zero=0, linear=1, quadratic=2, cubic=3, exp=4, sine=5, osci_sine=6, grad_exp=7, steep_exp=8
# exp_fluc=9, exp_mul=10, sin_mul=11, hole=12, franke=13, tanh=14, cone=15
### Real-world data sets
# 1-magnetic data, 2-bone data, 3-lake data, 4-lena data, 5-crater data, 6-iron mine, 
# 7-iron mountain, 8-Aeromagnetic Burney, 9-Grapevine Canyon, 10-river mouth, 11-black beach
### Data distribution
# 1-uniform, 2-uniform with holes, 3-uniformly random, 4-normally random, 5-line data
### Noise distribution pattern
# 1-uniform, 2-normal
### Noise level
# uniform:     0-0.0, 1-0.001, 2-0.005, 3-0.01, 4-0.03, 5-0.05, 6-0.06, 7-0.08, 8-0.09, 9-0.1
# normal (sd): 0-0.0, 1-0.005, 2-0.01, 3-0.02, 4-0.05, 5-0.07, 6-0.1, 7-0.15, 8-0.2, 9-0.25
### Data size
# 0-3, 1-10, 2-20, 3-50, 4-100, 5-250, 6-500, 7-750, 8-1000, 9-1500, 10-2000
#dataset = 2081209
#dataset = 2081202
dataset = 10
#dataset = 9

# Use only a subset of real-world data
subset = False

# Number of uniform refinement before adaptive refinement
uni_refine = 0

# Maximum number of refinement sweeps allowed
max_sweep = 0

# Number of nodes (in each direction)
n = 5

# Real-world data: 1-square domain, 2-use flexible domain, 3-use trimmed domain
grid_type = 1


trim_grid = False

# Boundary type: 1-Dirichlet, 2-Neumann
boundary = 2

# 1-Inf, 2-True, 3-Recovery, 4-Auxiliary, 5-Norm, 6-Residual, 7-GCV, 8-regression
indicator = 1

# Type of weights for error indicators
weight = 1

indicator_parameter = set_indicator_param(indicator)

#import time
#time.sleep(3300)


###### Start the program #####
parameter = init_parameter(boundary, basis, alpha, solver_tol, dataset, subset, error_tol, refine, uni_refine, \
		max_no, max_sweep, indicator, indicator_parameter, norm, min_data, \
		run_gcv, gcv_parameter, write_file, weight)

# Build a initial grid and data region
grid, region = main_init(parameter, n, grid_type)

from fileio.GridFunction import read_grid
from tpsfem.main_tpsfem import solve_tpsfem
from tpsfem.main_tpsfem import measure_error
from estimator.Error import Error

#path = "result/crater_final_grids/"
path = "result/"
#filename_list = ["7_6948","9_6528", "10_6642"]
#filename_list = ['5_66049']
#filename_list = ["10_53", "10_53", "7_16641", "7_6948", "7_6564", "7_6923", "7_6656", "7_11969", "7_7576", "7_7600", "7_7634", "7_7560", "9_16641", "9_6612", "9_6528", "9_6608", "9_6534", "9_10241", "9_7238", "9_7265", "9_7294", "9_7204", "10_16641", "10_6740", "10_6642", "10_6828", "10_6688", "10_9488", "10_6880", "10_6792", "10_6928", "10_6816"]
#filename_list = ["12_8321","12_6786", "12_6416", "12_6448", "12_6432", "12_6273", "12_5528", "12_5818", "12_5397", "12_5440"]
#filename_list = ["5_1_8321_n"]
#filename_list = ["10_53", "10_53", "7_7560", "9_16641", "9_6612", "9_6528", "9_6608", "9_6534", "9_10241", "9_7238", "9_7265", "9_7294", "9_7204", "10_16641", "10_6740", "10_6642", "10_6828", "10_6688", "10_9488", "10_6880", "10_6792", "10_6928", "10_6816"]

filename_list = ["7_7560", "9_7204", "10_6816"]

max_test = 1
for filename in filename_list:
	print(filename)	
	grid = read_grid(filename, path)
	for i in range(0,max_test):
		solve_tpsfem(grid, region, parameter)
#		error = Error()
#		print("Start measuring errors")
#		measure_error(grid, region, parameter, error)
#		print(error)
		print("")


#grid = read_grid(filename, path)
#error = Error()
#measure_error(grid, region, parameter, error)
#print(error)
"""
data = obtain_data_all(region)
print(len(data))
no_sample = 10000
chosen_data = obtain_boundary_data(data, grid, no_sample)
print(chosen_data)
#femPlot2D.plot_fem_grid_data_2D(grid, chosen_data, parameter=None)
from sys import exit
exit()
"""


#if grid_type == 1 and trim_grid:
#	grid = trim_elements(grid, region, parameter, trim_grid)

"""
#femPlot2D.plot_fem_grid_2D(grid, parameter, False)
data = obtain_data_all(region)
femPlot2D.plot_fem_grid_data_2D(grid, data)
plt.show()
from sys import exit
exit()
"""

#print(parameter)
#print("")
#
# Adaptively refine grid
#grid = main_tpsfem(grid, region, parameter, stop_criteria)

