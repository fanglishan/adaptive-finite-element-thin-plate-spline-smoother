#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-06-13 11:00:40
@Last Modified time: 2022-10-09 23:43:59

This class contains routines to build and access TPSFEM
solution with a fine grid. These solutions can be used as
the reference solution to estimate grid error.


##################################### This function is depreciated.

"""

# Import libraries
from build.InitialGrid import build_initial_grid
from data.RegionFunction import build_region
from data.DatasetManager import obtain_dataset
from fileio.SolutionFunction import read_soln, write_soln
from tpsfem.TpsfemSolver1D import solve_tpsfem_1D
from tpsfem.TpsfemSolver2D import solve_tpsfem_2D


def build_soln(dataset, parameter, grid_type):
	""" Build a TPSFEM solution with a fine grid """

	# Parameters
	n = parameter.get_solution_grid_size()

	d = parameter.get_dim()

	region_no = parameter.get_region_no()

	# Create a an initial FEM n*n grid
	fine_grid = build_initial_grid(parameter, n, grid_type)

	# Obtain data
	data = obtain_dataset(dataset)
	parameter.set_data_size(len(data))

	# Build a data region
	region = build_region(data, d, region_no)

	# Solve the discrete TPS smoother
	if fine_grid.get_dim() == 1:
		solve_tpsfem_1D(fine_grid, region, parameter)
		pass
	elif fine_grid.get_dim() == 2:
		solve_tpsfem_2D(fine_grid, region, parameter)
	else:
		print("Warning: estimator/ManageSolution -  invalid dimension")

	return fine_grid


def obtain_soln(parameter):
	""" Return required TPSFEM smoother """

	# Smoother's name corresponds to the dataset
	dataset = parameter.get_dataset()

	try:
		solution = read_soln(str(dataset))

	# If the solution does not exist, print error message
	except IOError:
		print(IOError)
#		print "Build a new smoother"
#		smoother = build_smoother(dataset, parameters, grid_type)

#		# Write the smoother into a file for further usage
#		write_file(smoother, str(dataset))

#	print("Smoother obtained with", smoother.get_no_nodes(), "nodes")

	return solution
