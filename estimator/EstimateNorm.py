#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2020-10-13 21:58:47
@Last Modified time: 2022-10-09 23:45:47

This class contains routines to calculate the norm of the TPSFEM.

"""

# Import libraries
from grid.NodeTable import node_iterator
from grid.EdgeTable import endpt_iterator
from adaptive.triangle.TriangleFunction import find_h, cal_distance, find_no_tri

from numpy import inf, sqrt


def estimate_y_norm(grid, parameter):
	""" Estimate the L2 norm of response values"""


	# Whether it is using real-world dataset
	real_data = False

	if len(str(abs(parameter.get_dataset()))) == 1:
		real_data = True

	dataset = parameter.get_dataset()

	if real_data:
		model_problem = None
	else:
		model_problem = int(str(dataset)[1:3])

	# One, two and max norm
	one_norm = 0.0
	two_norm = 0.0
	max_norm = 0.0

	# One and two norm with h or A
	one_norm_h = 0.0
	two_norm_h = 0.0

	no_nodes = 0

	# Iterate through each node
	for node in node_iterator(grid):

		coord = node.get_coord()
		node_id = node.get_node_id()

#		if model_problem == 13 or model_problem == 14:
##			if coord[0] < -0.9 or coord[0] > 0.9 or \
#				coord[1] < -0.9 or coord[1] > 0.9:
#				continue

		# Skip the boundary nodes
		if not real_data and parameter.get_boundary() == 1:
			if parameter.get_boundary() == 1:
				if node.get_slave():
					continue

		no_nodes += 1

		value = node.get_value()

		# Add up L norms
		one_norm += value
		two_norm += value**2
		if value > max_norm:
			max_norm = value

		# Find the longest edge connected to the node
		h = 0.0
		for endpt in endpt_iterator(grid, node_id): 
			end_node = grid.get_node(endpt)
			d = cal_distance(node, end_node)
			if d > h:
				h = d

		if abs(h) < 0.00000000001:
			print("Warning: estimator/EstimateNorm - h may not be valid")

		# h for different dimensions of L2 norm approximation
		# 1D: e = sqrt(sum(h*e**2))
		# 2D: e = sqrt(sum(h**2*e**2))
		if grid.get_dim() == 2:
			h = h**2

		# Add up h norms
		one_norm_h += h*value
		two_norm_h += h*(value**2)

	one_norm /= no_nodes
	two_norm = sqrt(two_norm/no_nodes)
	two_norm_h = sqrt(two_norm_h)

	return one_norm, two_norm, max_norm, one_norm_h, two_norm_h