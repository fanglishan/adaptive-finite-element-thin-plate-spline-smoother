#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2020-01-09 20:49:55
@Last Modified time: 2023-06-03 23:30:27

This class contains routines to calculate several regression
error of the given TPSFEM solution.

Note: RMSLE is changed to RMSPE on 2022/11/3.

"""

# Import libraries
from grid.NodeTable import node_iterator
from grid.EdgeTable import endpt_iterator
from adaptive.triangle.Triangle import triangle_iterator, triangle_iterator_ref
from adaptive.triangle.GridValue import cal_value_2d, cal_smoother_2d
from adaptive.line.GridValue import cal_value_1d, cal_smoother_1d
from data.ObtainData import obtain_data, obtain_data_all

from numpy import sqrt, log



def calc_regression_error(grid, region, parameter):
    """ Calculate regression errors of the TPSFEM solution """

    y_max = region.get_y_max()
    print("y max:", y_max)
    
    # Types of regression errors
    RMSE = 0.0
    MAE = 0.0
    MSPE = 0.0
    MAPE = 0.0
    RMSPE = 0.0
    MAX = 0.0
    MIN = 0.0
    ME = 0.0
    
    data_count = 0

    zero_cases = 0
    
    # Clear usage record
    region.init_usage()

    # 1D grid
    if grid.get_dim() == 1:

        # Iterate through each interval once
        for node in node_iterator(grid):
            node_id = node.get_node_id()
            for endpt in endpt_iterator(grid, node_id):
                if node_id > endpt:
                    end_node = grid.get_node_ref(endpt)

                    data, u = obtain_data(region, [node, end_node])

                    for data_point in data:
                        predictor = cal_value_1d(node, end_node, data_point[0:1])
                        data_value = data_point[-1]
                        
                        error = predictor - data_value

                        RMSE += error**2
                        MAE += abs(error)
                        
                        if data_value != 0:
                            MSPE += (error/data_value)**2
                            MAPE += abs(error/data_value)
                        else:
                            zero_cases += 1
                            
                        RMSPE += (error/y_max)**2
#                        if predictor +1 > 0 and data_value+1 > 0:
#                            RMSLE += (log(predictor+1)-log(data_value+1))**2
                    
                        MAX = max(MAX, error)
                        MIN = min(MIN, error)

                        ME += error

                    data_count += len(data)

    # 2D grid        
    elif grid.get_dim() == 2:

        # Iterate through each triangle once
        for tri in triangle_iterator_ref(grid):
            if tri[0].get_node_id() < tri[1].get_node_id() and \
                tri[1].get_node_id() < tri[2].get_node_id():


                data, u = obtain_data(region, tri)
                
                for data_point in data:
                    predictor = cal_value_2d(tri[0], tri[1], tri[2], data_point[0:2])
                    data_value = data_point[-1]
                    
                    error = predictor - data_value

                    RMSE += error**2
                    MAE += abs(error)

                    if data_value != 0:
                        MSPE += (error/data_value)**2
                        MAPE += abs(error/data_value)
                    else:
                        zero_cases += 1
                            
                    RMSPE += (error/y_max)**2
#                    if predictor+1 > 0.0 or data_value+1 > 0.0:
#                        RMSLE += (log(predictor+1)-log(data_value+1))**2
                
                    MAX = max(MAX, error)
                    MIN = min(MIN, error)

                    ME += error

                data_count += len(data)

    # Validating data count
    if data_count == 0:
        print("Warning: estimator/EstimateRegressionError -  only 1 data point")
        data_count = 1            

    # Normalise error metrics
    RMSE = sqrt(RMSE/data_count)
    MAE = MAE/data_count
    MSPE = MSPE/(data_count-zero_cases)
    MAPE = MAPE/(data_count-zero_cases)
    RMSPE = sqrt(RMSPE/data_count)
    ME = ME/data_count

    return RMSE, MAE, MSPE, MAPE, RMSPE, MAX, MIN, ME

    