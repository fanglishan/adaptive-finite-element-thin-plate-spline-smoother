#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-06-19 15:45:49
@Last Modified time: 2022-10-09 23:46:11

This class contains routines to calculate the L2 norm and 
energy norm of a given grid.

"""

# Import libraries
from grid.NodeTable import node_iterator
from grid.EdgeTable import endpt_iterator
from adaptive.triangle.Triangle import triangle_iterator, triangle_iterator_ref
from adaptive.triangle.TriangleIntegrateLinear import calc_area, linear_integrate_single
from adaptive.triangle.TriangleIntegrateQuadratic import quadratic_integrate
from adaptive.triangle.SetPolynomial import get_solution_polynomial_linear_2D
from adaptive.triangle.GridValue import cal_value_2d, cal_smoother_2d
from adaptive.line.GridValue import cal_value_1d, cal_smoother_1d
from estimator.MeasureError import calculate_error

from numpy import sqrt
from copy import deepcopy



def estimate_energy_norm(grid, parameter):
	""" Calculate the energy norm of the current grid """

	# Initialise two error norm
	L2_norm = 0.0
	energy_norm = 0.0

#	u = parameter.get_function()[0]

	if grid.get_dim() == 1: 
		pass

	elif grid.get_dim() == 2:

		
		for tri in triangle_iterator_ref(grid):
			if tri[0].get_node_id() < tri[1].get_node_id() and \
				tri[1].get_node_id() < tri[2].get_node_id():

				error1 = calculate_error(tri[0].get_value(), tri[0].get_coord(), parameter)
				error2 = calculate_error(tri[1].get_value(), tri[1].get_coord(), parameter)
				error3 = calculate_error(tri[2].get_value(), tri[2].get_coord(), parameter)

				poly = get_solution_polynomial_linear_2D(tri[0], tri[1], tri[2], error1, error2, error3)

				L2_norm += quadratic_integrate(poly, poly, tri[0], tri[1], tri[2])

				energy_norm += calc_area(tri[0].get_coord(), tri[1].get_coord(), \
					tri[2].get_coord())* (poly.get_x()**2+poly.get_y()**2)

	else:
		print("Warning: estimator/EstimateEnergyNorm - invalid grid dimension")

	# Normalise error norm
	L2_norm = sqrt(L2_norm)
	energy_norm = sqrt(energy_norm)

	return L2_norm, energy_norm