# -*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang
@Date:   2022-03-14 18:00:11
@Last Modified by:   Fang
@Last Modified time: 2022-10-09 23:44:27

This class estimate the error of the given grid.

"""


# Import libraries
from grid.NodeTable import node_iterator
from grid.EdgeTable import endpt_iterator
from adaptive.triangle.Triangle import triangle_iterator, triangle_iterator_ref
from adaptive.triangle.TriangleFunctions import get_mid_coord_2d, get_h_2d, \
		cal_value_2d, find_h
from adaptive.triangle.GridValue import cal_smoother_2d
from adaptive.interval.GridValue import cal_value_1d, cal_smoother_1d
from adaptive.triangle.TriangleIntegrateLinear import calc_area
from parameter.SettingParameters import ErrorType
from EstimateFunctional import calc_fit, calc_smooth

from scipy import linalg
from numpy import inf, sqrt
from copy import deepcopy


def calculate_error(value, point, parameters):
	""" Compute error at the given point """

	# Change the coordinates so it will remain in the grid
	# only works for square grids (may remove later)
	coord = deepcopy(point)
	for i in range(0, len(coord)):
		if coord[i] <= 0.0:
			coord[i] += 0.000000000000001
		if coord[i] >= 1.0:
			coord[i] -= 0.000000000000001

	# Initialise error
	error = 0

	# Estimate error using true solution
	if parameters.get_solution_type() == 1:
		u = parameters.get_u()
		error = abs(value-u(coord))

	# Estimate error using a discrete smoother with a fine grid
	elif parameters.get_solution_type() == 2:

		solution = parameters.get_solution()

		if solution is None:
			print "ErrorEstimate: no solution provided"

		# Calculate error for 1D grid
		if solution.get_dim() == 1:
			error = abs(value-cal_smoother_1d(solution, coord))

		# Calculate error for 2D grid
		elif solution.get_dim() == 2:	
			error = abs(value-cal_smoother_2d(solution, coord))

	else:
		print "EstimateError: unkown solution type"

	return error


def estimate_node_error_norm(grid, parameters):
	""" Estimate norms for the uniform grid """

	# Estimate error using the true solution
	u = parameters.get_u()

	# Initialise a list of error 
	error_list = []

	# Iterate through the nodes in the grid
	for node in node_iterator(grid):

		# Calculate error on the node
		error = calculate_error(node.get_value(), node.get_coord(), parameters)
		
		error_list.append(error)

	# One norm error
	one_norm = sum(error_list)/len(error_list)

	# Two norm error
	two_norm = 0.0
	for i in range(0, len(error_list)):
		two_norm += error_list[i]**2
	two_norm = sqrt(two_norm/len(error_list))

	# Max norm error
	max_norm = max(error_list)

	return one_norm, two_norm, max_norm


def estimate_error_norm_1d(grid, parameters):
	""" Estimate error norms of 2D grids """
	

	# One, two and max norm
	one_norm = 0.0
	two_norm = 0.0
	max_norm = 0.0

	# One and two norm with h
	one_norm_h = 0.0
	two_norm_h = 0.0

	u = parameters.get_u()

	# Iterate through the nodes
	for node in node_iterator(grid):
		node_id = node.get_node_id()
		for endpt in endpt_iterator(grid, node.get_node_id()):   
			if node_id < endpt:

				end_node = grid.get_node_ref(endpt)


				h = abs(node.get_coord()[0]-end_node.get_coord()[0])

				mid_coord = [(node.get_coord()[0]+end_node.get_coord()[0])/2.0]

				# Interpolated value at the middle point
				value =  cal_value_1d(node, end_node, mid_coord)

				# The interpolation error at the center point
				error = calculate_error(value, mid_coord, parameters)

				# Add L norms
				one_norm += error
				two_norm += error**2
				if error > max_norm:
					max_norm = error

				one_norm_h += h*error
				two_norm_h += h*(error**2)

	# Normalise norms
	one_norm /= tri_count
	two_norm = sqrt(two_norm/tri_count)

	two_norm_h = sqrt(two_norm_h)

	return one_norm, two_norm, max_norm, one_norm_h, two_norm_h



def estimate_error_norm_2d(grid, parameters):
	""" Estimate error norms of 2D grids """

	# Triangle counter
	tri_count = 0

	# One, two and max norm
	one_norm = 0.0
	two_norm = 0.0
	max_norm = 0.0

	# One and two norm with h or A
	one_norm_h = 0.0
	two_norm_h = 0.0
	one_norm_A = 0.0
	two_norm_A = 0.0

	u = parameters.get_u()

	# Iterate through each triangle once
	for tri in triangle_iterator_ref(grid):
		if tri[0].get_node_id() > tri[1].get_node_id() and \
			tri[1].get_node_id() > tri[2].get_node_id():

			# Longest edge of the triangle
			h = get_h_2d(tri[0], tri[1], tri[2])

			# Area of the triangle
			A = calc_area(tri[0].get_coord(), tri[1].get_coord(), tri[2].get_coord())

			# Center coordinate of the triangle
			mid_coord = get_mid_coord_2d(tri[0], tri[1], tri[2])

			# Interpolated value at the center point
			value = cal_value_2d(tri[0], tri[1], tri[2], mid_coord)

			# The interpolation error at the center point
			error = calculate_error(value, mid_coord, parameters)

			# Add L norms
			one_norm += error
			two_norm += error**2
			if error > max_norm:
				max_norm = error

			# Add h and A norms
			one_norm_h += (h**2)*error
			two_norm_h += (h**2)*(error**2)
			one_norm_A += A*error
			two_norm_A += A*(error**2)

			tri_count += 1

	# Normalise norms
	one_norm /= tri_count
	two_norm = sqrt(two_norm/tri_count)

	two_norm_h = sqrt(two_norm_h)
	two_norm_A = sqrt(two_norm_A)

	return one_norm, two_norm, max_norm, one_norm_h, two_norm_h, one_norm_A, two_norm_A


def calc_error(grid, parameters, grid_error):
	""" Estimate error of the grid """

	# 1D grid
	if grid.get_dim() == 1:
		one_norm, two_norm, max_norm, one_h, two_h = \
			estimate_error_norm_1d(grid, parameters)

	# 2D grid
	elif grid.get_dim() == 2:
		one_norm, two_norm, max_norm, one_h, two_h, one_A, two_A = \
			estimate_error_norm_2d(grid, parameters)

	# Record errors
	grid_error.add_error(one_norm, ErrorType.one_norm)
	grid_error.add_error(two_norm, ErrorType.two_norm)
	grid_error.add_error(max_norm, ErrorType.max_norm)
	grid_error.add_error(one_h, ErrorType.one_h)
	grid_error.add_error(two_h, ErrorType.two_h)

	if grid.get_dim() == 2:
		grid_error.add_error(one_A, ErrorType.one_A)
		grid_error.add_error(two_A, ErrorType.two_A)

	# Goodness and smoothness of the fit
	fit = 0.0#calc_fit(grid, parameters)
	smooth = 0.0#calc_smooth(grid, parameters)

	grid_error.add_error(fit, ErrorType.fit)
	grid_error.add_error(smooth, ErrorType.smooth)

	# Record number of nodes
	grid_error.add_no_nodes(grid.get_no_nodes())

	# Record max h
	max_h, min_h = find_h(grid)
	grid_error.add_max_h(max_h)
	grid_error.add_min_h(min_h)

	# Record smoothing parameter alp
	grid_error.add_alpha(parameters.get_alpha())

	