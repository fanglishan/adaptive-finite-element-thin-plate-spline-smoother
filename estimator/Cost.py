#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2021-02-10 16:41:17
@Last Modified time: 2022-03-29 11:03:06

This class defines a variable that records the computational cost
of various components of the TPSFEM program.

"""

from parameter.Definition import CostType


class Cost:
	""" Computational cost of TPSFEM program """

	# Computational time list
	_time_list = []

	# Computational time name list
	_time_name_list = []

	def __init__(self, n=7):
		self._time_list = [[] for x in range(n)]
		self._time_name_list = ["I_list", "Ir_list", "build", "solve", \
				"gcv", "gcv_auxi", "refine"]

	def __str__(self):
		string = ""
		# Output a warning message
		if len(self._time_list) != len(self._time_name_list):
			print("Warning: estimator/Cost - inconsistent lengths", len(self._time_list), len(self._time_name_list))
#		for i in range(0, len(self._time_list)):
#			string += self._time_name_list[i] + " = " + str(self._time_list[i]) + "\n"
		
		for i in range(2, len(self._time_list)):
			string += self._time_name_list[i] + " = " + str(self._time_list[i]) + "\n"

		no_list, sum_list, average_list = self.get_list_info(CostType.indicate)
		string += "I_no"+ " = " + str(no_list) + "\n"
		string += "I_sum"+ " = " + str(sum_list) + "\n"
		string += "I_ave"+ " = " + str(average_list) + "\n"

		no_list, sum_list, average_list = self.get_list_info(CostType.indicate_refine)
		string += "Ir_no"+ " = " + str(no_list) + "\n"
		string += "Ir_sum"+ " = " + str(sum_list) + "\n"
		string += "Ir_ave"+ " = " + str(average_list) + "\n"

		return string


	def add_time(self, time, i):
		""" Add a time to i-th list """
		self._time_list[i].append(time)

	def append_time(self, time, i, j):
		""" Add a time to j-th list in i-th list """
		self._time_list[i][j].append(time)

	def get_time_lists(self):
		""" Get time lists """
		return self._time_list

	def get_time_list(self, i):
		""" Get a time list """
		return self._time_list[i]


	def get_list_info(self, index=CostType.indicate):
		""" Get info of the indicate list """
		indicate_list = self._time_list[index]
		no_list = []
		sum_list = []
		average_list = []
		for i in range(0, len(indicate_list)):
			no_list.append(len(indicate_list[i]))
			sum_list.append(sum(indicate_list[i]))
			if no_list[-1] != 0:
				average_list.append(sum_list[-1]/no_list[-1])
		return no_list, sum_list, average_list