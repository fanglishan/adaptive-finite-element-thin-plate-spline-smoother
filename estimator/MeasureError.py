#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2020-01-09 21:03:13
@Last Modified time: 2022-10-09 23:45:31

This class contains routines to measure the error of a node
based on the true model problem solution or a TPSFEM smoother
with a much finer grid.

"""

# Import libraries
from adaptive.line.GridValue import cal_value_1d, cal_smoother_1d
from adaptive.triangle.GridValue import cal_value_2d, cal_smoother_2d

from copy import deepcopy


def calculate_error(value, point, parameter):
	""" Compute error at the given point """

	# Change the coordinates so it will remain in the grid
	# only works for square grids (may remove later)
	coord = deepcopy(point)
	for i in range(0, len(coord)):
		if coord[i] <= 0.0:
			coord[i] += 0.000000000000001
		if coord[i] >= 1.0:
			coord[i] -= 0.000000000000001

	# Initialise error
	error = 0

	u = parameter.get_function()[0]
	error = abs(value-u(coord))

	return error


