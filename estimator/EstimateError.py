#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-05-19 10:01:53
@Last Modified time: 2022-10-09 23:45:11

This class contains routines to estimate the error of the 
given grid. The error is estimated on the nodes. The error 
norm for the adaptively refined grids will associate the 
maximum linked edge length of the node.

"""

# Import libraries
from grid.NodeTable import node_iterator
from grid.EdgeTable import endpt_iterator
from adaptive.triangle.Triangle import triangle_iterator, triangle_iterator_ref
from adaptive.triangle.TriangleFunction import find_h, cal_distance, find_no_tri
from adaptive.triangle.TriangleIntegrateLinear import calc_area
from adaptive.triangle.TriangleIntegrateQuadratic import quadratic_integrate
from adaptive.triangle.GridValue import cal_value_2d, cal_smoother_2d
from adaptive.line.GridValue import cal_value_1d, cal_smoother_1d
from parameter.Definition import ErrorType
from estimator.MeasureError import calculate_error
from estimator.EstimateEnergyNorm import estimate_energy_norm
from estimator.EstimateRegressionError import calc_regression_error
#from EstimateFunctional import calc_fit, calc_smooth

from scipy import linalg
from numpy import inf, sqrt
from copy import deepcopy


def estimate_error_norm(grid, parameter, calc_h):
	""" Estimate error norms of the nodes """

	# Whether it is using real-world dataset
	real_data = False

	if len(str(abs(parameter.get_dataset()))) == 1:
		real_data = True

	dataset = parameter.get_dataset()

	if real_data:
		model_problem = None
	else:
		model_problem = int(str(dataset)[1:3])

	# One, two and max norm
	one_norm = 0.0
	two_norm = 0.0
	max_norm = 0.0

	# One and two norm with h or A
	one_norm_h = 0.0
	two_norm_h = 0.0

	# Count number of nodes
	node_count = 0
	for node in node_iterator(grid):
		if parameter.get_boundary() == 1:
			if not node.get_slave():
				node_count += 1
		elif parameter.get_boundary() == 2:
			node_count += 1

	no_nodes = 0

	# Iterate through each node
	for node in node_iterator(grid):

		coord = node.get_coord()

		if real_data:

			if grid.get_dim() == 2:
				if coord[0] < -0.000001 or coord[0] > 1.000001 or \
					coord[1] < -0.000001 or coord[1] > 1.000001:
					continue

		# Skip the boundary nodes
		if not real_data:
			if parameter.get_boundary() == 1:
				if node.get_slave():
					continue

		no_nodes += 1

		node_id = node.get_node_id()
		
		# The interpolation error at the center point
		error = calculate_error(node.get_value(), coord, parameter)

		# Add up L norms
		one_norm += error
		two_norm += error**2
		if error > max_norm:
			max_norm = error

		if calc_h:
			
			# Find the longest edge connected to the node
			h = 0.0
			for endpt in endpt_iterator(grid, node_id): 
				end_node = grid.get_node(endpt)
				d = cal_distance(node, end_node)
				if d > h:
					h = d

			if abs(h) < 0.00000000001:
				print("Warning: estimator/EstimateError - h may not be valid")

			# h for different dimensions of L2 norm approximation
			# 1D: e = sqrt(sum(h*e**2))
			# 2D: e = sqrt(sum(h**2*e**2))
			if grid.get_dim() == 2:
				h = h**2

			# Add up h norms
			one_norm_h += h*error
			two_norm_h += h*(error**2)

	node_count = no_nodes
	# Normalise error norms
	one_norm /= node_count
	two_norm = sqrt(two_norm/node_count)
	two_norm_h = sqrt(two_norm_h)

	return one_norm, two_norm, max_norm, one_norm_h, two_norm_h




def calc_error(grid, parameter, error, region=None, calc_h=True, \
	calc_L=True, calc_minimiser=False, calc_regression=False):
	""" Estimate error of the grid """

	dataset = parameter.get_dataset()
	if len(str(abs(dataset))) == 1:
		calc_h=False
		calc_L=False

	# calc_h: whether to refine error norms related to 
	# edge length h (for adaptively refined grids)

	# Estimate error norms
	one_norm, two_norm, max_norm, one_norm_h, two_norm_h = \
		estimate_error_norm(grid, parameter, calc_h)	

	# Record errors
	error.add_error(one_norm, ErrorType.one_norm)
	error.add_error(two_norm, ErrorType.two_norm)
	error.add_error(max_norm, ErrorType.max_norm)
	error.add_error(one_norm_h, ErrorType.one_h)
	error.add_error(two_norm_h, ErrorType.two_h)

	# Estimate L2 norm and energy norm
	L2_norm = 0.0
	energy_norm = 0.0
	
	if calc_L:
		L2_norm, energy_norm = estimate_energy_norm(grid, parameter)

	error.add_error(L2_norm, ErrorType.L2)
	error.add_error(energy_norm, ErrorType.energy)

	# Goodness and smoothness of the fit
	if calc_minimiser:
		fit = calc_fit(grid, parameter)
		smooth = calc_smooth(grid, parameter)
	else:
		fit = 0.0
		smooth = 0.0

	error.add_error(fit, ErrorType.fit)
	error.add_error(smooth, ErrorType.smooth)

	if region is not None:
		# Calculate regression (RMSE)
		if calc_regression:
			RMSE, MAE, MSPE, MAPE, RMSLE, MAX, MIN, ME = calc_regression_error(grid, region, parameter)
		else:
			RMSE = 0.0
			MAE = 0.0
			MSPE = 0.0
			MAPE = 0.0
			RMSLE = 0.0
			MAX = 0.0
			MIN = 0.0
			ME = 0.0
			
		error.add_error(RMSE, ErrorType.rmse)
		error.add_error(MAE, ErrorType.mae)
		error.add_error(MSPE, ErrorType.mspe)
		error.add_error(MAPE, ErrorType.mape)
		error.add_error(RMSLE, ErrorType.rmsle)
		error.add_error(MAX, ErrorType.max_data)
		error.add_error(MIN, ErrorType.min_data)
		error.add_error(ME, ErrorType.me)

	# Record number of nodes
	"""
	node_count = count_no_nodes(grid, parameter)

	if node_count == 0:
		error.add_no_nodes(grid.get_no_nodes())
	else:
		error.add_no_nodes(node_count)
	"""
	error.add_no_nodes(grid.get_no_nodes())

	# Record max h
	max_h, min_h = find_h(grid)
	error.add_max_h(max_h)
	error.add_min_h(min_h)

	# Record number of triangles
	if parameter.get_dim() == 2:
		no_tri = find_no_tri(grid)
		error.add_tri(no_tri)
	else:
		error.add_tri(0)

	# Record smoothing parameter alp
	error.add_alpha(parameter.get_alpha())
