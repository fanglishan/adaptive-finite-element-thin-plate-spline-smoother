# -*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang
@Date:   2022-03-14 18:00:11
@Last Modified by:   Fang
@Last Modified time: 2022-03-29 11:05:35

This class estimates minimiser functional for the given grid.

"""

def calc_fit(grid, parameters):
	""" Calculate goodness of the fit """

	fit = 0.0

	d = grid.get_dim()

	sum = 0
	data_regions = grid.get_data_regions()

	for region in data_regions:

		data_points = region.get_data_points()

		for data in data_points:

			# Calculate smoother value for 1D grid
			if d == 1:
				value = cal_smoother_1d(grid, data[:-1])

			# Calculate smoother value for 2D grid
			elif d == 2:			
				value = cal_smoother_2d(grid, data[:-1])

			else:
				print("Warning: estimator/EstimateFunctional - invalid dimension")

			# Add error square
			fit += (value-data[-1])**2

	# Normalise the goodness of fit
	fit /= parameters.get_data_size()

	return fit


def calc_smooth(grid, parameters):
	""" Calculate smoothness of the fit """
	
	smooth = 0.0

	node_list = build_node_list(grid, parameters.get_boundary_type())

	no_nodes = len(node_list)

	L_matrix =  lil_matrix((no_nodes,no_nodes),dtype=float)

	# Iterate through connections
	for j in range(0,len(node_list)):
		node_id2 = node_list[j].get_node_id()
		for i in range(0,len(node_list)):
			node_id1 = node_list[i].get_node_id()
			if grid.is_connection(node_id1, node_id2):

				L_matrix[i,j] = grid.get_matrix_L_value(node_id1, node_id2)

	d = grid.get_dim()


	L_matrix = csr_matrix(L_matrix)
	g_vector = zeros([no_nodes, 1])

	for j in range(0, d):
		for i in range(0, len(node_list)):
			g_vector[i,0] = node_list[i].get_d_values()[j]

		smooth += dot((g_vector.transpose()*L_matrix),g_vector)[0][0]

	return smooth

	