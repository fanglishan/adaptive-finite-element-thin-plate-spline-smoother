#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-05-19 10:01:53
@Last Modified time: 2022-10-09 23:45:03

This class contains routines to estimate the error of the 
given grid. The error is estimated on the nodes. The error 
norm for the adaptively refined grids will associate the 
maximum linked edge length of the node.

"""

# Import libraries
from grid.NodeTable import node_iterator
from grid.EdgeTable import endpt_iterator
from adaptive.triangle.Triangle import triangle_iterator, triangle_iterator_ref
from adaptive.triangle.TriangleFunction import find_h, cal_distance, find_no_tri
from adaptive.triangle.TriangleIntegrateLinear import calc_area
from adaptive.triangle.TriangleIntegrateQuadratic import quadratic_integrate
from adaptive.triangle.GridValue import cal_value_2d, cal_smoother_2d
from adaptive.line.GridValue import cal_value_1d, cal_smoother_1d
from parameter.Definition import ErrorType
from estimator.EstimateEnergyNorm import estimate_energy_norm
#from EstimateFunctional import calc_fit, calc_smooth
from data.ObtainData import obtain_data_grid

from scipy import linalg
from numpy import inf, sqrt
from copy import deepcopy


def calculate_error(value, point, parameter):
	""" Compute error at the given point """

	# Change the coordinates so it will remain in the grid
	# only works for square grids (may remove later)
	coord = deepcopy(point)
	for i in range(0, len(coord)):
		if coord[i] <= 0.0:
			coord[i] += 0.000000000000001
		if coord[i] >= 1.0:
			coord[i] -= 0.000000000000001

	# Initialise error
	error = 0

	# Estimate error using true solution
	if parameter.get_solution_type() == 1:
		u = parameter.get_function()[0]
		error = abs(value-u(coord))

	# Estimate error using a discrete smoother with a fine grid
	elif parameter.get_solution_type() == 2:

		solution = parameter.get_solution()

		if solution is None:
			print("Warning: estimator/EstimateError - no solution provided")

		# Calculate error for 1D grid
		if solution.get_dim() == 1:
			error = abs(value-cal_smoother_1d(solution, coord))

		# Calculate error for 2D grid
		elif solution.get_dim() == 2:
			error = abs(value-cal_smoother_2d(solution, coord))

	else:
		print("Warning: estimator/EstimateError - unkown solution type")

	return error


def estimate_error_norm(grid, parameter, calc_h):
	""" Estimate error norms of the nodes """

	# One, two and max norm
	one_norm = 0.0
	two_norm = 0.0
	max_norm = 0.0

	# One and two norm with h or A
	one_norm_h = 0.0
	two_norm_h = 0.0

	node_count = 0

	# Iterate through each node
	for node in node_iterator(grid):

		# Skip the boundary nodes
		if parameter.get_boundary() == 1:
			if node.get_slave():
				continue

		node_id = node.get_node_id()

		coord = node.get_coord()

		# The interpolation error at the center point
		error = calculate_error(node.get_value(), coord, parameter)

		# Add up L norms
		one_norm += error
		two_norm += error**2
		if error > max_norm:
			max_norm = error

		if calc_h:
			
			# Find the longest edge connected to the node
			h = 0.0
			for endpt in endpt_iterator(grid, node_id): 
				end_node = grid.get_node(endpt)
				d = cal_distance(node, end_node)
				if d > h:
					h = d

			if abs(h) < 0.00000000001:
				print("Warning: estimator/EstimateError - h may not be valid")

			# h for different dimensions of L2 norm approximation
			# 1D: e = sqrt(sum(h*e**2))
			# 2D: e = sqrt(sum(h**2*e**2))
			if grid.get_dim() == 2:
				h = h**2

			# Add up h norms
			one_norm_h += h*error
			two_norm_h += h*(error**2)

		node_count += 1

	# Normalise error norms
	one_norm /= node_count
	two_norm = sqrt(two_norm/node_count)
	two_norm_h = sqrt(two_norm_h)

	return one_norm, two_norm, max_norm, one_norm_h, two_norm_h


def calc_regression_error(grid, region, parameter):

	data = obtain_data_grid(region, grid)

	if len(data) < 2:
		print("Warning: estimator/EstimateError - insufficient data points")
		return

	sum = 0.0

	if grid.get_dim() == 1:

		for point in data:
			sum += (point[-1]-cal_smoother_1d(grid, [point[0]]))**2
		
	elif grid.get_dim() == 2:

		for point in data:
			sum += (point[-1]-cal_smoother_2d(grid, point[0:2]))**2

	return sqrt(sum/len(data))



def calc_error(grid, region, parameter, error, calc_h=True, \
	calc_L=True, calc_minimiser=False, calc_regression=False):
	""" Estimate error of the grid """

	# calc_h: whether to refine error norms related to 
	# edge length h (for adaptively refined grids)

	# Estimate error norms
	one_norm, two_norm, max_norm, one_norm_h, two_norm_h = \
		estimate_error_norm(grid, parameter, calc_h)	

	# Record errors
	error.add_error(one_norm, ErrorType.one_norm)
	error.add_error(two_norm, ErrorType.two_norm)
	error.add_error(max_norm, ErrorType.max_norm)
	error.add_error(one_norm_h, ErrorType.one_h)
	error.add_error(two_norm_h, ErrorType.two_h)

	# Estimate L2 norm and energy norm
	L2_norm = 0.0
	energy_norm = 0.0
	
	if calc_L:
		L2_norm, energy_norm = estimate_energy_norm(grid, parameter)

	error.add_error(L2_norm, ErrorType.L2)
	error.add_error(energy_norm, ErrorType.energy)

	# Goodness and smoothness of the fit
	if calc_minimiser:
		fit = calc_fit(grid, parameter)
		smooth = calc_smooth(grid, parameter)
	else:
		fit = 0.0
		smooth = 0.0

	error.add_error(fit, ErrorType.fit)
	error.add_error(smooth, ErrorType.smooth)

	# Calculate regression (RMSE)
	if calc_regression:
		reg_error = calc_regression_error(grid, region, parameter)
	else:
		reg_error = 0.0
		
	error.add_error(reg_error, ErrorType.rmse)

	# Record number of nodes
	error.add_no_nodes(grid.get_no_nodes())

	# Record max h
	max_h, min_h = find_h(grid)
	error.add_max_h(max_h)
	error.add_min_h(min_h)

	# Record number of triangles
	if parameter.get_dim() == 2:
		no_tri = find_no_tri(grid)
		error.add_tri(no_tri)
	else:
		error.add_tri(0)

	# Record smoothing parameter alp
	error.add_alpha(parameter.get_alpha())
