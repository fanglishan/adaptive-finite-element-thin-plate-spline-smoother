#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2021-02-10 13:46:31
@Last Modified time: 2022-03-29 11:06:06

This class contains routines to measure the computatinoal cost
of functions.

"""

# Import libraries
from parameter.Definition import CostType

from timeit import default_timer


def measure_time(costType):
	def decorator(func):
		def wrapper(parameter, n, grid_type):
			t_start = default_timer()
			res = func(parameter, n, grid_type)
			t = default_timer()-t_start
#			cost.add_cost(t, costType)
			print(t)
			return res

		return wrapper
	return decorator

