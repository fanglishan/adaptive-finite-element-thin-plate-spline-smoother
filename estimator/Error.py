#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-05-19 10:01:53
@Last Modified time: 2022-11-03 11:48:38

This class defines a variable that records the error of the grid 
and corresponding number of nodes and h for the study of the 
convergence rates.

###### Error norms at the center of elements
### For uniform grids
0 - One norm: (1/n)*sum(e_i)
1 - Two norm: (1/n)*sqrt(sum(e_i**2))
2 - Max norm: max(e_i)
### For adaptive grids
3 - One norm(h): sum(h_i*e_i)-1D, sum(h_i**2*e_i)-2D
4 - Two norm(h): sqrt(sum(h_i*e_i**2))-1D, sqrt(sum(h_i**2*e_i**2))-2D
5 - Energy norm: sqrt(integral((s-s_fine_grid)**2)

###### Minimiser functional
8 - goodness of the fit against the given dataset
9 - smoothness of the fit

###### Other parameters
number of nodes
mesh size h

"""

from parameter.Definition import ErrorType

class Error:
	""" Error of the grid """

	# Error list
	_error_list = []

	# Error name list
	_error_name_list = []

	# Number of nodes list
	_no_nodes_list = []

	# Max mesh size list
	_max_h_list = []

	# Min mesh size list
	_min_h_list = []

	# Number of triangles list
	_tri_list = []

	# Smoothing parameter
	_alpha_list = []

	def __init__(self, n=17):
		self._error_list = [[] for x in range(n)]
		self._error_name_list = ["one norm", "two norm", "max norm", \
			"one norm(h)", "two norm(h)", "L2 norm", "energy norm", \
			"fit", "smoothness", "RMSE", "MAE", "MSPE", "MAPE", "RMSPE", \
			"MAX", "MIN", "ME"]
		self._no_nodes_list = []
		self._max_h_list = []
		self._min_h_list = []
		self._tri_list = []
		self._alpha_list = []

	def __str__(self):

		string = ""
		for i in range(0, len(self._error_list)):
			string += self._error_name_list[i] + " = " + str(self._error_list[i]) + "\n"
		
		minimiser_list = []
		for i in range(0, len(self._alpha_list)):
			minimiser_list.append(self._error_list[ErrorType.fit][i]+self._alpha_list[i]*self._error_list[ErrorType.smooth][i])
		string += "minimiser = " + str(minimiser_list) + "\n"
		
		string += "nodes = " + str(self._no_nodes_list) + "\n"
		string += "Max h = " + str(self._max_h_list) + "\n"
		string += "Min h = " + str(self._min_h_list) + "\n"
		string += "triangles = " + str(self._tri_list)
		return string
		
	def add_error(self, error, i):
		""" Add an error to i-th list """
		self._error_list[i].append(error)

	def add_no_nodes(self, no_nodes):
		""" Add a number of nodes """
		self._no_nodes_list.append(no_nodes)

	def add_max_h(self, max_h):
		""" Add a max mesh size h """
		self._max_h_list.append(max_h)

	def add_min_h(self, min_h):
		""" Add a min mesh size h """
		self._min_h_list.append(min_h)

	def add_tri(self, no_tri):
		""" Add a number of triangles """
		self._tri_list.append(no_tri)

	def add_alpha(self, alpha):
		""" Add a smoothing paraemter alpha """
		self._alpha_list.append(alpha)

	def get_error_lists(self):
		""" Get error lists """
		return self._error_list

	def get_error_list(self, i):
		""" Get i-th error list """
		return self._error_list[i]

	def get_no_nodes_list(self):
		""" Get the number of nodes list """
		return self._no_nodes_list

	def get_max_h_list(self):
		""" Get a max mesh size h """
		return self._max_h_list

	def get_min_h_list(self):
		""" Get a min mesh size h """
		return self._min_h_list

	def get_tri_list(self):
		""" Get a number of triangles """
		return self._tri_list

	def get_alpha_list(self):
		""" Get the alpha list """
		return self._alpha_list