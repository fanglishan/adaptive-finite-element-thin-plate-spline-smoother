"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-05-29 16:09:40
@Last Modified time: 2024-10-06 22:43:33

This class contains the main calling module for 1D and 2D TPS.

"""

# Import libraries
from data.DatasetManager import obtain_dataset
from rbf.RbfSolver import rbf
from rbf.SampleBasis import sample_basis
from rbf.CalcError import calc_errors, calc_errors_tps_deri
from plot.DataPlot import plot_data, plot_data_dist_2D
from plot.RbfPlot import plot_rbf, plot_contour, plot_contour_2D_trial
from plot.RbfErrorPlot import plot_rbf_error_2D

import function.Function2D as f2D


def write_info(data, rbf):
    text_file = open("info_boundary.txt", "w+")
    text_file.write("data = " + str(data))
    text_file.write("\n")
    text_file.write("tps = " + str(rbf.tolist()))
    text_file.close()


### Dataset
# 1:dimension, 2-3:model, 4:distribution, 5:noise distribution, 6:noise level, 7:data size
### 1D Model problem
# zero=0, linear=1, quadratic=2, cubic=3, exp=4, sine=5, osci_sine=6, 
# grad_exp=7, steep_exp=8, exp_fluc=9, exp_mul=10, frac=11, shift=12, runge=13
# blocks=14, bumps=15, Heavisine=16, Doppler=17, Mexican_hat=18
### 2D Model problem
# zero=0, linear=1, quadratic=2, cubic=3, exp=4, sine=5, osci_sine=6, grad_exp=7, steep_exp=8
# exp_fluc=9, exp_mul=10, sin_mul=11, hole=12, franke=13, tanh=14, cone=15, peak=16
# peak(large domain)=17, nonhomosmooth1 = 18, nonhomosmooth2 = 19
### Real-world data sets
# 1-magnetic data, 2-bone data, 3-lake data, 4-lena data, 5-crater data, 6-iron mine, 
# 7-iron mountain, 8-Aeromagnetic Burney, 9-Grapevine Canyon, 10-river mouth, 11-black beach
# 12-coastal region


# The dataset 
dataset = 1181281
#dataset = 1181201
#dataset = 3012202
#dataset = 7
dataset = 1081242


# Smoothing parameter
#alpha = 0
alpha = 1e-5

no_basis = 100

deriv = 0

print("Dataset:", dataset)

subset_data = False

#build_dataset(dataset)

# Obtain dataset
data = obtain_dataset(dataset, subset_data)


# Sample a subset of data
sampled_data = sample_basis(data, no_basis)
#sampled_data = data


#print("data =", sampled_data)
print("Sampled", len(sampled_data), "points from", len(data), "points")

# Dimension of dataset
d = int(str(dataset)[0])
if int(str(dataset)) < 500:
    d = 2

# Basis function: 1-TPS r^2*log(r), Triharmonic r^3 (3D), 2-TPS (higher-order)
# 3-Wendland (1−r)^2, 4-Wendland (1−r)^4*(4r+1), 5-Wendland (1−r)^6*(35**2+18r+3)
b = 1

# Whether output statistics (system)
calc_stat = False

print("Basis function:", b)
print("Smoothing parameter:", alpha)


# Run RBF solver
rbf_values = rbf(sampled_data, alpha, d, b, calc_stat)
#rbf_values = rbf(data, alpha, d, b)

#print(rbf_values.tolist())

RMSE, MAE, ME, MAX, MIN = calc_errors(data, sampled_data, rbf_values, d, b)
print("RMSE",RMSE)
"""
deriv = 0
f = f2D.peak_u
RMSE = calc_errors_tps_deri(data, sampled_data, rbf_values, d, b, deriv, f)
"""

#for i in range(50, 1100, 50):
#    print(i)


#write_info(sampled_data, rbf_values)

# Plot the RBF solution
#plot_rbf(rbf_values, sampled_data, d, b)
"""
plot_contour(rbf_values, sampled_data, d, b, 0)
plot_contour(rbf_values, sampled_data, d, b, 1)
plot_contour(rbf_values, sampled_data, d, b, 2)
plot_contour(rbf_values, sampled_data, d, b, 3)
"""
#plot_contour_2D_trial(rbf_values, sampled_data, d, b, 0)
#plot_contour_2D_trial(rbf_values, sampled_data, d, b, 1)
#plot_data_dist_2D(sampled_data, True)
#plot_rbf_error_2D(rbf_values, sampled_data, data, d, b, f, uxy)
plot_rbf(rbf_values, sampled_data, d, b, 0)
#plot_rbf(rbf_values, sampled_data, d, b, 3)   # dx
#plot_rbf(rbf_values, sampled_data, d, b, 2)   # dy
