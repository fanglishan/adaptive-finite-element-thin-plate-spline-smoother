# -*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2024 Fang

@Author: Fang
@Date:   2024-10-13 11:04:20
@Last Modified by:   Fang
@Last Modified time: 2024-10-13 22:44:16

This class contains functions that marks edges for adaptive refinement.

"""

from adaptive.BuildBaseEdge import build_base_edges


def select_refine_edges(grid, base_edges, no_refine):
	""" Select edges with higher error indicators to refine """

	# At least one base edge will be refined
	if no_refine == 0:
		no_refine = 1

	# Initialise a list for error indicators
	error_indicator_list = []

	# Iterate through the base edges
	for i in range(0, len(base_edges)):

		# Get the ids of the endpoints of the edge
		endpt1 = base_edges[i].get_endpt1()
		endpt2 = base_edges[i].get_endpt2()

		# Add error indicator of the edge
		error_indicator = grid.get_error_indicator(endpt1, endpt2)
		error_indicator_list.append(error_indicator)

	# The sorting order	
	order = True    # Descending
#	order = False   # Ascending

	edge_tuple = zip(error_indicator_list, base_edges)
	# Sort the edges by error indicators
	error_indicator_list, refine_edges = \
		zip(*sorted(edge_tuple, key=lambda edge_tuple: edge_tuple[0], reverse=order))
		
	return refine_edges[:no_refine]


def mark_refine_edges(grid, parameter, info=None):

	# Marking strategy for adaptive refinement
	mark = parameter.get_marking_strategy()

	refine_edges = []

	base_edges = build_base_edges(grid)

	# Refine all base edges above preset error tolerance
	if mark == 1:
		for edge in base_edges:
			if edge.get_error_indicator() > parameter.get_error_tol():
				refine_edges.append(edge)

	elif mark == 2:
		refine_edges = select_refine_edges(grid, base_edges, info)


	return refine_edges