#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-04-26 10:33:54
@Last Modified time: 2022-10-09 23:37:31

This class contains routines to create polynomials using data points 
in the tetrahedron.

Adapted from Stals' FEM code.

"""


# Import libraries
from adaptive.tetrahedron.LinearPolynomial import LinearPolynomial

from math import fabs
import numpy as np


def set_polynomial_linear_3D(node1, node2, node3, node4):
	""" Construct a 3D linear polynomial """

	# Check the coordinates are three dimensional
	assert node1.get_dim() == 3 and node2.get_dim() == 3 \
		and node3.get_dim() == 3 and node4.get_dim() == 3, \
		"the tetrahedron coordinates must be three dimensional"

	tol = 1.0E-12

	# Break down the information to make the code easier to read
	x1 = node1.get_coord()[0]
	y1 = node1.get_coord()[1]
	z1 = node1.get_coord()[2]
	x2 = node2.get_coord()[0]
	y2 = node2.get_coord()[1]
	z2 = node2.get_coord()[2]
	x3 = node3.get_coord()[0]
	y3 = node3.get_coord()[1]
	z3 = node3.get_coord()[2]
	x4 = node4.get_coord()[0]
	y4 = node4.get_coord()[1]
	z4 = node4.get_coord()[2]


	division = (x1-x2)*(z2-z3)*(y3-y4) + (y1-y2)*(x2-x3)*(z3-z4) + (z1-z2)*(y2-y3)*(x3-x4)
	division += (x2-x1)*(y2-y3)*(z3-z4) + (y2-y1)*(z2-z3)*(x3-x4) + (z2-z1)*(x2-x3)*(y3-y4)

	# Avoid division by zero errors
	assert fabs(division) > tol, "divide by zero in set_polynomial_linear_3D"

	x_coeff = ((y3-y4)*(z2-z4)-(z3-z4)*(y2-y4))/division
	y_coeff = ((z3-z4)*(x2-x4)-(x3-x4)*(z2-z4))/division
	z_coeff = ((x3-x4)*(y2-y4)-(y3-y4)*(x2-x4))/division

	# Find the polynomial coefficients
	poly = LinearPolynomial()
	poly.set_x(x_coeff)
	poly.set_y(y_coeff)
	poly.set_z(z_coeff)
	poly.set_const(-x2*x_coeff - y2*y_coeff - z2*z_coeff)
	
	# Return the polynomial
	return poly


def get_solution_polynomial_linear_3D(node1, node2, node3, node4, \
									value1, value2, value3, value4):
	""" Construct a 3D linear polynomial """
	
	# Check the coordinates are two dimensional
	assert node1.get_dim() == 3 and node2.get_dim() == 3 \
		and node3.get_dim() == 3 and node4.get_dim() == 3, \
			"the triangle coordinates must be two dimensional"
	
	# Get the coordinates of the three vertices
	coord1 = node1.get_coord()
	coord2 = node2.get_coord()
	coord3 = node3.get_coord()
	coord4 = node3.get_coord()

	matrix = np.array([[coord1[0],coord1[1],coord1[2],1.0],[coord2[0],coord2[1],coord2[2],1.0],\
						[coord3[0],coord3[1],coord3[2],1.0],[coord4[0],coord4[1],coord4[2],1.0]])

	vector = np.array([[value1],[value2],[value3],[value4]])

	coef = np.linalg.solve(matrix, vector)

	# Find the polynomial coefficients
	poly = LinearPolynomial()
	poly.set_const(coef[3][0])
	poly.set_x(coef[0][0])
	poly.set_y(coef[1][0])
	poly.set_z(coef[2][0])

	# Return the polynomial
	return poly