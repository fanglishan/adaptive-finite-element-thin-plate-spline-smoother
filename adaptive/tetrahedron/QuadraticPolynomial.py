#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-04-26 10:33:54
@Last Modified time: 2022-07-31 14:02:07

This class keeps record of a quadratic polynomial in three dimensions.
It currently only offers minimal functionality, additional polynomial
opertations, such as addition and scalar multiplication, could also 
be included.

Adapted from Stals' FEM code.

"""


class QuadraticPolynomial:
	""" A quadratic polynomial in three dimensions"""
	
	# constant coefficient
	_const = 0.0
	
	# x coefficient
	_x = 0.0
	
	# y coefficient
	_y = 0.0

	# z coefficient
	_z = 0.0

	# xy coefficient
	_xy = 0.0

	# yz coefficient
	_yz = 0.0

	# zx coefficient
	_zx = 0.0

	# x^2 coefficient
	_xx = 0.0

	# y^2 coefficient
	_yy = 0.0

	# z^2 coefficient
	_zz = 0.0


	def __init__(self, const=0.0, x=0.0, y=0.0, z=0.0, xy=0.0, 
				 	yz=0.0, zx=0.0, xx=0.0, yy=0.0, zz=0.0):
		"""Initialise a polynomial
		
		By default, the polynomial is the zero polynomial
		"""
		self._const = const
		self._x = x
		self._y = y
		self._z = z
		self._xy = xy
		self._yz = yz
		self._zx = zx
		self._xx = xx
		self._yy = yy
		self._zz = zz

		
	def __str__(self):
		"""Convert a polynomial into a string so it can be printed
		
		The format is constant coefficient + x coefficient * x 
		+ y coefficient * y
		
		"""
		return str(self._const) + " + " + str(self._x) + "x + " \
		+ str(self._y) + "y + " + str(self._z) + "z + " \
		+ str(self._xy) + "xy + " + str(self._yz) + "yz + " \
		+ str(self._zx) + "zx + " + str(self._xx) + "x^2 + " \
		+ str(self._yy) + "y^2" + str(self._zz) + "z^2 + "
	
	def __add__(self, polynomial):
		""" addition operator """
		poly =  QuadraticPolynomial(self._const+polynomial2.get_const(), \
				self._x+polynomial2.get_x(), self._y+polynomial2.get_y(), \
				self._z+polynomial2.get_z(), self._xy+polynomial2.get_xy(), \
				self._yz+polynomial2.get_yz(), self._zx+polynomial2.get_zx(), \
				self._xx+polynomial2.get_xx(), self._yy+polynomial2.get_yy(), \
				self._zz+polynomial2.get_zz())
		return poly

	def __sub__(self, polynomial):
		""" substraction operator """
		poly =  QuadraticPolynomial(self._const-polynomial2.get_const(), \
				self._x-polynomial2.get_x(), self._y-polynomial2.get_y(), \
				self._z-polynomial2.get_z(), self._xy-polynomial2.get_xy(), \
				self._yz-polynomial2.get_yz(), self._zx-polynomial2.get_zx(), \
				self._xx-polynomial2.get_xx(), self._yy-polynomial2.get_yy(), \
				self._zz-polynomial2.get_zz())
		return poly


	def set_const(self, const):
		"""Set constant coefficient"""
		self._const = const
	
	def set_x(self, x):
		"""Set the x coefficient"""
		self._x = x
		
	def set_y(self, y):
		"""Set the y coefficient"""
		self._y = y

	def set_z(self, z):
		"""Set the z coefficient"""
		self._z = z

	def set_xy(self, xy):
		"""Set the xy coefficient"""
		self._xy = xy

	def set_yz(self, yz):
		"""Set the yz coefficient"""
		self._yz = yz

	def set_zx(self, zx):
		"""Set the zx coefficient"""
		self._zx = zx

	def set_xx(self, xx):
		"""Set the xx coefficient"""
		self._xx = xx

	def set_yy(self, yy):
		"""Set the yy coefficient"""
		self._yy = yy

	def set_zz(self, zz):
		"""Set the zz coefficient"""
		self._zz = zz

	def get_const(self):
		"""Get the constant coefficient"""
		return self._const
		
	def get_x(self):
		"""Get the x coefficient"""
		return self._x
	
	def get_y(self):
		"""Get the y coefficient"""
		return self._y

	def get_z(self):
		"""Get the z coefficient"""
		return self._z

	def get_xy(self):
		"""Get the xy coefficient"""
		return self._xy

	def get_yz(self):
		"""Get the yz coefficient"""
		return self._yz

	def get_zx(self):
		"""Get the zx coefficient"""
		return self._zx

	def get_xx(self):
		"""Get the xx coefficient"""
		return self._xx

	def get_yy(self):
		"""Get the yy coefficient"""
		return self._yy

	def get_zz(self):
		"""Get the zz coefficient"""
		return self._zz

	def eval(self, x, y, z): 
		"""evaluate the polynomial at the posn (x, y, z)"""
		return self._const + self._x * x + self._y * y \
				+ self._z * z + self._xy * x * y \
				+ self._yz * y * z + self._zx * z * x \
				+ self._xx * x * x + self._yy * y * y \
				+ self._zz * z * z

		
#	def dx(self): 
#		"""differentiate the polynomial in the x direction"""
#		poly = LinearPolynomial(self._x, 0.0, 0.0)
#		return poly
		
#	def dy(self): 
#		"""differentiate the polynomial in the y direction"""
#		poly = LinearPolynomial(self._y, 0.0, 0.0)
#		return poly
		