#-*- coding: utf-8 -*-
"""
@Author: Fang

@Date:   2022-10-16 16:31:07
@Last Modified time: 2022-12-04 10:29:38

This files contains the adaptive refinement routines for a tetrahedral grid.

Adapted from Stals' Multigrid code.

"""


def is_coarse(refine_type, refine_level, ngh_type, ngh_level):
	""" Check if tetrahedron is coarser than its neighbour """
	
	from grid.Edge import TetraRefineSet

	if ngh_type < TetraRefineSet.base_edge1 or ngh_type > TetraRefineSet.base_edge3:
		return False

	if ngh_level == refine_level
		return (ngh_type < refine_type)

	return (ngh_level < refine_level)

	

def ready_to_refine_triangle(grid, node_id1, node_id2, node_id3):
	""" Check if the tetrahedron with given triangle ready to refine """

	# Input triangle is defined by node id 1 to 3
	# This function checks if the tetrahedron neighbours a coarse tetrahedron

	refine_type = grid.get_refine_type(node_id1, node_id2)
	refine_level = grid.get_refine_level(node_id1, node_id2)

	# Check first neighbouring tetrahedron
	ngh_type = grid.get_refine_type(node_id1, node_id3)
	ngh_level = grid.get_refine_level(node_id1, node_id3)

	if is_coarse(refine_type, refine_level, ngh_type, ngh_level):
		return False

	# Check second neighbouring tetrahedron
	ngh_type = grid.get_refine_type(node_id3, node_id1)
	ngh_level = grid.get_refine_level(node_id3, node_id1)

	if is_coarse(refine_type, refine_level, ngh_type, ngh_level):
		return False

	# Check third neighbouring tetrahedron
	ngh_type = grid.get_refine_type(node_id2, node_id3)
	ngh_level = grid.get_refine_level(node_id2, node_id3)

	if is_coarse(refine_type, refine_level, ngh_type, ngh_level):
		return False

	# Check fourth neighbouring tetrahedron
	ngh_type = grid.get_refine_type(node_id3, node_id2)
	ngh_level = grid.get_refine_level(node_id3, node_id2)

	if is_coarse(refine_type, refine_level, ngh_type, ngh_level):
		return False

	# If there is no coarse neighbouring tetrahedron for this triangle
	return True


def ready_to_refine(grid, node_id1, node_id2):
	""" Check if the tetrahedron with given edge ready to refine """

	# Input triangle is defined by node id 1 and 2
	# This function checks if the tetrahedron neighbours a coarse tetrahedron

	from grid.EdgeTable import endpt_iterator

	for endpt in endpt_iterator(grid, node_id1):

		# If a triangle is found
		if grid.is_edge(endpt, node_id2):

			if not ready_to_refine_triangle(grid, node_id1, node_id2, endpt):
				return False

	# If there is no coarse neighbouring tetrahrdorn for this edge
	return True


def add_adaptive_refinement(grid, base_edges):
	""" Adaptively refine given edges of the grid """

	pass
	







def add_adaptive_local(grid, base_edges):
	""" Adaptively refine part of the grid """


	pass



def add_refinement(grid, base_edges):
	""" Adaptively refine part of the grid """

	# Make a copy of the current grid
	refine_grid = deepcopy(grid)

	no_base_edges = len(base_edges)





def build_base_edges(grid, parameter):
	"""Build a list of base edges"""

	dataset = parameter.get_dataset()

	# Initialise the bese edge list
	base_edges = list()

	# Domains that the refinement occurs
	domain = []
	if len(str(dataset)) < 4:
		domain = [[0.09999999,0.09999999,0.90000001,0.90000001]]

		
	# Loop through each edge
	for node in node_iterator(grid):
		node_id = node.get_node_id()
		for endpt in endpt_iterator(grid, node_id):
			if node_id < endpt:
				end_node = grid.get_node_ref(endpt)

				# If the refinement type is a base type
				if grid.get_refine_type(node_id, endpt) == RefineSet.base_edge or \
					grid.get_refine_type(node_id, endpt) == RefineSet.interface_base_edge:

					new_coord = [(node.get_coord()[0]+end_node.get_coord()[0])/2.0, \
								(node.get_coord()[1]+end_node.get_coord()[1])/2.0]

#					if not check_domain(new_coord, domain):
#						continue
					"""
					if parameter.get_boundary() == BoundaryType.Dirichlet and \
						(dataset == 1 or dataset == 2 or dataset == 3 or dataset == 4):

						# If one node lies inside the required domain
						if not (check_domain(node.get_coord(), domain) or \
							check_domain(end_node.get_coord(), domain)):
							continue

					if dataset == 5 and parameter.get_boundary() == BoundaryType.Neumann:
						# If one node lies inside the required domain
						if not (check_domain(node.get_coord(), domain) or \
							check_domain(end_node.get_coord(), domain)):
							continue

					if parameter.get_boundary() == BoundaryType.Neumann and \
						(dataset == 1 or dataset == 2):
						# If one node lies inside the required domain
						if not (check_domain(node.get_coord(), domain) or \
							check_domain(end_node.get_coord(), domain)):
							continue
					"""
					# Add the edge to the base edge list
					base_edges.append(grid.get_edge(node_id, endpt))

		
	# Return the list of base edges
	return base_edges


def select_refine_edges(refine_grid, base_edges, no_refine):
	""" Select edges with higher error indicators to refine """

	# At least one base edge will be refined
	if no_refine == 0:
		no_refine = 1

	# Initialise a list for error indicators
	error_indicator_list = []

	# Iterate through the base edges
	for i in range(0, len(base_edges)):

		# Get the ids of the endpoints of the edge
		endpt1 = base_edges[i].get_endpt1()
		endpt2 = base_edges[i].get_endpt2()

		# Add error indicator of the edge
		error_indicator = refine_grid.get_error_indicator(endpt1, endpt2)
		error_indicator_list.append(error_indicator)

	# The sorting order	
	order = True    # Descending
#	order = False   # Ascending

	edge_tuple = zip(error_indicator_list, base_edges)
	# Sort the edges by error indicators
	error_indicator_list, refine_edges = \
		zip(*sorted(edge_tuple, key=lambda edge_tuple: edge_tuple[0], reverse=order))

#	for i in range(0, len(refine_edges)):
#		if EIWeight.check_boundary_edge(refine_grid, refine_edges[i]):
#			print("Warning", error_indicator_list[i])

	# Return edges with higher error indicators
	return refine_edges[:no_refine]



def adaptive_refinement_iterative(grid, region, parameter):
	""" Adaptively refine the 2D grid iteratively """
	
	# Refine ratio
	# 0.1 means 10% each iteration
	refine_ratio = 0.1

	# Maximum ratio of edges refined in each sweep
	# 1 Means stops when nodes double
	max_refine_ratio = 1

	# Make a copy of the current grid
	refine_grid = deepcopy(grid)

	# Apply m sweeps of refinement
	for i in range(0, parameter.get_max_no()):
		
		# The number of nodes
		threshold = int(refine_grid.get_no_nodes()*(1+max_refine_ratio))

		# Number of refinement each iteration
		no_refine = int(refine_grid.get_no_nodes()*refine_ratio)

		# Keep refining if not enough new nodes
		while refine_grid.get_no_nodes() < threshold:

			# Find all of the base edges
			base_edges = build_base_edges(refine_grid, parameter)

			# Find all the base edges that will be refined this iteration
			refine_edges = select_refine_edges(refine_grid, base_edges, no_refine)

			if_refine = False

			# Loop over the base edges and split or refine the triangle along that
			# base edge
			for j in range(0, len(refine_edges)):

				edge = refine_edges[j]

				# If the edge needs refinement
				if edge.get_error_indicator() > parameter.get_error_tol():

					# Split the edge
					split_triangle(refine_grid, region, edge, parameter)

					if_refine = True

					# Breaks when number of nodes doubles
					if refine_grid.get_no_nodes() >= threshold:
						break

			# Break the loop if no base edge is up for refinement
			if not if_refine:
				break

	# Return the refined grid
	return refine_grid