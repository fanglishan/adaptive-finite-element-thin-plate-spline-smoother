#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-04-26 10:33:54
@Last Modified time: 2022-10-09 23:37:15

This class keeps record of a linear polynomial in three dimensions.
It currently only offers minimal functionality, additional polynomial
opertations, such as addition and scalar multiplication, could also 
be included.

Adapted from Stals' FEM code.

"""

# Import libraries
from adaptive.tetrahedron.QuadraticPolynomial import QuadraticPolynomial

import numpy


class LinearPolynomial:
	""" A linear polynomial in three dimensions"""
	
	# constant coefficient
	_const = 0.0
	
	# x coefficient
	_x = 0.0
	
	# y coefficient
	_y = 0.0
 
 	# z coefficient
	_z = 0.0  
   
	def __init__(self, const = 0.0, x = 0.0, y = 0.0, z = 0.0):
		"""Initialise a polynomial
		
		By default, the polynomial is the zero polynomial
		"""
		self._const = const
		self._x = x
		self._y = y
		self._z = z

		
	def __str__(self):
		"""Convert a polynomial into a string so it can be printed
		
		The format is constant coefficient + x coefficient * x 
		+ y coefficient * y + z coefficient * z
		
		"""
		return str(self._const) + " + " + str(self._x) + "x + " \
				 + str(self._y) + "y" + str(self._z) + "z"
	
	def __add__(self, polynomial):
		""" addition operator """
		poly = LinearPolynomial(self._const+polynomial.get_const(),
				self._x+polynomial.get_x(), self._y+polynomial.get_y(),
				self._z+polynomial.get_z())
		return poly

	def __sub__(self, polynomial):
		""" substraction operator """
		poly = LinearPolynomial(self._const-polynomial.get_const(),
				self._x-polynomial.get_x(), self._y-polynomial.get_y()
				, self._z-polynomial.get_z())
		return poly

	def __mul__(self, polynomial):
		""" Multiplication operator """
		
		# If the input is a linear polynomial
		if isinstance(polynomial,LinearPolynomial):
			poly = QuadraticPolynomial(self._const*polynomial.get_const(),
					self._const*polynomial.get_x()+self._x*polynomial.get_const(),
					self._const*polynomial.get_y()+self._y*polynomial.get_const(),
					self._x*polynomial.get_y()+self._y*polynomial.get_x(),
					self._x*polynomial.get_x(),self._y*polynomial.get_y())	
		return poly

	def set_const(self, const):
		"""Set constant coefficient"""
		self._const = const
	
	def set_x(self, x):
		"""Set the x coefficient"""
		self._x = x
		
	def set_y(self, y):
		"""Set the y coefficient"""
		self._y = y

	def set_z(self, z):
		"""Set the z coefficient"""
		self._z = z

	def get_const(self):
		"""Get the constant coefficient"""
		return self._const
		
	def get_x(self):
		"""Get the x coefficient"""
		return self._x
	
	def get_y(self):
		"""Get the y coefficient"""
		return self._y
		
	def get_z(self):
		"""Get the z coefficient"""
		return self._z

	def eval(self, x, y): 
		"""evaluate the polynomial at the posn (x, y, z)"""
		return self._const + self._x * x + self._y * y \
										 + self._z * z
		
	def dx(self): 
		"""differentiate the polynomial in the x direction"""
		poly = LinearPolynomial(self._x, 0.0, 0.0, 0.0)
		return poly
		
	def dy(self): 
		"""differentiate the polynomial in the y direction"""
		poly = LinearPolynomial(self._y, 0.0, 0.0, 0.0)
		return poly

	def dz(self): 
		"""differentiate the polynomial in the z direction"""
		poly = LinearPolynomial(self._z, 0.0, 0.0, 0.0)
		return poly