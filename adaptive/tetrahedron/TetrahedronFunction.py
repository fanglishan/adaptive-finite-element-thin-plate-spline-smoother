#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-05-05 23:15:37
@Last Modified time: 2022-10-09 23:37:21

This class contains popular routines that are only used for
2D triangular grids.

"""


# Import libraries
from grid.NodeTable import node_iterator
from grid.EdgeTable import endpt_iterator
from adaptive.triangle.Triangle import triangle_iterator, triangle_iterator_ref

from copy import deepcopy
from numpy import sqrt



def get_mid_coord_2d(node1, node2, node3):
	""" Find mid coordinate of a triangle """

	# Find the coordinates of the midpoint
	dim = 2
	coord = [0.0]*dim
	for i in range(dim):
		coord[i] = (node1.get_coord()[i] + node2.get_coord()[i] + \
					node3.get_coord()[i])/3.0

	return coord


def cal_distance_2d(node1, node2):
	""" Calculate distance in 2D """
	return sqrt((node1.get_coord()[0]-node2.get_coord()[0])**2 + \
			(node1.get_coord()[1]-node2.get_coord()[1])**2)


def cal_distance(node1, node2):
	# Find the coordinates of the midpoint
	
	dim = node1.get_dim()
	
	distance = 0.0
	
	for i in range(dim):
		distance += (node1.get_coord()[i]-node2.get_coord()[i])**2

	return sqrt(distance)


def get_h_2d(node1, node2, node3):
	""" Obtain the longest edge of a triangle """

	h12 = cal_distance_2d(node1, node2)

	h23 = cal_distance_2d(node2, node3)

	h31 = cal_distance_2d(node3, node1)

	return max(h12, h23, h31)


def get_mid_coord(grid, node1, node2):
	""" Calculate the midpoint coordinates of two points """

	# Find the coordinates of the midpoint
	dim = grid.get_dim()
	coord = [0.0]*dim
	for i in range(dim):
		coord[i] = (node1.get_coord()[i]+node2.get_coord()[i])/2.0

	return coord
	

def find_triangle_pairs(grid, endpt1, endpt2):
	""" Find the other two nodes of the triangle pairs """

	# Initialise node 3 and node 4
	node3 = None

	node4 = None

	# Make a list of all of the edges joined to endpt1 (this is used
	# as we only want to find one version of each triangle)
	edgestar = list()
	for endpt in endpt_iterator(grid, endpt1):
		edgestar.append(endpt)

	# An indicator for the end of triangle searching
	end = False

	# Loop over all of the endpoints joined to endpt1
	n = len(edgestar)
	for i in range(0, n):
		
		# If the endpoint is also joined to endpt2, we have found a triangle
		if grid.is_edge(edgestar[i], endpt2):
			
			node3 = deepcopy(grid.get_node(edgestar[i]))

			# See if we can find a second, different, triangle
			
			# Loop over other endpoints joined to endpt1
			for j in range(i+1, n):
				
				# If the new endpoints is also joined to endpt2, we have found
				# two triangle that share the edge between endpt1 and endpt2
				if grid.is_edge(edgestar[j], endpt2):

					# Make a deepcopy of node 4
					node4 = deepcopy(grid.get_node(edgestar[j]))

					# Set the end indicator true as two triangles have been found
					end = True

				# Break if two triangles have been found
				if end:
					break

		# Break if two triangles have been found
		if end:
			break

	return node3, node4


def build_midpoint(grid, node1, node2):
	""" 
	Build a midpoint on the edge between two given nodes 
	This midpoint will not be added to the grid
	"""

	# Make a copy of node 1
	midpoint = deepcopy(node1)

	# Find the coordinates of the midpoint
	dim = grid.get_dim()
	coord = [0.0]*dim
	for i in range(dim):
		coord[i] = (node1.get_coord()[i]+node2.get_coord()[i])/2.0
	
	# Set the cooridnates of the midpoint
	midpoint.set_coord(coord)

	# Set the value of midpoint as average of node 1 and node 2
	midpoint.set_value((node1.get_value()+node2.get_value())/2.0)

	# Set the value of midpoint as average of node 1 and node 2
#	midpoint.set_d_values((node1.get_d_values()+node2.get_d_values())/2.0)

	return midpoint


def build_two_midpoints(grid, node1, node2):
	""" 
	Build two midpoints on the edge between two given nodes 
	This midpoints will not be added to the grid
	"""

	# Make two copies of node 1
	midpoint1 = deepcopy(node1)
	midpoint2 = deepcopy(node1)

	# Find the coordinates of the midpoints
	dim = grid.get_dim()
	coord1 = [0.0]*dim
	coord2 = [0.0]*dim
	for i in range(dim):
		coord1[i] = (node1.get_coord()[i]*2.0+node2.get_coord()[i])/3.0
		coord2[i] = (node1.get_coord()[i]+node2.get_coord()[i]*2.0)/3.0
	
	# Set the cooridnates of the midpoints
	midpoint1.set_coord(coord1)
	midpoint2.set_coord(coord2)

	# Set the value of midpoints as the weighte average 
	# of node 1 and node 2
	midpoint1.set_value((node1.get_value()*2.0+node2.get_value())/3.0)
	midpoint2.set_value((node1.get_value()+node2.get_value()*2.0)/3.0)

	return midpoint1, midpoint2


def build_centre_point(grid, node1, node2, node3):
	""" 
	Build a centre point from three given nodes 
	This centre point will not be added to the grid
	"""

	# Make a copy of node 1
	centre_point = deepcopy(node1)

	# Find the coordinates of the midpoint
	dim = grid.get_dim()
	coord = [0.0]*dim
	for i in range(dim):
		coord[i] = (node1.get_coord()[i] + node2.get_coord()[i] + \
					node3.get_coord()[i])/3.0
	
	# Set the cooridnates of the midpoint
	centre_point.set_coord(coord)

	# Set the value of midpoint as average of node 1, node 2 and node 3
	centre_point.set_value((node1.get_value() + node2.get_value() + \
						node3.get_value())/3.0)

	return centre_point


def calc_tri_pair_area(grid, node1, node2, endpt1, endpt2):
	""" Calculate the area of the tirangle pair """

	from triangle.TriangleIntegrateLinear import calc_area

	# Initialise an area
	area = 0.0

	# Find two other vertices of the triangle pair
	node3, node4 = find_triangle_pairs(grid, endpt1, endpt2)

	# Coordinates of the base edge vertices
	coord1 = node1.get_coord()
	coord2 = node2.get_coord()

	# Area from triangle 1-2-3
	if node3 is not None:
		coord3 = node3.get_coord()
		area += calc_area(coord1, coord2, coord3)

	# Area from triangle 1-2-4
	if node4 is not None:
		coord4 = node4.get_coord()
		area += calc_area(coord1, coord2, coord4)

	# Print warning message if area is too small
	if area <= 0.000000000001:
		print("Warning: invalid area")

	return area


def find_h(grid):
	""" Calculate the maximum and minimum edge length h of the grid """

	# Initialise maximum and minimum h 
	max_h = -10000000000
	min_h = 10000000000

	# Iterate through each edge once
	for node in node_iterator(grid):
		for endpt in endpt_iterator(grid, node.get_node_id()):
			if node.get_node_id() < endpt:

				# Calculate the length of the edge between two nodes
				edge_length = cal_distance(node, grid.get_node_ref(endpt))

				# Check maximum edge length
				if max_h < edge_length:
					max_h = edge_length

				# Check minimum edge length
				if min_h > edge_length:
					min_h = edge_length

	return max_h, min_h


def find_no_tri(grid):
	""" Find the number of triangles in the grid """

	counter = 0

	# Iterate through each triangle once
	for tri in triangle_iterator_ref(grid):
		if tri[0].get_node_id()>tri[1].get_node_id() and \
			tri[1].get_node_id()>tri[2].get_node_id():

			counter += 1

	return counter


def sign(point1, point2, point3):
	""" Check which side point 1 is on for edge 2-3 """

	# Determine the side of point 1
	if (point1[0]-point3[0])*(point2[1]-point3[1]) - \
	   (point2[0]-point3[0])*(point1[1]-point3[1]) < 0:

		return True

	# Return False if on the other side
	else:
		return False


def is_in_triangle(point, coord1, coord2, coord3):
	""" Check if the point in in the triangle"""

	# Determine the side of point for edge 1-2
	b1 = sign(point, coord1, coord2)

	# Determine the side of point for edge 2-3
	b2 = sign(point, coord2, coord3)

	# Determine the side of point for edge 3-1
	b3 = sign(point, coord3, coord1)

	# If the point is on the same side for every edge
	return (b1==b2) and (b2==b3)