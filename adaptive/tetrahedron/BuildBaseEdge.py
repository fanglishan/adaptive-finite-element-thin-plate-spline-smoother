#-*- coding: utf-8 -*-
"""
@Author: Fang

@Date:   2022-10-15 14:58:00
@Last Modified time: 2022-11-27 10:40:35

This file contains a function to select and build a list of 
base edges from tetrahedron that fits the input refine type.
For example, base_edge1, base_edge2, base_edge3
"""


def build_base_edges(grid, refine_type):
	""" Build a list of base edges that fits in given type """

	from grid.NodeTable import node_iterator
	from grid.EdgeTable import endpt_iterator

	# Initialise the bese edge list
	base_edges = list()

	# Iterate through each edge once
	for node in node_iterator(grid):
		node_id = node.get_node_id()
		for endpt in endpt_iterator(grid, node_id):
			if node_id < endpt:
				
				# If the refinement type is the specified base type
				if grid.get_refine_type(node_id, endpt) == refine_type:
					base_edges.append(grid.get_edge(node_id, endpt))

	return base_edges


def build_base_edges_all(grid):
	""" Build a list of base edges """

	from grid.NodeTable import node_iterator
	from grid.EdgeTable import endpt_iterator
	from grid.Edge import TetraRefineSet

	# Initialise the bese edge list
	base_edges = list()

	edge_types = [TetraRefineSet.base_edge1,TetraRefineSet.base_edge2,TetraRefineSet.base_edge3]

	for i in range(0,len(edge_types)):

		# Iterate through each edge once
		for node in node_iterator(grid):
			node_id = node.get_node_id()
			for endpt in endpt_iterator(grid, node_id):
				if node_id < endpt:
					
					# If the refinement type is the specified base type
					if grid.get_refine_type(node_id, endpt) == edge_types[i]:
						base_edges.append(grid.get_edge(node_id, endpt))

	return base_edges