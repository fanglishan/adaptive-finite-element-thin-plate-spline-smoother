# -*- coding: utf-8 -*-
"""
@Author: Fang
@Date:   2022-12-04 16:10:43
@Last Modified by:   Fang
@Last Modified time: 2022-12-04 17:19:57

This function determines whether every tetrahedron in the grid
has a base edge.

"""

def check_base_edge(grid, node_id1, node_id2):
	""" determine whether given edge is of base type """

	from grid.Edge import TetraRefineSet

	refine_type = grid.get_refine_type(node_id1, node_id2)

	if refine_type == TetraRefineSet.base_edge1 or \
		refine_type == TetraRefineSet.base_edge2 or \
		refine_type == TetraRefineSet.base_edge3:
		return True
	else:
		return False

def check_tetra_base(grid):

	from adaptive.tetrahedron.Tetrahedron import tetrahedron_iterator

	# Number of tetrahedrons that do not have a base edge
	count = 0

	for tetra in tetrahedron_iterator(grid):
		node_id1 = tetra[0].get_node_id()
		node_id2 = tetra[1].get_node_id()
		node_id3 = tetra[2].get_node_id()
		node_id4 = tetra[3].get_node_id()
		if node_id1 > node_id2 and node_id2 > node_id3 and node_id3 > node_id4:

			count += 1

			if check_base_edge(grid, node_id1, node_id2):
				continue
			if check_base_edge(grid, node_id1, node_id2):
				continue
			if check_base_edge(grid, node_id1, node_id3):
				continue
			if check_base_edge(grid, node_id1, node_id4):
				continue
			if check_base_edge(grid, node_id2, node_id3):
				continue
			if check_base_edge(grid, node_id2, node_id4):
				continue
			if check_base_edge(grid, node_id3, node_id4):
				continue

			print("Tetrahedron",node_id1, node_id2, node_id3, node_id4,"does not have a base edge")
			count -= 1

	return count