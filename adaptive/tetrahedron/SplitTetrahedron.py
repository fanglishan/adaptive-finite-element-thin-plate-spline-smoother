# -*- coding: utf-8 -*-
"""
@Author: Fang
@Date:   2022-10-03 23:23:16
@Last Modified by:   Fang
@Last Modified time: 2022-12-04 22:08:20

This file contains modules to split a tetrahedron and update related
information like refine types on nodes and edges.

The split tetrahedron routines are implemented based on the C++ code
for multigrid FEM program from Linda Stals.

"""


class NodeSet:
	""" Is the node a full node or a ghost node """
	full_node = 1
	ghost_node = 2



def build_midpoint_global_id(grid, global_id1, global_id2):
	""" Find the global id of the new midpoint """ 

	from grid.GlobalNodeID import GlobalNodeID, mesh_integers_3D, unmesh_integers_3D

	# The node is being added a what level of refinement
	current_level = 0
	
	# Find refine level
	if global_id1.get_level() > global_id2.get_level():
		current_level = global_id1.get_level()+1
	else:
		current_level = global_id2.get_level()+1
		
	# The number must be scaled according to the level of refinement
	scale1 = 2**(current_level-global_id1.get_level()-1)
	scale2 = 2**(current_level-global_id2.get_level()-1)
		
	# Extract the coordinates that were used to find the ids of the endpoints 
	id1x, id1y, id1z = unmesh_integers_3D(global_id1.get_no())
	id2x, id2y, id2z = unmesh_integers_3D(global_id2.get_no())
		
	# Find the coordinates of the midpoint
	idx = id1x*scale1+id2x*scale2
	idy = id1y*scale1+id2y*scale2
	idz = id1z*scale1+id2z*scale2

	# Create a new global id
	id = mesh_integers_3D(idx, idy, idz)
	global_id = GlobalNodeID(id, current_level)

	# Return the global id
	return global_id
	
	
def add_midpoint(grid, parameter, node1, node2, node_type):
	""" Add the midpoint toadd_midpoint the grid """ 
	
	from grid.Edge import DomainSet

	# Get the ids of the two endpoints
	node_id1 = node1.get_node_id()
	node_id2 = node2.get_node_id()

	# Find the coordinates of the midpoint
	dim = grid.get_dim()
	coord = [0.0]*dim
	for i in range(dim):
		coord[i] = (node1.get_coord()[i]+node2.get_coord()[i])/2.0
		
	# Find the global id of the midpoint
	mid_global_id = build_midpoint_global_id(grid, node1.get_global_id(), node2.get_global_id())
											 
	# Determine if the midpoint will sit on the boundary                                         
	location = grid.get_location(node_id1, node_id2)
	bnd_func = grid.get_boundary_function(node_id1, node_id2)

	# If the midpoint is to be added as a full node
	if node_type == NodeSet.full_node:
		"""
		# If the midpoint sits on the boundary
		if location == DomainSet.boundary:
			
			# Add the midpoint as a full slave node
			value = bnd_func(coord)
			mid_node = grid.create_node(mid_global_id, coord, True, value)

			function = parameter.get_function()
			mid_node.set_d_values([function[1](coord),\
				function[2](coord), function[3](coord), function[4](coord)])

			grid.add_node(mid_node)
				
		else:
		"""
		# Add the midpoint as a full node (and initialise the value and
		# load to be the average of the endpoints)
		value = (node1.get_value() + node2.get_value())/2.0
		data_load = (node1.get_data_load() + node2.get_data_load())/2.0

		# Set node location as boundary or interior
		if location == DomainSet.boundary:
			mid_node = grid.create_node(mid_global_id, coord, True, value)
		elif location == DomainSet.interior:
			mid_node = grid.create_node(mid_global_id, coord, False, value)
		else:
			print("SplitTetrahedron: unknown node location")

		mid_node.set_data_load(data_load)

		# Add derivative values if exist
		if len(node1.get_d_values())>0 and len(node2.get_d_values())>0:
			ux_value = (node1.get_d_values()[0]+node2.get_d_values()[0])/2.0
			uy_value = (node1.get_d_values()[1]+node2.get_d_values()[1])/2.0
			uz_value = (node1.get_d_values()[2]+node2.get_d_values()[2])/2.0
			u2_value = (node1.get_d_values()[3]+node2.get_d_values()[3])/2.0
			mid_node.set_d_values([ux_value,uy_value,uz_value,u2_value])

		grid.add_node(mid_node)

	return mid_node



def find_midpoint(grid, node_id1, node_id2):
	""" Find midpoint between two nodes """

	mid_global_id = build_midpoint_global_id(grid, grid.get_global_id(node_id1), grid.get_global_id(node_id2))
	
	if grid.is_in_global(mid_global_id):
		mid_id = grid.get_local_id(mid_global_id)
		return mid_id

	else:
		return None


def get_midpoint(grid, node_id1, node_id2):
	""" Get midpoint between two nodes """

	mid_id = find_midpoint(grid, node_id1, node_id2)
	
	if mid_id is None:
		print("SplitTetrahedron: midpoint is not found!")
		from sys import exit
		exit(-1)
	else:
		return mid_id



def split_edge(grid, parameter, node1, node2, node_type):
	""" Split the edge between node 1 and node 2 """

	from grid.Edge import DomainSet, TetraRefineSet

	# Get the ids of the endpoints
	node_id1 = node1.get_node_id()
	node_id2 = node2.get_node_id()	

	# Find the refinement level 
	refine_level = grid.get_refine_level(node_id1, node_id2)

	# Find the edge location 
	location = grid.get_location(node_id1, node_id2)
	boundary_function = grid.get_boundary_function(node_id1, node_id2)

	# Add the midpoint to the grid
	mid_node = add_midpoint(grid, parameter, node1, node2, node_type) 
	mid_id = mid_node.get_node_id()

	# Remove the old edges
#	grid.delete_edge(node_id1, node_id2)
#	grid.delete_edge(node_id2, node_id1)

	# Add a new edge between the midpoint and node1
	grid.add_edge(mid_id, node_id1)
	grid.set_location(mid_id, node_id1, location)
	grid.set_boundary_function(mid_id, node_id1, boundary_function)
	grid.set_refine_type(mid_id, node_id1, TetraRefineSet.not_base_edge)
	grid.set_refine_level(mid_id, node_id1, refine_level)

	# Add a new edge between the node1 and midpoint
	grid.add_edge(node_id1, mid_id)
	grid.set_location(node_id1, mid_id, location)
	grid.set_boundary_function(node_id1, mid_id, boundary_function)
	grid.set_refine_type(node_id1, mid_id, TetraRefineSet.not_base_edge) 
	grid.set_refine_level(node_id1, mid_id, refine_level) 

	# Add a new edge between the midpoint and node2
	grid.add_edge(mid_id, node_id2)
	grid.set_location(mid_id, node_id2, location)
	grid.set_boundary_function(mid_id, node_id2, boundary_function)
	grid.set_refine_type(mid_id, node_id2, TetraRefineSet.not_base_edge)
	grid.set_refine_level(mid_id, node_id2, refine_level)

	# Add a new edge between the node2 and midpoint
	grid.add_edge(node_id2, mid_id)
	grid.set_location(node_id2, mid_id, location)
	grid.set_boundary_function(node_id2, mid_id, boundary_function)
	grid.set_refine_type(node_id2, mid_id, TetraRefineSet.not_base_edge) 
	grid.set_refine_level(node_id2, mid_id, refine_level)

	return mid_id


def is_boundary_face(grid, node_id1, node_id2, node_id3):
	""" Check if a given triangle sits on boundaries """
	
	from grid.Edge import DomainSet
	from grid.EdgeTable import endpt_iterator

	# if none of the edges are boundary edges, it can't be a boundary face
	if grid.get_location(node_id1, node_id2) != DomainSet.boundary or \
	   grid.get_location(node_id1, node_id3) != DomainSet.boundary or \
	   grid.get_location(node_id2, node_id3) != DomainSet.boundary:

	   return False

	tetra_found = False
	end_id = None

	# Triangle may not sit on bundaries even if all three edges are on boundaries
	# e.g. corner of cubic unit. However, if triangle sits between two tetrahedron 
	# it must be in the interior, here we try to find a tetrahedron
	for endpt in endpt_iterator(grid, node_id1):

		if endpt != node_id2 and endpt != node_id3:
			if grid.is_edge(endpt, node_id2) and grid.is_edge(endpt, node_id3):
				tetra_found = True
				end_id = endpt
				break

	# If not even one tetrahedron is found
	if not tetra_found:
		return False

	# Try to find a second tetrahedron
	tetra_found = False
#	start = False

	for endpt in endpt_iterator(grid, node_id1):

		# Skip searched end points
#		if endpt == end_id:
#			start = True
#			continue

#		if start:
		if endpt != end_id:
			if endpt != node_id2 and endpt != node_id3:
				if grid.is_edge(endpt, node_id2) and grid.is_edge(endpt, node_id3):
					tetra_found = True
					break

	# If second tetrahedron is not found, the triangle sits on boundary
	# If it is found, return False
	return not tetra_found



def split_triangle(grid, node_id1, node_id2, node_id3, mid_id):
	""" Split one of the faces of a tetrahedron """

	from grid.Edge import DomainSet, TetraRefineSet

	print("Split triangle:", node_id1, node_id2, node_id3)

	refine_level = grid.get_refine_level(node_id1, mid_id)

	bnd_func = grid.get_boundary_function(node_id1, node_id3)

	# If triangle has been refined
	# Set edge sitting between tetrahedon as interior edge
	if grid.is_edge(mid_id, node_id3):
		grid.set_location(mid_id, node_id3, DomainSet.interior)
		grid.set_location(node_id3, mid_id, DomainSet.interior)
		return


	# If the triangle sits on a boundary face, set edge locations
	if is_boundary_face(grid, node_id1, node_id2, node_id3):
		grid.add_edge(mid_id, node_id3)
		grid.add_edge(node_id3, mid_id)
		grid.set_location(mid_id, node_id3, DomainSet.boundary)
		grid.set_location(node_id3, mid_id, DomainSet.boundary)
	else:
		grid.add_edge(mid_id, node_id3)
		grid.add_edge(node_id3, mid_id)
		grid.set_location(mid_id, node_id3, DomainSet.interior)
		grid.set_location(node_id3, mid_id, DomainSet.interior)

	# Set refine type
	grid.set_refine_type(mid_id, node_id3, TetraRefineSet.not_base_edge)
	grid.set_refine_type(node_id3, mid_id, TetraRefineSet.orient)

	# Set boundary function
	grid.set_boundary_function(mid_id, node_id3, bnd_func)
	grid.set_boundary_function(node_id3, mid_id, bnd_func)

	# Set refinement level
	grid.set_refine_level(mid_id, node_id3, refine_level)
	grid.set_refine_level(node_id3, mid_id, refine_level)

	# Add connections
	grid.add_connection(mid_id, node_id3)
	grid.add_connection(node_id3, mid_id)



def add_tetrahedron1(grid, node_id1, node_id2, node_id3, node_id4, mid_id):
	""" bisect a given tetrahedron for base 1 edge """
	
	from grid.Edge import TetraRefineSet

	print("Add tetrahedron 1:", node_id1, node_id2, node_id3, node_id4)

	refine_level = grid.get_refine_level(node_id1, mid_id)

	# Split the two faces which share the base edge
	split_triangle(grid, node_id1, node_id2, node_id4, mid_id)
	split_triangle(grid, node_id1, node_id2, node_id3, mid_id)

	# update the refine type for the surrounding edges
	if grid.get_refine_type(node_id1, node_id3) != TetraRefineSet.base_edge2:
		grid.set_refine_type(node_id1, node_id3, TetraRefineSet.base_edge2)
		grid.set_refine_level(node_id1, node_id3, refine_level+1)
		print(node_id1, node_id3)

	if grid.get_refine_type(node_id4, node_id2) != TetraRefineSet.base_edge2:
		grid.set_refine_type(node_id4, node_id2, TetraRefineSet.base_edge2)
		grid.set_refine_level(node_id4, node_id2, refine_level+1)
		print(node_id4, node_id2)


def add_tetrahedron2(grid, node_id1, node_id2, node_id3, node_id4, mid_id):
	""" bisect a given tetrahedron for base 2 edge """

	from grid.Edge import DomainSet, TetraRefineSet

	print("Add tetrahedron 2:", node_id1, node_id2, node_id3, node_id4)

	refine_level = grid.get_refine_level(node_id1, mid_id)

	# Split the two faces which share the base edge
	split_triangle(grid, node_id1, node_id2, node_id4, mid_id)
	split_triangle(grid, node_id1, node_id2, node_id3, mid_id)

	# update the refine type for the surrounding edges
	if grid.get_refine_type(node_id1, node_id3) != TetraRefineSet.base_edge3:
		grid.set_refine_type(node_id1, node_id3, TetraRefineSet.base_edge3)
		grid.set_refine_level(node_id1, node_id3, refine_level+1)

	if grid.get_refine_type(node_id3, node_id2) != TetraRefineSet.base_edge3:
		grid.set_refine_type(node_id3, node_id2, TetraRefineSet.base_edge3)
		grid.set_refine_level(node_id3, node_id2, refine_level+1)



def add_tetrahedron3(grid, node_id1, node_id2, node_id3, node_id4, mid_id):
	""" bisect a given tetrahedron for base 3 edge """

	from grid.Edge import DomainSet, TetraRefineSet

	print("Add tetrahedron 3:", node_id1, node_id2, node_id3, node_id4)

	refine_level = grid.get_refine_level(node_id1, mid_id)

	# Split the two faces which share the base edge
	split_triangle(grid, node_id1, node_id2, node_id4, mid_id)
	split_triangle(grid, node_id1, node_id2, node_id3, mid_id)

	# update the refine type for the surrounding edges
	if grid.get_refine_type(node_id1, node_id3) != TetraRefineSet.base_edge1:
		grid.set_refine_type(node_id1, node_id3, TetraRefineSet.base_edge1)
		grid.set_refine_level(node_id1, node_id3, refine_level+1)

	if grid.get_refine_type(node_id2, node_id3) != TetraRefineSet.base_edge1:
		grid.set_refine_type(node_id2, node_id3, TetraRefineSet.base_edge1)
		grid.set_refine_level(node_id2, node_id3, refine_level+1)



def check_edge(edgestar, node_id):
	""" Check if a node is in an edgestar """

	for i in range(0, len(edgestar)):
		if edgestar[i] == node_id:
			return True

	return False


def add_all_tetrahedron1(grid, edgestar1, edgestar2, node_id1, node_id2, mid_id):
	""" bisect all tetrahedron with node 1 and 2 as base 1 edge """
	
	from grid.Edge import TetraRefineSet

#	print("Add tetrahedron 1:", node_id1, node_id2)

	node_id3 = None
	node_id4 = None

	found = False

	# find all of the tetrahedron with id1, id2 as one of its base edges
	for i in range(0, len(edgestar1)):
		node_id3 = edgestar1[i]
		grid.set_error_indicator(node_id1, node_id3, -1.0)
		grid.set_error_indicator(node_id3, node_id1, -1.0)
#		print("endpt:", grid.get_coord(node_id3))
		if check_edge(edgestar2, node_id3):
			found = False
			grid.set_error_indicator(node_id2, node_id3, -1.0)
			grid.set_error_indicator(node_id3, node_id2, -1.0)			

			###### Check if j needs to +1 ######

			for j in range(i, len(edgestar1)):
				node_id4 = edgestar1[j]

				if grid.is_edge(node_id3, node_id4) and grid.is_edge(node_id2, node_id4):
					found = True

#					print("found:", grid.get_coord(node_id3), grid.get_coord(node_id4))

					grid.set_error_indicator(node_id3, node_id4, -1.0)
					grid.set_error_indicator(node_id4, node_id3, -1.0)	

					if grid.get_refine_type(node_id3, node_id4) == TetraRefineSet.orient:
						add_tetrahedron1(grid, node_id1, node_id2, node_id3, node_id4, mid_id)	
					elif grid.get_refine_type(node_id4, node_id3) == TetraRefineSet.orient:
						add_tetrahedron1(grid, node_id1, node_id2, node_id4, node_id3, mid_id)
					else:
						print("SplitTetrahedron: no orient found for base 1")
#						from sys import exit
#						exit(-1)
			
			if not found and not grid.is_edge(mid_id, node_id3):
				split_triangle(grid, node_id1, node_id2, node_id3, mid_id)



def add_all_tetrahedron2(grid, edgestar1, edgestar2, node_id1, node_id2, mid_id):
	""" bisect all tetrahedron with node 1 and 2 as base 2 edge """

	from grid.Edge import TetraRefineSet

#	print("Add tetrahedron 2:", node_id1, node_id2)

	node_id3 = None
	node_id4 = None

	found = False

	# find all of the tetrahedron with id1, id2 as one of its base edges
	for i in range(0, len(edgestar1)):
		node_id3 = edgestar1[i]
		grid.set_error_indicator(node_id1, node_id3, -1.0)
		grid.set_error_indicator(node_id3, node_id1, -1.0)

		if check_edge(edgestar2, node_id3):
			found = False
			grid.set_error_indicator(node_id2, node_id3, -1.0)
			grid.set_error_indicator(node_id3, node_id2, -1.0)			

			for j in range(i, len(edgestar1)):
				node_id4 = edgestar1[j]

				if grid.is_edge(node_id3, node_id4) and grid.is_edge(node_id2, node_id4):
					found = True

					grid.set_error_indicator(node_id3, node_id4, -1.0)
					grid.set_error_indicator(node_id4, node_id3, -1.0)	

					if grid.get_refine_type(node_id3, node_id4) == TetraRefineSet.orient or \
					   grid.get_refine_type(node_id3, node_id4) == TetraRefineSet.base_edge1:
						add_tetrahedron2(grid, node_id1, node_id2, node_id3, node_id4, mid_id)

					elif grid.get_refine_type(node_id4, node_id3) == TetraRefineSet.orient or \
						 grid.get_refine_type(node_id4, node_id3) == TetraRefineSet.base_edge1:
						add_tetrahedron2(grid, node_id1, node_id2, node_id4, node_id3, mid_id)
					else:
						print("SplitTetrahedron: no orient found for base 1")
#						from sys import exit
#						exit(-1)

			if not found and not grid.is_edge(mid_id, node_id3):
				split_triangle(grid, node_id1, node_id2, node_id3, mid_id)



def add_all_tetrahedron3(grid, edgestar1, edgestar2, node_id1, node_id2, mid_id):
	""" bisect all tetrahedron with node 1 and 2 as base 3 edge """

	from grid.Edge import TetraRefineSet

#	print("Add tetrahedron 3:", node_id1, node_id2)

	node_id3 = None
	node_id4 = None

	found = False

	# find all of the tetrahedron with id1, id2 as one of its base edges
	for i in range(0, len(edgestar1)):
		node_id3 = edgestar1[i]
		grid.set_error_indicator(node_id1, node_id3, -1.0)
		grid.set_error_indicator(node_id3, node_id1, -1.0)

		if check_edge(edgestar2, node_id3):
			found = False
			grid.set_error_indicator(node_id2, node_id3, -1.0)
			grid.set_error_indicator(node_id3, node_id2, -1.0)			

			for j in range(i, len(edgestar1)):
				node_id4 = edgestar1[j]

				if grid.is_edge(node_id3, node_id4) and grid.is_edge(node_id2, node_id4):
					found = True

					grid.set_error_indicator(node_id3, node_id4, -1.0)
					grid.set_error_indicator(node_id4, node_id3, -1.0)	

					if grid.get_refine_type(node_id3, node_id4) == TetraRefineSet.orient:
						add_tetrahedron3(grid, node_id1, node_id2, node_id3, node_id4, mid_id)	
					elif grid.get_refine_type(node_id4, node_id3) == TetraRefineSet.orient:
						add_tetrahedron3(grid, node_id1, node_id2, node_id4, node_id3, mid_id)
					else:
						print("SplitTetrahedron:no orient found for base 1")
#						from sys import exit
#						exit(-1)

			if not found and not grid.is_edge(mid_id, node_id3):
				split_triangle(grid, node_id1, node_id2, node_id3, mid_id)





def obtain_edgestar(grid, node_id):
	""" Obtain a list of node ids linked to given node """

	from grid.EdgeTable import endpt_iterator

	edgestar = list()
	for endpt in endpt_iterator(grid, node_id):
		edgestar.append(endpt)

	return edgestar


def add_tetrahedron(grid, parameter, node1, node2):
	""" bisect all tetrahedrons which have node 1 and node 2 as base edge """

	from grid.Edge import TetraRefineSet

	# Get the ids of the endpoints
	node_id1 = node1.get_node_id()
	node_id2 = node2.get_node_id()

	node_type = NodeSet.full_node

	refine_type = grid.get_refine_type(node_id1, node_id2)
#	print("refine type:", refine_type)

	edgestar1 = obtain_edgestar(grid, node_id1)
	edgestar2 = obtain_edgestar(grid, node_id2)

	mid_id = split_edge(grid, parameter, node1, node2, node_type)

	print("Split:", node_id1, node_id2)
	print("new node:", mid_id)

	# Remove the old connections
	grid.delete_connection(node_id1, node_id2)
	grid.delete_connection(node_id2, node_id1)

	# Add connections between the midpoint and two vertices
	grid.add_connection(mid_id, mid_id)
	grid.add_connection(mid_id, node_id1)
	grid.add_connection(mid_id, node_id2)
	grid.add_connection(node_id1, mid_id)
	grid.add_connection(node_id2, mid_id)

	# find all of the tetrahedron with id1, id2 as one of its base edges
	if refine_type == TetraRefineSet.base_edge1:
		add_all_tetrahedron1(grid, edgestar1, edgestar2, node_id1, node_id2, mid_id)
	elif refine_type == TetraRefineSet.base_edge2:
		add_all_tetrahedron2(grid, edgestar1, edgestar2, node_id1, node_id2, mid_id)
	elif refine_type == TetraRefineSet.base_edge3:
		add_all_tetrahedron3(grid, edgestar1, edgestar2, node_id1, node_id2, mid_id)
	else:
		print("Error: unkonwn refine type for tetrahedron")
		from sys import exit
		exit(-1)

	# remove the old edge
	grid.delete_edge(node_id1, node_id2)
	grid.delete_edge(node_id2, node_id1)

	return mid_id