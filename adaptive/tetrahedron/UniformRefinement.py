#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-04-26 14:22:02
@Last Modified time: 2022-12-05 22:48:33

This files contains the refinement routines for a tetrahedral grid. The
main routine of interest is uniform_refinement, the rest are helper
routines.

Adapted from Stals' FEM code.

"""

# Import libraries
from grid.NodeTable import node_iterator
from grid.EdgeTable import endpt_iterator
from grid.Edge import DomainSet, TetraRefineSet
	
from copy import deepcopy


class NodeSet:
	""" Is the node a full node or a ghost node """
	full_node = 1
	ghost_node = 2



def uniform_refinement(grid, parameter):
	""" Uniform refinement before adaptive refinement """
	
	from adaptive.tetrahedron.BuildBaseEdge import build_base_edges_all, build_base_edges
	from adaptive.tetrahedron.SplitTetrahedron import add_tetrahedron

	# Number of uniform refinements
	m = parameter.get_refine()[1]
	
	# Make a copy of the current grid
	refine_grid = deepcopy(grid)
	
#	grid = orientate_grid(refine_grid)

	# Apply m sweeps of refinement
	for i in range(m):
		
		# Find all of the base edges in the order of base 1, base 2 and base 3
		base_edges = build_base_edges_all(refine_grid)
	
		# Loop over the base edges and split or refine the triangle along base edge
		for edge in base_edges:
			node1 = grid.get_node(edge.get_endpt1())
			node2 = grid.get_node(edge.get_endpt2())
			
#			print(node1.get_coord(), node2.get_coord())

			if grid.is_edge(edge.get_endpt1(), edge.get_endpt2()):

				mid_id = add_tetrahedron(refine_grid, parameter, node1, node2)

#			import plot.FemPlot3D as femPlot3D
#			femPlot3D.plot_fem_grid_3D_line(refine_grid)

#			from sys import exit
#			exit()
			
	# Return the refined grid
	return refine_grid
