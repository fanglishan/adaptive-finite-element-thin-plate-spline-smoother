#-*- coding: utf-8 -*-
"""
@Author: Fang

@Date:   2022-10-16 16:30:35
@Last Modified time: 2022-12-05 22:58:28

This file contains modules to orientate finite element grids. It will modify some edge 
types to orient, which helps to determine the order of refinement during the iterative
adaptive refinement process.

"""


def manual_orientate(grid):

	from grid.NodeTable import node_iterator
	from grid.EdgeTable import endpt_iterator
	from grid.Edge import TetraRefineSet

	orient = TetraRefineSet.orient

	orient_edges = ["9", "10", "11", "12", "13", "14"]

	for node in node_iterator(grid):
		node_id = node.get_node_id()
		for endpt in endpt_iterator(grid, node.get_node_id()):
			if node_id < endpt:
				if str(node_id) == "8":
					found = False
					for item in orient_edges:
						if str(endpt) == item:
							found = True
					if found:
						print(node_id, endpt)
						grid.set_refine_type(node_id, endpt, orient)
						grid.set_refine_type(endpt, node_id, orient)



def orientate_edge(grid, node, refine_type):

	from grid.Edge import TetraRefineSet
	from grid.EdgeTable import endpt_iterator

	node_id = node.get_node_id()

	if grid.get_orient_load(node_id) > 1.5:
		grid.set_orient_load(node_id, 3.0)

		for endpt in endpt_iterator(grid, node_id):

			if grid.get_refine_type(node_id, endpt) == refine_type:
				grid.set_refine_type(node_id, endpt, TetraRefineSet.not_base_edge)

				if grid.get_orient_load(endpt) < 0:
					grid.set_orient_load(endpt, 1.0)
					endpt_node = grid.get_node(endpt)
					grid = orientate_edge(grid, endpt_node, refine_type)
	
	else:
		grid.set_orient_load(node_id, 3.0)

		for endpt in endpt_iterator(grid, node_id):

			if grid.get_refine_type(node_id, endpt) == refine_type:
				grid.set_refine_type(endpt, node_id, TetraRefineSet.not_base_edge)

				if grid.get_orient_load(endpt) < 0:
					grid.set_orient_load(endpt, 2.0)
					endpt_node = grid.get_node(endpt)
					grid = orientate_edge(grid, endpt_node, refine_type)

	return grid



def orientate_tetrahedron(grid, node1, node2):

	from grid.Edge import TetraRefineSet
	from grid.EdgeTable import endpt_iterator

	node_id1 = node1.get_node_id()
	node_id2 = node2.get_node_id()

	grid.set_refine_type(node_id1, node_id2, TetraRefineSet.base_edge1)
	grid.set_refine_type(node_id2, node_id1, TetraRefineSet.base_edge1)


	for endpt1 in endpt_iterator(grid, node_id1):

		if grid.is_edge(node_id2, endpt1):

#			reach = False

			for endpt2 in endpt_iterator(grid, node_id1):

#				if endpt1 == endpt2:
#					reach = True
#					continue

#				if reach:
				if endpt1 != endpt2:
					if grid.is_edge(node_id2, endpt2) and \
					   grid.is_edge(endpt1, endpt2):

						grid.set_refine_type(endpt1, endpt2, TetraRefineSet.orient)
						grid.set_refine_type(endpt2, endpt1, TetraRefineSet.orient)

						start_edge = grid.get_edge(endpt1, endpt2)

	return start_edge


def orientate_grid(grid):
	""" Orientate tetrahedral grid before adaptive refinement """

	from grid.Edge import TetraRefineSet
	from grid.NodeTable import node_iterator
	from adaptive.tetrahedron.BuildBaseEdge import build_base_edges_all, build_base_edges

	base_edges = build_base_edges_all(grid)


	# Iterate trhough each base edge to orientate
	for i in range(0, len(base_edges)):
		node1 = grid.get_node(base_edges[i].get_endpt1())
		node2 = grid.get_node(base_edges[i].get_endpt2())

		start_edge = orientate_tetrahedron(grid, node1, node2)


	# Orientate first edge
	for node in node_iterator(grid):
		grid.set_orient_load(node.get_node_id(), -1.0)

	edge = base_edges[0]
	endpt1 = edge.get_endpt1()
	node1 = grid.get_node(endpt1)

	grid.set_orient_load(endpt1, 1.0)
	grid = orientate_edge(grid, node1, TetraRefineSet.base_edge1)


	# Orientate last edge
	for node in node_iterator(grid):
		grid.set_orient_load(node.get_node_id(), -1.0)

	edge = base_edges[-1]
	endpt1 = edge.get_endpt1()
	node1 = grid.get_node(endpt1)

	grid.set_orient_load(endpt1, 1.0)
	grid = orientate_edge(grid, node1, TetraRefineSet.orient)


	return grid