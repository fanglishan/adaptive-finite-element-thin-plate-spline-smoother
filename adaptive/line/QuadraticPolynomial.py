#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-05-07 21:44:02
@Last Modified time: 2022-03-29 11:20:59

This class contains the information in a quadratic polynomial with basic
functions to access and update them.

"""

# Import libraries
#from interval.LinearPolynomial import LinearPolynomial


class QuadraticPolynomial:
	""" A quadratic polynomial in one dimension """
	
	# constant coefficient
	_const = 0.0
	
	# x coefficient
	_x = 0.0

	# xx coefficient
	_xx = 0.0  
   
	def __init__(self, const = 0.0, x = 0.0, xx = 0.0):
		""" Initialise a polynomial """
		self._const = const
		self._x = x
		self._xx = xx

	def __str__(self):
		"""Convert a polynomial into a string so it can be printed
		
		The format is constant coefficient + x coefficient * x 
		+  xx coefficient * x^2 
		"""
		return str(self._const) + " + " + str(self._x) + "x" + " + " + str(self._xx) + "x^2"
	
	def __add__(self, polynomial):
		""" addition operator """
		poly = QuadraticPolynomial(self._const+polynomial.get_const(),
			self._x+polynomial.get_x(), self._xx+polynomial.get_xx())
		return poly

	def __sub__(self, polynomial):
		""" substraction operator """
		poly = QuadraticPolynomial(self._const-polynomial.get_const(),
			self._x-polynomial.get_x(),self._xx-polynomial.get_xx())
		return poly

	def set_const(self, const):
		""" Set constant coefficient """
		self._const = const
	
	def set_x(self, x):
		""" Set the x coefficient """
		self._x = x

	def set_xx(self, xx):
		""" Set the xx coefficient """
		self._xx = xx

	def get_const(self):
		""" Get the constant coefficient """
		return self._const
		
	def get_x(self):
		""" Get the x coefficient """
		return self._x

	def get_xx(self):
		""" Get the xx coefficient """
		return self._xx

	def eval(self, x): 
		""" Evaluate the polynomial at the posn (x) """
		return self._const + self._x * x + self._xx * x*x
		
#	def dx(self): 
#		""" Differentiate the polynomial in the x direction """
#		poly = LinearPolynomial(self._x, self._xx)
#		return poly