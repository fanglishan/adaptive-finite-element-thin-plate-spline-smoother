#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-05-19 10:17:03
@Last Modified time: 2022-10-09 23:35:42

This class contains routines to calculate the interpolated
value of the 1D grid on the given coordinate.

"""

# Import libraries
from adaptive.line.SetPolynomial import get_solution_polynomial_linear_1D
from grid.NodeTable import node_iterator
from grid.EdgeTable import endpt_iterator


def cal_value_1d(node1, node2, coord):
	""" Calculate grid value at the coord """

	poly = get_solution_polynomial_linear_1D(node1, node2, \
			node1.get_value(), node2.get_value())

	return poly.eval(coord[0])


def cal_smoother_1d(grid, coord):
	""" Calculate an interpolated value from a 1D grid """

	# Check input grid and coordinate
	if grid.get_dim() != 1:
		print("Warning: invalid grid dimension")

	if len(coord) != 1:
		print("Warning: invalid coordinate dimension")

	# Iterate through each edge once
	for node in node_iterator(grid):
		for endpt in endpt_iterator(grid, node.get_node_id()):   
			if node.get_node_id() < endpt:

				# Coordinates of two nodes
				coord1 = node.get_coord()
				coord2 = grid.get_coord(endpt)

				# Check if the point is in the interval
				if (coord[0] >= coord1[0] and coord[0] <= coord2[0]) or \
					(coord[0] >= coord2[0] and coord[0] <= coord1[0]):

					return cal_value_1d(node, grid.get_node(endpt), coord)

	print("Warning: interval not found")