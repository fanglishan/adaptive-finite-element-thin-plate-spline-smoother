#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-05-08 21:39:47
@Last Modified time: 2022-10-09 23:35:28

This class contains routines to build a 1D local domain.

"""

# Import libraries
from grid.Grid import Grid
from grid.NodeID import NodeID
from grid.NodeTable import node_iterator
from grid.EdgeTable import endpt_iterator
from build.BuildLine import add_edge, RefineSet, DomainSet
from adaptive.line.UniformRefinement import uniform_refinement
from data.DataThinning import determine_data_ratio_1D, count_data_local_grid_1D, count_data_local_grid_1D
from parameter.Definition import IndicatorType
import plot.TpsfemPlot1D as tpsfemPlot1D

from copy import deepcopy



def initialise_connections(grid):
	""" Initialise connections for 1D linear basis """

	for node in node_iterator(grid):
		node_id = node.get_node_id()
		node.set_data_load(0.0)
		if not grid.is_connection(node_id, node_id):
			grid.add_connection(node_id, node_id)
		grid.set_matrix_A_value(node_id, node_id, 0.0)
		grid.set_matrix_L_value(node_id, node_id, 0.0)
		grid.set_matrix_G_value(node_id, node_id, [0.0])
		for endpt1 in endpt_iterator(grid, node.get_node_id()):
			if not grid.is_connection(node_id, endpt1):
				grid.add_connection(node_id, endpt1)
			grid.set_matrix_A_value(node_id, endpt1, 0.0)
			grid.set_matrix_L_value(node_id, endpt1, 0.0)
			grid.set_matrix_G_value(node_id, endpt1, [0.0])


def create_mid_node(grid, node1, node2):
	""" Create a midpoint for node 1 and node 2 """

	# Initialise the node
	node =  deepcopy(node1)

	# Initialise a node id
	node_id = NodeID()

	# Set node id
	node_id.set_no(grid._counter)
	grid._counter = grid._counter+1
	node.set_node_id(node_id)

	# Find the coordinates of the midpoint
	dim = grid.get_dim()
	coord = [0.0]*dim
	for i in range(dim):
		coord[i] = (node1.get_coord()[i]+node2.get_coord()[i])/2.0
	
	# Set the cooridnates of the midpoint
	node.set_coord(coord)

	# Set the value of midpoint as average of node 1 and node 2
	node.set_value((node1.get_value()+node2.get_value())/2.0)
	g1 = (node1.get_d_values()[0]+node2.get_d_values()[0])/2.0
	w = (node1.get_d_values()[1]+node2.get_d_values()[1])/2.0
	node.set_d_values([g1, w])

	return node


def build_local_grid_1D(grid, region, node1, node2, parameter, \
		no_layers, refine_choice, uni_refine, count_data=False):
	""" Build a local 1D grid based on refinement of the edge """

	# Definitions
	base = RefineSet.base_edge
	intp = DomainSet.interior

	# Initialise a new 1D local grid
	local_grid = Grid()
	local_grid.set_dim(1)

	# Reset node number counter of the grid
	local_grid.reset_counter(-1000)

	if refine_choice == 1 or refine_choice == 2:
		# Build a mid point for the edge
		mid_node = create_mid_node(local_grid, node1, node2)

		# Mid midpoint to the local grid
		local_grid.add_node(mid_node)
		local_grid.set_slave(mid_node.get_node_id(), False)

	# Add two nodes near mid node
	local_grid.add_node(node1)
	local_grid.add_node(node2)

	# Create a list for inner nodes
	inner_nodes = [node1, node2]

	# Iterate through inner nodes
	for node in inner_nodes:

		# If local grid is only a triangle pair
		if no_layers == 1:
		
			# Set the triangle nodes as exterior
			local_grid.set_slave(node.get_node_id(), True)

		# If the local grid contains multiple layers
		else:

			# Set the triangle nodes as its slave value
			local_grid.set_slave(node.get_node_id(), node.get_slave())

			# Print error message
			if grid.get_slave(node.get_node_id()) != node.get_slave():
				print("Warning: Local grid")

	# Refine the given edge if required
	if refine_choice == 1 or refine_choice == 2:
		# Add edges to the mid node
		add_edge(local_grid, node1.get_node_id(), mid_node.get_node_id(), base, intp)
		add_edge(local_grid, node2.get_node_id(), mid_node.get_node_id(), base, intp)
	else:
		add_edge(local_grid, node1.get_node_id(), node2.get_node_id(), base, intp)

	# Iterate and add nodes in each layer
	for i in range(1, no_layers):

		# Initialise a list for outer nodes
		outer_nodes = []

		# Iterate through inner nodes to check endpoints
		for node in inner_nodes:
			for endpt in endpt_iterator(grid, node.get_node_id()):

				# Add if it has not been added to the local grid
				if not local_grid.is_in(endpt):
					endpt_node = grid.get_node(endpt)
					local_grid.add_node(endpt_node)
					outer_nodes.append(endpt_node)

					# If it is not the outer layer
					if i != no_layers - 1:
	
						# Set the node as its slave value
						local_grid.set_slave(endpt, grid.get_slave(endpt))

					else:

						# Otherwise set it as slave nodes
						local_grid.set_slave(endpt, True)

		# Iterate through inner nodes if no outer node is found
		if len(outer_nodes) < 0:
			for node in inner_nodes:
				if not local_grid.get_slave(node.get_node_id()):
					print("Warning: SolveRefineTraingle")

			# Break the loop
			break

		# Iterate through the outer nodes
		for i in range(0, len(outer_nodes)):

			# Obtain node id
			node_id1 = outer_nodes[i].get_node_id()

			# Iterate through the rest of outer nodes
			for j in range(i+1, len(outer_nodes)):

				# if edge exists in the original grid
				if grid.is_edge(node_id1, outer_nodes[j].get_node_id()):

					# Add the edge into the local grid
					add_edge(local_grid, node_id1, outer_nodes[j].get_node_id(), base, intp)

		# Iterate through the outer nodes
		for i in range(0, len(outer_nodes)):

			# Obtain node id
			node_id = outer_nodes[i].get_node_id()

			# Iterate through the inner nodes
			for j in range(0, len(inner_nodes)):

				# if edge exists in the original grid
				if grid.is_edge(node_id, inner_nodes[j].get_node_id()):

					# Add the edge into the local grid
					add_edge(local_grid, node_id, inner_nodes[j].get_node_id(), base, intp)
		
		# Outer nodes become inner nodes for the next iteration
		inner_nodes = outer_nodes


	# Determine the data ratio for the local grid
	# The ratio will depend on the approximate number 
	# of data inside the local grid
	if parameter.get_indicator() == IndicatorType.auxiliary:
		no_data = count_data_local_grid_1D(grid, local_grid)
		determine_data_ratio_1D(local_grid, region, parameter, no_data)

#	if_plot = True

#	tpsfemPlot1D.plot_tpsfem_solution_1D(local_grid, None, parameter, False, 1, False, if_plot)
	
	initialise_connections(local_grid)
	
	# Uniformly refine the local grid
	local_grid = uniform_refinement(local_grid, parameter, uni_refine)

	if count_data:
		# Return the local grid
		if refine_choice == 0:
			return local_grid, None, no_data

		elif refine_choice == 1 or refine_choice == 2:
			return local_grid, mid_node.get_node_id(), no_data

	else:
		# Return the local grid
		if refine_choice == 0:
			return local_grid, None

		elif refine_choice == 1 or refine_choice == 2:
			return local_grid, mid_node.get_node_id()

