#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-05-07 21:44:11
@Last Modified time: 2022-10-09 23:39:16

This class contains routines to build polynomial of degree 1.

"""


# Import libraries
from adaptive.line.LinearPolynomial import LinearPolynomial
from math import fabs
import numpy as np


def set_polynomial_linear_1D(node1, node2):
	""" Construct a 1D linear polynomial """
	
	# Check the coordinates are two dimensional
	assert node1.get_dim() == 1 and node2.get_dim() == 1, \
			"the triangle coordinates must be one dimensional"
			
	# Get the coordinates of the two vertices
	x1 = node1.get_coord()[0]
	x2 = node2.get_coord()[0]

	# Find the polynomial coefficients
	poly = LinearPolynomial()

	x_coefficient = 1/(x1-x2)
	poly.set_const(1-x_coefficient*x1)
	poly.set_x(x_coefficient)
	
	# Return the polynomial
	return poly


def get_solution_polynomial_linear_1D(node1, node2, value1, value2):
	""" Construct a 1D linear polynomial """
	
	# Check the coordinates are two dimensional
	assert node1.get_dim() == 1 and node2.get_dim() == 1, \
			"the interval coordinates must be one dimensional"
	
	# Get the coordinates of the three vertices
	coord1 = node1.get_coord()
	coord2 = node2.get_coord()
	
	matrix = np.array([[coord1[0],1.0],[coord2[0],1.0]])

	vector = np.array([[value1],[value2]])

	# Solve system of equations, switch to least square if singular
	try:
		coef = np.linalg.solve(matrix, vector)
	except Exception:
		coef = np.linalg.lstsq(matrix, vector)[0]
		
	# Find the polynomial coefficients
	poly = LinearPolynomial()
	poly.set_const(coef[1][0])
	poly.set_x(coef[0][0])
	
	# Return the polynomial
	return poly