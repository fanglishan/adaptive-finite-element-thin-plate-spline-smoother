#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-05-07 21:44:02
@Last Modified time: 2022-03-29 11:20:44

This class contains numerical quadrature routines to integrate
a function over a triangle.

Adapted from Stals' FEM code.

"""

# Import libraries
from numpy import array


def linear_integrate(poly1, poly2, node1, node2): 
	""" Evaluate the approximate integral over the given triangle """
	
	# Check the coordinates are two dimensional
	assert node1.get_dim() == 1 and node2.get_dim() == 1, \
			"the interval coordinates must be one dimensional"
			
	# Get the coordinates of the 2 vertices
	coord1 = node1.get_coord()[0]
	coord2 = node2.get_coord()[0]
	
	# Find the point at the centre of the interval
	x = (coord1+coord2)/2.0

	# Apply the quadrature rule    
	return  poly1.eval(x) * poly2.eval(x) * abs(coord2-coord1)
		

def linear_integrate_single(poly, node1, node2): 
	""" Evaluate the approximate integral over the given triangle """
	
	# Check the coordinates are two dimensional
	assert node1.get_dim() == 1 and node2.get_dim() == 1, \
			"the interval coordinates must be one dimensional"
			
	# Get the coordinates of the 2 vertices
	coord1 = node1.get_coord()[0]
	coord2 = node2.get_coord()[0]
	
	# Find the point at the centre of the interval
	x = (coord1+coord2)/2.0

	# Apply the quadrature rule    
	return  poly.eval(x) * abs(coord2-coord1)


def linear_func_integrate(rhs, poly, node1, node2, node3): 
	""" Evaluate the approximate integral over the given triangle """
	
	# Check the coordinates are two dimensional
	assert node1.get_dim() == 2 and node2.get_dim() == 2 \
	 and node3.get_dim() == 2, \
			"the triangle coordinates must be two dimensional"

	# Get the coordinates of the 3 vertices
	coord1 = array(node1.get_coord())
	coord2 = array(node2.get_coord())
	coord3 = array(node3.get_coord())
	
	# Find the point at the centre of the triangle
	centre_point = (coord1+coord2+coord3)/3.0
	x = centre_point[0]
	y = centre_point[1]
	
	# Apply the quadrature rule    
	return  rhs(centre_point) * poly.eval(x, y) * \
			calc_area(coord1, coord2, coord3)