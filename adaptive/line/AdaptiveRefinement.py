#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-05-07 21:15:30
@Last Modified time: 2024-10-13 22:15:41

This class contains routines for the adaptive refinement of the grid.

Adapted from Stals' FEM code.

"""

# import libraries
from grid.GlobalNodeID import GlobalNodeID, mesh_integers_1D, unmesh_integers_1D
from grid.NodeTable import node_iterator
from grid.EdgeTable import endpt_iterator
from grid.Edge import DomainSet, RefineSet
from adaptive.MarkEdges import mark_refine_edges
#from data.DataThinning import record_no_data_edge, update_no_data
from indicator.ErrorIndicator import *
from parameter.Definition import IndicatorType

from copy import deepcopy


class NodeSet:
	"""Is the node a full node or a ghost node
	
	"""
	full_node = 1
	ghost_node = 2


def build_midpoint_global_id(grid, global_id1, global_id2):
	"""Find the global id of the new midpoint""" 

	# The node is being added a what level of refinement
	current_level = 0
	
	if global_id1.get_level() > global_id2.get_level():
		current_level = global_id1.get_level()+1
	else:
		current_level = global_id2.get_level()+1
		
	# The number must be scaled according to the level of refinement
	scale1 = 2**(current_level-global_id1.get_level()-1)
	scale2 = 2**(current_level-global_id2.get_level()-1)
		
	# Extract the coordinates that were used to find the ids of the endpoints 
	id1x = unmesh_integers_1D(global_id1.get_no())
	id2x = unmesh_integers_1D(global_id2.get_no())

	# Find the coordinates of the midpoint
	idx = id1x*scale1+id2x*scale2
		
	# Create a new global id
	id = mesh_integers_1D(idx)
	global_id = GlobalNodeID(id, current_level)

	# Return the global id
	return global_id
	
	 
def add_midpoint(grid, node1, node2, node_type, parameter):
	"""Add the midpoint to the grid""" 
	
	# Get the ids of the two endpoints
	node_id1 = node1.get_node_id()
	node_id2 = node2.get_node_id()
	
	# Find the coordinates of the midpoint
	dim = grid.get_dim()
	coord = [0.0]*dim
	for i in range(dim):
		coord[i] = (node1.get_coord()[i]+node2.get_coord()[i])/2.0
		
	# Find the global id of the midpoint
	mid_global_id = build_midpoint_global_id(grid, node1.get_global_id(),
											 node2.get_global_id())
									 
	# Determine if the midpoint will sit on the boundary                                         
	location = grid.get_location(node_id1, node_id2)
	bnd_func = grid.get_boundary_function(node_id1, node_id2)

	# If the midpoint is to be added as a full node
	if node_type == NodeSet.full_node:
		
		# If the midpoint sits on the boundary
		if location == DomainSet.boundary:
			
			# Add the midpoint as a full slave node
			value = bnd_func(coord)
			mid_node = grid.create_node(mid_global_id, coord, True, value)
			mid_node.set_d_values([parameter.get_ux()(coord),parameter.get_uxx()(coord)])
			grid.add_node(mid_node)
				
		else:
			
			# Add the midpoint as a full node (and initialise the value and
			# load to be the average of the endpoints)
			value = (node1.get_value() + node2.get_value())/2.0
			data_load = (node1.get_data_load() + node2.get_data_load())/2.0
			mid_node = grid.create_node(mid_global_id, coord, False, value)
			mid_node.set_data_load(data_load)

			# Add derivative values if exist
			if len(node1.get_d_values())>0 and len(node2.get_d_values())>0:
				ux_value = (node1.get_d_values()[0]+node2.get_d_values()[0])/2.0
				uxx_value = (node1.get_d_values()[1]+node2.get_d_values()[1])/2.0
				mid_node.set_d_values([ux_value,uxx_value])

			grid.add_node(mid_node)

	return mid_node
	

def split_edge(grid, node1, node2, node_type, parameter):
	"""Split the edge between node1 and node2""" 
	
	# Get the ids of the endpoints
	node_id1 = node1.get_node_id()
	node_id2 = node2.get_node_id()
	
	# Find the edge location 
	location = grid.get_location(node_id1, node_id2)

	boundary_function = grid.get_boundary_function(node_id1, node_id2)
	
	# Find the refinement level
	refine_level = grid.get_refine_level(node_id1, node_id2)+1

	# Add the midpoint to the grid
	mid_node = add_midpoint(grid, node1, node2, node_type, parameter) 
	mid_id = mid_node.get_node_id()

	# Remove the old edges
	grid.delete_edge(node_id1, node_id2)
	grid.delete_edge(node_id2, node_id1)
	
	# Add a new edge between the midpoint and node1
	grid.add_edge(mid_id, node_id1)
	grid.set_location(mid_id, node_id1, location)
	grid.set_boundary_function(mid_id, node_id1, boundary_function)
	grid.set_refine_type(mid_id, node_id1, RefineSet.base_edge)
	grid.set_refine_level(mid_id, node_id1, refine_level)

	# Add a new edge between the node1 and midpoint
	grid.add_edge(node_id1, mid_id)
	grid.set_location(node_id1, mid_id, location)
	grid.set_boundary_function(node_id1, mid_id, boundary_function)
	grid.set_refine_type(node_id1, mid_id, RefineSet.base_edge) 
	grid.set_refine_level(node_id1, mid_id, refine_level) 

	# Add a new edge between the midpoint and node2
	grid.add_edge(mid_id, node_id2)
	grid.set_location(mid_id, node_id2, location)
	grid.set_boundary_function(mid_id, node_id2, boundary_function)
	grid.set_refine_type(mid_id, node_id2, RefineSet.base_edge)
	grid.set_refine_level(mid_id, node_id2, refine_level)

	# Add a new edge between the node2 and midpoint
	grid.add_edge(node_id2, mid_id)
	grid.set_location(node_id2, mid_id, location)
	grid.set_boundary_function(node_id2, mid_id, boundary_function)
	grid.set_refine_type(node_id2, mid_id, RefineSet.base_edge) 
	grid.set_refine_level(node_id2, mid_id, refine_level)

	# Return the midpoint id
	return mid_id
	 
  
def split_interval(grid, region, edge, parameter):
	""" Split the interval with the edge """ 

	# Get the ids of the endpoints of the edge
	endpt1 = edge.get_endpt1()
	endpt2 = edge.get_endpt2()

	# Find the nodes at the endpoints of the edge between node_id1 and node_id2
	if grid.is_in(endpt1):
		node1 = grid.get_node(endpt1)

	if grid.is_in(endpt2):
		node2 = grid.get_node(endpt2)
	
	# Return if the given edge is not found in the grid
	if not grid.is_edge(endpt1,endpt2):
		return

	# Set node as full node
	node_type = NodeSet.full_node

	# Record number of data of neighouring triangles
	if parameter.get_indicator() == IndicatorType.auxiliary:
		if parameter.get_indicator_parameter()[6]:
			if endpt1 < endpt2:
				no_data = grid.get_no_data(endpt1, endpt2)[0]
			else:
				no_data = grid.get_no_data(endpt2, endpt1)[0]

	# Split the edge
	mid_id = split_edge(grid, node1, node2, node_type, parameter)

	# Update number of data of neighouring triangles
	if parameter.get_indicator() == IndicatorType.auxiliary:
		if parameter.get_indicator_parameter()[6]:
			if mid_id > endpt1 and mid_id > endpt2:
				grid.set_no_data(endpt1, mid_id, [int(no_data/2.0)])
				grid.set_no_data(endpt2, mid_id, [int(no_data/2.0)])
			else:
				print("Warning: interval/AdaptiveRefinement - invalid node id")
	
	# Update the connections
	
	# Remove the old connections
	grid.delete_connection(endpt1, endpt2)
	grid.delete_connection(endpt2, endpt1)
		
	# Add connections between the midpoint and four vertices
	grid.add_connection(mid_id, mid_id)
	grid.add_connection(mid_id, endpt1)
	grid.add_connection(mid_id, endpt2)
	grid.add_connection(endpt1, mid_id)
	grid.add_connection(endpt2, mid_id)

	# No error indicators for local problems
	if parameter.get_local():
		return mid_id

	# Compute error indicators
	error_indicator_1 = indicate_edge_error(grid, region, grid.get_edge(mid_id, endpt1), parameter)
	error_indicator_2 = indicate_edge_error(grid, region, grid.get_edge(mid_id, endpt2), parameter)

	# Add error indicator to the grid
	grid.set_error_indicator(mid_id, endpt1, error_indicator_1)
	grid.set_error_indicator(endpt1, mid_id, error_indicator_1)

	grid.set_error_indicator(mid_id, endpt2, error_indicator_2)
	grid.set_error_indicator(endpt2, mid_id, error_indicator_2)

	return mid_id


def adaptive_refinement_all(grid, region, parameter):
	""" Adaptively refine the 2D grid in one iteration """

	# Make a copy of the current grid
	refine_grid = deepcopy(grid)
	
	# Apply m sweeps of refinement
	for i in range(parameter.get_max_no()):
		
		# Mark a set of edges for refinement
		refine_edges = mark_refine_edges(refine_grid, parameter)
	
		# Stop refinements when no edge needs to be refined
		if len(refine_edges) == 0:
			return refine_grid

		for edge in refine_edges:
			split_interval(refine_grid, region, edge, parameter)

	return refine_grid


def adaptive_refinement_iterative(grid, region, parameter):
	""" Adaptively refine the 2D grid iteratively """
	
	# Refine ratio
	# 0.1 means 10% each iteration
	refine_ratio = 0.1

	# Maximum ratio of edges refined in each sweep
	# 1 Means stops when nodes double
	max_refine_ratio = 1

	# Make a copy of the current grid
	refine_grid = deepcopy(grid)

	# Apply m sweeps of refinement
	for i in range(0, parameter.get_max_no()):
		
		# The number of nodes
		threshold = int(refine_grid.get_no_nodes()*(1+max_refine_ratio))

		# Number of refinement each iteration
		no_refine = int(refine_grid.get_no_nodes()*refine_ratio)

		# Keep refining if not enough new nodes
		while refine_grid.get_no_nodes() < threshold:

			# Mark a set of edges for refinement
			refine_edges = mark_refine_edges(refine_grid, parameter, no_refine)

			if_refine = False

			# Loop over the base edges and split or refine the triangle along that
			# base edge
			for j in range(0, len(refine_edges)):

				edge = refine_edges[j]

				# If the edge needs refinement
#				if edge.get_error_indicator() > parameter.get_error_tol():

				# Split the edge
				split_interval(refine_grid, region, edge, parameter)

				if_refine = True

				# Breaks when number of nodes doubles
				if refine_grid.get_no_nodes() >= threshold:
					break

			# Break the loop if no base edge is up for refinement
			if not if_refine:
				break

	# Return the refined grid
	return refine_grid


def adaptive_refinement(grid, region, parameter):
	""" Adpative refinement of the grid """

	# Marking strategy for adaptive refinement
	mark = parameter.get_marking_strategy()

	# If using infity error indicator, just refine all (just in case)
	if parameter.get_indicator() == IndicatorType.infinity:
		mark = 1

	# Refine all base edges above error tolerance
	if mark == 1:
		refine_grid = adaptive_refinement_all(grid, region, parameter)
	
	# Refine a portion of base edges with high indicator iteratively
	else:
		refine_grid = adaptive_refinement_iterative(grid, region, parameter)

	return refine_grid