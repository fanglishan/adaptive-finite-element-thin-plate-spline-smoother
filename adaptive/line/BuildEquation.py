#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-05-07 21:37:29
@Last Modified time: 2022-10-09 23:35:52

This class contains routines to calculate the entries of
1D TPSFEM system of equations.

Adapted from Stals' FEM code.

"""

# Import libraries
from grid.EdgeTable import endpt_iterator
from grid.NodeTable import node_iterator    
from adaptive.line.LinearPolynomial import LinearPolynomial


def Poisson_lin_integrate_linear(poly1, poly2, node1, node2):
	"""Poisson model problem""" 
	
	# Apply numerical quadrature routines to approximate the integrals
	local_stiffness_x = \
		linear_integrate(poly1.dx(), poly2.dx(), node1, node2)

	return local_stiffness_x


def set_polynomial_linear_2D(node1, node2, node3):
	"""Construct a linear polynomial"""
	
	# Import appropriate information from other classes
	from math import fabs
	
	# Check the coordinates are two dimensional
	assert node1.get_dim() == 2 and node2.get_dim() == 2 \
		and node3.get_dim() == 2, \
			"the triangle coordinates must be two dimensional"
			
	# Get the coordinates of the three vertices
	coord1 = node1.get_coord()
	coord2 = node2.get_coord()
	coord3 = node3.get_coord()
	
	# Break down the information to make the code easier to read
	x1 = coord1[0]
	y1 = coord1[1]
	x2 = coord2[0]
	y2 = coord2[1]
	x3 = coord3[0]
	y3 = coord3[1]

	# Find h
	division = (y1-y2)*(x2-x3)-(y2-y3)*(x1-x2);
	
	# Avoid division by zero errors
	assert fabs(division) > 1.0E-12, "divide by zero in set_polynomial_linear_2D"

	# Find the polynomial coefficients
	poly = LinearPolynomial()
	poly.set_const((x3*y2 - y3*x2)/division)
	poly.set_x((y3-y2)/division)
	poly.set_y((x2-x3)/division)
	
	# Return the polynomial
	return poly


def local_stiffness_linear_2D(node1, node2, node3, tri_integrate):
	"""Find the element stiffness matrix"""

	# Find the polynomials who's support is on the trinalge
	poly1 = set_polynomial_linear_2D(node1, node2, node3)
	poly2 = set_polynomial_linear_2D(node2, node3, node1)
	poly3 = set_polynomial_linear_2D(node3, node1, node2)

	# Evaluate the contribution to the local stiffness matrix
	local_stiffness1 = tri_integrate(poly1, poly1, node1, node2, node3)
	local_stiffness2 = tri_integrate(poly1, poly2, node1, node2, node3)
	local_stiffness3 = tri_integrate(poly1, poly3, node1, node2, node3)

	return local_stiffness1, local_stiffness2, local_stiffness3


def local_load_linear_2D(node1, node2, node3, rhs):
	"""Find the element load vector"""
	
	# Import appropriate information from other classes
	from TriangleIntegrate import linear_func_integrate

	# Find a polynomial whose value is 1 at node1 and 0 at the other 
	# nodes
	poly1 = set_polynomial_linear_2D(node1, node2, node3)

	# Apply a numerical quadrature scheme to approximate the integral
	local_load = linear_func_integrate(rhs, poly1, node1, node2, node3)
									  
	return local_load

		
def sum_data_matrix(grid, node1, node2, local_data):
	"""Add local_data to the current matrix A value"""
	
	# Get the node ids
	id1 = node1.get_node_id()
	id2 = node2.get_node_id()
	
	# Find the current data matrix value
	data_entry = grid.get_matrix_A_value(id1, id2)
	
	# Add local_stiff
	grid.set_matrix_A_value(id1, id2, data_entry + local_data)


def sum_data_load(node, local_data):
	"""Add local_data to the current d value"""
	
	# Find the current load value
	data_load = node.get_data_load()

	# Add local load
	node.set_data_load(data_load + local_data)


def sum_KTu_load(node, local_KTu):
	"""Add local_KTu to the current KTu value"""
	
	# Find the current load value
	KTu_load = node.get_KTu_load()

	# Add local load
	node.set_KTu_load(KTu_load + local_KTu)



def sum_stiffness(grid, node1, node2, local_stiff):
	"""Add local_stiff to the current matrix L value"""
	
	# Get the node ids
	id1 = node1.get_node_id()
	id2 = node2.get_node_id()
	
	# Find the current stiffness value
	stiff = grid.get_matrix_L_value(id1, id2)
	
	# Add local_stiff
	grid.set_matrix_L_value(id1, id2, stiff + local_stiff)
	

def sum_gradient(grid, node1, node2, local_grad):
	"""Add local_grad to the current matrix G value"""
	
	# Get the node ids
	id1 = node1.get_node_id()
	id2 = node2.get_node_id()
	
	# Find the current stiffness value
	grad = grid.get_matrix_G_value(id1, id2)
	
	# Add local_stiff
	grid.set_matrix_G_value(id1, id2, grad + local_grad)



def normalise_A_d(grid, parameter):
	""" Divide A and d entries with number of data points """

	# Normalisation ratio of A and d entries
	if parameter.get_data_size() == 0:
		ratio = 1.0
	else:
		ratio = 1.0/parameter.get_data_size()

	# Iterate through the nodes
	for node in node_iterator(grid):

		# Node id
		node_id = node.get_node_id()

		# Find the current stiffness value
		data_entry = grid.get_matrix_A_value(node_id, node_id)

		# Apply ratio
		grid.set_matrix_A_value(node_id, node_id, data_entry*ratio)

		# Find the current data load value
		data_load = node.get_data_load()
		
		# Apply ratio
		node.set_data_load(data_load*ratio)

		# Find the current KTu load value
		KTu_load = node.get_KTu_load()

		# Apply ratio
		node.set_KTu_load(KTu_load*ratio)

		# Iterate through the endpoints
		for endpt in endpt_iterator(grid, node.get_node_id()):

			# Find the current stiffness value
			data_entry = grid.get_matrix_A_value(node_id, endpt)

			# Apply ratio
			grid.set_matrix_A_value(node_id, endpt, data_entry*ratio)


def set_slave_value(grid, parameter):
	""" Assign the slave node values using boundary function """

	# Model problem function
	function = parameter.get_function()
	
	# Iterate through slave (boundary) nodes
	for node in node_iterator(grid):
		if node.get_slave():

			node.set_value(function[0](node.get_coord()))
			node.set_d_values([function[1](node.get_coord()), \
							-function[2](node.get_coord())])


def initialise_connections(grid):
	""" Initialise connections for 1D linear basis """

	for node in node_iterator(grid):
		node_id = node.get_node_id()
		node.set_data_load(0.0)
		if not grid.is_connection(node_id, node_id):
			grid.add_connection(node_id, node_id)
		grid.set_matrix_A_value(node_id, node_id, 0.0)
		grid.set_matrix_L_value(node_id, node_id, 0.0)
		grid.set_matrix_G_value(node_id, node_id, [0.0])
		for endpt1 in endpt_iterator(grid, node.get_node_id()):
			if not grid.is_connection(node_id, endpt1):
				grid.add_connection(node_id, endpt1)
			grid.set_matrix_A_value(node_id, endpt1, 0.0)
			grid.set_matrix_L_value(node_id, endpt1, 0.0)
			grid.set_matrix_G_value(node_id, endpt1, [0.0])
