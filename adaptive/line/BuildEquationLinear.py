#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-05-07 21:37:29
@Last Modified time: 2022-10-09 23:35:48

This class builds and stores the TPSFEM submatrix connections and data load
vector. It also sets the Dirichlet boundary conditions if required.

Adapted from Stals' FEM code.

"""

# Import libraries
from grid.EdgeTable import endpt_iterator
from grid.NodeTable import node_iterator
from adaptive.line.SetPolynomial import set_polynomial_linear_1D 
from adaptive.line.BuildEquation import set_slave_value, sum_data_matrix, \
	sum_data_load, sum_KTu_load, sum_stiffness, sum_gradient, \
	initialise_connections, normalise_A_d
from adaptive.line.IntervalIntegrateLinear import linear_integrate
from data.ObtainData import obtain_data
from data.DataThinning import record_data_1D
from parameter.Definition import BoundaryType, IndicatorType
from fileio.WriteResult import write_line
from plot.DataPlot import plot_data_1D, plot_data_dist



def local_A_linear_1D(poly1, poly2, data_points, u_vector):
	""" Find the data projection matrix A values """

	# Initialise data projection matrix and vector entries as zero
	data_project1 = 0.0
	data_project2 = 0.0
	data_vector = 0.0
	KTu_vector = 0.0

	# Iterate through each data point
	for i in range(0, len(data_points)):

		# x coodinate of the data point
		coord = data_points[i][0]

		poly1_proj = poly1.eval(coord)

		# Evaluate the contribution to the data projection matrix
		data_project1 += poly1_proj*poly1_proj
		data_project2 += poly1_proj*poly2.eval(coord)

		data_vector += poly1_proj*data_points[i][1]
		KTu_vector += poly1_proj*u_vector[i]

	return data_project1, data_project2, data_vector, KTu_vector



def local_L_linear_1D(node1, node2, poly1, poly2):
	""" Find the element stiffness matrix L entries"""

	# Evaluate the contribution to the local stiffness matrix
	local_stiffness1 = linear_integrate(poly1.dx(), poly1.dx(), node1, node2)
	local_stiffness2 = linear_integrate(poly1.dx(), poly2.dx(), node1, node2)

	return local_stiffness1, local_stiffness2


def local_G_linear_1D(node1, node2, poly1, poly2):
	""" Find the element gradient matrix G entries """

	# Evaluate the contribution to the local gradient matrix
	local_gradient1 = linear_integrate(poly1, poly1.dx(), node1, node2)
	local_gradient2 = linear_integrate(poly1, poly2.dx(), node1, node2)

	return local_gradient1, local_gradient2
	

def calculate_connections(grid, node1, node2, data_points, u_vector):
	""" Calculate some connections between node 1 and node 2 """

	# Find the polynomials who's support is on the trinalge
	poly1 = set_polynomial_linear_1D(node1, node2)
	poly2 = set_polynomial_linear_1D(node2, node1)

	# Find the local A and d entries
	data1, data2, data_vector, KTu_vector = local_A_linear_1D(poly1, poly2, data_points, u_vector)

	sum_data_matrix(grid, node1, node1, data1)
	sum_data_matrix(grid, node1, node2, data2)

	sum_data_load(node1, data_vector)
	sum_KTu_load(node1, KTu_vector)

	# Find the local stiffness entries
	stiff1, stiff2 = local_L_linear_1D(node1, node2, poly1, poly2)

	sum_stiffness(grid, node1, node1, stiff1)
	sum_stiffness(grid, node1, node2, stiff2)


	# Find the local gradient entries
	grad1, grad2 = local_G_linear_1D(node1, node2, poly1, poly2)

	sum_gradient(grid, node1, node1, grad1)
	sum_gradient(grid, node1, node2, grad2)



def build_equation_linear_1D(grid, region, parameter):
	""" Calculate entries of A, L and G submatrices and d vector """

	# Iterate through data regions and reset all signals
	region.init_usage()

	local = parameter.get_local()

	# Boundary type of the problem
	boundary_type = parameter.get_boundary()

	# Set slave values for Dirichlet boundaries (not for error indicators)
	if boundary_type == BoundaryType.Dirichlet and not parameter.get_local():
		set_slave_value(grid, parameter)

	# Add the matrix connections for linear basis functions and 
	# initialise to zero
	initialise_connections(grid)

	if not local:
		parameter.set_data_ratio(1.0)

		write_line(parameter, "data ratio: "+str(parameter.get_data_ratio()), True)

	# Number of data points
	data_sum = 0
	data = []

	# Evalue that A, L, G matrices and d vector

	# Iterate through each edge once
	for node in node_iterator(grid):
		node_id = node.get_node_id()
		for endpt in endpt_iterator(grid, node_id):
			if node_id > endpt:
				end_node = grid.get_node_ref(endpt)

				# Gather data points for the interval
				data_points, u_vector = obtain_data(region, [node, end_node], parameter)
				data = data+data_points

				# Calculate connections for two nodes
				calculate_connections(grid, node, end_node, data_points, u_vector)
				calculate_connections(grid, end_node, node, data_points, u_vector)

				data_sum += len(data_points)

				# Record number of data points for this triangle
				# This is only needed for auxiliary problem error indicator
				if not local and parameter.get_indicator() == IndicatorType.auxiliary:
					record_data_1D(grid, endpt, node_id, len(data_points))

#	plot_data_1D(data)
#	plot_data_dist(data)

	parameter.set_data_size(data_sum)

	# Divide A and d entries by number of data points
	normalise_A_d(grid, parameter)
