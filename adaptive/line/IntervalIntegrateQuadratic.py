#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-05-07 21:44:02
@Last Modified time: 2022-03-29 11:20:47

This class contains numerical quadrature routines to integrate
a quadratic function over a triangle.

"""


def quadratic_integrate(poly1, poly2, node1, node2): 
	""" Evaluate the approximate integral over the given triangle """
	
	# Check the coordinates are two dimensional
	assert node1.get_dim() == 1 and node2.get_dim() == 1, \
			"the interval coordinates must be one dimensional"
			
	# Get the coordinates of the 2 vertices
	coord1 = node1.get_coord()[0]
	coord2 = node2.get_coord()[0]
	
	# Find the point at the centre of the interval
	x = (coord1+coord2)/2.0

	poly_product = 4 * poly1.eval(x) * poly2.eval(x) + \
				poly1.eval(coord1) * poly2.eval(coord1) + \
				poly1.eval(coord2) * poly2.eval(coord2)

	# Apply the quadrature rule    
	return  poly_product / 6.0 * abs(coord2-coord1)
		

def quadratic_integrate_single(poly, node1, node2): 
	""" Evaluate the approximate integral over the given triangle """
	
	# Check the coordinates are two dimensional
	assert node1.get_dim() == 1 and node2.get_dim() == 1, \
			"the interval coordinates must be one dimensional"
			
	# Get the coordinates of the 2 vertices
	coord1 = node1.get_coord()[0]
	coord2 = node2.get_coord()[0]
	
	# Find the point at the centre of the interval
	x = (coord1+coord2)/2.0

	poly_product = 4*poly.eval(x) + poly.eval(coord1) + poly.eval(coord2)

	# Apply the quadrature rule    
	return poly_product / 6.0 * abs(coord2-coord1)

