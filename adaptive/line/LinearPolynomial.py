#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-05-07 21:44:02
@Last Modified time: 2022-10-09 23:35:33

This class contains the information in a linear polynomial with basic
functions to access and update them.

Adapted from Stals' FEM code.

"""

# Import libraries
from adaptive.line.QuadraticPolynomial import QuadraticPolynomial


class LinearPolynomial:
	""" A linear polynomial in one dimension """
	
	# constant coefficient
	_const = 0.0
	
	# x coefficient
	_x = 0.0
   
   
	def __init__(self, const = 0.0, x = 0.0):
		"""Initialise a polynomial """
		self._const = const
		self._x = x

	def __str__(self):
		""" Convert a polynomial into a string so it can be printed
		
		The format is constant coefficient + x coefficient * x 
		+ y coefficient * y
		"""
		return str(self._const) + " + " + str(self._x) + "x"
	
	def __add__(self, polynomial):
		""" Addition operator """
		poly = LinearPolynomial(self._const+polynomial.get_const(),
				self._x+polynomial.get_x())
		return poly

	def __sub__(self, polynomial):
		""" Substraction operator """
		poly = LinearPolynomial(self._const-polynomial.get_const(),
				self._x-polynomial.get_x())
		return poly

	def __mul__(self, polynomial):
		""" Multiplication operator """
		
		# If the input is a linear polynomial
		if isinstance(polynomial,LinearPolynomial):
			poly = QuadraticPolynomial(self._const*polynomial.get_const(),
					self._const*polynomial.get_x()+self._x*polynomial.get_const(),
					self._x*polynomial.get_x())

		else:
			print(type(polynomial))
			
		return poly

	def set_const(self, const):
		""" Set constant coefficient """
		self._const = const
	
	def set_x(self, x):
		""" Set the x coefficient """
		self._x = x

	def get_const(self):
		""" Get the constant coefficient """
		return self._const
		
	def get_x(self):
		""" Get the x coefficient """
		return self._x

	def eval(self, x): 
		""" Evaluate the polynomial at the posn (x) """
		return self._const + self._x * x
		
	def dx(self): 
		""" Differentiate the polynomial in the x direction """
		poly = LinearPolynomial(self._x, 0.0)
		return poly