# -*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2024 Fang

@Author: Fang
@Date:   2024-10-13 22:40:41
@Last Modified by:   Fang
@Last Modified time: 2024-10-13 22:45:06

This class contains functions to select base edges from given grid.

"""

from grid.NodeTable import node_iterator
from grid.EdgeTable import endpt_iterator
from grid.Edge import RefineSet


def build_base_edges_1D(grid):
	""" Build a list of base edges """

	base_edges = list()

	# Loop over each edge
	for node in node_iterator(grid):
		node_id = node.get_node_id()
		for endpt in endpt_iterator(grid, node_id):
			if node_id < endpt:
				
				# Check edge type
				if grid.get_refine_type(node_id, endpt) == RefineSet.base_edge:
					base_edges.append(grid.get_edge(node_id, endpt))

	return base_edges


def build_base_edges_2D(grid):

	# Initialise the bese edge list
	base_edges = list()
		
	# Loop through each edge
	for node in node_iterator(grid):
		node_id = node.get_node_id()
		for endpt in endpt_iterator(grid, node_id):
			if node_id < endpt:
				end_node = grid.get_node_ref(endpt)

				# If the refinement type is a base type
				if grid.get_refine_type(node_id, endpt) == RefineSet.base_edge or \
					grid.get_refine_type(node_id, endpt) == RefineSet.interface_base_edge:

					new_coord = [(node.get_coord()[0]+end_node.get_coord()[0])/2.0, \
								(node.get_coord()[1]+end_node.get_coord()[1])/2.0]

					# Add the edge to the base edge list
					base_edges.append(grid.get_edge(node_id, endpt))

	return base_edges


def build_base_edges(grid):
	
	if grid.get_dim() == 1:
		base_edges = build_base_edges_1D(grid)
	elif grid.get_dim() == 2:
		base_edges = build_base_edges_2D(grid)

	return base_edges