#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-04-26 10:33:54
@Last Modified time: 2022-03-29 11:26:30

This class defines an iterator that extracts the triangles in a grid.

Adapted from Stals' FEM code.

"""

# Import libraries
from grid.NodeTable import node_iterator
from grid.EdgeTable import endpt_iterator

    
def triangle_iterator(grid):
    """Iterate over the triangles in a grid"""
    
    # Get the ghost node table
    #ghost_table = grid.reference_ghost_table()

    # Loop over the nodes in the grid
    for node in node_iterator(grid):
        
        # Loop over the edges joined to the node
        for endpt1 in endpt_iterator(grid, node.get_node_id()): 
            
            # Get the node sitting to the endpoint (which may be a
            # ghost node)
            if grid.is_in(endpt1):
                node1 = grid.get_node(endpt1)
            #else:
                #node1 = ghost_table.get_node(endpt1)
               
            
            #Loop over another set of edges joined to the node
            for endpt2 in endpt_iterator(grid, node.get_node_id()):
                
                # If the endpoints are joined by an additional edge
                if grid.is_edge(endpt1, endpt2):
                    
                    # Get the node sitting to the endpoint (which may be a
                    # ghost node)
                    if grid.is_in(endpt2):
                        node2 = grid.get_node(endpt2)
                    #else:
                        #node2 = ghost_table.get_node(endpt2)
 
                    # Then return the triangle
                    yield [node, node1, node2]
        
                      
    
def triangle_iterator_ref(grid):
    """Iterate over the triangles in a grid (actual nodes, not copy)"""
    
    # Get the ghost node table
    #ghost_table = grid.reference_ghost_table()

    # Loop over the nodes in the grid
    for node in node_iterator(grid):
        
        # Loop over the edges joined to the node
        for endpt1 in endpt_iterator(grid, node.get_node_id()): 
            
            # Get the node sitting to the endpoint (which may be a
            # ghost node)
            if grid.is_in(endpt1):
                node1 = grid.get_node_ref(endpt1)
            #else:
                #node1 = ghost_table.get_node(endpt1)
               
            
            #Loop over another set of edges joined to the node
            for endpt2 in endpt_iterator(grid, node.get_node_id()):
                
                # If the endpoints are joined by an additional edge
                if grid.is_edge(endpt1, endpt2):
                    
                    # Get the node sitting to the endpoint (which may be a
                    # ghost node)
                    if grid.is_in(endpt2):
                        node2 = grid.get_node_ref(endpt2)
                    #else:
                        #node2 = ghost_table.get_node(endpt2)
 
                    # Then return the triangle
                    yield [node, node1, node2]


def neighbour_triangles_iterator_ref(grid, node):

    # Loop over the edges joined to the node
    for endpt1 in endpt_iterator(grid, node.get_node_id()): 
            
        # Get the node sitting to the endpoint (which may be a
        # ghost node)
        if grid.is_in(endpt1):
            node1 = grid.get_node_ref(endpt1)
        #else:
            #node1 = ghost_table.get_node(endpt1)
               
        #Loop over another set of edges joined to the node
        for endpt2 in endpt_iterator(grid, node.get_node_id()):
                
            # If the endpoints are joined by an additional edge
            if grid.is_edge(endpt1, endpt2):
                    
                # Get the node sitting to the endpoint (which may be a
                # ghost node)
                if grid.is_in(endpt2):
                    node2 = grid.get_node_ref(endpt2)
                #else:
                    #node2 = ghost_table.get_node(endpt2)

                yield [node, node1, node2]