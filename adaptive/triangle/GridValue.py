#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-05-19 10:14:03
@Last Modified time: 2022-10-09 23:36:41

This class contains routines to calculate the interpolated
value of the 2D grid on the given coordinate.

"""

# Import libraries
from grid.NodeTable import node_iterator
from grid.EdgeTable import endpt_iterator
from adaptive.triangle.Triangle import triangle_iterator, neighbour_triangles_iterator_ref
from adaptive.triangle.SetPolynomial import get_solution_polynomial_linear_2D, set_polynomial_linear_2D
from adaptive.triangle.TriangleFunction import is_in_triangle

from math import sqrt



def check_triangle(data_point, node1, node2, node3, tol=0.0):

	# Polynomials of the triangle
	poly1 = set_polynomial_linear_2D(node1, node2, node3)
	poly2 = set_polynomial_linear_2D(node2, node3, node1)
	poly3 = set_polynomial_linear_2D(node3, node1, node2)

	x1 = data_point[0]
	x2 = data_point[1]

	if poly1.eval(x1, x2)>=tol and poly2.eval(x1, x2)>=tol and poly3.eval(x1, x2)>=tol:
		return True

	else:
		return False


def find_triangle(grid, coord):
	""" Find the triangle that contains coord by finding closest node first """

	# Initialise node and distance
	closest_node = None
	min_d = 10000000.0

	# Iterate throught nodes to find the closest node
	for node in node_iterator(grid):

		# Distance between the node and given coordinate
		d = 0.0
		for i in range(0, 2):
			d += (node.get_coord()[i] - coord[i])**2
		d = sqrt(d)

		# Check if it is closer than the previous ones
		if d < min_d:
			min_d = d
			closest_node = node

	for tri in neighbour_triangles_iterator_ref(grid, closest_node):

		# Check if the point is in the triangle
#		if is_in_triangle(coord, tri[0].get_coord(), \
#				tri[1].get_coord(), tri[2].get_coord()):
		if check_triangle(coord, tri[0], tri[1], tri[2]):
			return tri[0], tri[1], tri[2]

	print("Warning: triangle not found")
	print("Coordinate:", coord)
	return None, None, None



def cal_value_2d(node1, node2, node3, coord):
	""" Calculate grid value at the coord """

	poly = get_solution_polynomial_linear_2D(node1, node2, node3, \
		node1.get_value(), node2.get_value(), node3.get_value())

	return poly.eval(coord[0], coord[1])



def cal_smoother_2d(grid, coord):
	""" Calculate an interpolated value from a 2D grid """

	# Check input grid and coordinate
	if grid.get_dim() != 2:
		print("Warning: invalid grid dimension")

	if len(coord) != 2:
		print("Warning: invalid coordinate dimension")

	# Find triangle using the second approach
	node1, node2, node3 = find_triangle(grid, coord)

	if node1 is None and node2 is None and node3 is None:
		return None

	else:
		return cal_value_2d(node1, node2, node3, coord)