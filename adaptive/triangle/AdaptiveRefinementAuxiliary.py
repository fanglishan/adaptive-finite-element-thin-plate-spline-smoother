#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-04-26 11:14:10
@Last Modified time: 2022-04-25 19:42:24

This class contains routines to adaptively refine the grid.

Adapted from Stals' FEM code.

"""

# import libraries
from grid.GlobalNodeID import GlobalNodeID, mesh_integers_2D, unmesh_integers_2D
from grid.NodeTable import node_iterator
from grid.EdgeTable import endpt_iterator
from grid.Edge import DomainSet, RefineSet
from grid.Grid import Grid
#from data.DataThinning import record_no_data_edge, update_no_data
import plot.FemPlot2D as femPlot2D
from parameter.Definition import IndicatorType

from copy import deepcopy


class NodeSet:
	""" Is the node a full node or a ghost node """
	full_node = 1
	ghost_node = 2


def build_midpoint_global_id(grid, global_id1, global_id2):
	""" Find the global id of the new midpoint """ 

	# The node is being added a what level of refinement
	current_level = 0
	
	# Find refine level
	if global_id1.get_level() > global_id2.get_level():
		current_level = global_id1.get_level()+1
	else:
		current_level = global_id2.get_level()+1
		
	# The number must be scaled according to the level of refinement
	scale1 = 2**(current_level-global_id1.get_level()-1)
	scale2 = 2**(current_level-global_id2.get_level()-1)
		
	# Extract the coordinates that were used to find the ids of the endpoints 
	id1x, id1y = unmesh_integers_2D(global_id1.get_no())
	id2x, id2y = unmesh_integers_2D(global_id2.get_no())
		
	# Find the coordinates of the midpoint
	idx = id1x*scale1+id2x*scale2
	idy = id1y*scale1+id2y*scale2
		
	# Create a new global id
	id = mesh_integers_2D(idx, idy)
	global_id = GlobalNodeID(id, current_level)

	# Return the global id
	return global_id
	
	
def add_midpoint(grid, node1, node2, node_type, parameter):
	""" Add the midpoint to the grid """ 
	
	# Get the ids of the two endpoints
	node_id1 = node1.get_node_id()
	node_id2 = node2.get_node_id()

	# Find the coordinates of the midpoint
	dim = grid.get_dim()
	coord = [0.0]*dim
	for i in range(dim):
		coord[i] = (node1.get_coord()[i]+node2.get_coord()[i])/2.0
		
	# Find the global id of the midpoint
	mid_global_id = build_midpoint_global_id(grid, node1.get_global_id(),
											 node2.get_global_id())
											 
	# Determine if the midpoint will sit on the boundary                                         
	location = grid.get_location(node_id1, node_id2)
	bnd_func = grid.get_boundary_function(node_id1, node_id2)

	
	# If the midpoint is to be added as a full node
	if node_type == NodeSet.full_node:
		"""
		# If the midpoint sits on the boundary
		if location == DomainSet.boundary:
			
			# Add the midpoint as a full slave node
			value = bnd_func(coord)
			mid_node = grid.create_node(mid_global_id, coord, True, value)

			function = parameter.get_function()
			mid_node.set_d_values([function[1](coord),\
				function[2](coord), function[3](coord)])

			grid.add_node(mid_node)
			
		else:
		"""	
		# Add the midpoint as a full node (and initialise the value and
		# load to be the average of the endpoints)
		value = (node1.get_value() + node2.get_value())/2.0
		data_load = (node1.get_data_load() + node2.get_data_load())/2.0
		mid_node = grid.create_node(mid_global_id, coord, False, value)
		mid_node.set_data_load(data_load)

		# Add derivative values if exist
		if len(node1.get_d_values())>0 and len(node2.get_d_values())>0:
			ux_value = (node1.get_d_values()[0]+node2.get_d_values()[0])/2.0
			uy_value = (node1.get_d_values()[1]+node2.get_d_values()[1])/2.0
			uxx_value = (node1.get_d_values()[2]+node2.get_d_values()[2])/2.0
			mid_node.set_d_values([ux_value,uy_value,uxx_value])

		grid.add_node(mid_node)

	return mid_node
	

def split_edge(grid, node1, node2, node_type, parameter):
	"""Split the edge between node1 and node2""" 

	# Get the ids of the endpoints
	node_id1 = node1.get_node_id()
	node_id2 = node2.get_node_id()
	
	# Find the edge location 
	location = grid.get_location(node_id1, node_id2)
	boundary_function = grid.get_boundary_function(node_id1, node_id2)
	
	# Find the refinement level
	refine_level = grid.get_refine_level(node_id1, node_id2)+1

	# Add the midpoint to the grid
	mid_node = add_midpoint(grid, node1, node2, node_type, parameter) 
	mid_id = mid_node.get_node_id()

	# Remove the old edges
	grid.delete_edge(node_id1, node_id2)
	grid.delete_edge(node_id2, node_id1)
	
	# Add a new edge between the midpoint and node1
	grid.add_edge(mid_id, node_id1)
	grid.set_location(mid_id, node_id1, location)
	grid.set_boundary_function(mid_id, node_id1, boundary_function)
	grid.set_refine_type(mid_id, node_id1, RefineSet.not_base_edge)
	grid.set_refine_level(mid_id, node_id1, refine_level)

	# Add a new edge between the node1 and midpoint
	grid.add_edge(node_id1, mid_id)
	grid.set_location(node_id1, mid_id, location)
	grid.set_boundary_function(node_id1, mid_id, boundary_function)
	grid.set_refine_type(node_id1, mid_id, RefineSet.not_base_edge) 
	grid.set_refine_level(node_id1, mid_id, refine_level) 

	# Add a new edge between the midpoint and node2
	grid.add_edge(mid_id, node_id2)
	grid.set_location(mid_id, node_id2, location)
	grid.set_boundary_function(mid_id, node_id2, boundary_function)
	grid.set_refine_type(mid_id, node_id2, RefineSet.not_base_edge)
	grid.set_refine_level(mid_id, node_id2, refine_level)

	# Add a new edge between the node2 and midpoint
	grid.add_edge(node_id2, mid_id)
	grid.set_location(node_id2, mid_id, location)
	grid.set_boundary_function(node_id2, mid_id, boundary_function)
	grid.set_refine_type(node_id2, mid_id, RefineSet.not_base_edge) 
	grid.set_refine_level(node_id2, mid_id, refine_level)

	# Return the midpoint
	return mid_node
	 
	 
def set_base_type(grid, node_id1, node_id2):
	
	if grid.get_location(node_id1, node_id2) != DomainSet.interior:
		grid.set_refine_type(node_id1, node_id2, RefineSet.base_edge)
	elif grid.get_refine_type(node_id1, node_id2) == RefineSet.not_base_edge:
		grid.set_refine_type(node_id1, node_id2, RefineSet.interface_base_edge)
	else:
		grid.set_refine_type(node_id1, node_id2, RefineSet.base_edge)


def add_triangle4(grid, region, node_id1, node_id2, node_id3, node_id4, parameter):
	"""Refine two triangles along the given base edge""" 

	# Find the nodes at the endpoints of the edge between node_id1 and node_id2
	if grid.is_in(node_id1):
		node1 = grid.get_node(node_id1)

	if grid.is_in(node_id2):
		node2 = grid.get_node(node_id2)

	# Record at which refinement level the new node was added
	refine_level = grid.get_refine_level(node_id1, node_id2)+1

	node_type = NodeSet.full_node

	# Record number of data of neighouring triangles
#	if parameter.get_indicator() == IndicatorType.auxiliary:
#		if parameter.get_indicator_parameter()[6]:
#		if node_id1 < node_id2:
#			no_data = record_no_data_edge(grid, node_id1, node_id2, node_id3)
#		else:
#			no_data = record_no_data_edge(grid, node_id2, node_id1, node_id3)


	# Add the midpoint and split the edge between node1 and node2
	mid_node = split_edge(grid, node1, node2, node_type, parameter)
	mid_id = mid_node.get_node_id()

	# Add the edges going from the midpoint to node_id3
	grid.add_edge(mid_id, node_id3)
	grid.set_location(mid_id, node_id3, DomainSet.interior)
	grid.set_refine_type(mid_id, node_id3, RefineSet.not_base_edge)
	grid.set_refine_level(mid_id, node_id3, refine_level)

	grid.add_edge(node_id3, mid_id)
	grid.set_location(node_id3, mid_id, DomainSet.interior)
	grid.set_refine_type(node_id3, mid_id, RefineSet.not_base_edge)
	grid.set_refine_level(node_id3, mid_id, refine_level)
	
	# Add the edges going from the midpoint to node_id4
	grid.add_edge(mid_id, node_id4)
	grid.set_location(mid_id, node_id4, DomainSet.interior)
	grid.set_refine_type(mid_id, node_id4, RefineSet.not_base_edge)
	grid.set_refine_level(mid_id, node_id4, refine_level)

	grid.add_edge(node_id4, mid_id)
	grid.set_location(node_id4, mid_id, DomainSet.interior)
	grid.set_refine_type(node_id4, mid_id, RefineSet.not_base_edge)
	grid.set_refine_level(node_id4, mid_id, refine_level)

	# Update number of data of neighouring triangles
#	if parameter.get_indicator() == IndicatorType.auxiliary:
#		if parameter.get_indicator_parameter()[6]:
#		update_no_data(grid, no_data, mid_id, node_id1, node_id2, node_id3, node_id4)
	
	# Update the connections
	
	# Remove the old connections
	grid.delete_connection(node_id1, node_id2)
	grid.delete_connection(node_id2, node_id1)
		
	# Add connections between the midpoint and four vertices
	grid.add_connection(mid_id, mid_id)
	grid.add_connection(mid_id, node_id1)
	grid.add_connection(mid_id, node_id2)
	grid.add_connection(mid_id, node_id3)
	grid.add_connection(mid_id, node_id4)
	grid.add_connection(node_id1, mid_id)
	grid.add_connection(node_id2, mid_id)
	grid.add_connection(node_id3, mid_id)
	grid.add_connection(node_id4, mid_id)   
	
	# Update the refinement level to record when the triangle was added
	# (used mainly for adaptive refinement)
	grid.set_refine_level(node_id1, node_id3, refine_level)
	grid.set_refine_level(node_id3, node_id1, refine_level)
	grid.set_refine_level(node_id1, node_id4, refine_level)
	grid.set_refine_level(node_id4, node_id1, refine_level)    
	grid.set_refine_level(node_id2, node_id3, refine_level)
	grid.set_refine_level(node_id3, node_id2, refine_level)
	grid.set_refine_level(node_id2, node_id4, refine_level)
	grid.set_refine_level(node_id4, node_id2, refine_level)    
	
	# Update the base types of the external edges
	set_base_type(grid, node_id1, node_id3)
	set_base_type(grid, node_id3, node_id1)
	set_base_type(grid, node_id1, node_id4)
	set_base_type(grid, node_id4, node_id1)
	set_base_type(grid, node_id2, node_id3)
	set_base_type(grid, node_id3, node_id2)
	set_base_type(grid, node_id2, node_id4)
	set_base_type(grid, node_id4, node_id2)  

	# Skip error indicators for auxiliary problems
	if parameter.get_local():
		return mid_id

	error_indicator = 1000000.0

	# Initialise the error indicator 
	# (used for adaptive refinement)
	if grid.get_refine_type(node_id1,node_id3) == RefineSet.interface_base_edge or \
	   grid.get_refine_type(node_id1,node_id3) == RefineSet.base_edge:

		grid.set_error_indicator(node_id1, node_id3, error_indicator)
		grid.set_error_indicator(node_id3, node_id1, error_indicator)

	if grid.get_refine_type(node_id1,node_id4) == RefineSet.interface_base_edge or \
	   grid.get_refine_type(node_id1,node_id4) == RefineSet.base_edge:

		grid.set_error_indicator(node_id1, node_id4, error_indicator)
		grid.set_error_indicator(node_id4, node_id1, error_indicator)

	if grid.get_refine_type(node_id2,node_id3) == RefineSet.interface_base_edge or \
	   grid.get_refine_type(node_id2,node_id3) == RefineSet.base_edge:

		grid.set_error_indicator(node_id2, node_id3, error_indicator)
		grid.set_error_indicator(node_id3, node_id2, error_indicator)

	if grid.get_refine_type(node_id2,node_id4) == RefineSet.interface_base_edge or \
	   grid.get_refine_type(node_id2,node_id4) == RefineSet.base_edge:

		grid.set_error_indicator(node_id2, node_id4, error_indicator)
		grid.set_error_indicator(node_id4, node_id2, error_indicator)    
  
	return mid_id


def add_triangle3(grid, region, node_id1, node_id2, node_id3, parameter):
	"""Refine a triangle along the given base edge""" 

	# Find the nodes at the endpoints of the edge between node_id1 and node_id2
	if grid.is_in(node_id1):
		node1 = grid.get_node(node_id1)

	if grid.is_in(node_id2):
		node2 = grid.get_node(node_id2)
		
	# Record at which refinement level the new node was added
	refine_level = grid.get_refine_level(node_id1, node_id2)+1

	node_type = NodeSet.full_node

	# Record number of data of neighouring triangles
#	if parameter.get_indicator() == IndicatorType.auxiliary:
#		if parameter.get_indicator_parameter()[6]:		
#			if node_id1 < node_id2:
#				no_data = record_no_data_edge(grid, node_id1, node_id2, node_id3)
#			else:
#				no_data = record_no_data_edge(grid, node_id2, node_id1, node_id3)

	# Add the midpoint and split the edge between node1 and node2
	mid_node = split_edge(grid, node1, node2, node_type, parameter)
	mid_id = mid_node.get_node_id()

	# Add the edges going from the midpoint to node_id3
	grid.add_edge(mid_id, node_id3)
	grid.set_location(mid_id, node_id3, DomainSet.interior)
	grid.set_refine_type(mid_id, node_id3, RefineSet.not_base_edge)
	grid.set_refine_level(mid_id, node_id3, refine_level)

	grid.add_edge(node_id3, mid_id)
	grid.set_location(node_id3, mid_id, DomainSet.interior)
	grid.set_refine_type(node_id3, mid_id, RefineSet.not_base_edge)
	grid.set_refine_level(node_id3, mid_id, refine_level)

	# Update number of data of neighouring triangles
#	if parameter.get_indicator() == IndicatorType.auxiliary:
#		if parameter.get_indicator_parameter()[6]:
#			update_no_data(grid, no_data, mid_id, node_id1, node_id2, node_id3)

	# Update the connections
	
	# Remove the old connections
	grid.delete_connection(node_id1, node_id2)
	grid.delete_connection(node_id2, node_id1)

	# Add connections between the midpoint and three vertices
	grid.add_connection(mid_id, mid_id)
	grid.add_connection(mid_id, node_id1)
	grid.add_connection(mid_id, node_id2)
	grid.add_connection(mid_id, node_id3)
	grid.add_connection(node_id1, mid_id)
	grid.add_connection(node_id2, mid_id)
	grid.add_connection(node_id3, mid_id)

	# Update the refinement level to record when the triangle was added
	# (used mainly for adaptive refinement)
	grid.set_refine_level(node_id1, node_id3, refine_level)
	grid.set_refine_level(node_id3, node_id1, refine_level) 
	grid.set_refine_level(node_id2, node_id3, refine_level)
	grid.set_refine_level(node_id3, node_id2, refine_level)

	# Update the base types of the external edges
	set_base_type(grid, node_id1, node_id3)
	set_base_type(grid, node_id3, node_id1) 
	set_base_type(grid, node_id2, node_id3)
	set_base_type(grid, node_id3, node_id2)

	# Skip error indicators for auxiliary problems
	if parameter.get_local():
		return mid_id

	# Initialise the error indicator 
	# (used for adaptive refinement)
	error_indicator = 1000000.0

	if grid.get_refine_type(node_id1,node_id3) == RefineSet.interface_base_edge or \
	   grid.get_refine_type(node_id1,node_id3) == RefineSet.base_edge:

		grid.set_error_indicator(node_id1, node_id3, error_indicator)
		grid.set_error_indicator(node_id3, node_id1, error_indicator)

	if grid.get_refine_type(node_id2,node_id3) == RefineSet.interface_base_edge or \
	   grid.get_refine_type(node_id2,node_id3) == RefineSet.base_edge:

		grid.set_error_indicator(node_id2, node_id3, error_indicator)
		grid.set_error_indicator(node_id3, node_id2, error_indicator)  

	return mid_id
 

def add_triangle2(grid, node_id1, node_id2):
	"""Refine a triangle along the given base edge""" 

	# Find the nodes at the endpoints of the edge between node_id1 and node_id2
	if grid.is_in(node_id1):
		node1 = grid.get_node(node_id1)

	if grid.is_in(node_id2):
		node2 = grid.get_node(node_id2)

	# Add the midpoint and split the edge between node1 and node2
	mid_node = split_edge(grid, node1, node2, node_type)
	mid_id = mid_node.get_node_id()

	# May need to divide data regions for parallel implementations

	# Update the connections
	
	# Remove the old connections
	grid.delete_connection(node_id1, node_id2)
	grid.delete_connection(node_id2, node_id1)

	# Add connections between the midpoint and two vertices
	grid.add_connection(mid_id, mid_id)
	grid.add_connection(mid_id, node_id1)
	grid.add_connection(mid_id, node_id2)
	grid.add_connection(node_id1, mid_id)
	grid.add_connection(node_id2, mid_id)

	return mid_id


def find_coarse_triangle(grid,endpt1,endpt2,endpt3,endpt4):
	""" Find the triangle with lower refinement level """

	# Initialise refinement level for triangle 3 and 4
	refine_level_3 = 0
	refine_level_4 = 0

	# Find the refinement level for triangle 3
	if grid.get_refine_type(endpt1,endpt3) == RefineSet.base_edge:
		refine_level_3 = grid.get_refine_level(endpt1,endpt3)

	elif grid.get_refine_type(endpt2,endpt3) == RefineSet.base_edge:
		refine_level_3 = grid.get_refine_level(endpt2,endpt3)

	elif grid.get_refine_type(endpt1,endpt3) == RefineSet.interface_base_edge:
		refine_level_3 = grid.get_refine_level(endpt1,endpt3)

		if grid.get_refine_type(endpt2,endpt3) == RefineSet.interface_base_edge:
			refine_level_3 = min(refine_level_3,grid.get_refine_level(endpt2,endpt3))

	elif grid.get_refine_type(endpt2,endpt3) == RefineSet.interface_base_edge:
		refine_level_3 = grid.get_refine_level(endpt2,endpt3)
	else:
		# Set refinement level from the interface base edge if no other base edges are found
		refine_level_3 = grid.get_refine_level(endpt1,endpt2)

	# Find the refinement level for triangle 4
	if grid.get_refine_type(endpt1,endpt4) == RefineSet.base_edge:
		refine_level_4 = grid.get_refine_level(endpt1,endpt4)

	elif grid.get_refine_type(endpt2,endpt4) == RefineSet.base_edge:
		refine_level_4 = grid.get_refine_level(endpt2,endpt4)

	elif grid.get_refine_type(endpt1,endpt4) == RefineSet.interface_base_edge:
		refine_level_4 = grid.get_refine_level(endpt1,endpt4)

		if grid.get_refine_type(endpt2,endpt4) == RefineSet.interface_base_edge:
			refine_level_4 = min(refine_level_4,grid.get_refine_level(endpt2,endpt4))

	elif grid.get_refine_type(endpt2,endpt4) == RefineSet.interface_base_edge:
		refine_level_4 = grid.get_refine_level(endpt2,endpt4)
	else:
		# Set refinement level from the interface base edge if no other base edges are found
		refine_level_4 = grid.get_refine_level(endpt1,endpt2)
	"""
	print endpt1,endpt2,endpt3,endpt4
	print "type:", RefineSet.base_edge, RefineSet.interface_base_edge
	print refine_level_3, refine_level_4
	print "type 1:",grid.get_refine_type(endpt1,endpt3), grid.get_refine_type(endpt2,endpt3)
	print grid.get_refine_level(endpt1,endpt3), grid.get_refine_level(endpt2,endpt3)
	print "type 2:",grid.get_refine_type(endpt1,endpt4), grid.get_refine_type(endpt2,endpt4)
	print grid.get_refine_level(endpt1,endpt4), grid.get_refine_level(endpt2,endpt4)
	"""
	# Print error message if refinement level between 
	# two neighouring triangles differs larger than 2
	if abs(refine_level_3-refine_level_4) > 2:
		print("refine difference between triangles larger than 2")

#	if str(endpt1) == "-1000" and str(endpt2)=="20" and str(endpt3)=="19" and str(endpt4)=="-999":
#		femPlot2D.plot_fem_grid_2D(grid, None, True)

	# Assign the triangle with lower and higher refinement levels to the references
	if refine_level_3 < refine_level_4:
		endpt_low = endpt3
		endpt_high = endpt4
	elif refine_level_3 > refine_level_4:
		endpt_low = endpt4
		endpt_high = endpt3
	else:
		# Return if two sides of the interface base edges have the same refinement level
		print("refinement level is the same")
#		print(endpt1,endpt2,endpt3,endpt4)
#		femPlot2D.plot_fem_grid_2D(grid, None, True)
		return

	return endpt_low, endpt_high


def refine_coarse_triangle(grid, region, endpt1, endpt2, endpt_low, parameter):
	""" Refine coarse triangle for interface base edge """

	# Find the base edge of low refinement level triangle
	if grid.get_refine_type(endpt1,endpt_low) == RefineSet.base_edge:

		# Split the base edge if it ends at end point 1
		mid_node_id = split_triangle(grid, region, grid.get_edge(endpt1,endpt_low),parameter)

	elif grid.get_refine_type(endpt2,endpt_low) == RefineSet.base_edge:

		# Split the base edge if it ends at end point 2
		mid_node_id = split_triangle(grid, region, grid.get_edge(endpt2,endpt_low),parameter)

	elif grid.get_refine_type(endpt1,endpt_low) == RefineSet.interface_base_edge and \
		grid.get_refine_type(endpt2,endpt_low) == RefineSet.interface_base_edge:

		# Compare the refinement level of two interface base edges
		if grid.get_refine_level(endpt2,endpt_low) > grid.get_refine_level(endpt1,endpt_low):

			# Refine the edge with lower refinement level
			mid_node_id = split_triangle(grid, region, grid.get_edge(endpt1,endpt_low),parameter)

		elif grid.get_refine_level(endpt2,endpt_low) < grid.get_refine_level(endpt1,endpt_low):

			# Refine the edge with lower refinement level
			mid_node_id = split_triangle(grid, region, grid.get_edge(endpt2,endpt_low),parameter)
		else:
			print("two interface base edges have the same refine level")

	elif grid.get_refine_type(endpt1,endpt_low) == RefineSet.interface_base_edge:

		# Split the base edge if it ends at end point 1
		mid_node_id = split_triangle(grid, region, grid.get_edge(endpt1,endpt_low),parameter)

	elif grid.get_refine_type(endpt2,endpt_low) == RefineSet.interface_base_edge:
	
		# Split the base edge if it ends at end point 2
		mid_node_id = split_triangle(grid, region, grid.get_edge(endpt2,endpt_low),parameter)

	else:
		# Return no base edge is found in this triangle
		print("no base edge found")
		return

	return mid_node_id
  

def split_triangle(grid, region, edge, parameter):
	""" Split the triangles with the given edge """ 

	# Get the ids of the endpoints of the edge
	endpt1 = edge.get_endpt1()
	endpt2 = edge.get_endpt2()
	
	# Return if the given edge is not found in the grid
	if not grid.is_edge(endpt1,endpt2):
		return

	# Make a list of all of the edges joined to endpt1 (this is used
	# as we only want to find one version of each triangle)
	edgestar = list()
	for endpt in endpt_iterator(grid, endpt1):
		edgestar.append(endpt)
		
	# Loop over all of the endpoints joined to endpt1
	n = len(edgestar)
	for i in range(n):
		
		# If the endpoint is also joined to endpt2, we have found a triangle
		if grid.is_edge(edgestar[i], endpt2):
			
			# See if we can find a second, different, triangle
			
			# Loop over other endpoints joined to endpt1
			for j in range(i+1, n):
				
				# If the new endpoints is also joined to endpt2, we have found
				# two triangle that share the edge between endpt1 and endpt2
				if grid.is_edge(edgestar[j], endpt2):
					
					# Check if the edge is an interface base edge
					if grid.get_refine_type(endpt1,endpt2) == RefineSet.interface_base_edge:

						endpt_low, endpt_high = find_coarse_triangle(grid,endpt1,endpt2,edgestar[i],edgestar[j])

						if endpt_low is None and endpt_high is None:
							print("Warning: coarse triangle not found")
							print(endpt1,endpt2,edgestar[i],edgestar[j])

						mid_node_id = refine_coarse_triangle(grid, region, endpt1,endpt2,endpt_low, parameter)

						# Refine the interface base edge
						mid_id = add_triangle4(grid, region, endpt1, endpt2, mid_node_id, endpt_high, parameter)

						return mid_id
						
					# Refine those two triangles
					mid_id = add_triangle4(grid, region, endpt1, endpt2, edgestar[i], edgestar[j], parameter)

					return mid_id
					
			# If only one triangle was found, refine that triangle
			mid_id = add_triangle3(grid, region, endpt1, endpt2, edgestar[i], parameter)

			return mid_id
	
	# Sometime in the parallel implementation no triangle is found, so refine
	# the edge
	mid_id = add_triangle2(grid, endpt1, endpt2)

	return mid_id


def check_domain(coord, domain):
	""" Check if a coordinate is inside the domain """

	dim = len(coord)

	# Go through each given domain
	for i in range(0, len(domain)):
		
		in_domain = True

		for j in range(0, dim):

			if not (coord[j] >= domain[i][j] and \
					coord[j] <= domain[i][j+dim]):
				in_domain = False

		if in_domain:
			return in_domain
	
	return False


def build_base_edges(grid, parameter):
	"""Build a list of base edges"""

	dataset = parameter.get_dataset()

	# Initialise the bese edge list
	base_edges = list()
	"""
	# Domains that the refinement occurs
	domain = []
	if dataset == 1:
		#domain = [[-0.00000001,-0.00000001,1.00000001,1.00000001]]
		domain = [[0.0499999999,0.0499999999,0.9499999999,0.9499999999]]
	"""
	# Loop through each 
	for node in node_iterator(grid):
		node_id = node.get_node_id()
		for endpt in endpt_iterator(grid, node_id):
			if node_id < endpt:
				end_node = grid.get_node_ref(endpt)

				# If the refinement type is a base type
				if grid.get_refine_type(node_id, endpt) == RefineSet.base_edge or \
					grid.get_refine_type(node_id, endpt) == RefineSet.interface_base_edge:
					
					"""
					if dataset == 1:
						# If one node lies inside the required domain
						if not (check_domain(node.get_coord(), domain) or \
							check_domain(end_node.get_coord(), domain)):
							continue
					"""
					# Add the edge to the base edge list
					base_edges.append(grid.get_edge(node_id, endpt))

		
	# Return the list of base edges
	return base_edges


def select_refine_edges(refine_grid, base_edges, no_refine):
	""" Select edges with higher error indicators to refine """

	# At least one base edge will be refined
	if no_refine == 0:
		no_refine = 1

	# Initialise a list for error indicators
	error_indicator_list = []

	# Iterate through the base edges
	for i in range(0, len(base_edges)):

		# Get the ids of the endpoints of the edge
		endpt1 = base_edges[i].get_endpt1()
		endpt2 = base_edges[i].get_endpt2()

		# Add error indicator of the edge
		error_indicator = refine_grid.get_error_indicator(endpt1, endpt2)
		error_indicator_list.append(error_indicator)

	# Sort the edges by error indicators in decending order
	error_indicator_list, refine_edges = \
		zip(*sorted(zip(error_indicator_list, base_edges), reverse=True))

	# Return edges with higher error indicators
	return refine_edges[:no_refine]

	
def adaptive_refinement_all(grid, region, parameter):
	""" Adaptively refine the 2D grid in one iteration """

	# Make a copy of the current grid
	refine_grid = deepcopy(grid)
	
	# Apply m sweeps of refinement
	for i in range(parameter.get_max_no()):
		
		# Find all of the base edges
		base_edges = build_base_edges(refine_grid, parameter)
	
		# Stop refinements when no edge needs to be refined
		if len(base_edges) == 0:
			return refine_grid

		# Loop over the base edges and split or refine the triangle along that
		# base edge
		for edge in base_edges:
			if edge.get_error_indicator() > parameter.get_error_tol():
				split_triangle(refine_grid, region, edge, parameter)

	# Return the refined grid
	return refine_grid


def adaptive_refinement_iterative(grid, region, parameter):
	""" Adaptively refine the 2D grid iteratively """
	
	# Refine ratio
	# 0.1 means 10% each iteration
	refine_ratio = 0.1

	# Maximum ratio of edges refined in each sweep
	# 1 Means stops when nodes double
	max_refine_ratio = 1

	# Make a copy of the current grid
	refine_grid = deepcopy(grid)
	femPlot2D.plot_fem_grid_2D(grid, parameter, True)
	# Apply m sweeps of refinement
	for i in range(0, parameter.get_max_no()):
		
		# The number of nodes
		threshold = int(refine_grid.get_no_nodes()*(1+max_refine_ratio))

		# Number of refinement each iteration
		no_refine = int(refine_grid.get_no_nodes()*refine_ratio)

		# Keep refining if not enough new nodes
		while refine_grid.get_no_nodes() < threshold:

			# Find all of the base edges
			base_edges = build_base_edges(refine_grid, parameter)

			# Find all the base edges that will be refined this iteration
			refine_edges = select_refine_edges(refine_grid, base_edges, no_refine)

			if_refine = False

			# Loop over the base edges and split or refine the triangle along that
			# base edge
			for j in range(0, len(refine_edges)):

				edge = refine_edges[j]

				# If the edge needs refinement
				if edge.get_error_indicator() > parameter.get_error_tol():

					# Split the edge
					split_triangle(refine_grid, region, edge, parameter)

					if_refine = True

					# Breaks when number of nodes doubles
					if refine_grid.get_no_nodes() >= threshold:
						break

			# Break the loop if no base edge is up for refinement
			if not if_refine:
				break

	# Return the refined grid
	return refine_grid


def adaptive_refinement(grid, region, parameter, refine_approach=None):
	""" Adpative refinement of the grid """
	
	# If using infity error indicator, just refine all
	if refine_approach is None:
		
		# Adaptive refinement approach
		refine_approach = parameter.get_refine_approach()

		if parameter.get_indicator() == 1:
			approach = 1

	# Refine all base edges above error tolerance
	if refine_approach == 1:
		refine_grid = adaptive_refinement_all(grid, region, parameter)
	
	# Refine a portion of base edges with high indicator iteratively
	elif refine_approach == 2:
		refine_grid = adaptive_refinement_iterative(grid, region, parameter)

	return refine_grid