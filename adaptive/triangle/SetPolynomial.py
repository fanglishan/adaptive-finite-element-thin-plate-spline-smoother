#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-04-26 10:33:54
@Last Modified time: 2022-11-05 20:46:07

This class contains routines to create polynomials using data points 
in the triangle.

Adapted from Stals' FEM code.

"""


# Import libraries
from adaptive.triangle.LinearPolynomial import LinearPolynomial

from math import fabs
import numpy as np


def set_polynomial_linear_2D(node1, node2, node3):
	""" Construct a 2D linear polynomial """
	
	# Check the coordinates are two dimensional
	assert node1.get_dim() == 2 and node2.get_dim() == 2 \
		and node3.get_dim() == 2, \
			"the triangle coordinates must be two dimensional"
			
	# Get the coordinates of the three vertices
	coord1 = node1.get_coord()
	coord2 = node2.get_coord()
	coord3 = node3.get_coord()
	
	# Break down the information to make the code easier to read
	x1 = coord1[0]
	y1 = coord1[1]
	x2 = coord2[0]
	y2 = coord2[1]
	x3 = coord3[0]
	y3 = coord3[1]

	# Find h
	division = (y1-y2)*(x2-x3)-(y2-y3)*(x1-x2);
	
	# Avoid division by zero errors
	assert fabs(division) > 1.0E-12, "divide by zero in set_polynomial_linear_2D"

	# Find the polynomial coefficients
	poly = LinearPolynomial()
	poly.set_const((x3*y2 - y3*x2)/division)
	poly.set_x((y3-y2)/division)
	poly.set_y((x2-x3)/division)
	
	# Return the polynomial
	return poly


def get_solution_polynomial_linear_2D(node1, node2, node3, value1, value2, value3):
	""" Construct a 2D linear polynomial """
	
	# Check the coordinates are two dimensional
	assert node1.get_dim() == 2 and node2.get_dim() == 2 \
		and node3.get_dim() == 2, \
			"the triangle coordinates must be two dimensional"
	
	# Get the coordinates of the three vertices
	coord1 = node1.get_coord()
	coord2 = node2.get_coord()
	coord3 = node3.get_coord()
	
	matrix = np.array([[coord1[0],coord1[1],1.0],[coord2[0],coord2[1],1.0],[coord3[0],coord3[1],1.0]])

	vector = np.array([[value1],[value2],[value3]])

	# Solve system of equations, switch to least square if singular
	try:
		coef = np.linalg.solve(matrix, vector)
	except Exception:
		coef = np.linalg.lstsq(matrix, vector)[0]

	# Find the polynomial coefficients
	poly = LinearPolynomial()
	poly.set_const(coef[2][0])
	poly.set_x(coef[0][0])
	poly.set_y(coef[1][0])

	# Return the polynomial
	return poly