#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-04-26 10:36:42
@Last Modified time: 2022-03-29 11:26:41

This class offers a number of numerical quadrature routines to integrate
a function over a triangle. These quadrature rules are exact for quadratic
polynomials.

Adapted from Stals' FEM code.

"""

# Import libraries
from numpy import array
from math import fabs


def calc_area(coord1, coord2, coord3):
	""" Calculate the area of the triangle """

	return 0.5 * fabs((coord2[0]-coord1[0])*(coord3[1]-coord1[1])
		- (coord3[0]-coord1[0])*(coord2[1]-coord1[1]))     


def quadratic_integrate(poly1, poly2, node1, node2, node3): 
	""" Evaluate the approximate integral over the given triangle """
	
	# Check the coordinates are two dimensional
	assert node1.get_dim() == 2 and node2.get_dim() == 2 \
		and node3.get_dim() == 2, \
			"the triangle coordinates must be two dimensional"
			
	# Get the coordinates of the 3 vertices
	coord1 = array(node1.get_coord())
	coord2 = array(node2.get_coord())
	coord3 = array(node3.get_coord())
	
	# Find the mid point on edge 1-2
	mid_point1 = (coord1+coord2)/2.0
	x1 = mid_point1[0]
	y1 = mid_point1[1]

	# Find the mid point on edge 2-3
	mid_point2 = (coord2+coord3)/2.0
	x2 = mid_point2[0]
	y2 = mid_point2[1]

	# Find the mid point on edge 3-1
	mid_point3 = (coord3+coord1)/2.0
	x3 = mid_point3[0]
	y3 = mid_point3[1]
		
	poly_product = poly1.eval(x1, y1) * poly2.eval(x1, y1) + \
				   poly1.eval(x2, y2) * poly2.eval(x2, y2) + \
				   poly1.eval(x3, y3) * poly2.eval(x3, y3)

	# Apply the quadrature rule    
	return  poly_product / 3.0 * \
		calc_area(coord1, coord2, coord3)
		

def quadratic_integrate_single(poly, node1, node2, node3): 
	""" Evaluate the approximate integral over the given triangle """
	
	# Check the coordinates are two dimensional
	assert node1.get_dim() == 2 and node2.get_dim() == 2 \
		and node3.get_dim() == 2, \
			"the triangle coordinates must be two dimensional"
			
	# Get the coordinates of the 3 vertices
	coord1 = array(node1.get_coord())
	coord2 = array(node2.get_coord())
	coord3 = array(node3.get_coord())
	
	# Find the mid point on edge 1-2
	mid_point1 = (coord1+coord2)/2.0
	x1 = mid_point1[0]
	y1 = mid_point1[1]

	# Find the mid point on edge 2-3
	mid_point2 = (coord2+coord3)/2.0
	x2 = mid_point2[0]
	y2 = mid_point2[1]

	# Find the mid point on edge 3-1
	mid_point3 = (coord3+coord1)/2.0
	x3 = mid_point3[0]
	y3 = mid_point3[1]
		
	poly_product = poly.eval(x1, y1) + poly.eval(x2, y2) + poly.eval(x3, y3)

	# Apply the quadrature rule    
	return  poly_product / 3.0 * calc_area(coord1, coord2, coord3)

