#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-05-05 21:27:59
@Last Modified time: 2023-03-11 10:39:19

This class contains routines to build a 2D local domain based on the given triangle

"""

# Import libraries
from grid.Grid import Grid
from grid.Node import Node
from grid.NodeID import NodeID
from grid.Edge import DomainSet, RefineSet
from grid.NodeTable import node_iterator
from grid.EdgeTable import endpt_iterator
from adaptive.triangle.AdaptiveRefinementAuxiliary import adaptive_refinement, split_triangle
from adaptive.triangle.BuildEquation import initialise_connections_only
from adaptive.triangle.RefinementFunction import split_edge_only
from adaptive.triangle.TriangleFunction import find_triangle_pairs
from adaptive.triangle.UpdateRefineTriangle import split_edge_update_connections
from build.BuildSquare import add_edge
from data.DataThinning import determine_data_ratio_2D, count_data_local_grid_2D
from parameter.Definition import NodeSet, BoundaryType, RefineSet, IndicatorType
import plot.FemPlot2D as femPlot2D
import plot.TpsfemPlot2D as tpsfemPlot2D

from copy import deepcopy
import math
import matplotlib.pyplot as plt


def add_connections(grid, local_grid, node_id1, node_id2=None):
	""" Add connections from node 1 to node 2 """

	if node_id2 is None:
		node_id2 = node_id1

	if not local_grid.is_connection(node_id1, node_id2):
		local_grid.add_connection(node_id1, node_id2)
		
		if not grid.is_connection(node_id1, node_id2):
			print("Warning: no connection found")
			A_value = 0.0
			L_value = 0.0
			G_values = [0.0, 0.0]
		else:
			A_value = grid.get_matrix_A_value(node_id1, node_id2)
			L_value = grid.get_matrix_L_value(node_id1, node_id2)
			G_values = grid.get_matrix_G_value(node_id1, node_id2)

		local_grid.set_matrix_A_value(node_id1, node_id2, A_value)
		local_grid.set_matrix_L_value(node_id1, node_id2, L_value)
		local_grid.set_matrix_G_value(node_id1, node_id2, G_values)



def add_edge_connections(grid, local_grid, node_id1, node_id2, refine_type, location):
	""" Add edges and connections """

	add_edge(local_grid, node_id1, node_id2, refine_type, location)

	add_connections(grid, local_grid, node_id1, node_id2)
	add_connections(grid, local_grid, node_id2, node_id1)


def add_node_connections(grid, local_grid):
	""" Add connections for nodes """

	for node in node_iterator(local_grid):
		add_connections(grid, local_grid, node.get_node_id())


def set_boundary_edges(local_grid):
	""" Set location for boundary edges """

	bnd = DomainSet.boundary

	# Iterate through each edge between boundary nodes
	for node in node_iterator(local_grid):
		node_id = node.get_node_id()
		for endpt in endpt_iterator(local_grid, node.get_node_id()):
			if node_id > endpt:
				if local_grid.get_slave(node_id) and local_grid.get_slave(endpt):

					# Set boundary if only one triangle is found
					node3, node4 = find_triangle_pairs(local_grid, node_id, endpt)

					if node3 is None or node4 is None:
						local_grid.set_location(node_id, endpt, bnd)
						local_grid.set_location(endpt, node_id, bnd)



def set_edge_no_data(grid, local_grid):
	""" Set location for boundary edges """

	# Iterate through each edge between boundary nodes
	for node in node_iterator(local_grid):
		node_id = node.get_node_id()
		for endpt in endpt_iterator(local_grid, node.get_node_id()):
			if node_id < endpt:

				no_data = grid.get_no_data(node_id, endpt)
				local_grid.set_no_data(node_id, endpt, no_data)



def update_refine_level_type(grid, local_grid):
	""" Update the refine level and refine type of boundary edges """
	
	# Definition parameters
	bnd = DomainSet.boundary
	base_edge = RefineSet.base_edge
	not_base_edge = RefineSet.not_base_edge
	interface_base_edge = RefineSet.interface_base_edge

	# Iterate through each edge
	for node in node_iterator(local_grid):
		node_id = node.get_node_id()
		for endpt in endpt_iterator(local_grid, node.get_node_id()):

			# Update refine level
			local_grid.set_refine_level(node_id, endpt, grid.get_refine_level(node_id, endpt))

			# Correct refine type of boundary edges
			if local_grid.get_location(node_id, endpt) == bnd and \
				local_grid.get_refine_type(node_id, endpt) == interface_base_edge:
			
				for endpt_2 in endpt_iterator(local_grid, node_id):
					if local_grid.is_edge(endpt, endpt_2):

						# Calculate distance between nodes
						coord1 = local_grid.get_coord(node_id)
						coord2 = local_grid.get_coord(endpt)
						coord3 = local_grid.get_coord(endpt_2)

						h1 = (coord1[0]-coord2[0])**2 + (coord1[1]-coord2[1])**2
						h2 = (coord2[0]-coord3[0])**2 + (coord2[1]-coord3[1])**2
						h3 = (coord1[0]-coord3[0])**2 + (coord1[1]-coord3[1])**2
		
						# Set as base edge if it is the longest
						# Otherwise set as not base edge
						if h1 > h2 and h1 > h3:
							local_grid.set_refine_type(node_id, endpt, base_edge)
						else:
							local_grid.set_refine_type(node_id, endpt, not_base_edge)



def build_triangle_pair_grid(grid, node1, node2, node3, node4, no_layers=0):
	""" Build a local grid consisting a triangle pair """

	# Initialise a new local grid
	local_grid = Grid()
	
	# Set 2D grid
	local_grid.set_dim(2)

	# Create a list for triangle pair nodes
	inner_nodes = [node1, node2]

	if node3 is not None:
		inner_nodes.append(node3)
	if node4 is not None:
		inner_nodes.append(node4)

	node_id1 = node1.get_node_id()
	node_id2 = node2.get_node_id()

	# Add triangle nodes to the local grid
	for node in inner_nodes:
		local_grid.add_node(deepcopy(node))

	# Iterate through nodes of the triangle pair
	for node in inner_nodes:

		# If local grid is only a triangle pair
		if no_layers == 1:
			local_grid.set_slave(node.get_node_id(), True)

		# If the local grid contains multiple layers
		else:
			local_grid.set_slave(node.get_node_id(), node.get_slave())

	# Add edge for node 1 and node 2
	add_edge_connections(grid, local_grid, node_id1, node_id2, grid.get_refine_type(node_id1, \
			 node_id2), grid.get_location(node_id1, node_id2))

	# Add edge for node 4 with node 1 and node 2
	if node3 is not None:
		node_id3 = node3.get_node_id()
		add_edge_connections(grid, local_grid, node_id1, node_id3, grid.get_refine_type(node_id1, \
			 node_id3), grid.get_location(node_id1, node_id3))
		add_edge_connections(grid, local_grid, node_id2, node_id3, grid.get_refine_type(node_id2, \
			 node_id3), grid.get_location(node_id2, node_id3))

	# Add edge for node 4 with node 1 and node 2
	if node4 is not None:
		node_id4 = node4.get_node_id()
		add_edge_connections(grid, local_grid, node_id1, node_id4, grid.get_refine_type(node_id1, \
			 node_id4), grid.get_location(node_id1, node_id4))
		add_edge_connections(grid, local_grid, node_id2, node_id4, grid.get_refine_type(node_id2, \
			 node_id4), grid.get_location(node_id2, node_id4))

	return local_grid, inner_nodes


def expand_local_grid(grid, local_grid, inner_nodes, no_layers):
	""" Expand given local grid to include more layers """

	bnd = DomainSet.boundary

	# Iterate and add nodes in each layer
	for i in range(1, no_layers):

		# Initialise a list for outer nodes
		outer_nodes = []

		# Iterate through inner nodes' endpoints
		for node in inner_nodes:
			for endpt in endpt_iterator(grid, node.get_node_id()):

				# If it has not been added to the local grid
				if not local_grid.is_in(endpt):

					# Obtain end point node
					endpt_node = deepcopy(grid.get_node(endpt))

					# Add it to the grid
					local_grid.add_node(endpt_node)

					# Add it to the outer nodes
					outer_nodes.append(endpt_node)

					# Set slave nodes
					if i == no_layers - 1:
						local_grid.set_slave(endpt, True)

		# If no outer node is found, inner nodes become outer
		# nodes, iterate to check any non slave nodes
		if len(outer_nodes) < 0:
			for node in inner_nodes:
				if not local_grid.get_slave(node.get_node_id()):
					print("Warning: SolveRefineTraingle")
			break

		# Iterate through edges between outer nodes
		for i in range(0, len(outer_nodes)):
			node_id = outer_nodes[i].get_node_id()
			for j in range(i+1, len(outer_nodes)):
				outer_node_id = outer_nodes[j].get_node_id()

				# Add boundary edges if exist
				if grid.is_edge(node_id, outer_node_id):
					add_edge_connections(grid, local_grid, node_id, outer_node_id, \
						grid.get_refine_type(node_id, outer_node_id), \
						grid.get_location(node_id, outer_node_id))
		
		# Iterate through edges between outer nodes and inner nodes
		for i in range(0, len(outer_nodes)):
			node_id = outer_nodes[i].get_node_id()
			for j in range(0, len(inner_nodes)):
				inner_node_id = inner_nodes[j].get_node_id()

				# Add edges if exist
				if grid.is_edge(node_id, inner_node_id):
					add_edge_connections(grid, local_grid, node_id, inner_node_id, \
						grid.get_refine_type(node_id, inner_node_id), \
						grid.get_location(node_id, inner_node_id))

		# Outer nodes become inner nodes for the next iteration
		inner_nodes = outer_nodes


def build_local_grid_point_2d(grid, node, parameters):
	""" Build a local 2D grid starting from a given node """

	# Layers of local grid
	no_layers = parameters.get_indicator_parameters()[0]

	# Initialise a new local grid
	local_grid = Grid()
	
	# Set 2D grid
	local_grid.set_dim(2)

	# Create a list for triangle nodes
	inner_nodes = [deepcopy(node)]

	local_grid.add_node(node)

	# Expand the local grid to include extra layers
	expand_local_grid(grid, local_grid, inner_nodes, no_layers)

	# Update refine level
	for node in node_iterator(local_grid):
		node_id = node.get_node_id()
		for endpt in endpt_iterator(local_grid, node.get_node_id()):
			local_grid.set_refine_level(node_id, endpt, grid.get_refine_level(node_id, endpt))

	set_boundary_edges(local_grid)

#	plot2d.plot_tpsfem_grid_base(local_grid)

	return local_grid



def build_local_grid_2D(grid, region, node1, node2, node3, node4, parameter, \
						no_layers=2, refine_choice=1, uni_refine=0, count_data=False):
	""" Build a local 2D grid starting from the given edge """

	# Stick with refining the base edge for now
	refine_choice = 2

	node_id1 = node1.get_node_id()
	node_id2 = node2.get_node_id()

	local_grid, inner_nodes = build_triangle_pair_grid(grid, node1, node2, node3, node4, no_layers)

	# Expand the local grid to include extra layers
	expand_local_grid(grid, local_grid, inner_nodes, no_layers)

	# Add connections for nodes
	add_node_connections(grid, local_grid)

	# Set boundary edges
	set_boundary_edges(local_grid)

	# Update refine level and refine type
	update_refine_level_type(grid, local_grid)

	# Reset the node counter so node ids won't be duplicated
	local_grid.reset_counter(-1000)
	
	mid_node_id = None

	if count_data:
		local_data_no = count_data_local_grid_2D(grid, local_grid)


	# Determine the data ratio for the local grid
	# The ratio will depend on the approximate number 
	# of data inside the local grid
	if parameter.get_indicator() == IndicatorType.auxiliary:
		if parameter.get_indicator_parameter()[6]:
			set_edge_no_data(grid, local_grid)
			determine_data_ratio_2D(local_grid, region, parameter)

#	femPlot2D.plot_fem_grid_2D(local_grid, None, True)

	# Do not do any refine
	if refine_choice == 0:
		pass

	# Refine the base edge of the triangle pair in a standard way
	# This process might lead to more than one refinement
	elif refine_choice == 1:

		grid_repo = deepcopy(local_grid)
		try:
			initialise_connections_only(local_grid)
			mid_node_id = split_triangle(local_grid, None, local_grid.get_edge(node_id1, node_id2), parameter)

		except Exception as e:
			print("Caught:", e)	
#			femPlot2D.plot_fem_grid_2D(grid_repo, parameter, True)
			mid_node_id = split_edge_only(grid_repo, grid_repo.get_edge(node_id1, node_id2), parameter)
#			femPlot2D.plot_fem_grid_2D(grid_repo, parameter, True)
			local_grid = grid_repo
			
	# Only split the given edge, but still build equation for the local domain
	elif refine_choice == 2:
		mid_node_id = split_edge_only(local_grid, local_grid.get_edge(node_id1, node_id2), parameter)
		
	# Only split the given edge and no build equation for the local domain
	# This is more efficient, but can only used in the whole error indication stage
	# It cannot be used during the refine grid stage
	elif refine_choice == 3:

		# The implementation still needs to be validated
		# For now we don't use this routine
		print("This part should not be used")
		mid_node_id = split_edge_update_connections(local_grid, local_grid.get_edge(node_id1, node_id2), region, parameter)
	
	else:
		print("Warning: LocalGrid - unknown refine choice")


	# Do further refinement to increase the accuracy
	if refine_choice != 0:
		
		mid_node = local_grid.get_node_ref(mid_node_id)

		# The refinement of grids in the error indicators will be
		# automatically set as interior, set as the middle node
		# as the boundary node, but keep the interpolated value
		# as we want it to be interpolated solution before solving
		# Set the middle nodes values using the boundary function
		# to show the difference between the interpolated solution
		# and the true solution
		if grid.get_location(node_id1, node_id2) == DomainSet.boundary and \
			   parameter.get_boundary() == BoundaryType.Dirichlet:

			mid_node.set_slave(True)

			# Model functions
			function = parameter.get_function()
			coord = mid_node.get_coord()
			mid_node.set_value(function[0](coord))
			mid_node.set_d_values([function[1](coord), function[2](coord), function[3](coord)])


		# Do further uniform refinement to achieve higher accuracy
		refine_approach = 1

		for i in range(0, uni_refine):
			local_grid = adaptive_refinement(local_grid, None, parameter, refine_approach)

#	femPlot2D.plot_fem_grid_2D(local_grid, None, True)
	
	if count_data:
		return local_grid, mid_node_id, local_data_no
	else:
		return local_grid, mid_node_id
