#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-04-26 10:33:54
@Last Modified time: 2023-09-30 10:29:41

This class builds and stores the finite element matrix and load vector.
It also sets the Dirichlet boundary conditions.

Adapted from Stals' FEM code.

"""

# Import libraries
from adaptive.triangle.SetPolynomial import set_polynomial_linear_2D
from adaptive.triangle.Triangle import triangle_iterator_ref#, triangle_iterator
from adaptive.triangle.BuildEquation import set_slave_value, sum_data_matrix, \
	sum_data_load, sum_KTu_load, sum_stiffness, sum_gradient, \
	Poisson_tri_integrate_linear, initialise_connections, normalise_A_d 
from adaptive.triangle.TriangleIntegrateLinear import linear_integrate
from data.ObtainData import obtain_data
from data.DataThinning import record_data_2D
from fileio.WriteResult import write_line
from tpsfem.BuildBasis import obtain_node_index_2D
from parameter.Definition import BoundaryType, IndicatorType
from plot.DataPlot import plot_data_dist
from plot.FemPlot2D import plot_fem_grid_2D, plot_fem_grid_data_2D

from numpy import sqrt, zeros
from scipy.sparse import lil_matrix, csc_matrix
from random import random


def random_function():
	# Randomly return 1 or -1
	return 1.0 if random() < 0.5 else -1.0


def local_A_linear_2D(data_points, u_vector, node1, node2, node3, poly1, poly2, poly3):
	""" Find the data projection matrix A values """

	# Initialise data projection matrix and vector entries as zero
	data_project1 = 0.0
	data_project2 = 0.0
	data_project3 = 0.0
	data_vector = 0.0
	KTu_vector = 0.0
	
	# Iterate through each data point
	for i in range(0, len(data_points)):

		# Coodinates of the data point
		coord1 = data_points[i][0]
		coord2 = data_points[i][1]

		poly1_proj = poly1.eval(coord1, coord2)

		# Evaluate the contribution to the data projection matrix
		data_project1 += poly1_proj*poly1_proj
		data_project2 += poly1_proj*poly2.eval(coord1, coord2)
		data_project3 += poly1_proj*poly3.eval(coord1, coord2)

		data_vector += poly1_proj*data_points[i][2]

		KTu_vector += poly1_proj*u_vector[i]

	return data_project1, data_project2, data_project3, data_vector, KTu_vector


def local_L_linear_2D(node1, node2, node3, poly1, poly2, poly3, tri_integrate):
	""" Find the element stiffness matrix L values """

	# Evaluate the contribution to the local stiffness matrix
	local_stiffness1 = tri_integrate(poly1, poly1, node1, node2, node3)
	local_stiffness2 = tri_integrate(poly1, poly2, node1, node2, node3)
	local_stiffness3 = tri_integrate(poly1, poly3, node1, node2, node3)

	return local_stiffness1, local_stiffness2, local_stiffness3


def local_G_linear_2D(node1, node2, node3, poly1, poly2, poly3):
	""" Find the gradient matrix G values """

	# Evaluate the contribution to the local gradient matrix wrt x
	local_gx1 = linear_integrate(poly1, poly1.dx(), node1, node2, node3)
	local_gx2 = linear_integrate(poly1, poly2.dx(), node1, node2, node3)
	local_gx3 = linear_integrate(poly1, poly3.dx(), node1, node2, node3)
	
	# Evaluate the contribution to the local gradient matrix wrt y
	local_gy1 = linear_integrate(poly1, poly1.dy(), node1, node2, node3)
	local_gy2 = linear_integrate(poly1, poly2.dy(), node1, node2, node3)
	local_gy3 = linear_integrate(poly1, poly3.dy(), node1, node2, node3)

	return [local_gx1,local_gy1], [local_gx2,local_gy2], [local_gx3,local_gy3]


def calculate_connections(grid, data_points, u_vector, node1, node2, node3, tri_integrate, reverse=False):

	# Find the polynomials who's support is on the trinalge
	poly1 = set_polynomial_linear_2D(node1, node2, node3)
	poly2 = set_polynomial_linear_2D(node2, node3, node1)
	poly3 = set_polynomial_linear_2D(node3, node1, node2)

	# Find the local A and d entries
	data1, data2, data3, data_vector, KTu_vector \
		= local_A_linear_2D(data_points, u_vector, node1, node2, node3, poly1, poly2, poly3)

	if reverse:
		data1 = -data1
		data2 = -data2
		data3 = -data3
		data_vector = -data_vector
		KTu_vector = -KTu_vector

	sum_data_matrix(grid, node1, node1, data1)
	sum_data_matrix(grid, node1, node2, data2)
	sum_data_matrix(grid, node1, node3, data3)

	sum_data_load(node1, data_vector)
	sum_KTu_load(node1, KTu_vector)

	# Find the local stiffness entries
	stiff1, stiff2, stiff3 = local_L_linear_2D(node1, node2, node3, \
			poly1, poly2, poly3, tri_integrate)

	if reverse:
		stiff1 = -stiff1
		stiff2 = -stiff2
		stiff3 = -stiff3

	sum_stiffness(grid, node1, node1, stiff1)
	sum_stiffness(grid, node1, node2, stiff2)
	sum_stiffness(grid, node1, node3, stiff3)


	# Find the local gradient entries
	grad1, grad2, grad3 = local_G_linear_2D(node1, node2, node3, poly1, poly2, poly3)

	if reverse:
		grad1 = [i*-1.0 for i in grad1]
		grad2 = [i*-1.0 for i in grad2]
		grad3 = [i*-1.0 for i in grad3]

	sum_gradient(grid, node1, node1, grad1)
	sum_gradient(grid, node1, node2, grad2)
	sum_gradient(grid, node1, node3, grad3)
	

 
def build_B_y(grid, node_list, data_list, no_data):
	""" build matrix B and vector y """

	"""######## This part is not used for now ######"""
	# B and y cannot be built without data points
	if no_data == 0:
		return

	# Number of unknowns
	no_nodes = len(node_list)

	# Ratio of B matrix
	ratio = 1.0/sqrt(no_data)

	# Initialise a matrix B transpose
	B =  lil_matrix((no_nodes,no_data),dtype=float)

	# Initialise data point value list y
	y = zeros([no_data, 1])

	triangle_index = 0

	data_index = 0

	# Iterate through each triangle
	for tri in triangle_iterator_ref(grid):
		if tri[0].get_node_id() < tri[1].get_node_id() and \
		   tri[1].get_node_id() < tri[2].get_node_id():

			# Polynomials of three vertices
			poly1 = set_polynomial_linear_2D(tri[0], tri[1], tri[2])
			poly2 = set_polynomial_linear_2D(tri[1], tri[2], tri[0])
			poly3 = set_polynomial_linear_2D(tri[2], tri[0], tri[1])

			# Obtain three node index in the list
			index1, index2, index3 = \
				obtain_node_index_2D(node_list, tri[0], tri[1], tri[2])

			# If all three vertices of the triangle are boundary nodes
			# Skip this triangle
			if index1 == -1 and index2 == -1 and index3 == -1:
				continue

			data_points = data_list[triangle_index]

			for i in range(0, len(data_points)):
				
				# Coodinate of the data point
				coord = data_points[i][:2]

				if index1 >= 0:
					B[index1, i+data_index] = poly1.eval(coord[0], coord[1])*ratio

				if index2 >= 0:
					B[index2, i+data_index] = poly2.eval(coord[0], coord[1])*ratio

				if index3 >= 0:
					B[index3, i+data_index] = poly3.eval(coord[0], coord[1])*ratio

				y[i+data_index,0] = data_points[i][-1]

			data_index += len(data_points)

			triangle_index += 1

	return B, y



def build_equation_linear_2D(grid, region, parameter, \
			reverse=False, node_list=[]):#, if_build_B_y=False):
	"""Define the stiffness matrix and load vector"""

	tri_integrate = Poisson_tri_integrate_linear

	local = parameter.get_local()

	if not local:
		print("Start build equation")
		
	# Iterate through data regions and reset all signals
	region.init_usage()

	# Boundary type of the problem
	boundary_type = parameter.get_boundary()

	# Set slave values for Dirichlet boundaries (not for error indicators)
	if boundary_type == BoundaryType.Dirichlet and not local:
		set_slave_value(grid, parameter)

	# Add the matrix connections for linear basis functions and 
	# initialise to zero
	initialise_connections(grid)
	

	if not local:
		parameter.set_data_ratio(1.0)

		write_line(parameter, "data ratio: "+str(parameter.get_data_ratio()), True)
	else:
#		determine_data_ratio_2D(grid, region, parameter)
		parameter.set_data_ratio(1.0)#0.01)
		pass

#	parameter.set_data_ratio(0.2)

	# Number of data points
	data_sum = 0
#	data_size = []

	# Evaluate the TPSFEM system of equations
	# Loop over the triangles once
	for tri in triangle_iterator_ref(grid):
		if tri[0].get_node_id() < tri[1].get_node_id() and \
			tri[1].get_node_id() < tri[2].get_node_id():
			
			# Obtain data points in the triangle
			data_points, u_vector = obtain_data(region, tri, parameter)
#			data = data+data_points

			# Add connections between tri0 and tri1, tri2
			calculate_connections(grid, data_points, u_vector, tri[0], tri[1], tri[2], tri_integrate, reverse)

			# Add connections between tri1 and tri0, tri2
			calculate_connections(grid, data_points, u_vector, tri[1], tri[0], tri[2], tri_integrate, reverse)

			# Add connections between tri2 and tri0, tri1
			calculate_connections(grid, data_points, u_vector, tri[2], tri[0], tri[1], tri_integrate, reverse)

			data_sum += len(data_points)
			
#			data_size.append(len(data_points))

			# Record number of data points for this triangle
			# This is only needed for auxiliary problem error indicator
#			if not local and parameter.get_indicator() == IndicatorType.auxiliary:
#				if parameter.get_indicator_parameter()[6]:
			record_data_2D(grid, tri, len(data_points))

#	print("data_size =", data_size)
#	plot_data_dist(data)
#	plot_fem_grid_2D(grid, parameter, True)
#	plot_fem_grid_data_2D(grid, data)
	
	parameter.set_data_size(data_sum)

	# Record number of data points in the local domain
#	if parameter.get_local():
#		parameter.add_stat(data_sum)
	
	# Divide A and d entries by number of data points
	normalise_A_d(grid, parameter)