#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-11-02 23:45:34
@Last Modified time: 2022-10-09 23:36:05

This class update the connections of the triangle pair that is 
splitted into smaller triangles. It first reverses the connections
of the original triangle and add back connections of the splitted
triangles. This approach aims to reduce computation costs of reading
data for a larger local domain.

"""

# Import libraries
from grid.EdgeTable import endpt_iterator
from adaptive.triangle.BuildEquation import Poisson_tri_integrate_linear
from adaptive.triangle.BuildEquationLinear import calculate_connections
from adaptive.triangle.TriangleFunction import find_triangle_pairs
from adaptive.triangle.RefinementFunction import add_triangle3, add_triangle4
from data.ObtainData import obtain_data, obtain_data_u_list



def reverse_triangle_connections(grid, data_points1, data_points2, u_vector1, u_vector2, node1, node2, node3, node4, tri_integrate):
	""" Reverse the build equation process for the triangle pair """
	
	reverse = True

	if node3 is not None:
		calculate_connections(grid, data_points1, u_vector1, node1, node2, node3, tri_integrate, reverse)
		calculate_connections(grid, data_points1, u_vector1, node2, node1, node3, tri_integrate, reverse)
		calculate_connections(grid, data_points1, u_vector1, node3, node1, node2, tri_integrate, reverse)

	if node4 is not None:
		calculate_connections(grid, data_points2, u_vector2, node1, node2, node4, tri_integrate, reverse)
		calculate_connections(grid, data_points2, u_vector2, node2, node1, node4, tri_integrate, reverse)
		calculate_connections(grid, data_points2, u_vector2, node4, node1, node2, tri_integrate, reverse)



def update_triangle_connections(grid, data_points1, data_points2, u_vector1, u_vector2, node1, node2, node3, node4, mid_node, tri_integrate):
	""" Update connections for the splitted triangles """

	reverse = False

	# Add connections for splitted triangle 1
	if node3 is not None:

		data_points1a, u1a = obtain_data_u_list(data_points1, u_vector1, [node1, node3, mid_node])
		data_points1b, u1b = obtain_data_u_list(data_points1, u_vector1, [node2, node3, mid_node])

		calculate_connections(grid, data_points1a, u1a, node1, node3, mid_node, tri_integrate, reverse)
		calculate_connections(grid, data_points1a, u1a, node3, node1, mid_node, tri_integrate, reverse)
		calculate_connections(grid, data_points1a, u1a, mid_node, node1, node3, tri_integrate, reverse)

		calculate_connections(grid, data_points1b, u1b, node2, node3, mid_node, tri_integrate, reverse)
		calculate_connections(grid, data_points1b, u1b, node3, node2, mid_node, tri_integrate, reverse)
		calculate_connections(grid, data_points1b, u1b, mid_node, node2, node3, tri_integrate, reverse)

	# Add connections for splitted triangle 2
	if node4 is not None:

		data_points2a, u2a = obtain_data_u_list(data_points2, u_vector2, [node1, node4, mid_node])
		data_points2b, u2b = obtain_data_u_list(data_points2, u_vector2, [node2, node4, mid_node])

		calculate_connections(grid, data_points2a, u2a, node1, node4, mid_node, tri_integrate, reverse)
		calculate_connections(grid, data_points2a, u2a, node4, node1, mid_node, tri_integrate, reverse)
		calculate_connections(grid, data_points2a, u2a, mid_node, node1, node4, tri_integrate, reverse)

		calculate_connections(grid, data_points2b, u2b, node2, node4, mid_node, tri_integrate, reverse)
		calculate_connections(grid, data_points2b, u2b, node4, node2, mid_node, tri_integrate, reverse)
		calculate_connections(grid, data_points2b, u2b, mid_node, node2, node4, tri_integrate, reverse)


def initialise_mid_connections(grid, mid_id):
	""" Initialise connections to mid node to zero """

	if not grid.is_connection(mid_id, mid_id):
		grid.add_connection(mid_id, mid_id)
	grid.set_matrix_A_value(mid_id, mid_id, 0.0)
	grid.set_matrix_L_value(mid_id, mid_id, 0.0)
	grid.set_matrix_G_value(mid_id, mid_id, [0.0,0.0])

	for endpt in endpt_iterator(grid, mid_id):
		if not grid.is_connection(mid_id, endpt):
			grid.add_connection(mid_id, endpt)
		grid.set_matrix_A_value(mid_id, endpt, 0.0)
		grid.set_matrix_L_value(mid_id, endpt, 0.0)
		grid.set_matrix_G_value(mid_id, endpt, [0.0,0.0])

		if not grid.is_connection(endpt, mid_id):
			grid.add_connection(endpt, mid_id)
		grid.set_matrix_A_value(endpt, mid_id, 0.0)
		grid.set_matrix_L_value(endpt, mid_id, 0.0)
		grid.set_matrix_G_value(endpt, mid_id, [0.0,0.0])


def split_edge_update_connections(grid, edge, region, parameter):
	""" This function split the edge and update connections for new triangles """

	# Get the ids of the endpoints of the edge
	endpt1 = edge.get_endpt1()
	endpt2 = edge.get_endpt2()

	tri_integrate = Poisson_tri_integrate_linear

	# Return if the given edge is not found in the grid
	if not grid.is_edge(endpt1,endpt2):
		return

	node1 = grid.get_node_ref(endpt1)
	node2 = grid.get_node_ref(endpt2)

	# Obtain other vertices of the triangle pairs
	node3, node4 = find_triangle_pairs(grid, endpt1, endpt2)

	# Obtain data points in the triangle
	data_points1 = []
	data_points2 = []
	u_vector1 = []
	u_vector2 = []
	
	# Iterate through data regions and reset all signals
	region.init_usage()

	# Obtain data points in the triangle or triangle pair
	if node3 is not None:
		data_points1, u_vector1 = obtain_data(region, [node1, node2, node3])

	if node4 is not None:
		data_points2, u_vector2 = obtain_data(region, [node1, node2, node4])	

	"""
	print "data 1"
	for data in data_points1:
		print data[0:2]
	print "data 2"
	for data in data_points2:
		print data[0:2]	
	"""
	# Reverse the build equation process
	reverse_triangle_connections(grid, data_points1, data_points2, u_vector1, \
						u_vector2, node1, node2, node3, node4, tri_integrate)

	# Split the triangle or triangle pair
	if node3 is not None and node4 is not None:
		mid_id = add_triangle4(grid, endpt1, endpt2, node3.get_node_id(), node4.get_node_id(), parameter)
	
	elif node4 is None:
		mid_id = add_triangle3(grid, endpt1, endpt2, node3.get_node_id(), parameter)

	else:
		print("Warning: triangle2 in error indicators")

	# Initialise connections from new mid node to other nodes
	initialise_mid_connections(grid, mid_id)

	mid_node = grid.get_node_ref(mid_id)

	# Read data and build equation for the new triangles
	update_triangle_connections(grid, data_points1, data_points2, u_vector1, u_vector2, node1, node2, node3, node4, mid_node, tri_integrate)

	return mid_id