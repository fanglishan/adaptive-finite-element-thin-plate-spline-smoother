#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-05-05 21:36:11
@Last Modified time: 2022-10-09 23:36:20

This class contains routines to functions of refinements that 
cannot be referenced from the adaptive refinement class to avoid
circular reference.

"""

# import libraries
from grid.GlobalNodeID import GlobalNodeID, mesh_integers_2D, unmesh_integers_2D
from grid.EdgeTable import endpt_iterator
from grid.Edge import DomainSet, RefineSet
from adaptive.triangle.TriangleFunction import find_triangle_pairs

from copy import deepcopy


class NodeSet:
	"""Is the node a full node or a ghost node
	
	"""
	full_node = 1
	ghost_node = 2


def build_midpoint_global_id(grid, global_id1, global_id2):
	"""Find the global id of the new midpoint""" 

	# The node is being added a what level of refinement
	current_level = 0
	
	if global_id1.get_level() > global_id2.get_level():
		current_level = global_id1.get_level()+1
	else:
		current_level = global_id2.get_level()+1
		
	# The number must be scaled according to the level of refinement
	scale1 = 2**(current_level-global_id1.get_level()-1)
	scale2 = 2**(current_level-global_id2.get_level()-1)
		
	# Extract the coordinates that were used to find the ids of the endpoints 
	id1x, id1y = unmesh_integers_2D(global_id1.get_no())
	id2x, id2y = unmesh_integers_2D(global_id2.get_no())
		
	# Find the coordinates of the midpoint
	idx = id1x*scale1+id2x*scale2
	idy = id1y*scale1+id2y*scale2
		
	# Create a new global id
	id = mesh_integers_2D(idx, idy)
	global_id = GlobalNodeID(id, current_level)

	# Return the global id
	return global_id
	
	
def add_midpoint(grid, node1, node2, node_type, parameter):
	"""Add the midpoint to the grid""" 
	
	# Get the ids of the two endpoints
	node_id1 = node1.get_node_id()
	node_id2 = node2.get_node_id()

	# Find the coordinates of the midpoint
	dim = grid.get_dim()
	coord = [0.0]*dim
	for i in range(dim):
		coord[i] = (node1.get_coord()[i]+node2.get_coord()[i])/2.0
		
	# Find the global id of the midpoint
	mid_global_id = build_midpoint_global_id(grid, node1.get_global_id(),
											 node2.get_global_id())
											 
	# Determine if the midpoint will sit on the boundary                                         
	location = grid.get_location(node_id1, node_id2)
	bnd_func = grid.get_boundary_function(node_id1, node_id2)

	if parameter.get_local():
		location = DomainSet.interior

	# If the midpoint is to be added as a full node
	if node_type == NodeSet.full_node:
		
		# If the midpoint sits on the boundary
		if location == DomainSet.boundary:
			
			# Add the midpoint as a full slave node
			value = bnd_func(coord)
			mid_node = grid.create_node(mid_global_id, coord, True, value)

			function = parameter.get_function()
			mid_node.set_d_values([function[1](coord),\
				function[2](coord), function[3](coord)])

			grid.add_node(mid_node)
				
		else:
			
			# Add the midpoint as a full node (and initialise the value and
			# load to be the average of the endpoints)
			value = (node1.get_value() + node2.get_value())/2.0
			data_load = (node1.get_data_load() + node2.get_data_load())/2.0
			mid_node = grid.create_node(mid_global_id, coord, False, value)
			mid_node.set_data_load(data_load)

			# Add derivative values if exist
			if len(node1.get_d_values())>0 and len(node2.get_d_values())>0:
				ux_value = (node1.get_d_values()[0]+node2.get_d_values()[0])/2.0
				uy_value = (node1.get_d_values()[1]+node2.get_d_values()[1])/2.0
				uxx_value = (node1.get_d_values()[2]+node2.get_d_values()[2])/2.0
				mid_node.set_d_values([ux_value,uy_value,uxx_value])

			grid.add_node(mid_node)

	return mid_node
	

def split_edge(grid, node1, node2, node_type, parameter):
	"""Split the edge between node1 and node2""" 
	
	# Get the ids of the endpoints
	node_id1 = node1.get_node_id()
	node_id2 = node2.get_node_id()
	
	# Find the edge location 
	location = grid.get_location(node_id1, node_id2)
	boundary_function = grid.get_boundary_function(node_id1, node_id2)
	
	# Find the refinement level
	refine_level = grid.get_refine_level(node_id1, node_id2)+1

	# Add the midpoint to the grid
	mid_node = add_midpoint(grid, node1, node2, node_type, parameter) 
	mid_id = mid_node.get_node_id()

	# Remove the old edges
	grid.delete_edge(node_id1, node_id2)
	grid.delete_edge(node_id2, node_id1)
	
	# Add a new edge between the midpoint and node1
	grid.add_edge(mid_id, node_id1)
	grid.set_location(mid_id, node_id1, location)
	grid.set_boundary_function(mid_id, node_id1, boundary_function)
	grid.set_refine_type(mid_id, node_id1, RefineSet.not_base_edge)
	grid.set_refine_level(mid_id, node_id1, refine_level)

	# Add a new edge between the node1 and midpoint
	grid.add_edge(node_id1, mid_id)
	grid.set_location(node_id1, mid_id, location)
	grid.set_boundary_function(node_id1, mid_id, boundary_function)
	grid.set_refine_type(node_id1, mid_id, RefineSet.not_base_edge) 
	grid.set_refine_level(node_id1, mid_id, refine_level) 

	# Add a new edge between the midpoint and node2
	grid.add_edge(mid_id, node_id2)
	grid.set_location(mid_id, node_id2, location)
	grid.set_boundary_function(mid_id, node_id2, boundary_function)
	grid.set_refine_type(mid_id, node_id2, RefineSet.not_base_edge)
	grid.set_refine_level(mid_id, node_id2, refine_level)

	# Add a new edge between the node2 and midpoint
	grid.add_edge(node_id2, mid_id)
	grid.set_location(node_id2, mid_id, location)
	grid.set_boundary_function(node_id2, mid_id, boundary_function)
	grid.set_refine_type(node_id2, mid_id, RefineSet.not_base_edge) 
	grid.set_refine_level(node_id2, mid_id, refine_level)

	# Return the midpoint
	return mid_node
	 
	 
def set_base_type(grid, node_id1, node_id2):

	if grid.get_location(node_id1, node_id2) != DomainSet.interior:
		grid.set_refine_type(node_id1, node_id2, RefineSet.base_edge)
	elif grid.get_refine_type(node_id1, node_id2) == RefineSet.not_base_edge:
		grid.set_refine_type(node_id1, node_id2, RefineSet.interface_base_edge)
	else:
		grid.set_refine_type(node_id1, node_id2, RefineSet.base_edge)



def add_triangle4(grid, node_id1, node_id2, node_id3, node_id4, parameter):
	"""Refine two triangles along the given base edge""" 

	# Find the nodes at the endpoints of the edge between node_id1 and node_id2
	if grid.is_in(node_id1):
		node1 = grid.get_node(node_id1)

	if grid.is_in(node_id2):
		node2 = grid.get_node(node_id2)

	# Record at which refinement level the new node was added
	refine_level = grid.get_refine_level(node_id1, node_id2)+1

	node_type = NodeSet.full_node

	# Add the midpoint and split the edge between node1 and node2
	mid_node = split_edge(grid, node1, node2, node_type, parameter)
	mid_id = mid_node.get_node_id()

	# Add the edges going from the midpoint to node_id3
	grid.add_edge(mid_id, node_id3)
	grid.set_location(mid_id, node_id3, DomainSet.interior)
	grid.set_refine_type(mid_id, node_id3, RefineSet.not_base_edge)
	grid.set_refine_level(mid_id, node_id3, refine_level)

	grid.add_edge(node_id3, mid_id)
	grid.set_location(node_id3, mid_id, DomainSet.interior)
	grid.set_refine_type(node_id3, mid_id, RefineSet.not_base_edge)
	grid.set_refine_level(node_id3, mid_id, refine_level)
	
	# Add the edges going from the midpoint to node_id4
	grid.add_edge(mid_id, node_id4)
	grid.set_location(mid_id, node_id4, DomainSet.interior)
	grid.set_refine_type(mid_id, node_id4, RefineSet.not_base_edge)
	grid.set_refine_level(mid_id, node_id4, refine_level)

	grid.add_edge(node_id4, mid_id)
	grid.set_location(node_id4, mid_id, DomainSet.interior)
	grid.set_refine_type(node_id4, mid_id, RefineSet.not_base_edge)
	grid.set_refine_level(node_id4, mid_id, refine_level)
	
	# Update the connections
	
	# Remove the old connections
	grid.delete_connection(node_id1, node_id2)
	grid.delete_connection(node_id2, node_id1)
		
	# Add connections between the midpoint and four vertices
	grid.add_connection(mid_id, mid_id)
	grid.add_connection(mid_id, node_id1)
	grid.add_connection(mid_id, node_id2)
	grid.add_connection(mid_id, node_id3)
	grid.add_connection(mid_id, node_id4)
	grid.add_connection(node_id1, mid_id)
	grid.add_connection(node_id2, mid_id)
	grid.add_connection(node_id3, mid_id)
	grid.add_connection(node_id4, mid_id)   
	
	# Update the refinement level to record when the triangle was added
	# (used mainly for adaptive refinement)
	grid.set_refine_level(node_id1, node_id3, refine_level)
	grid.set_refine_level(node_id3, node_id1, refine_level)
	grid.set_refine_level(node_id1, node_id4, refine_level)
	grid.set_refine_level(node_id4, node_id1, refine_level)    
	grid.set_refine_level(node_id2, node_id3, refine_level)
	grid.set_refine_level(node_id3, node_id2, refine_level)
	grid.set_refine_level(node_id2, node_id4, refine_level)
	grid.set_refine_level(node_id4, node_id2, refine_level)    
	
	# Update the base types of the external edges
	set_base_type(grid, node_id1, node_id3)
	set_base_type(grid, node_id3, node_id1)
	set_base_type(grid, node_id1, node_id4)
	set_base_type(grid, node_id4, node_id1)
	set_base_type(grid, node_id2, node_id3)
	set_base_type(grid, node_id3, node_id2)
	set_base_type(grid, node_id2, node_id4)
	set_base_type(grid, node_id4, node_id2)   
  
	return mid_id


def add_triangle3(grid, node_id1, node_id2, node_id3, parameter):
	"""Refine a triangle along the given base edge""" 

	# Find the nodes at the endpoints of the edge between node_id1 and node_id2
	if grid.is_in(node_id1):
		node1 = grid.get_node(node_id1)

	if grid.is_in(node_id2):
		node2 = grid.get_node(node_id2)
		
	# Record at which refinement level the new node was added
	refine_level = grid.get_refine_level(node_id1, node_id2)+1

	node_type = NodeSet.full_node

	# Add the midpoint and split the edge between node1 and node2
	mid_node = split_edge(grid, node1, node2, node_type, parameter)
	mid_id = mid_node.get_node_id()

	# Add the edges going from the midpoint to node_id3
	grid.add_edge(mid_id, node_id3)
	grid.set_location(mid_id, node_id3, DomainSet.interior)
	grid.set_refine_type(mid_id, node_id3, RefineSet.not_base_edge)
	grid.set_refine_level(mid_id, node_id3, refine_level)

	grid.add_edge(node_id3, mid_id)
	grid.set_location(node_id3, mid_id, DomainSet.interior)
	grid.set_refine_type(node_id3, mid_id, RefineSet.not_base_edge)
	grid.set_refine_level(node_id3, mid_id, refine_level)

	# Update the connections
	
	# Remove the old connections
	grid.delete_connection(node_id1, node_id2)
	grid.delete_connection(node_id2, node_id1)

	# Add connections between the midpoint and three vertices
	grid.add_connection(mid_id, mid_id)
	grid.add_connection(mid_id, node_id1)
	grid.add_connection(mid_id, node_id2)
	grid.add_connection(mid_id, node_id3)
	grid.add_connection(node_id1, mid_id)
	grid.add_connection(node_id2, mid_id)
	grid.add_connection(node_id3, mid_id)

	# Update the refinement level to record when the triangle was added
	# (used mainly for adaptive refinement)
	grid.set_refine_level(node_id1, node_id3, refine_level)
	grid.set_refine_level(node_id3, node_id1, refine_level) 
	grid.set_refine_level(node_id2, node_id3, refine_level)
	grid.set_refine_level(node_id3, node_id2, refine_level)

	# Update the base types of the external edges
	set_base_type(grid, node_id1, node_id3)
	set_base_type(grid, node_id3, node_id1) 
	set_base_type(grid, node_id2, node_id3)
	set_base_type(grid, node_id3, node_id2)

	return mid_id
 

def add_triangle2(grid, node_id1, node_id2):
	"""Refine a triangle along the given base edge""" 

	# Find the nodes at the endpoints of the edge between node_id1 and node_id2
	if grid.is_in(node_id1):
		node1 = grid.get_node(node_id1)

	if grid.is_in(node_id2):
		node2 = grid.get_node(node_id2)

	# Add the midpoint and split the edge between node1 and node2
	mid_node = split_edge(grid, node1, node2, node_type)
	mid_id = mid_node.get_node_id()

	# Update the connections
	
	# Remove the old connections
	grid.delete_connection(node_id1, node_id2)
	grid.delete_connection(node_id2, node_id1)

	# Add connections between the midpoint and two vertices
	grid.add_connection(mid_id, mid_id)
	grid.add_connection(mid_id, node_id1)
	grid.add_connection(mid_id, node_id2)
	grid.add_connection(node_id1, mid_id)
	grid.add_connection(node_id2, mid_id)

	return mid_id

def find_coarse_triangle(grid,endpt1,endpt2,endpt3,endpt4):
	""" Find the triangle with lower refinement level """

	# Initialise refinement level for triangle 3 and 4
	refine_level_3 = 0
	refine_level_4 = 0

	# Find the refinement level for triangle 3
	if grid.get_refine_type(endpt1,endpt3) == RefineSet.base_edge:
		refine_level_3 = grid.get_refine_level(endpt1,endpt3)

	elif grid.get_refine_type(endpt2,endpt3) == RefineSet.base_edge:
		refine_level_3 = grid.get_refine_level(endpt2,endpt3)

	elif grid.get_refine_type(endpt1,endpt3) == RefineSet.interface_base_edge:
		refine_level_3 = grid.get_refine_level(endpt1,endpt3)

	elif grid.get_refine_type(endpt2,endpt3) == RefineSet.interface_base_edge:
		refine_level_3 = grid.get_refine_level(endpt2,endpt3)
	else:
		# Set refinement level from the interface base edge if no other base edges are found
		refine_level_3 = grid.get_refine_level(endpt1,endpt2)

	# Find the refinement level for triangle 4
	if grid.get_refine_type(endpt1,endpt4) == RefineSet.base_edge:
		refine_level_4 = grid.get_refine_level(endpt1,endpt4)

	elif grid.get_refine_type(endpt2,endpt4) == RefineSet.base_edge:
		refine_level_4 = grid.get_refine_level(endpt2,endpt4)

	elif grid.get_refine_type(endpt1,endpt4) == RefineSet.interface_base_edge:
		refine_level_4 = grid.get_refine_level(endpt1,endpt4)

	elif grid.get_refine_type(endpt2,endpt4) == RefineSet.interface_base_edge:
		refine_level_4 = grid.get_refine_level(endpt2,endpt4)
	else:
		# Set refinement level from the interface base edge if no other base edges are found
		refine_level_4 = grid.get_refine_level(endpt1,endpt2)

	# Print error message if refinement level between 
	# two neighouring triangles differs larger than 2
	if abs(refine_level_3-refine_level_4) > 2:
		print("refine difference between triangles larger than 2")

	# Assign the triangle with lower and higher refinement levels to the references
	if refine_level_3 < refine_level_4:
		endpt_low = endpt3
		endpt_high = endpt4
	elif refine_level_3 > refine_level_4:
		endpt_low = endpt4
		endpt_high = endpt3
	else:
		# Return if two sides of the interface base edges have the same refinement level
		print("refinement level is the same")
		return None, None

	return endpt_low, endpt_high


def refine_coarse_triangle(grid,endpt1,endpt2,endpt_low,parameter):
	""" Refine coarse triangle for interface base edge """

	# Find the base edge of low refinement level triangle
	if grid.get_refine_type(endpt1,endpt_low) == RefineSet.base_edge:

		# Split the base edge if it ends at end point 1
		mid_node_id = split_triangle(grid, grid.get_edge(endpt1,endpt_low),parameter)

	elif grid.get_refine_type(endpt2,endpt_low) == RefineSet.base_edge:

		# Split the base edge if it ends at end point 2
		mid_node_id = split_triangle(grid, grid.get_edge(endpt2,endpt_low),parameter)

	elif grid.get_refine_type(endpt1,endpt_low) == RefineSet.interface_base_edge and \
		grid.get_refine_type(endpt2,endpt_low) == RefineSet.interface_base_edge:

		# Compare the refinement level of two interface base edges
		if grid.get_refine_level(endpt2,endpt_low) > grid.get_refine_level(endpt1,endpt_low):

			# Refine the edge with lower refinement level
			mid_node_id = split_triangle(grid, grid.get_edge(endpt1,endpt_low),parameter)

		elif grid.get_refine_level(endpt2,endpt_low) < grid.get_refine_level(endpt1,endpt_low):

			# Refine the edge with lower refinement level
			mid_node_id = split_triangle(grid, grid.get_edge(endpt2,endpt_low),parameter)
		else:
			print("two interface base edges have the same refine level")

	elif grid.get_refine_type(endpt1,endpt_low) == RefineSet.interface_base_edge:

		# Split the base edge if it ends at end point 1
		mid_node_id = split_triangle(grid, grid.get_edge(endpt1,endpt_low),parameter)

	elif grid.get_refine_type(endpt2,endpt_low) == RefineSet.interface_base_edge:
	
		# Split the base edge if it ends at end point 2
		mid_node_id = split_triangle(grid, grid.get_edge(endpt2,endpt_low),parameter)

	else:
		# Return no base edge is found in this triangle
		print("no base edge found")
		return

	return mid_node_id
  

def split_triangle(grid, edge, parameter):
	""" Split the triangles with the given edge """ 

	# Get the ids of the endpoints of the edge
	endpt1 = edge.get_endpt1()
	endpt2 = edge.get_endpt2()

	# Return if the given edge is not found in the grid
	if not grid.is_edge(endpt1,endpt2):
		return

	# Make a list of all of the edges joined to endpt1 (this is used
	# as we only want to find one version of each triangle)
	edgestar = list()
	for endpt in endpt_iterator(grid, endpt1):
		edgestar.append(endpt)
	
	# Loop over all of the endpoints joined to endpt1
	n = len(edgestar)
	for i in range(n):
		
		# If the endpoint is also joined to endpt2, we have found a triangle
		if grid.is_edge(edgestar[i], endpt2):
			
			# See if we can find a second, different, triangle
			
			# Loop over other endpoints joined to endpt1
			for j in range(i+1, n):
				
				# If the new endpoints is also joined to endpt2, we have found
				# two triangle that share the edge between endpt1 and endpt2
				if grid.is_edge(edgestar[j], endpt2):
					
					# Check if the edge is an interface base edge
					if grid.get_refine_type(endpt1,endpt2) == RefineSet.interface_base_edge:
						
						endpt_low, endpt_high = find_coarse_triangle(grid,endpt1,endpt2,edgestar[i],edgestar[j])

						if endpt_low is None and endpt_high is None:
							continue

						mid_node_id = refine_coarse_triangle(grid,endpt1,endpt2,endpt_low, parameter)

						# Refine the interface base edge
						mid_id = add_triangle4(grid, endpt1, endpt2, mid_node_id, endpt_high, parameter)

						return mid_id
						
					# Refine those two triangles
					mid_id = add_triangle4(grid, endpt1, endpt2, edgestar[i], edgestar[j], parameter)

					return mid_id
					
			# If only one triangle was found, refine that triangle
			mid_id = add_triangle3(grid, endpt1, endpt2, edgestar[i], parameter)

			return mid_id
	
	# Sometime in the parallel implementation no triangle is found, so refine
	# the edge
	mid_id = add_triangle2(grid, endpt1, endpt2)

	return mid_id


def split_edge_only(grid, edge, parameter):
	""" Split the triangles with the given edge """ 

	# Get the ids of the endpoints of the edge
	endpt1 = edge.get_endpt1()
	endpt2 = edge.get_endpt2()

	# Return if the given edge is not found in the grid
	if not grid.is_edge(endpt1,endpt2):
		return

	# Obtain other vertices of the triangle pairs
	node3, node4 = find_triangle_pairs(grid, endpt1, endpt2)

	if node3 is not None and node4 is not None:
		node_id3 = node3.get_node_id()
		node_id4 = node4.get_node_id()
		mid_id = add_triangle4(grid, endpt1, endpt2, node_id3, node_id4, parameter)
	
	elif node4 is None:
		node_id3 = node3.get_node_id()
		mid_id = add_triangle3(grid, endpt1, endpt2, node_id3, parameter)

	else:
		mid_id = add_triangle2(grid, endpt1, endpt2)

	return mid_id
