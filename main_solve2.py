#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-06-13 10:13:44
@Last Modified time: 2022-10-29 10:03:05

This class reads a FEM grid and corresponding data and solve again.

"""

# Import libraries
from build.CopyGrid import copy_grid
from function.FunctionManager import obtain_model_function
from parameter.Parameter import Parameter
from parameter.ModelProblemFunction import find_model_problem
from estimator.Error import Error
from fileio.GridFunction import read_grid
import plot.FemPlot2D as femPlot2D
import plot.TpsfemPlot1D as tpsfemPlot1D
import plot.TpsfemPlot2D as tpsfemPlot2D
import plot.TpsfemPlotly2D as tpsfemPlotly2D
import plot.IndicatorPlot as indicatorPlot
import plot.ErrorPlot as errorPlot
import plot.SurfaceTriangulation as stPlot
import matplotlib.pyplot as plt


# Obtain grid for error estimation
#path = "result/" + "result_1_6_20_02_01_11_02" + "/"
#filename = "1_5677"#path[14:19]
#filename = "1_177"
path = "result/" 
#path = "result/" + "result_5_4_21_03_20_14_11" + "/"
#filename = path[14:21]
#filename = "1081200"
filename = "5_3_6401_n"
#filename = "9_7254"
#filename = "9_226"
#filename = "5_8321"
#filename = "2135204_6592"

d = 2


grid = read_grid(filename, path)

dataset = 5
parameter = Parameter()
parameter.set_dataset(dataset)
#grid = copy_grid(grid)

#dataset = 1081200
"""
dataset = 7#int(filename[:7])

#data = obtain_dataset(dataset)
parameter = Parameter()
parameter.set_dataset(dataset)

# Find model problems
model_problem = find_model_problem(dataset)
function = obtain_model_function(model_problem, d)
parameter.set_function(function)
"""
#print(dataset)
print("grid node number", grid.get_no_nodes())

show_plot = True

if show_plot:

	# Plot resulting grids
	if grid.get_dim() == 1:
#		indicatorPlot.plot_error_indicator(grid, parameter)
#		tpsfemPlot1D.plot_tpsfem_solution_1D(grid, None, None, False)

		pass

	elif grid.get_dim() == 2:
		stPlot.plot_surface_triangulation(grid)
#		femPlot2D.plot_fem_grid_2D(grid, None, False)
#		tpsfemPlot2D.plot_tpsfem_solution_2D(grid)
#		tpsfemPlot2D.plot_contour(grid)
#		tpsfemPlotly2D.plot_contour(grid)
#		errorPlot.plot_error_contour_2D(grid, parameter)
		pass
#	plt.xlim(0.1,0.9)
#	plt.ylim(0.1,0.9)
	plt.show()