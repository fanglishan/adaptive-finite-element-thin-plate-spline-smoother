# -*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang
@Date:   2024-09-22 22:45:36
@Last Modified by:   Fang
@Last Modified time: 2024-10-12 23:04:11

This function interpolate given data using RBFs.

"""

from fileio.DataFile import read_file
from rbf.BuildRBF import build_RBF
from rbf.EstimateError import estimate_errors
from plot.DataPlot import plot_data
from plot.RBFPlot import plot_rbf, plot_rbf_error

import numpy as np
import matplotlib.pyplot as plt

# Dataset
dataset = 1181221

# Kernel function of RBFs
# 1-multiquadric, 2-inverse, 3-gaussian, 4-linear, 5-cubic, 6-quintic
# 7-thin_plate, 8-thin_plate(4th), 9~11-wendland's CSRBF
kernel = 7

# Smoothing parameter
alpha = 1e-6

# epsilon constant for gaussian and multiquadrics
epsilon = 0

# The radius of support for CSRBFs is set in rbf.RBFKernel

data = read_file(str(dataset))

rbfi = build_RBF(data,kernel,alpha,epsilon)


# Estimate RMSE, MAE, ME, MAX, MIN
error_list = estimate_errors(data, rbfi)


# Print interpolated surfaces
plot_rbf(data, rbfi)

plot_rbf_error(data, rbfi)



# Use interpolants from R codes 


