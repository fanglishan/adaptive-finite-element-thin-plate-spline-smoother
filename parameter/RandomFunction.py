#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-11-24 17:17:11
@Last Modified time: 2022-03-29 11:27:16

This class contains routines that uses the random library.

"""

# Import libraries
import random


def random_pos_neg_one():
	# Randomly return 1 or -1
	return 1.0 if random.random() < 0.5 else -1.0