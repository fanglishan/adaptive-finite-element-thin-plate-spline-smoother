#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-04-20 12:18:59
@Last Modified time: 2024-10-13 10:40:24

This class contain parameter related routines. Currently it iniitialises
parameters at the beginning of the program and validate inputs.

"""

# Import other libraries
from function.FunctionManager import obtain_model_function
from parameter.Parameter import Parameter
from parameter.ModelProblemFunction import find_model_problem
from parameter.Definition import IndicatorType
from estimator.Cost import Cost


def init_parameter(boundary, basis, alpha, solver_tol, dataset, subset, \
	error_tol, mark, uni_refine, max_no, max_sweep, indicator, \
	indicator_parameter, norm, min_data, run_gcv, gcv_parameter, write_file, \
	weight, bnd_init):
	""" Initialise a parameter to record input settings """

	region_no = 100
	
	# Validate input parameters
	d = int(str(dataset)[0])

	# Real-world data sets
	if len(str(abs(dataset))) < 4:
		d = 2

	# Find model problems
	model_problem = find_model_problem(dataset)

	# Obtain model problem functions
	function = obtain_model_function(model_problem, d)

	# Correct indicator index
	if indicator == 0:
		indicator == 1

	# Uniformly refine L shaped initial grid
	if len(str(dataset)) > 2 and int(str(dataset)[3]) == 6:
		uni_refine = 2
	
	# Swtich marking strategy for uniform refinement
	if indicator == IndicatorType.infinity:
		mark = 1

	# Set refine choice if auxiliary problems need uniform refinement
	if indicator == IndicatorType.auxiliary and indicator_parameter[3] > 0:
		indicator_parameter[2] = 1

	# Initialise a parameter
	parameter = Parameter(d, boundary, basis, region_no, function, alpha,
	   solver_tol, dataset, subset, error_tol, mark, uni_refine, \
		max_no, max_sweep, indicator, indicator_parameter, norm, \
		min_data, run_gcv, gcv_parameter, write_file, weight, bnd_init)

	# Computational cost records
	cost = Cost()
	parameter.set_cost(cost)

	
	return parameter