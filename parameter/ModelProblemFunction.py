#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2020-04-10 15:36:10
@Last Modified time: 2022-03-29 11:21:45


"""

# Import libraries
from parameter.Definition import ModelProblem2D



def find_model_problem(dataset):

	# Set model problems
	if len(str(dataset)) > 3:
		model_problem = int(str(dataset)[1:3])

	else:
		model_problem = abs(dataset)*-1

	return model_problem