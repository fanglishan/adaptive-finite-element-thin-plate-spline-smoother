#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-04-16 14:10:32
@Last Modified time: 2024-10-13 11:33:43

This class contains a set of parameters that may be used in the 
TPSFE program. Most of the parameters are specified at the main file. 

"""

# Import libraries
import time


class Parameter:
	""" A set of parameters for the TPSFEM program """

	# Dimension (default:2)
	_d = 2

	# Boundary type
	_boundary = 0

	# Basis function
	_basis = 0

	# Number of regions
	_region_no = []

	# Model problem function
	_function = []

	# Number of data points
	_data_size = 0

	# Smoothing parameter
	_alpha = 0.0

	# Smoothing parameters for auxiliary problems
	_auxiliary_alpha = []

	# Solver tolerance
	_solver_tol = 0.0

	# Index of the dataset
	_dataset = 0

	_subset = False

	# Error tolerance
	_error_tol = 0.0

	# Adaptive refinement parameters
	# 1-refine approach, 2-#uniform refinement
	# 3-max refine in a sweep, 4-#refine sweep
	_refine = []

	# Type of error indicator
	_indicator = 0

	# Indicator parameters
	_indicator_parameter = []

	# List of error indicator values with corresponding ids
	_indicator_list = []

	# Norm type
	_norm = 0

	# Minimum number of data points for refinement
	_min_data = 0

	# Whether or not to run GCV to obtain a new smoothign parameter
	_run_gcv = False

	# Parameters of the GCV
	_gcv_parameter = []

	# Weather to write results to a file
	_write_file = [False]

	# Solution type (1-true solution, 2-TPSFEM fine grid)
	_solution_type = 1

	# Solution grid size
	_solution_grid_size = 10

	# TPSFEM with a fine grid
	_solution = None
	
	# More parameter stored in a list
	_parameter_list = []

	# Result folder name
	_folder = ""

	# Additional optional information
	# 0-iteration number, 1-whether it is in indicator, 2-marker
	# 3-sum of a quantity, 4-count, 5-ratio of gcv during refinement
	# 6-ratio of data used for build equation
	# 7-initialisation of boundary node values during adaptive refinement
	_info = []

	# Data statistics
	_stat = []

	# Cost records
	_cost = None

	def __init__(self,d=2,boundary=1,basis=1,region_no=[], function=[], \
		alpha=0.0,solver_tol=0.1,dataset=0, subset=False, error_tol=0.1,mark=1, \
		uni_refine=0, max_no=1, max_sweep=1,indicator=0,indicator_parameter=[], \
		norm=0, min_data=0, run_gcv=False, gcv_parameter=[], write_file=[False], weight=1,bnd_init = 1):
		""" Initialise the parameters"""

		self._d = d
		self._boundary = boundary
		self._basis = basis
		self._region_no = region_no
		self._function = function
		self._data_size = 0
		self._alpha = alpha
		self._auxiliary_alpha = []
		self._solver_tol = solver_tol
		self._dataset = dataset
		self._subset = subset
		self._error_tol = error_tol
		self._refine = [mark, uni_refine, max_no, max_sweep, weight]
		self._indicator = indicator
		self._indicator_parameter = indicator_parameter
		self._inicator_list = []
		self._norm = norm
		self._min_data = min_data
		self._run_gcv=run_gcv
		self._gcv_parameter=gcv_parameter
		self._write_file = write_file
		self._solution_type = None        # Depreciated function
		self._solution_grid_size = None   # Depreciated function
		self._solution = None
		self._folder = "result/result_"+str(dataset)+"_"+str(boundary)+"_"+str(indicator)+ \
				"_"+time.strftime('%y_%m_%d_%H_%M',time.localtime())+"/"
		self._info = [0, False, 0, 0.0, 0, [], 1.0,bnd_init]
		self._stat = []
		self._cost = None
		

	def __str__(self):
		""" Output parameters """

		string = "dimension: " + str(self._d) + "\n"
		string += "Boundary type: " + str(self._boundary) + "\n"
#		string += "Basis function: " + str(self._basis) + "\n"
#		string += "Number of regions (per dim): " + str(self._region_no) + "\n"
		string += "Model problem: " + str(self._function[0]) + "\n"
		string += "Number of data points: " + str(self._data_size) + "\n"
		string += "Smoothing parameter: " + str(self._alpha) + "\n"
#		string += "Solver tolerance: " + str(self._solver_tol) + "\n"
		string += "Dataset: " + str(self._dataset) + ", subset:" + str(self._subset) + "\n"
#		string += "Error tolerance: " + str(self._error_tol) + "\n"
		string += "Marking strategy: " + str(self._refine[0]) + "\n"
		string += "Number of uniform refine: " + str(self._refine[1]) + "\n"
		string += "Refine each sweep: " + str(self._refine[2]) + "\n"
		string += "Max refine sweeps: " + str(self._refine[3]) + "\n"
		string += "Error indicator: " + str(self._indicator) + "\n"
		string += "Indicator parameters: " + str(self._indicator_parameter) + "\n"
		string += "Auxiliary alpha: " + str(self._auxiliary_alpha) + "\n"
#		string += "Norm type: " + str(self._norm) + "\n"
#		string += "Min data refine: " + str(self._min_data) + "\n"
		string += "Run GCV: " + str(self._run_gcv) + "\n"
		string += "GCV parameters: " + str(self._gcv_parameter) + "\n"
		string += "Extra info: " + str(self._info) + "\n"
		if self._write_file[0]:
			string += "Results stored at: " + str(self._folder)
	
		return string


	def set_dim(self, dim):	
		""" Set Dimension """
		self._d = dim

	def set_boundary(self, boundary):	
		""" Set boundary type """
		self._boundary = boundary

	def set_basis(self, basis):
		""" Set basis function """
		self._basis = basis

	def set_region_no(self, region_no):
		""" Set number of data regions """
		self._region_no = region_no

	def set_function(self, function):
		""" Set model problem functions """
		self._function = function

	def set_data_size(self, data_size):
		""" Set number of data points """
		self._data_size = data_size

	def set_alpha(self, alpha):
		""" Set smoothing parameter alpha """
		self._alpha = alpha

	def set_auxiliary_alpha(self, alpha_list):
		""" Set smoothing parameter alpha for auxiliary problems """
		self._auxiliary_alpha = alpha_list

	def add_auxiliary_alpha(self, alpha):
		""" Add a new smoothing parameter alpha for auxiliary problems """
		self._auxiliary_alpha.append(alpha)

	def set_solver_tol(self, solver_tol):
		"""  Set solver tolerance """
		self._solver_tol = solver_tol

	def set_dataset(self, dataset):
		""" Set dataset index """
		self._dataset = dataset

	def set_subset(self, subset):
		""" Set whether to subset data """
		self._subset = subset

	def set_error_tol(self, error_tol):
		"""  Set error tolerance """
		self._error_tol = error_tol

	def set_refine(self, refine):
		""" Set adaptive refinement parameters """
		self._refine = refine

	def set_uni_refine(self, no_uni_refine):
		""" Set the number of uniform refinement """
		self._refine[1] = no_uni_refine

	def set_indicator(self, indicator):
		""" Set error indicator type """
		self._indicator = indicator

	def set_indicator_parameter(self, indicator_parameter):
		""" Set error indicator parameters """
		self._indicator_parameter = indicator_parameter

	def set_indicator_list(self, indicator_list):
		""" Set error indicator lists """
		self._indicator_list = indicator_list

	def add_indicator_list(self, new_indicator):
		""" Add error indicator value """
		self._indicator_list.append(new_indicator)

	def clear_indicator_list(self):
		""" Clear existing error indicator lists """
		self._indicator_list = []

	def set_norm(self, norm):
		""" Set norm type """
		self._norm = norm

	def set_min_data(self, min_data):
		""" Set min number of data for refine """
		self._min_data = min_data

	def set_run_gcv(self, run_gcv):
		""" Set whether to run GCV """
		self._run_gcv = run_gcv

	def set_gcv_parameter(self, gcv_parameter):
		""" Set parameters of the GCV """
		self._gcv_parameter = gcv_parameter

	def set_write_file(self, write_file):
		""" Set whether file parameters """
		self._write_file = write_file

	def set_folder(self, folder):
		""" Set the folder for the results """
		self._folder = folder

	def set_iteration(self, iteration):
		""" Set iteration number """
		self._info[0] = iteration

	def set_local(self, local=True):
		""" Set local parameter error indicator """
		self._info[1] = local

	def set_marker(self, marker=0):
		""" Set marker """
		self._info[2] = marker

	def add_marker(self):
		""" Set marker """
		self._info[2] += 1

	def set_sum(self, given_sum):
		""" Set a sum """
		self._info[3] = given_sum

	def add_sum(self, item):
		""" Add an item to the sum """
		self._info[3] += item

	def set_count(self, count):
		""" Set a count """
		self._info[4] = count

	def add_count(self):
		""" Add a count """
		self._info[4] += 1

	def set_ratios(self, ratios):
		""" Set ratios of the GCV during refinement """
		self._info[5] = ratios

	def add_ratio(self, ratio):
		""" Add a new ratio of the GCV during refinement """
		self._info[5].append(ratio)

	def set_data_ratio(self, data_ratio):
		""" Set ratio for data of the build equation """
		self._info[6] = data_ratio
		
	def set_boundary_init(self, boundary_init):
		""" Set initialisation of boundary node values """
		self._info[7] = boundary_init
		
	def init_stat(self):
		""" Set statistics """
		self._stat = []

	def set_stat(self, stat):
		""" Set statistics """
		self._stat = stat

	def set_cost(self, cost):
		""" Set cost record """
		self._cost = cost

	def add_stat(self, stat_item):
		""" Add statistics """
		self._stat.append(stat_item)

	def get_dim(self):	
		""" Get Dimension """
		return self._d

	def get_boundary(self):	
		""" Get boundary type """
		return self._boundary

	def get_basis(self):
		""" Get basis function """
		return self._basis

	def get_region_no(self):
		""" Get number of data regions """
		return self._region_no

	def get_function(self):
		""" Get model problem functions """
		return self._function

	def get_data_size(self):
		""" Get number of data points """
		return self._data_size

	def get_alpha(self):
		""" Get smoothing parameter alpha """
		return self._alpha

	def get_auxiliary_alpha(self):
		""" Get smoothing parameter alpha for auxiliary problems """
		return self._auxiliary_alpha

	def get_current_auxiliary_alpha(self):
		""" Get the current smoothing parameter alpha for auxiliary problems """
		if len(self._auxiliary_alpha) > 0:
			return self._auxiliary_alpha[-1]
		else:
			return 0.0

	def get_solver_tol(self):
		"""  Get solver tolerance """
		return self._solver_tol

	def get_dataset(self):
		""" Get dataset index """
		return self._dataset

	def get_subset(self):
		""" Get whether to subset data """
		return self._subset

	def get_error_tol(self):
		"""  Get error tolerance """
		return self._error_tol

	def get_refine(self):
		""" Get adaptive refinement parameters """
		return self._refine

	def get_marking_strategy(self):
		""" Get marking strategy for adaptive refinement """
		return self._refine[0]

	def get_uni_refine(self):
		""" Get the number of uniform refinement """
		return self._refine[1]

	def get_max_no(self):
		""" Get the max number of refinement each sweep """
		return self._refine[2]

	def get_max_sweep(self):
		""" Get the max number of refinement sweeps """
		return self._refine[3]

	def get_weight(self):
		""" Get weight for error indicator """
		return self._refine[4]

	def get_indicator(self):
		""" Get error indicator type """
#		print("function called",self._indicator)
		return self._indicator

	def get_indicator_parameter(self):
		""" Get error indicator parameters """
		return self._indicator_parameter

	def get_indicator_list(self):
		""" Get error indicator list """
		return self._indicator_list

	def get_norm(self):
		""" Get norm type """
		return self._norm

	def get_min_data(self):
		""" Get min number of data for refine """
		return self._min_data

	def get_run_gcv(self):
		""" Get whether to run GCV """
		return self._run_gcv

	def get_gcv_parameter(self):
		""" Get parameters of the GCV """
		return self._gcv_parameter

	def get_write_file(self):
		""" Get write file paramemeters """
		return self._write_file

	def get_write(self):
		""" Get whether to write results to a file """
		return self._write_file[0]

	def get_folder(self):
		""" Get the folder for the results """
		return self._folder

	def get_iteration(self):
		""" Get iteration number """
		return self._info[0]

	def get_local(self):
		""" Get local parameter error indicator """
		return self._info[1]

	def get_marker(self):
		""" Get marker """
		self._info[2] += 1
		return self._info[2]-1

	def get_sum(self):
		""" Get a sum """
		return self._info[3]

	def get_count(self):
		""" Get a count """
		return self._info[4]

	def get_ratios(self):
		""" Get ratios of the GCV during refinement """
		return self._info[5]

	def get_data_ratio(self):
		""" Get ratio for data of the build equation """
		return self._info[6]

	def get_boundary_init(self):
		""" Get initialisation of boundary node values """
		return self._info[7] 
		
	def get_stat(self):
		""" Get statistics """
		return self._stat

	def get_mean(self):
		""" Get mean of data set """
		return self._stat[0]

	def get_cost(self):
		""" Get cost record """
		return self._cost
