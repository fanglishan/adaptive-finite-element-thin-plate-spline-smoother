#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-04-16 14:11:41
@Last Modified time: 2024-04-13 15:05:19

This class contains a set of classes that define the index of certain
parameter. The classes in this file relates to the grid.

"""

class NodeSet:
	""" Node types """
	full_node = 1
	ghost_node = 2


class RefineSet:
    """ Refine edge types """
    base_edge = 1
    not_base_edge = 2
    interface_base_edge = 3


class BoundaryType:
	""" Edge boundary types """
	Dirichlet = 1
	Neumann = 2


class BasisFunction:
	""" Degree of polynomial basis function """
	linear = 1
	quadratic = 2
	cubic = 3


class ModelProblem1D:
	""" 1D model problem functions """

	zero = 0
	linear = 1
	quadratic = 2
	cubic = 3
	exp = 4
	sine = 5              # Sine function
	osci_sine = 6         # Oscillatory sine function
	gradual_exp = 7       # Gradual exponential function
	steep_exp = 8         # Steep exponential function
	exp_fluc = 9          # Exponential function with some fluctuation
	exp_mul = 10          # Multiple exponential functions
	frac = 11             # Fraction function
	shift = 12            # Shifted absolute function
	runge = 13            # Runge function
	blocks = 14           # Blocks function
	bumps = 15            # Bumps function
	heavisine = 16        # HeaviSine function
	doppler = 17          # Doppler function
	hat = 18              # Mexican hat function

class ModelProblem2D:
	""" 2D model problem functions """

	magnet = -1             # Obagoola magnetic data
	bone = -2             	# Bone data
	lake = -3               # Medicine lake data
	lena = -4               # Lena image data
	crater = -5             # Crater lake data
	ironmine = -6           # Magnetic survey over the Pea Ridge iron mine
	mountain = -7           # Magnetic survey over Iron Mountain-Menominee Region
	burney = -8             # Aeromagnetic survey over Burney region
	canyon = -9             # Ground observations from Grapevine Canyon
	river = -10             # Bathymetry data for Unalakleet river mouth
	beach = -11             # Elevations survey for Black Beach,
	coast = -12             # Coastal bathymetry in Mississippi-Alabama
	
	zero = 0
	linear = 1
	quadratic = 2
	cubic = 3
	exp = 4
	sine = 5              # Sine function
	osci_sine = 6         # Oscillatory sine function
	gradual_exp = 7       # Gradual exponential function
	steep_exp = 8         # Steep exponential function
	exp_fluc = 9          # Exponential function with some fluctuation
	exp_mul = 10          # Multiple exponential functions
	sin_mul = 11          # Multiple oscillatory sine functions
	hole = 12             # Dataset with a hole in the middle in a diamond shape
	franke = 13           # The franke function
	tanh = 14             # Tanh function
	cone = 15             # Cone function
	peak = 16             # Peak function from MatLab
	lpeak = 17            # Peak function from MatLab with a larger domain
	nonhomo1 = 18         # Spatially nonhomogeneous test function 1
	nonhomo2 = 19         # Spatially nonhomogeneous test function 2

class ModelProblem3D:
	""" 3D model problem functions """

	zero = 0


class IndicatorType:
	""" Indicator types """

	test = 0        # NULL
	infinity = 1    # Uniform refinement
	true = 2        # True solution
	recovery = 3    # Recovery-based error indicator
	auxiliary = 4   # Auxiliary problem error indicator
	norm = 5        # Norm-based error indicator
	residual = 6    # Residual-based error indicator

	GCV = 7         # GCV error indicator         ### Depreciated
	regression = 8  # Measures regression error   ### Depreciated
	

class NormType:
	""" Norm types """
	maximum = 0
	one = 1
	two = 2

class CostType:
	""" Types of computational cost """

	indicate = 0
	indicate_refine = 1
	build = 2
	solve = 3
	gcv = 4
	gcv_auxiliary = 5
	refine = 6


	
class ErrorType:
	""" Types of error norm """

	### For uniform grids
	
	# 0 - One norm: (1/n)*sum(e_i)
	one_norm = 0

	# 1 - Two norm: (1/n)*sqrt(sum(e_i**2))
	two_norm = 1

	# 2 - Max norm: max(e_i)
	max_norm = 2

	### For adaptive grids

	# 3 - One norm(h): 
	# sum(h_i*e_i)-1D, sum(h_i**2*e_i)-2D
	one_h = 3
	
	# 4 - Two norm(h):
	# sqrt(sum(h_i*e_i**2))-1D, sqrt(sum(h_i**2*e_i**2))-2D
	two_h = 4

	# 5 - L2 norm: sqrt(integral((s-s_fine_grid)**2)
	L2 = 5

	# 6 - energy norm: sqrt(integral(d/dx(s-s_fine_grid)**2)
	energy = 6

	# 7 - Goodness of fit
	fit = 7

	# 8 - Smoothness of fit
	smooth = 8

	# 9 - root mean square error (RMSE)
	rmse = 9

	# 10 - Mean absolute error (MAE)
	mae = 10

	# 11- Mean squared percentage error (MSPE)
	mspe = 11
	
	# 12 - Mean absolute percentage error (MAPE)
	mape = 12
	
	# 13 - Root mean squared logarithm error (RMSLE)
	rmsle = 13
	#####
	# Note this one has been changed to root mean square percentage error (RMSPE)
	
	# 14 - Max data error
	max_data = 14

	# 15 - Min data error
	min_data = 15

	# 16 - Mean error
	me = 16
	

class NoiseDist:
	""" Type of noise distribution """

	# Uniform distribution
	uniform = 1

	# Normal distribution
	normal = 2



class OutputType:
	""" Type of noise distribution """

	# Uniform distribution
	uniform = 1

	# Normal distribution
	normal = 2