# -*- coding: utf-8 -*-
"""
@Author: Fang
@Date:   2022-04-03 08:31:05
@Last Modified by:   Fang
@Last Modified time: 2022-04-11 13:22:56

This class is intended to examine to the performance of the TPS.

"""

# Import libraries
from data.DatasetManager import obtain_dataset
from rbf.RbfPlot import plot_rbf, plot_contour
from rbf.SampleBasis import sample_basis
from rbf.RbfSolver import rbf
from rbf.CalcError import calc_errors, calc_errors_tps_deri
from data.BuildDataset import build_dataset
from plot.DataPlot import plot_data_dist_2D
import function.Function2D as f2D

import matplotlib.pyplot as plt



def find_optimal_alpha(alpha_list, RMSE_list):

	tol = 0.05

	for i in range(0, len(RMSE_list)-1):

		if abs((RMSE_list[i]-RMSE_list[i+1])/RMSE_list[i+1]) < tol:
			print("optimal alpha", i, alpha_list[i],RMSE_list[i]) 
			break


def average_lists(given_list, repeat):

#	length = int(len(given_list)/repeat)
#	print(len(given_list), repeat, length)
	new_list = []

	for j in range(0, len(given_list[0])):
		value = 0.0
		for i in range(0, repeat):
			value += given_list[i][j]
		value = value/repeat
		new_list.append(value)

	return new_list


def test_alpha(b, dataset, no_basis, uxy, f, repeat=1):

#	alpha = 0.0001
	d = 2

	data = obtain_dataset(dataset, False)

	alpha_list = []
	error_list = []

	alpha = 0.001
	for i in range(0, 9):
		alpha_list.append(alpha*0.1**i)

	for j in range(0, repeat):
		sampled_data = sample_basis(data, no_basis)

		RMSE_list = []

		print("Sampled", len(sampled_data), "points from", len(data), "points")
#		print("Smoothing parameter:", alpha)

		for i in range(0, len(alpha_list)):
			alpha = alpha_list[i]
			rbf_values = rbf(sampled_data, alpha, d, b)

		#	RMSE, MAE, ME, MAX, MIN = calc_errors(data, sampled_data, rbf_values, d, b)
		#	RMSE, MAE, ME, MAX, MIN = calc_errors_tps_deri(data, sampled_data, rbf_values, d, b, uxy, f)
			RMSE = calc_errors_tps_deri(data, sampled_data, rbf_values, d, b, uxy, f)
			RMSE_list.append(RMSE)
		error_list.append(RMSE_list)
#		print("alpha list =", alpha_list)
#		print("RMSE list =", RMSE_list)

		find_optimal_alpha(alpha_list, RMSE_list)

	print(alpha_list)
	print(error_list)
#	plt.loglog(alpha_list, RMSE_list)
#	plt.show()
#	plot_rbf(rbf_values, sampled_data, d, 1)

basis = 2
dataset = 2131224
no_basis = 100
repeat = 5
uxy = 0
f = f2D.franke_u
test_alpha(basis, dataset, no_basis, uxy, f, repeat)