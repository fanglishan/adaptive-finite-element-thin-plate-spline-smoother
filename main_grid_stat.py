#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-06-13 10:13:44
@Last Modified time: 2024-08-07 13:39:37

This class plots a TPSFEM solution from a stored grid in a file.

"""

# Import libraries
from grid.NodeTable import node_iterator
from grid.EdgeTable import endpt_iterator
from build.CopyGrid import copy_grid
from function.FunctionManager import obtain_model_function
from parameter.Parameter import Parameter
from parameter.ModelProblemFunction import find_model_problem
from estimator.Error import Error
from fileio.GridFunction import read_grid
import plot.FemPlot2D as femPlot2D
import plot.TpsfemPlot1D as tpsfemPlot1D
import plot.TpsfemPlot2D as tpsfemPlot2D
import plot.TpsfemPlotly2D as tpsfemPlotly2D
import plot.IndicatorPlot as indicatorPlot
import plot.ErrorPlot as errorPlot
import plot.SurfaceTriangulation as stPlot
import matplotlib.pyplot as plt

from numpy import sqrt
import numpy as np


def calc_dist(coord1, coord2):

	dist = 0.0
	for i in range(0, len(coord1)):
		dist += (coord1[i] - coord2[i])**2

	return sqrt(dist)


def calc_nearboundary_nodes(grid, tol):

	total = 0
	count = 0

	for node in node_iterator(grid):
		node_id = node.get_node_id()
		coord1 = grid.get_coord(node_id)

		total += 1

		if not grid.get_slave(node_id):

			for end_node in node_iterator(grid):
				endpt = end_node.get_node_id()

				if grid.get_slave(endpt):

					coord2 = grid.get_coord(endpt)
#					print(coord2)
#					print(calc_dist(coord1, coord2))
					if calc_dist(coord1, coord2) < tol:
						count += 1
						break

	print("count", count, ", total", total)
	print("Percentage of interior nodes near boundaries nodes within", tol, ":", 100.0*count/total, "%")


def plot_data_hist(data):

	plt.hist(data, bins=100)

	x_ticks = np.linspace(0, 0.1, 19)  # 产生区间在-5至4间的10个均匀数值
	plt.xticks(x_ticks)

	plt.show()


def obtain_mesh_sizes(grid,tol):

	mesh_sizes = []

	count = 0
	total = 0
	for node in node_iterator(grid):
		node_id = node.get_node_id()
		coord1 = grid.get_coord(node_id)
		for endpt in endpt_iterator(grid, node_id):
			if node_id < endpt:
	#			end_node = grid.get_node_ref(endpt)
				coord2 = grid.get_coord(endpt)

				dist = calc_dist(coord1, coord2)
				
#				print(coord1, coord2, dist)
				mesh_sizes.append(dist)

				if dist <= tol:
					count += 1
				total += 1
#	print(mesh_sizes)
	print("count", count, ", total", total)
	print("Percentage of mesh sizes below", tol, ":", 100.0*count/total, "%")

#	plot_data_hist(mesh_sizes)


# Obtain grid for error estimation
#path = "result/" + "result_1_6_20_02_01_11_02" + "/"
#filename = "1_5677"#path[14:19]
#filename = "1_177"
path = "result/" 
#path = "result/" + "result_5_4_21_03_20_14_11" + "/"
#filename = path[14:21]


#filename_list = ["7_16641", "7_6948", "7_6564", "7_6923", "7_6656", "7_11969", "7_7576", "7_7600", "7_7634", "7_7560", "9_16641", "9_6612", "9_6528", "9_6608", "9_6534", "9_10241", "9_7238", "9_7265", "9_7294", "9_7204", "10_16641", "10_6740", "10_6642", "10_6828", "10_6688", "10_9488", "10_6880", "10_6792", "10_6928", "10_6816"]
#filename = "10_10193"
filename_list = ["10_6816"]
#tol_list = [0.05, 0.01, 0.005, 0.001]
tol_list = [0.005]



for filename in filename_list:

	print("===", filename)

	for tol in tol_list:

		d = 2
		grid = read_grid(filename, path)
		parameter = Parameter()

#		tol = 0.005

		calc_nearboundary_nodes(grid, tol)

		obtain_mesh_sizes(grid, tol)

		print("")


"""
filename = "7_7560"
#filename = "10_6801"

d = 2


grid = read_grid(filename, path)

#grid = copy_grid(grid)

#dataset = 1081200

#dataset = 9#int(filename[:7])

#data = obtain_dataset(dataset)
parameter = Parameter()
#parameter.set_dataset(dataset)

# Find model problems
#model_problem = find_model_problem(dataset)
#function = obtain_model_function(model_problem, d)
#parameter.set_function(function)


#print(dataset)
#print("grid node number", grid.get_no_nodes())


tol = 0.005

calc_nearboundary_nodes(grid, tol)

obtain_mesh_sizes(grid, tol)
"""

show_plot = False

if show_plot:

	# Plot resulting grids
	if grid.get_dim() == 1:
#		indicatorPlot.plot_error_indicator(grid, parameter)
#		tpsfemPlot1D.plot_tpsfem_solution_1D(grid, None, None, False)

		pass

	elif grid.get_dim() == 2:
#		stPlot.plot_surface_triangulation(grid)
#		femPlot2D.plot_fem_grid_2D(grid, None, False)
#		tpsfemPlot2D.plot_tpsfem_solution_2D(grid)
#		tpsfemPlot2D.plot_contour(grid)
#		tpsfemPlotly2D.plot_contour(grid)
		errorPlot.plot_error_contour_2D(grid, parameter)
		pass
#	plt.xlim(-0.05,1.05)
#	plt.ylim(-0.05,1.05)
	plt.show()