#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-04-21 14:23:38
@Last Modified time: 2022-10-30 21:07:32

This files contains routines to build a 2D intial grid 
with "L" shape.

"""

# Import libraries
from grid.GlobalNodeID import GlobalNodeID
from grid.Node import Node
from function.FunctionStore import zero
from grid.Edge import Edge, DomainSet, RefineSet
from grid.NodeTable import node_iterator
from grid.EdgeTable import endpt_iterator
from grid.GlobalNodeID import mesh_integers_2D

from copy import copy
from numpy import arange


def add_edge(grid, id1, id2, refine_type, location, \
		boundary_function = zero):
	""" Add two edges to the grid """

	# Create the first edge from 1 to 2
	edge1 = Edge(id1, id2)

	# Set location for the edge
	edge1.set_location(location)

	# Set refine type for the edge
	edge1.set_refine_type(refine_type)

	# Set the boundary function for the edge
	edge1.set_boundary_function(boundary_function)
	
	# Set refine level as 0 since no refinement yet
	edge1.set_refine_level(0)
	
	# Copy the frist edge with all its information
	edge2 = copy(edge1)

	# Replace start node 1 with node 2
	edge2.set_endpt1(id2)

	# Replace end node 2 with node 1
	edge2.set_endpt2(id1)

	# Add edge 1 to the grid
	grid.add_edge_all(edge1)

	# Add edge 2 to the grid
	grid.add_edge_all(edge2)


def add_L_edges(grid, node_mesh, boundary_function):
	""" Link nodes with edges to form the L shape """

	# Set base as the base edge in RefineSet
	base = RefineSet.base_edge

	# Set not_base as not base edge in RefineSet
	not_base = RefineSet.not_base_edge

	# Set intp as interior in DomainSet
	intp = DomainSet.interior

	# Set bnd as boundary in DomainSet
	bnd = DomainSet.boundary
	
	# Add edges connected to the centre node
	id1 = node_mesh[1][1].get_node_id()

	id2 = node_mesh[0][0].get_node_id()
	add_edge(grid, id1, id2, base, intp, boundary_function)

	id2 = node_mesh[0][2].get_node_id()
	add_edge(grid, id1, id2, base, intp, boundary_function)

	id2 = node_mesh[2][0].get_node_id()
	add_edge(grid, id1, id2, base, intp, boundary_function)

	id2 = node_mesh[1][2].get_node_id()
	add_edge(grid, id1, id2, not_base, bnd, boundary_function)

	id2 = node_mesh[2][1].get_node_id()
	add_edge(grid, id1, id2, not_base, bnd, boundary_function)

	id2 = node_mesh[0][1].get_node_id()
	add_edge(grid, id1, id2, not_base, intp, boundary_function)

	id2 = node_mesh[1][0].get_node_id()
	add_edge(grid, id1, id2, not_base, intp, boundary_function)

	# Add boundary edges
	id1 = node_mesh[0][0].get_node_id()

	id2 = node_mesh[1][0].get_node_id()
	add_edge(grid, id1, id2, not_base, bnd, boundary_function)

	id2 = node_mesh[0][1].get_node_id()
	add_edge(grid, id1, id2, not_base, bnd, boundary_function)

	id1 = node_mesh[0][2].get_node_id()

	id2 = node_mesh[0][1].get_node_id()
	add_edge(grid, id1, id2, not_base, bnd, boundary_function)

	id2 = node_mesh[1][2].get_node_id()
	add_edge(grid, id1, id2, not_base, bnd, boundary_function)

	id1 = node_mesh[2][0].get_node_id()

	id2 = node_mesh[1][0].get_node_id()
	add_edge(grid, id1, id2, not_base, bnd, boundary_function)

	id2 = node_mesh[2][1].get_node_id()
	add_edge(grid, id1, id2, not_base, bnd, boundary_function)


def add_L_nodes(grid, n, node_mesh):
	""" Add nodes to the grid """

	# Calcualte grid interval
	h = 0.5

	# Calculate an array of grid coordinates
	spacing = arange(0.0, 1.0+h, h)
	
	# Iterate in the x-direction
	for i in range(n):
		# Select the x coordinate
		x = spacing[i]

		# Iterate in the y-direction
		for j in range(n):
			# Select the y coordinate
			y = spacing[j]
			
			if i == 2 and j == 2:
				continue

			# Create x and y coordinate list
			coord = [x, y]
			
			# Create an new global ID
			global_id = GlobalNodeID()

			# Set the number for the gloal ID based on the iterator
			global_id.set_no(mesh_integers_2D(i, j))

			# Set level as 0 since there is no refinement yet
			global_id.set_level(0)
			
			# Create a new node in the grid with the global ID and coordinates
			# In default, this node is a slave node and has value 0
			node = grid.create_node(global_id, coord, True, 0.0)
			
			# Add the new node into the grid
			grid.add_node(node)
			
			# Add the new node into the node meshs
			node_mesh[i][j] = copy(node)
	

def add_L_connections(grid, node_mesh):
	""" Add connections between connected nodes """
	
	# Loop over edges in the grid
	for node in node_iterator(grid):
		for endpt in endpt_iterator(grid, node.get_node_id()):

			# Add a connection between two nodes
			grid.add_connection(node.get_node_id(),endpt,-1)


def build_L_grid(grid, boundary_function):
	""" Build a L-shaped grid with nodes and edges """

	# Create a empty new node
	node = Node()

	n = 3

	# Create a set of empty nodes in a 2D array
	node_mesh = [[node]*n for x in range(n)]
	
	# Add nodes to the grid
	add_L_nodes(grid, n, node_mesh)

	# Add edges to connect the nodes
	add_L_edges(grid, node_mesh, boundary_function)

	# Store the connections in the matrix
	add_L_connections(grid, node_mesh)   
