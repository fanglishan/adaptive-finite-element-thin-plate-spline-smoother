#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-04-21 14:23:38
@Last Modified time: 2022-12-06 12:01:00

This class contains routines to build an initial grid in
different shapes.

"""

# Import libraries
from grid.Grid import Grid
from grid.NodeTable import node_iterator
from grid.EdgeTable import endpt_iterator
from grid.Edge import RefineSet
from build.BuildLine import build_line_grid
from build.BuildSquare import build_square_grid
from build.BuildBigSquare import build_big_square_grid
from build.BuildPacman import build_pacman_grid
from build.BuildLShaped import build_L_grid
from build.BuildHexagon import build_hexagon_grid
from build.BuildSquareHole import build_square_hole_grid
from build.BuildDiamond import build_diamond_grid
from build.BuildCube import build_cube_grid
from build.BuildPolygon import build_polygon_grid
from adaptive.triangle.AdaptiveRefinement import split_triangle, check_domain
from adaptive.tetrahedron.OrientateGrid import orientate_grid, manual_orientate
from parameter.Definition import BoundaryType
from parameter.ModelProblemFunction import find_model_problem
from parameter.Definition import ModelProblem2D
from plot.FemPlot2D import plot_fem_grid_2D


from copy import deepcopy



def build_base_edges(grid, domain):
	""" Build a list of base edges """

	# Initialise the bese edge list
	base_edges = list()

	# Loop over the full nodes
	for node in node_iterator(grid):
		node_id = node.get_node_id()
		for endpt in endpt_iterator(grid, node_id):
			if node_id < endpt:
				end_node = grid.get_node_ref(endpt)

				if grid.get_refine_type(node_id, endpt) == RefineSet.base_edge or \
				grid.get_refine_type(node_id, endpt) == RefineSet.interface_base_edge:

					# If one node lies inside the required domain
					if check_domain(node.get_coord(), domain) or \
						check_domain(end_node.get_coord(), domain):

						# Add the edge to the base edge list
						base_edges.append(grid.get_edge(node_id, endpt))

	return base_edges


def refine_region(grid, parameter, m, domain):
	""" Refine certain region of the initial grid """

	region = None

	# Make a copy of the current grid
	refine_grid = deepcopy(grid)
	
	# Apply m sweeps of refinement
	for i in range(m):

		# Find all of the base edges
		base_edges = build_base_edges(refine_grid, domain)
	
		# Stop refinements when no edge needs to be refined
		if len(base_edges) == 0:
			return refine_grid

		# Loop over the base edges and split or refine the triangle along that
		# base edge
		for edge in base_edges:
			split_triangle(refine_grid, region, edge, parameter)
	
	return refine_grid


def build_initial_grid(parameter, n, grid_type):
	""" Build an initial grid """

	# Paramters
	dataset = parameter.get_dataset()

	# Set model problems
	if len(str(dataset)) > 2:
		model_problem = int(str(dataset)[1:3])

	# Set local as true to avoid error indicators
	parameter.set_local(True)

	# Validate grid types
	if parameter.get_dim() == 1:
		grid_type = 1
	
	u = parameter.get_function()[0]

	grid = Grid()

	################################### Test polygon on model problems
#	if dataset == 2081204:
#		build_polygon_grid(grid, dataset)
	###################################
	if str(dataset) == "12":
		if grid_type == 1:
			build_square_grid(n, grid, u)
		elif grid_type == 2 or grid_type == 3:
			build_L_grid(grid, u)

	elif (grid_type == 2 or grid_type == 3) and len(str(dataset)) < 4:
#		build_square_grid(n, grid, u)
		build_polygon_grid(grid, dataset, grid_type)

	# 1D problems
	elif parameter.get_dim() == 1:

		# Build a standard [0,1] grid
		if grid_type == 1:
			build_line_grid(n, grid, u, model_problem)
		else:
			print("Warning: build/BuildInitialGrid - invalid grid type")

	# 2D problems
	elif parameter.get_dim() == 2:

		model_problem = find_model_problem(dataset)

		if model_problem == ModelProblem2D.franke or model_problem == ModelProblem2D.tanh or model_problem == ModelProblem2D.cone:
			build_big_square_grid(n, grid, u, -1.0, 2.0)

		elif model_problem == ModelProblem2D.peak:
			build_big_square_grid(n, grid, u, -3.0, 6.0)

		elif model_problem == ModelProblem2D.lpeak:
			build_big_square_grid(n, grid, u, -4.0, 8.0)

		elif model_problem == ModelProblem2D.hole:
			build_diamond_grid(n, grid, u)
			
		elif len(str(dataset)) > 2 and int(str(dataset)[3]) == 6:
			build_L_grid(grid, u)

		else:
			build_square_grid(n, grid, u)

		# Decide shape of grid using dataset for now
		"""
		# Build a square grid
		elif grid_type == 1:
			build_square_grid(n, grid, u)

		# Build a bigger square grid
		elif grid_type == 2:
			build_big_square_grid(n, grid, u)

		# Build an L-shaped grid
		elif grid_type == 3:
			build_L_grid(grid, u)

		# Build a hexagon grid
		elif grid_type == 4:
			build_hexagon_grid(grid, u)

		# Build a square hole grid
		elif grid_type == 5:
			build_square_hole_grid(grid, u)

		elif grid_type == 10:	
			# starting angle of the pacman, minimum 0
			start_angle = 0.0
			end_angle = 4.5
			N_angle = 5
			end_length = 1
			N_length = 3

			# Create a square FEM grid of size n*n
			build_pacman_grid(grid, start_angle, end_angle, N_angle, \
				end_length, N_length, u)

		else:
			print("Warning: build/BuildInitialGrid - invalid grid type")
		"""
	# 3D problems
	elif parameter.get_dim() == 3:
		build_cube_grid(n, grid, u)
		
#		orientate_grid(grid)

		manual_orientate(grid)

#		print(grid.is_edge("7", "10"))
#		print(grid.is_edge("10", "7"))
		"""
		for node in node_iterator(grid):
			node_id = node.get_node_id()
			for endpt in endpt_iterator(grid, node.get_node_id()):
				if node_id < endpt:
					print(node_id, endpt)
		"""

		"""
		for node in node_iterator(grid):
			node_id = node.get_node_id()
			for endpt in endpt_iterator(grid, node.get_node_id()):
				if node_id < endpt:
					if grid.get_refine_type(node_id, endpt) != grid.get_refine_type(endpt, node_id):
						print("ID:", node_id,",", endpt, "; base type:", grid.get_refine_type(node_id, endpt),",",grid.get_refine_type(endpt, node_id))
						grid.set_refine_type(node_id, endpt, 1)
						grid.set_refine_type(endpt, node_id, 1)
		"""
		import plot.FemPlot3D as femPlot3D
		femPlot3D.plot_fem_grid_3D_line(grid)

	parameter.set_local(False)

#	plot_fem_grid_2D(grid, parameter, True)

	return grid