# -*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang
@Date:   2022-03-17 23:15:17
@Last Modified by:   Fang
@Last Modified time: 2022-06-02 14:07:16

Build a polygon initial grid for real-world data sets, which may be distributed 
within a domain that is not polygon.

"""

# Import libraries
from grid.GlobalNodeID import GlobalNodeID
from grid.Node import Node
from grid.Edge import Edge, DomainSet, RefineSet
from grid.NodeTable import node_iterator
from grid.EdgeTable import endpt_iterator
from function.FunctionStore import zero
from parameter.InfoMesh import obtain_mesh_info

from copy import copy
from math import sqrt


def add_edge(grid, id1, id2, refine_type, location, boundary_function = zero):
	""" Add two edges to the grid """

	# Create the first edge from 1 to 2
	edge1 = Edge(id1, id2)

	# Set location for the edge
	edge1.set_location(location)

	# Set refine type for the edge
	edge1.set_refine_type(refine_type)

	# Set the boundary function for the edge
	edge1.set_boundary_function(boundary_function)
	
	# Set refine level as 0 since no refinement yet
	edge1.set_refine_level(0)
	
	# Copy the frist edge with all its information
	edge2 = copy(edge1)

	# Replace start node 1 with node 2
	edge2.set_endpt1(id2)

	# Replace end node 2 with node 1
	edge2.set_endpt2(id1)

	# Add edge 1 to the grid
	grid.add_edge_all(edge1)

	# Add edge 2 to the grid
	grid.add_edge_all(edge2)

	
def add_polygon_edges(grid, node_mesh, mesh_info, boundary_function=zero):
	""" Create and  edges to the grid """

	# Set base as the base edge in RefineSet
	base = RefineSet.base_edge

	# Set not_base as not base edge in RefineSet
	not_base = RefineSet.not_base_edge

	# Set intp as interior in DomainSet
	intp = DomainSet.interior

	# Set bnd as boundary in DomainSet
	bnd = DomainSet.boundary
	
	# Mesh information
	triangles = mesh_info[1]
	bnd_edges = mesh_info[2]
	base_edges = mesh_info[3]

	# Add edges
	for tri in triangles:
		id1 = node_mesh[tri[0]].get_node_id()
		id2 = node_mesh[tri[1]].get_node_id()
		id3 = node_mesh[tri[2]].get_node_id()

		if not grid.is_edge(id1, id2):
			add_edge(grid, id1, id2, not_base, intp, boundary_function)

		if not grid.is_edge(id2, id3):
			add_edge(grid, id2, id3, not_base, intp, boundary_function)

		if not grid.is_edge(id3, id1):
			add_edge(grid, id3, id1, not_base, intp, boundary_function)

	# Set boundary edges
	for edge in bnd_edges:
		node_id1 = node_mesh[edge[0]].get_node_id()
		node_id2 = node_mesh[edge[1]].get_node_id()
		grid.set_location(node_id1, node_id2, bnd)
		grid.set_location(node_id2, node_id1, bnd)

	# Set base edges
	for edge in base_edges:
		node_id1 = node_mesh[edge[0]].get_node_id()
		node_id2 = node_mesh[edge[1]].get_node_id()
		grid.set_refine_type(node_id1, node_id2, base)
		grid.set_refine_type(node_id2, node_id1, base)



def add_polygon_nodes(grid, node_mesh, mesh_info):
	""" Add nodes to the grid """

	# Import Python libraries
	from numpy import arange
	from grid.GlobalNodeID import mesh_integers_2D

	vertices = mesh_info[0]
	bnd_edges = mesh_info[2]

	n = int(sqrt(len(vertices)))+1

	k = 0

	for i in range(0, n):
		for j in range(0, n):

			if k >= len(vertices):
				continue
			print
			vertice = vertices[k]

			# Create x and y coordinate list
			coord = vertice[:2]
				
			# Create an new global ID
			global_id = GlobalNodeID()

			# Set the number for the gloal ID based on the iterator
			global_id.set_no(mesh_integers_2D(i, j))

			# Set level as 0 since there is no refinement yet
			global_id.set_level(0)
				
			# Create a new node in the grid with the global ID and coordinates
			# In default, this node is not a slave node and has value 0
			node = grid.create_node(global_id, coord, False, 0.0)
				
			# Add the new node into the grid
			grid.add_node(node)
				
			# Add the new node into the node meshs
			node_mesh[k] = copy(node)		

			k += 1

	# Set boundary nodes
	for edge in bnd_edges:
		node1 = node_mesh[edge[0]]
		node2 = node_mesh[edge[1]]
		grid.set_slave(node1.get_node_id(), True)
		grid.set_slave(node2.get_node_id(), True)



def add_polygon_connections(grid):
	""" Add connections between polygon nodes """
	
	# Loop over the nodes in the grid
	for node in node_iterator(grid):

		# Add connection to itself
		grid.add_connection(node.get_node_id(),node.get_node_id(),-1)

		# Loop over all of the endpoints of an edge joined to the
		# current node
		for endpt in endpt_iterator(grid, node.get_node_id()):

			# Add a connection between two nodes
			grid.add_connection(node.get_node_id(),endpt,-1)



def build_polygon_grid(grid, dataset, grid_type=1):
	""" Build a polygon grid with nodes, edges and connections """

	# Create a empty new node
	node = Node()

	# Obtain precomputed mesh info
	# 0-vertices, 1-triangles, 2-boundary edges, 3-base edges
	mesh_info = obtain_mesh_info(dataset, grid_type)
#	print(len(mesh_info[0]))
	# Create a set of empty nodes in a 2D array
	node_mesh = [node for x in range(len(mesh_info[0]))]
	
	# call a function to add a set of nodes to the grid
	add_polygon_nodes(grid, node_mesh, mesh_info)
	
	# call a function to craete and add a set of edges to the grid      
	add_polygon_edges(grid, node_mesh, mesh_info)

	# Store the 5-point stencil matrix in the table of connections
	add_polygon_connections(grid)   