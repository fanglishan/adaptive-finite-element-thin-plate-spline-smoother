# -*- coding: utf-8 -*-
"""
@Author: Fang
@Date:   2022-05-22 09:18:35
@Last Modified by:   Fang
@Last Modified time: 2022-10-09 23:46:27

This class contains routines to trim elements that does not contain any 
data point from the grid. Currently it only works for 2D grids.

"""


from grid.EdgeTable import endpt_iterator
from grid.NodeTable import node_iterator
from grid.Edge import Edge, DomainSet, RefineSet
from adaptive.triangle.Triangle import triangle_iterator_ref
from adaptive.triangle.TriangleFunction import find_triangle_pairs
from data.CheckData import check_data
from fileio.WriteResult import write_line


def delete_edge_info(grid, node_id1, node_id2):
	""" Delete information between two nodes """
	grid.delete_edge(node_id1, node_id2)
	grid.delete_edge(node_id2, node_id1)
	grid.delete_connection(node_id1, node_id2)
	grid.delete_connection(node_id2, node_id1)


def remove_edge(grid, node_id1, node_id2, node_id3):
	""" Remove boundary edge """

	no_removed = 1

	boundary_function = grid.get_boundary_function(node_id1, node_id2)

	# Remove the first edge
	delete_edge_info(grid, node_id1, node_id2)

	# Remove or set boundary for second edge
	if grid.get_location(node_id1, node_id3) == DomainSet.boundary:
		delete_edge_info(grid, node_id1, node_id3)
		no_removed += 1
	else:
		grid.set_location(node_id1, node_id3, DomainSet.boundary)
		grid.set_location(node_id3, node_id1, DomainSet.boundary)
		grid.set_boundary_function(node_id1, node_id3, boundary_function)
		grid.set_boundary_function(node_id3, node_id1, boundary_function)

	# Remove or set boundary for third edge
	if grid.get_location(node_id2, node_id3) == DomainSet.boundary:
		delete_edge_info(grid, node_id2, node_id3)
		no_removed += 1
	else:
		grid.set_location(node_id2, node_id3, DomainSet.boundary)
		grid.set_location(node_id3, node_id2, DomainSet.boundary)
		grid.set_boundary_function(node_id2, node_id3, boundary_function)
		grid.set_boundary_function(node_id3, node_id2, boundary_function)

	grid.set_slave(node_id3, True)

	return no_removed
		

def calc_dist_2D(coord1, coord2):
	return (coord1[0]-coord2[0])**2+(coord1[1]-coord2[1])**2


def set_base_edges(grid, node_id1, node_id2):
	""" Set base edges if missing """

	node3, node4 = find_triangle_pairs(grid, node_id, endpt)
	node_id3 = node3.get_node_id()


	if not (grid.get_refine_type(node_id1, node_id2) == RefineSet.base_edge or \
		grid.get_refine_type(node_id2, node_id3) == RefineSet.base_edge or \
		grid.get_refine_type(node_id1, node_id3) == RefineSet.base_edge):

		coord1 = grid.get_coord(node_id1)
		coord2 = grid.get_coord(node_id2)
		coord3 = grid.get_coord(node_id3)

	dist1 = calc_dist(coord1, coord2)
	dist2 = calc_dist(coord2, coord3)
	dist3 = calc_dist(coord1, coord3)

	max_dist = max([dist1,dist2,dist3])

	if dist1 == max_dist:
		grid.set_refine_type(node_id1, node_id2, RefineSet.base_edge)
		grid.set_refine_type(node_id2, node_id1, RefineSet.base_edge)
	elif dist2 == max_dist:
		grid.set_refine_type(node_id2, node_id3, RefineSet.base_edge)
		grid.set_refine_type(node_id3, node_id2, RefineSet.base_edge)
	elif dist3 == max_dist:
		grid.set_refine_type(node_id1, node_id3, RefineSet.base_edge)
		grid.set_refine_type(node_id3, node_id1, RefineSet.base_edge)



def remove_unconnected_nodes(grid):
	""" Remove nodes that are not connected by edges """

	# Find connected nodes
	unconnected_list = []
	for node in node_iterator(grid):
		node_id = node.get_node_id()
		connected = False
		for endpt in endpt_iterator(grid, node_id):
			connected = True
		if not connected:
			unconnected_list.append(node_id)

	# Delete unconnected nodes
	for node_id in unconnected_list:
		grid.delete_node(node_id)


def fill_nonconvex_boundary(grid):
	""" Fill convex parts of the boundary """

	######### Not implemented for now
	pass
	"""
	for node in node_iterator(grid):
		node_id = node.get_node_id()

		if not grid.get_slave():
			continue

		for endpt in endpt_iterator(grid, node_id):

			if grid.get_location(node_id, endpt) == DomainSet.boundary:

				if 
	"""

def trim_elements(grid, region, parameter, trim_grid):
	""" Trim elements that does not contain data point """

	if not trim_grid:
		return grid

	region.init_usage()

	total_removed = 0

	trim_not_over = True

	while trim_not_over:

		no_edge_removed = 0
		# Iterate through each edge once
		for node in node_iterator(grid):
			node_id = node.get_node_id()
			for endpt in endpt_iterator(grid, node_id):
				if node_id > endpt:
					end_node = grid.get_node_ref(endpt)

					if grid.get_location(node_id, endpt) == DomainSet.boundary:

						node3, node4 = find_triangle_pairs(grid, node_id, endpt)
	#					print(node, end_node, node3, node4)
						if node3 is None:
							break
						no_data = check_data(region, [node,end_node, node3])

						if no_data == 0:
							no_removed = remove_edge(grid, node_id, endpt, node3.get_node_id())
							no_edge_removed += 1
							total_removed += no_removed
							break


		if no_edge_removed == 0:
			trim_not_over = False
		
	print("Trim", total_removed, "edges")

	"""
	# Set base edges in case some triangle does not have any base edge
	# No need for the regular square domain
	for node in node_iterator(grid):
		node_id = node.get_node_id()
		for endpt in endpt_iterator(grid, node_id):
			if node_id > endpt:
				end_node = grid.get_node_ref(endpt)

				if grid.get_location(node_id, endpt) == DomainSet.boundary:

					set_base_edges(grid, node_id1, node_id2)
	"""

#	grid.reset_counter()

	remove_unconnected_nodes(grid)

#	fill_nonconvex_boundary(grid)

	write_line(parameter, "Initial grid is trimmed", True)

	return grid
