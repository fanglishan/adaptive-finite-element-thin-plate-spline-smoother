#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2021-07-03 16:57:20
@Last Modified time: 2022-10-03 23:18:25

This class contains modules to build a cube. The codes are modified
from Stals'code.

"""

# Import libraries
from grid.GlobalNodeID import GlobalNodeID, mesh_integers_3D
from grid.Node import Node
from grid.Edge import Edge, DomainSet, TetraRefineSet
from grid.NodeTable import node_iterator
from grid.EdgeTable import endpt_iterator
from function.ConstantFunction import zero

from copy import copy, deepcopy
from numpy import arange


def add_node(grid, coord, slave, node_list):
	""" Add a node to the grid """

	# Create an new global ID
	global_id = GlobalNodeID()

	# Set the number for the gloal ID based on the iterator
	global_id.set_no(len(node_list))

	# Set level as 0 since there is no refinement yet
	global_id.set_level(0)

	# Create a new node in the grid with the global ID and coordinates
	# In default, this node is not a slave node and has value 0
	node = grid.create_node(global_id, coord, slave, 0.0)
			
	# Add the new node into the grid
	grid.add_node(node)

	node_list.append(node)



def add_nodes(grid, scale, initial, node_list):
	""" Add nodes to the grid """

	add_node(grid, [0.0,0.0,0.0], True, node_list)              # node 1
	add_node(grid, [scale,0.0,0.0], True, node_list)            # node 2
	add_node(grid, [0.0,scale,0.0], True, node_list)            # node 3
	add_node(grid, [0.0,0.0,scale], True, node_list)            # node 4
	add_node(grid, [0.0,scale,scale], True, node_list)          # node 5
	add_node(grid, [scale,0.0,scale], True, node_list)          # node 6      
	add_node(grid, [scale,scale,scale], True, node_list)        # node 7
	add_node(grid, [scale,scale,0.0], True, node_list)          # node 8
	add_node(grid, [scale/2,scale/2,scale/2], False, node_list) # node 9
	add_node(grid, [scale/2,scale/2,0.0], True, node_list)      # node 10
	add_node(grid, [scale/2,0.0,scale/2], True, node_list)      # node 11
	add_node(grid, [0.0,scale/2,scale/2], True, node_list)      # node 12
	add_node(grid, [scale/2,scale/2,scale], True, node_list)    # node 13
	add_node(grid, [scale/2,scale,scale/2], True, node_list)    # node 14
	add_node(grid, [scale,scale/2,scale/2], True, node_list)    # node 15


def add_edge(grid, id1, id2, refine_type, location, bnd_func=zero):
	""" Add two edges to the grid """

	if not grid.is_edge(id1, id2):

		# Create the first edge from 1 to 2
		edge1 = Edge(id1, id2)

		# Set location for the edge
		edge1.set_location(location)

		# Set refine type for the edge
		edge1.set_refine_type(refine_type)

		# Set the boundary function for the edge
		edge1.set_boundary_function(bnd_func)
		
		# Set refine level as 0 since no refinement yet
		edge1.set_refine_level(0)
		
		# Copy the frist edge with all its information
		edge2 = copy(edge1)

		# Replace start node 1 with node 2
		edge2.set_endpt1(id2)

		# Replace end node 2 with node 1
		edge2.set_endpt2(id1)

		# Add edge 1 to the grid
		grid.add_edge_all(edge1)

		# Add edge 2 to the grid
		grid.add_edge_all(edge2)


def add_edges(grid, bnd_func, node_list):
	""" Create and add edges to the grid """

	# Set base as the base edge in RefineSet
	base = TetraRefineSet.base_edge1

	# Set not_base as not base edge in RefineSet
	not_base = TetraRefineSet.not_base_edge

	# Set intp as interior in DomainSet
	intp = DomainSet.interior

	# Set bnd as boundary in DomainSet
	bnd = DomainSet.boundary
	

	id1 = node_list[0].get_node_id()
	id2 = node_list[1].get_node_id()
	id3 = node_list[2].get_node_id()
	id4 = node_list[3].get_node_id()
	id5 = node_list[4].get_node_id()
	id6 = node_list[5].get_node_id()
	id7 = node_list[6].get_node_id()
	id8 = node_list[7].get_node_id()
	id9 = node_list[8].get_node_id()
	id10 = node_list[9].get_node_id()
	id11 = node_list[10].get_node_id()
	id12 = node_list[11].get_node_id()
	id13 = node_list[12].get_node_id()
	id14 = node_list[13].get_node_id()
	id15 = node_list[14].get_node_id()

	# Set boundary edges
	add_edge(grid, id1, id2, base, bnd, bnd_func)
	add_edge(grid, id1, id4, base, bnd, bnd_func)
	add_edge(grid, id1, id11, not_base, bnd, bnd_func)
	add_edge(grid, id1, id3, base, bnd, bnd_func)
	add_edge(grid, id1, id10, not_base, bnd, bnd_func)
	add_edge(grid, id1, id12, not_base, bnd, bnd_func)
	add_edge(grid, id2, id11, not_base, bnd, bnd_func)
	add_edge(grid, id2, id6, base, bnd, bnd_func)
	add_edge(grid, id2, id15, not_base, bnd, bnd_func)
	add_edge(grid, id2, id8, base, bnd, bnd_func)
	add_edge(grid, id2, id10, not_base, bnd, bnd_func)
	add_edge(grid, id3, id10, not_base, bnd, bnd_func)	
	add_edge(grid, id3, id12, not_base, bnd, bnd_func)	
	add_edge(grid, id3, id14, not_base, bnd, bnd_func)	
	add_edge(grid, id3, id8, base, bnd, bnd_func)	
	add_edge(grid, id3, id5, base, bnd, bnd_func)	
	add_edge(grid, id4, id5, base, bnd, bnd_func)
	add_edge(grid, id4, id6, base, bnd, bnd_func)
	add_edge(grid, id4, id11, not_base, bnd, bnd_func)
	add_edge(grid, id4, id12, not_base, bnd, bnd_func)
	add_edge(grid, id4, id13, not_base, bnd, bnd_func)
	add_edge(grid, id5, id7, base, bnd, bnd_func)
	add_edge(grid, id5, id12, not_base, bnd, bnd_func)
	add_edge(grid, id5, id13, not_base, bnd, bnd_func)
	add_edge(grid, id5, id14, not_base, bnd, bnd_func)
	add_edge(grid, id6, id7, base, bnd, bnd_func)
	add_edge(grid, id6, id11, not_base, bnd, bnd_func)
	add_edge(grid, id6, id13, not_base, bnd, bnd_func)
	add_edge(grid, id6, id15, not_base, bnd, bnd_func)
	add_edge(grid, id7, id8, base, bnd, bnd_func)
	add_edge(grid, id7, id13, not_base, bnd, bnd_func)
	add_edge(grid, id7, id14, not_base, bnd, bnd_func)
	add_edge(grid, id7, id15, not_base, bnd, bnd_func)
	add_edge(grid, id8, id15, not_base, bnd, bnd_func)
	add_edge(grid, id8, id14, not_base, bnd, bnd_func)
	add_edge(grid, id8, id10, not_base, bnd, bnd_func)

	# Set internal edges
	add_edge(grid, id1, id9, not_base, intp, bnd_func)
	add_edge(grid, id2, id9, not_base, intp, bnd_func)
	add_edge(grid, id3, id9, not_base, intp, bnd_func)
	add_edge(grid, id4, id9, not_base, intp, bnd_func)
	add_edge(grid, id5, id9, not_base, intp, bnd_func)
	add_edge(grid, id6, id9, not_base, intp, bnd_func)
	add_edge(grid, id7, id9, not_base, intp, bnd_func)
	add_edge(grid, id8, id9, not_base, intp, bnd_func)
	add_edge(grid, id10, id9, not_base, intp, bnd_func)
	add_edge(grid, id11, id9, not_base, intp, bnd_func)
	add_edge(grid, id12, id9, not_base, intp, bnd_func)
	add_edge(grid, id13, id9, not_base, intp, bnd_func)
	add_edge(grid, id14, id9, not_base, intp, bnd_func)
	add_edge(grid, id15, id9, not_base, intp, bnd_func)


def add_connections(grid):
	""" Add connections between nodes """
	
	# Loop over the nodes in the grid
	for node in node_iterator(grid):

		# Add connection to itself
		grid.add_connection(node.get_node_id(),node.get_node_id(),-1)

		# Loop over all of the endpoints of an edge joined to the
		# current node
		for endpt in endpt_iterator(grid, node.get_node_id()):

			# Add a connection between two nodes
			grid.add_connection(node.get_node_id(),endpt,-1)	


def build_cube_grid(n, grid, u, bnd_func=zero):
	""" Build a 3D triangulation with nodes, edges and connections """

	# Build a coarse grid
	n = 2
	scale = 1.0
	initial = 0.0

	node_list = []

	# Set grid dimension as 3D
	grid.set_dim(3)

	# call a function to add a set of nodes to the grid
	add_nodes(grid, scale, initial, node_list)
	
	# call a function to craete and add a set of edges to the grid      
	add_edges(grid, bnd_func, node_list)

	# Store the 5-point stencil matrix in the table of connections
	add_connections(grid)