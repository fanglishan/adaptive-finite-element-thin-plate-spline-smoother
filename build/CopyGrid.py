#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2020-02-02 16:22:35
@Last Modified time: 2022-03-29 10:58:50

This class contains routines to copy a grid with some restrictions,
for example, only copy the grid in a certain domain.

"""

# Import libraries
from grid.Grid import Grid
from grid.NodeTable import node_iterator
from grid.EdgeTable import endpt_iterator
from function.FunctionStore import zero
from build.BuildSquare import add_edge



def copy_grid_2D(grid):

	new_grid = Grid()

	new_grid.set_dim(grid.get_dim())

	# Limit of the domain
	# x1 min, x1 max, x2 min, x2 max
	domain = [-0.00000000001, 1.00000000001, -0.00000000001, 1.00000000001]

	for node in node_iterator(grid):

		coord = node.get_coord()

		if coord[0] > domain[0] and coord[0] < domain[1] and \
			coord[1] > domain[2] and coord[1] < domain[3]:

			new_grid.add_node(node)

	for node in node_iterator(grid):
		node_id = node.get_node_id()
		for endpt in endpt_iterator(grid, node_id):
			if node_id > endpt:
	
				if new_grid.is_in(node_id) and new_grid.is_in(endpt): 

					add_edge(new_grid, node_id, endpt, grid.get_refine_type(node_id, endpt), grid.get_location(node_id, endpt), zero)


	return new_grid

	
def copy_grid(grid):



	return copy_grid_2D(grid)
