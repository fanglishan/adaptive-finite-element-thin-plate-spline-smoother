#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-04-21 14:23:38
@Last Modified time: 2022-03-29 10:58:17

This files contains routines to build a 2D intial grid 
with a hexagon shape.

"""

# Import libraries
from grid.GlobalNodeID import GlobalNodeID
from grid.Node import Node
from function.FunctionStore import zero
from grid.Edge import Edge, DomainSet, RefineSet
from grid.NodeTable import node_iterator
from grid.EdgeTable import endpt_iterator
from grid.GlobalNodeID import mesh_integers_2D

from copy import copy
from numpy import sqrt, arange


def add_edge(grid, id1, id2, refine_type, location, \
		boundary_function = zero):
	""" Add two edges to the grid """

	# Create the first edge from 1 to 2
	edge1 = Edge(id1, id2)

	# Set location for the edge
	edge1.set_location(location)

	# Set refine type for the edge
	edge1.set_refine_type(refine_type)

	# Set the boundary function for the edge
	edge1.set_boundary_function(boundary_function)
	
	# Set refine level as 0 since no refinement yet
	edge1.set_refine_level(0)
	
	# Copy the frist edge with all its information
	edge2 = copy(edge1)

	# Replace start node 1 with node 2
	edge2.set_endpt1(id2)

	# Replace end node 2 with node 1
	edge2.set_endpt2(id1)

	# Add edge 1 to the grid
	grid.add_edge_all(edge1)

	# Add edge 2 to the grid
	grid.add_edge_all(edge2)


def add_hexagon_edges(grid, node_mesh, boundary_function):
	""" Link nodes with edges to form the L shape """

	# Set base as the base edge in RefineSet
	base = RefineSet.base_edge

	# Set not_base as not base edge in RefineSet
	not_base = RefineSet.not_base_edge

	# Set intp as interior in DomainSet
	intp = DomainSet.interior

	# Set bnd as boundary in DomainSet
	bnd = DomainSet.boundary
	
	# Add edges connected to the centre node
	id1 = node_mesh[3].get_node_id()

	id2 = node_mesh[0].get_node_id()
	add_edge(grid, id1, id2, not_base, intp, boundary_function)

	id2 = node_mesh[1].get_node_id()
	add_edge(grid, id1, id2, not_base, intp, boundary_function)

	id2 = node_mesh[2].get_node_id()
	add_edge(grid, id1, id2, base, intp, boundary_function)
	
	id2 = node_mesh[4].get_node_id()
	add_edge(grid, id1, id2, base, intp, boundary_function)

	id2 = node_mesh[5].get_node_id()
	add_edge(grid, id1, id2, not_base, intp, boundary_function)

	id2 = node_mesh[6].get_node_id()
	add_edge(grid, id1, id2, not_base, intp, boundary_function)

	# Add boundary edges
	id1 = node_mesh[0].get_node_id()

	id2 = node_mesh[1].get_node_id()
	add_edge(grid, id1, id2, base, bnd, boundary_function)

	id2 = node_mesh[2].get_node_id()
	add_edge(grid, id1, id2, not_base, bnd, boundary_function)

	id1 = node_mesh[4].get_node_id()

	id2 = node_mesh[1].get_node_id()
	add_edge(grid, id1, id2, not_base, bnd, boundary_function)

	id2 = node_mesh[6].get_node_id()
	add_edge(grid, id1, id2, not_base, bnd, boundary_function)

	id1 = node_mesh[5].get_node_id()

	id2 = node_mesh[2].get_node_id()
	add_edge(grid, id1, id2, not_base, bnd, boundary_function)

	id2 = node_mesh[6].get_node_id()
	add_edge(grid, id1, id2, base, bnd, boundary_function)


def add_hexagon_nodes(grid, node_mesh):
	""" Add nodes to the grid """

	sqrt32 = sqrt(3)/2

	# Bottom left node
	coord = [-0.5, -1*sqrt32]
	global_id = GlobalNodeID()
	global_id.set_no(mesh_integers_2D(0, 0))
	global_id.set_level(0)
	node = grid.create_node(global_id, coord, True, 0.0)
	grid.add_node(node)
	node_mesh.append(node)

	# Bottom right node
	coord = [0.5, -1*sqrt32]
	global_id = GlobalNodeID()
	global_id.set_no(mesh_integers_2D(1, 0))
	global_id.set_level(0)
	node = grid.create_node(global_id, coord, True, 0.0)
	grid.add_node(node)
	node_mesh.append(node)

	# Left node
	coord = [-1.0, 0.0]
	global_id = GlobalNodeID()
	global_id.set_no(mesh_integers_2D(0, 1))
	global_id.set_level(0)
	node = grid.create_node(global_id, coord, True, 0.0)
	grid.add_node(node)
	node_mesh.append(node)

	# Centre node
	coord = [0.0, 0.0]
	global_id = GlobalNodeID()
	global_id.set_no(mesh_integers_2D(1, 1))
	global_id.set_level(0)
	node = grid.create_node(global_id, coord, False, 0.0)
	grid.add_node(node)
	node_mesh.append(node)

	# Right node
	coord = [1.0, 0.0]
	global_id = GlobalNodeID()
	global_id.set_no(mesh_integers_2D(2, 1))
	global_id.set_level(0)
	node = grid.create_node(global_id, coord, True, 0.0)
	grid.add_node(node)
	node_mesh.append(node)

	# Top left node
	coord = [-0.5, 1*sqrt32]
	global_id = GlobalNodeID()
	global_id.set_no(mesh_integers_2D(0, 2))
	global_id.set_level(0)
	node = grid.create_node(global_id, coord, True, 0.0)
	grid.add_node(node)
	node_mesh.append(node)

	# Top right node
	coord = [0.5, 1*sqrt32]
	global_id = GlobalNodeID()
	global_id.set_no(mesh_integers_2D(1, 2))
	global_id.set_level(0)
	node = grid.create_node(global_id, coord, True, 0.0)
	grid.add_node(node)
	node_mesh.append(node)


def add_hexagon_connections(grid, node_mesh):
	""" Add connections between connected nodes """
	
	# Loop over edges in the grid
	for node in node_iterator(grid):
		for endpt in endpt_iterator(grid, node.get_node_id()):

			# Add a connection between two nodes
			grid.add_connection(node.get_node_id(),endpt,-1)


def build_hexagon_grid(grid, boundary_function):
	""" Build a L-shaped grid with nodes and edges """

	# Create a empty new node
	node = Node()

	# Create an empty list
	node_mesh = []
	
	# Add nodes to the grid
	add_hexagon_nodes(grid, node_mesh)

	# Add edges to connect the nodes
	add_hexagon_edges(grid, node_mesh, boundary_function)

	# Store the connections in the matrix
	add_hexagon_connections(grid, node_mesh)   
