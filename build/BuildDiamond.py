#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2020-04-10 20:47:21
@Last Modified time: 2022-03-29 10:58:10

This files contains routines to build a 2D intial grid in diamond shape.

"""


# Import libraries
from grid.GlobalNodeID import GlobalNodeID
from grid.Node import Node
from grid.Edge import Edge, DomainSet, RefineSet
from grid.NodeTable import node_iterator
from grid.EdgeTable import endpt_iterator
from function.FunctionStore import zero

from copy import copy
from math import sin, cos, pi
from numpy import sqrt



def add_edge(grid, id1, id2, refine_type, location, \
		boundary_function = zero):
	""" Add two edges to tdhe grid """

	# Create the first edge from 1 to 2
	edge1 = Edge(id1, id2)

	# Set location for the edge
	edge1.set_location(location)

	# Set refine type for the edge
	edge1.set_refine_type(refine_type)

	# Set the boundary function for the edge
	edge1.set_boundary_function(boundary_function)
	
	# Set refine level as 0 since no refinement yet
	edge1.set_refine_level(0)
	
	# Copy the frist edge with all its information
	edge2 = copy(edge1)

	# Replace start node 1 with node 2
	edge2.set_endpt1(id2)

	# Replace end node 2 with node 1
	edge2.set_endpt2(id1)

	# Add edge 1 to the grid
	grid.add_edge_all(edge1)

	# Add edge 2 to the grid
	grid.add_edge_all(edge2)

	
def add_square_edges(grid, n, node_mesh, boundary_function):
	""" Create and  edges to the grid """

	# Set base as the base edge in RefineSet
	base = RefineSet.base_edge

	# Set not_base as not base edge in RefineSet
	not_base = RefineSet.not_base_edge

	# Set intp as interior in DomainSet
	intp = DomainSet.interior

	# Set bnd as boundary in DomainSet
	bnd = DomainSet.boundary
	
	# Iterate through interior nodes in x-direction
	for i in range(1, n-1):
		# Iterate through interior nodes in y-direction
		for j in range(1, n-1):
			# Get the node id from one interior node
			id1 = node_mesh[i][j].get_node_id()

			# Get the node id from the node on the right of the first node
			id2 = node_mesh[i+1][j].get_node_id()

			# Add two non base iterior edges between these two nodes
			add_edge(grid, id1, id2, not_base, intp, boundary_function)
			
			# Get the node id from the node on the right of the previous node
			id2 = node_mesh[i][j+1].get_node_id()

			# Add two non base iterior edges between these two nodes
			add_edge(grid, id1, id2, not_base, intp, boundary_function)
			
			# Get the node id from the node on the top right of the previous node
			id2 = node_mesh[i+1][j+1].get_node_id()

			# Add two base iterior edges between these two nodes
			add_edge(grid, id1, id2, base, intp, boundary_function)

	# Iterate through interior nodes in y-direction        
	for i in range(1, n-1):
		# Get the node id from the left boundary nodes
		id1 = node_mesh[0][i].get_node_id()

		# Get the node id from the node on the right of the first node
		id2 = node_mesh[1][i].get_node_id()

		# Add two non base iterior edges between these two nodes
		add_edge(grid, id1, id2, not_base, intp, boundary_function)
		
		# Get the node id from the node on the top of the first node
		id2 = node_mesh[0][i+1].get_node_id()

		# Add two non base boundary edges between these two nodes
		add_edge(grid, id1, id2, not_base, bnd, boundary_function)
		
		# Get the node id from the node on the top right of the previous node
		id2 = node_mesh[1][i+1].get_node_id()

		# Add two base interior edges between these two nodes
		add_edge(grid, id1, id2, base, intp, boundary_function)
	
	# Iterate through interior nodes in y-direction     
	for i in range(0, n-1):
		# Get the node id from the right boundary nodes
		id1 = node_mesh[n-1][i].get_node_id()

		# Get the node id from the node on the top of the first node
		id2 = node_mesh[n-1][i+1].get_node_id()

		# Add two non base boundary edges between these two nodes
		add_edge(grid, id1, id2, not_base, bnd, boundary_function)
	
	# Iterate through interior nodes in x-direction
	for i in range(1, n-1):
		# Get the node id from the bottom boundary nodes
		id1 = node_mesh[i][0].get_node_id()

		# Get the node id from the node on the top of the first node
		id2 = node_mesh[i][1].get_node_id()
		
		# Add two non base interior edges between these two nodes
		add_edge(grid, id1, id2, not_base, intp, boundary_function)
		
		# Get the node id from the node on the right of the first node
		id2 = node_mesh[i+1][0].get_node_id()

		# Add two non base boundary edges between these two nodes
		add_edge(grid, id1, id2, not_base, bnd, boundary_function)
		
		# Get the node id from the node on the top right of the first node
		id2 = node_mesh[i+1][1].get_node_id()

		# Add two base interior edges between these two nodes
		add_edge(grid, id1, id2, base, intp, boundary_function)
	
	# Iterate through interior nodes in x-direction        
	for i in range(0, n-1):
		# Get the node id from the top boundary nodes
		id1 = node_mesh[i][n-1].get_node_id()

		# Get the node id from the node on the right of the first node
		id2 = node_mesh[i+1][n-1].get_node_id()

		# Add two non base boundary edges between these two nodes
		add_edge(grid, id1, id2, not_base, bnd, boundary_function)
			
 
	# Get the node id from the node at (0,0)
	id1 = node_mesh[0][0].get_node_id()

	# Get the node id from the node on the right of the first node
	id2 = node_mesh[1][0].get_node_id()

	# Add two non base boundary edges between these two nodes
	add_edge(grid, id1, id2, not_base, bnd, boundary_function)
	
	# Get the node id from the node on the top of the first node
	id2 = node_mesh[0][1].get_node_id()

	# Add two non base boundary edges between these two nodes
	add_edge(grid, id1, id2, not_base, bnd, boundary_function)
	
	# Get the node id from the node on the top right of the first node
	id2 = node_mesh[1][1].get_node_id()

	# Add two base interior edges between these two nodes
	add_edge(grid, id1, id2, base, intp, boundary_function)    


def add_square_nodes(grid, n, node_mesh):
	""" Add nodes to the grid """

	# Import Python libraries
	from numpy import arange
	from grid.GlobalNodeID import mesh_integers_2D

	# Calcualte grid interval
	h = 1.0/(n-1)

	# Calculate an array of grid coordinates
	spacing = arange(0.0, 1.0+h, h)
	
	# Iterate in the x-direction
	for i in range(n):
		# Select the x coordinate
		x = spacing[i]

		# Iterate in the y-direction
		for j in range(n):
			# Select the y coordinate
			y = spacing[j]
			
			# Create x and y coordinate list
			coord = [x, y]
			
			# Create an new global ID
			global_id = GlobalNodeID()

			# Set the number for the gloal ID based on the iterator
			global_id.set_no(mesh_integers_2D(i, j))

			# Set level as 0 since there is no refinement yet
			global_id.set_level(0)
			
			# Create a new node in the grid with the global ID and coordinates
			# In default, this node is not a slave node and has value 0
			node = grid.create_node(global_id, coord, False, 0.0)
			
			# Add the new node into the grid
			grid.add_node(node)
			
			# Add the new node into the node meshs
			node_mesh[i][j] = copy(node)
	
	# Iterate through the boundary nodes        
	for i in range(n):
		# Find nodes on the left boundary of the square
		node = node_mesh[0][i]

		# Set the node as a slave node
		grid.set_slave(node.get_node_id(), True)

		# Find nodes on the right boundary of the square
		node= node_mesh[n-1][i]

		# Set the node as a slave node
		grid.set_slave(node.get_node_id(), True)

		# Find nodes on the bottom boundary of the square
		node = node_mesh[i][0]

		# Set the node as a slave node
		grid.set_slave(node.get_node_id(), True)

		# Find nodes on the top boundary of the square
		node = node_mesh[i][n-1]

		# Set the node as a slave node
		grid.set_slave(node.get_node_id(), True)
		

def add_square_connections(grid, n, node_mesh):
	""" Add connections between square nodes """
	
	# Loop over the nodes in the grid
	for node in node_iterator(grid):

		# Add connection to itself
		grid.add_connection(node.get_node_id(),node.get_node_id(),-1)

		# Loop over all of the endpoints of an edge joined to the
		# current node
		for endpt in endpt_iterator(grid, node.get_node_id()):

			# Add a connection between two nodes
			grid.add_connection(node.get_node_id(),endpt,-1)


def rotate_grid(grid, degree=45.0):
	""" Rotate the grid to be a diamond shape """

	# Convert the angle from degree to radian
	radian = degree/180.0*pi
	
	for node in node_iterator(grid):

		coord = node.get_coord()

		# Rotate the coordinate and shift
		x = (cos(radian)*coord[0]-sin(radian)*coord[1])/sqrt(2)+0.5
		y = (sin(radian)*coord[0]+cos(radian)*coord[1])/sqrt(2)

		grid.set_coord(node.get_node_id(), [x,y])
		node.set_coord([x,y])


def build_diamond_grid(n, grid, boundary_function):
	""" Build a square grid with nodes, edges and connections """

	# Create a empty new node
	node = Node()

	# Create a set of empty nodes in a 2D array
	node_mesh = [[node]*n for x in xrange(n)]
	
	# The number of grids on x and y direction be should larger than 2
	assert n > 2, "the grid size must be > 2"
	
	# call a function to add a set of nodes to the grid
	add_square_nodes(grid, n, node_mesh)
	
	# call a function to craete and add a set of edges to the grid      
	add_square_edges(grid, n, node_mesh, boundary_function)

	# Store the 5- point stencil matrix in the table of connections
	add_square_connections(grid, n, node_mesh) 

	# Rotate the coordinates of the domain counter-clockwise by 45 degrees
	# Shrink it so it fit into [0,1]^2 domain and shift to the right by 0.5
	degree = 45

	rotate_grid(grid, degree)