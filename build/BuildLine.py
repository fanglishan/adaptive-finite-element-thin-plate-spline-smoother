#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-04-21 14:23:38
@Last Modified time: 2022-03-29 10:58:26

This files contains routines to build a 1D intial grid.

"""

# Import other classes
from grid.GlobalNodeID import GlobalNodeID, mesh_integers_1D
from grid.Node import Node
from function.FunctionStore import zero
from grid.Edge import Edge, DomainSet, RefineSet
from grid.NodeTable import node_iterator
from grid.EdgeTable import endpt_iterator
from parameter.Definition import ModelProblem1D

from numpy import arange
from copy import copy


def add_line_nodes(grid, n, node_mesh, start=0.0, end=1.0):
	""" Add nodes to the grid """

	# Calcualte grid interval
	h = (end-start)/(n-1)

	# Calculate an array of grid coordinates
	spacing = arange(start, end+h, h)
	
	# Iterate in the x-direction
	for i in range(n):

		# Create x coordinate
		coord = [spacing[i]]

		# Create an new global ID
		global_id = GlobalNodeID()

		# Set the number for the gloal ID based on the iterator
		global_id.set_no(mesh_integers_1D(i))

		# Set level as 0 since there is no refinement yet
		global_id.set_level(0)
			
		# Create a new node in the grid with the global ID and coordinates
		# In default, this node is not a slave node and has value 0
		node = grid.create_node(global_id, coord, False, 0.0)
			
		# Add the new node into the grid
		grid.add_node(node)
			
		# Add the new node into the node meshs
		node_mesh[i] = copy(node)

	# Set the first node as a slave node
	grid.set_slave(node_mesh[0].get_node_id(), True)

	# Set the last node as a slave node
	grid.set_slave(node_mesh[-1].get_node_id(), True)


def add_edge(grid, id1, id2, refine_type, location, \
        boundary_function = zero):
    """ Add two edges to the grid """

    # Create the first edge from 1 to 2
    edge1 = Edge(id1, id2)

    # Set location for the edge
    edge1.set_location(location)

    # Set refine type for the edge
    edge1.set_refine_type(refine_type)

    # Set the boundary function for the edge
    edge1.set_boundary_function(boundary_function)
    
    # Set refine level as 0 since no refinement yet
    edge1.set_refine_level(0)
    
    # Copy the frist edge with all its information
    edge2 = copy(edge1)

    # Replace start node 1 with node 2
    edge2.set_endpt1(id2)

    # Replace end node 2 with node 1
    edge2.set_endpt2(id1)

    # Add edge 1 to the grid
    grid.add_edge_all(edge1)

    # Add edge 2 to the grid
    grid.add_edge_all(edge2)

    
def add_line_edges(grid, n, node_mesh, boundary_function):
    """ Create and  edges to the grid """

    # Set base as the base edge in RefineSet
    base = RefineSet.base_edge

    # Set intp as interior in DomainSet
    intp = DomainSet.interior

    # Iterate through interior nodes in x-direction
    for i in range(0, n-1):

        # Get the node id from a node
        id1 = node_mesh[i].get_node_id()

        # Get the node id from a right node
        id2 = node_mesh[i+1].get_node_id()

        # Add two non base iterior edges between these two nodes
        add_edge(grid, id1, id2, base, intp, boundary_function)


def add_line_connections(grid, n, node_mesh):
	""" Add connections between square nodes """
	
	# Loop over the nodes in the grid
	for node in node_iterator(grid):

		# Add connection to itself
		grid.add_connection(node.get_node_id(),node.get_node_id(),-1)

		# Loop over all of the endpoints of an edge joined to the
		# current node
		for endpt in endpt_iterator(grid, node.get_node_id()):

			# Add a connection between two nodes
			grid.add_connection(node.get_node_id(),endpt,-1)


def build_line_grid(n, grid, boundary_function, model_problem):
	""" Build a 1D grid with nodes, edges and connections """

	# The range of domain
	if model_problem == ModelProblem1D.frac:
		start = -2.0
		end = 2.0
	elif model_problem == ModelProblem1D.shift or model_problem == ModelProblem1D.runge:
		start = -1.0
		end = 1.0
	else:
		start = 0.0
		end = 1.0

	# Set grid dimension as 1D
	grid.set_dim(1)

	# Create a empty new node
	node = Node()

	# Create a set of empty nodes in a 1D array
	node_mesh = [node for x in range(n)]

	# The number of grids on x direction be should larger than 2
	assert n > 2, "the grid size must be > 2"

	# call a function to add a set of nodes to the grid
	add_line_nodes(grid, n, node_mesh, start, end)

	# call a function to craete and add a set of edges to the grid      
	add_line_edges(grid, n, node_mesh, boundary_function)

	# Store the 5- point stencil matrix in the table of connections
	add_line_connections(grid, n, node_mesh)