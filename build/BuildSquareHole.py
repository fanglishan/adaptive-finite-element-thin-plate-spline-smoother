#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-04-21 14:23:38
@Last Modified time: 2022-03-29 10:58:47

This files contains routines to build a 2D intial square grid 
with a square hole in the middle.

"""

# Import libraries
from grid.GlobalNodeID import GlobalNodeID
from grid.Node import Node
from function.FunctionStore import zero
from grid.Edge import Edge, DomainSet, RefineSet
from grid.NodeTable import node_iterator
from grid.EdgeTable import endpt_iterator
from grid.GlobalNodeID import mesh_integers_2D

from copy import copy
from numpy import sqrt, arange


def add_edge(grid, id1, id2, refine_type, location, \
		boundary_function = zero):
	""" Add two edges to the grid """

	# Create the first edge from 1 to 2
	edge1 = Edge(id1, id2)

	# Set location for the edge
	edge1.set_location(location)

	# Set refine type for the edge
	edge1.set_refine_type(refine_type)

	# Set the boundary function for the edge
	edge1.set_boundary_function(boundary_function)
	
	# Set refine level as 0 since no refinement yet
	edge1.set_refine_level(0)
	
	# Copy the frist edge with all its information
	edge2 = copy(edge1)

	# Replace start node 1 with node 2
	edge2.set_endpt1(id2)

	# Replace end node 2 with node 1
	edge2.set_endpt2(id1)

	# Add edge 1 to the grid
	grid.add_edge_all(edge1)

	# Add edge 2 to the grid
	grid.add_edge_all(edge2)


def add_square_hole_edges(grid, n, node_mesh, boundary_function):
	""" Link nodes with edges to form the L shape """

	# Set base as the base edge in RefineSet
	base = RefineSet.base_edge

	# Set not_base as not base edge in RefineSet
	not_base = RefineSet.not_base_edge

	# Set intp as interior in DomainSet
	intp = DomainSet.interior

	# Set bnd as boundary in DomainSet
	bnd = DomainSet.boundary

	# Add outer boundary edges
	for i in range(0, n-1):

		id1 = node_mesh[i][0].get_node_id()
		id2 = node_mesh[i+1][0].get_node_id()
		add_edge(grid, id1, id2, not_base, bnd, boundary_function)

		id1 = node_mesh[i][n-1].get_node_id()
		id2 = node_mesh[i+1][n-1].get_node_id()
		add_edge(grid, id1, id2, not_base, bnd, boundary_function)

		id1 = node_mesh[0][i].get_node_id()
		id2 = node_mesh[0][i+1].get_node_id()
		add_edge(grid, id1, id2, not_base, bnd, boundary_function)

		id1 = node_mesh[n-1][i].get_node_id()
		id2 = node_mesh[n-1][i+1].get_node_id()
		add_edge(grid, id1, id2, not_base, bnd, boundary_function)

	# Add inner boundary edges
	id1 = node_mesh[1][1].get_node_id()

	id2 = node_mesh[1][2].get_node_id()
	add_edge(grid, id1, id2, not_base, bnd, boundary_function)

	id2 = node_mesh[2][1].get_node_id()
	add_edge(grid, id1, id2, not_base, bnd, boundary_function)

	id1 = node_mesh[3][1].get_node_id()

	id2 = node_mesh[2][1].get_node_id()
	add_edge(grid, id1, id2, not_base, bnd, boundary_function)

	id2 = node_mesh[3][2].get_node_id()
	add_edge(grid, id1, id2, not_base, bnd, boundary_function)

	id1 = node_mesh[1][3].get_node_id()

	id2 = node_mesh[1][2].get_node_id()
	add_edge(grid, id1, id2, not_base, bnd, boundary_function)

	id2 = node_mesh[2][3].get_node_id()
	add_edge(grid, id1, id2, not_base, bnd, boundary_function)

	id1 = node_mesh[3][3].get_node_id()

	id2 = node_mesh[3][2].get_node_id()
	add_edge(grid, id1, id2, not_base, bnd, boundary_function)

	id2 = node_mesh[2][3].get_node_id()
	add_edge(grid, id1, id2, not_base, bnd, boundary_function)

	# Add interior edges
	id1 = node_mesh[1][1].get_node_id()

	id2 = node_mesh[0][0].get_node_id()
	add_edge(grid, id1, id2, base, intp, boundary_function)

	id2 = node_mesh[0][2].get_node_id()
	add_edge(grid, id1, id2, base, intp, boundary_function)

	id2 = node_mesh[2][0].get_node_id()
	add_edge(grid, id1, id2, base, intp, boundary_function)

	id2 = node_mesh[1][0].get_node_id()
	add_edge(grid, id1, id2, not_base, intp, boundary_function)

	id2 = node_mesh[0][1].get_node_id()
	add_edge(grid, id1, id2, not_base, intp, boundary_function)

	id1 = node_mesh[3][1].get_node_id()

	id2 = node_mesh[2][0].get_node_id()
	add_edge(grid, id1, id2, base, intp, boundary_function)

	id2 = node_mesh[4][2].get_node_id()
	add_edge(grid, id1, id2, base, intp, boundary_function)

	id2 = node_mesh[4][0].get_node_id()
	add_edge(grid, id1, id2, base, intp, boundary_function)

	id2 = node_mesh[3][0].get_node_id()
	add_edge(grid, id1, id2, not_base, intp, boundary_function)

	id2 = node_mesh[4][1].get_node_id()
	add_edge(grid, id1, id2, not_base, intp, boundary_function)

	id1 = node_mesh[1][3].get_node_id()

	id2 = node_mesh[0][2].get_node_id()
	add_edge(grid, id1, id2, base, intp, boundary_function)

	id2 = node_mesh[2][4].get_node_id()
	add_edge(grid, id1, id2, base, intp, boundary_function)

	id2 = node_mesh[0][4].get_node_id()
	add_edge(grid, id1, id2, base, intp, boundary_function)

	id2 = node_mesh[0][3].get_node_id()
	add_edge(grid, id1, id2, not_base, intp, boundary_function)

	id2 = node_mesh[1][4].get_node_id()
	add_edge(grid, id1, id2, not_base, intp, boundary_function)

	id1 = node_mesh[3][3].get_node_id()

	id2 = node_mesh[4][4].get_node_id()
	add_edge(grid, id1, id2, base, intp, boundary_function)

	id2 = node_mesh[2][4].get_node_id()
	add_edge(grid, id1, id2, base, intp, boundary_function)

	id2 = node_mesh[4][2].get_node_id()
	add_edge(grid, id1, id2, base, intp, boundary_function)

	id2 = node_mesh[4][3].get_node_id()
	add_edge(grid, id1, id2, not_base, intp, boundary_function)

	id2 = node_mesh[3][4].get_node_id()
	add_edge(grid, id1, id2, not_base, intp, boundary_function)

	id1 = node_mesh[2][1].get_node_id()
	id2 = node_mesh[2][0].get_node_id()
	add_edge(grid, id1, id2, not_base, intp, boundary_function)

	id1 = node_mesh[1][2].get_node_id()
	id2 = node_mesh[0][2].get_node_id()
	add_edge(grid, id1, id2, not_base, intp, boundary_function)

	id1 = node_mesh[2][3].get_node_id()
	id2 = node_mesh[2][4].get_node_id()
	add_edge(grid, id1, id2, not_base, intp, boundary_function)

	id1 = node_mesh[3][2].get_node_id()
	id2 = node_mesh[4][2].get_node_id()
	add_edge(grid, id1, id2, not_base, intp, boundary_function)


def add_square_hole_nodes(grid, n, node_mesh):
	""" Add nodes to the grid """

	# Calcualte grid interval
	h = 1.0

	# Calculate an array of grid coordinates
	spacing = arange(-2.0, 2.0+h, h)
	
	# Iterate in the x-direction
	for i in range(n):
		# Select the x coordinate
		x = spacing[i]

		# Iterate in the y-direction
		for j in range(n):
			# Select the y coordinate
			y = spacing[j]
			
			# Skip the centre node
			if i == 2 and j == 2:
				continue

			# Create x and y coordinate list
			coord = [x, y]
			
			# Create an new global ID
			global_id = GlobalNodeID()

			# Set the number for the gloal ID based on the iterator
			global_id.set_no(mesh_integers_2D(i, j))

			# Set level as 0 since there is no refinement yet
			global_id.set_level(0)
			
			# Create a new node in the grid with the global ID and coordinates
			# In default, this node is a slave node and has value 0
			node = grid.create_node(global_id, coord, True, 0.0)
			
			# Add the new node into the grid
			grid.add_node(node)
			
			# Add the new node into the node meshs
			node_mesh[i][j] = copy(node)


def add_square_hole_connections(grid, node_mesh):
	""" Add connections between connected nodes """
	
	# Loop over edges in the grid
	for node in node_iterator(grid):
		for endpt in endpt_iterator(grid, node.get_node_id()):

			# Add a connection between two nodes
			grid.add_connection(node.get_node_id(),endpt,-1)


def build_square_hole_grid(grid, boundary_function):
	""" Build a square grid with a hole in the centre """

	# Create a empty new node
	node = Node()

	n = 5

	# Create a set of empty nodes in a 2D array
	node_mesh = [[node]*n for x in xrange(n)]
	
	# Add nodes to the grid
	add_square_hole_nodes(grid, n, node_mesh)

	# Add edges to connect the nodes
	add_square_hole_edges(grid, n, node_mesh, boundary_function)

	# Store the connections in the matrix
	add_square_hole_connections(grid, node_mesh)   
