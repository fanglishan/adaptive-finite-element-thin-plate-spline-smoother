# -*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang
@Date:   2022-03-26 14:27:57
@Last Modified by:   Fang
@Last Modified time: 2022-04-05 10:46:57

This class plots given functions to test whether derivatives are defined correctly.

"""

# Import libraries
import function.Function2D as function2D
import function.FunctionTPS as functionTPS

import matplotlib.pyplot as plt
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D
from matplotlib.ticker import LinearLocator, FormatStrFormatter
import numpy as np
from numpy import sqrt
from math import pi, sin, exp, cos, tanh


def plot_data_2D(data):
	""" Plot 2D data points """

#	plt.clf()

	# Initialise values of 3D coordinate for the data points
	x_vals = []
	y_vals = []
	z_vals = []

	# Add data coordinates and values to the arrays
	for data_point in data:
		x_vals.append(data_point[0])
		y_vals.append(data_point[1])
		z_vals.append(data_point[2])

	# Initialise a new figure and axis
	fig = plt.figure()
	ax = Axes3D(fig)
	fig.add_axes(ax)

	# Create the scatter data points
	points = ax.scatter(x_vals, y_vals, z_vals, s=10, cmap = cm.coolwarm, marker="o")

	plt.show()



f = function2D.franke_ux
#f = function2D.stexp_uy
#f = functionTPS.river_y

data = []
for i in range(0, 101):
	for j in range(0, 101):
#		data.append([i/100.0, j/100.0, f([i/100.0, j/100.0])])
		data.append([i/50.0-1, j/50.0-1, f([i/50.0-1, j/50.0-1])])

plot_data_2D(data)