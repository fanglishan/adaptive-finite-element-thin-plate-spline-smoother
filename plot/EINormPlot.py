#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2020-03-09 15:13:52
@Last Modified time: 2022-10-09 23:40:18

This class contains routines that plot the double derivative approximation
using c, g or w.

"""

# Import libraries
from grid.NodeTable import node_iterator
from grid.EdgeTable import endpt_iterator
from grid.Edge import DomainSet
from adaptive.line.SetPolynomial import get_solution_polynomial_linear_1D
from indicator.line.EINorm1D import approximate_local_Dxx_1D

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
from scipy.interpolate import griddata
from numpy import mgrid, array
import numpy as np



def plot_Dx_approximation_1D(grid, parameter):


	# Initialisea a figure
	fig = plt.figure("TPSFEM_" + str(parameter.get_dataset()) + "_Dx_1D")	
	
	ux = parameter.get_function()[1]

	xi = np.linspace(0.0,1.0,100).tolist()
	yi = []
	for i in range(len(xi)):
		yi.append(ux([xi[i]]))

	plt.plot(xi, yi,"k")
	plt.plot(xi, yi, "k", label="true derivative")
	
	approximate_local_Dxx_1D(grid, parameter, 1)

	# Loop through each edge
	for node in node_iterator(grid):
		node_id = node.get_node_id()
		print(node_id, node.get_parameters_entry(0))
		for endpt in endpt_iterator(grid, node.get_node_id()):
			if node_id > endpt:
				end_node = grid.get_node(endpt)

				# Create a list of x coordinates of two nodes
				x = [node.get_coord()[0],grid.get_coord(endpt)[0]]
				y = [node.get_parameters_entry(0),end_node.get_parameters_entry(0)]
				
				# Plot the line between two nodes
				plt.plot(x,y,"g:", linewidth=3.0)
				
	plt.plot(x,y,"g:", linewidth=3.0, label="Dx approximation using c")
	print("")

	approximate_local_Dxx_1D(grid, parameter, 2)

	# Loop through each edge
	for node in node_iterator(grid):
		node_id = node.get_node_id()
		for endpt in endpt_iterator(grid, node.get_node_id()):
			if node_id > endpt:
				end_node = grid.get_node(endpt)

				# Create a list of x coordinates of two nodes
				x = [node.get_coord()[0],grid.get_coord(endpt)[0]]
				y = [node.get_parameters_entry(0),end_node.get_parameters_entry(0)]
				
				# Plot the line between two nodes
				plt.plot(x,y,"r--", linewidth=3.0)
				print(x,y)

	plt.plot(x,y,"r--", linewidth=3.0, label="Dx approximation using g")
	print("")

	for node in node_iterator(grid):
#		plt.plot(node.get_coord()[0],node.get_value(),"ro")
		plt.plot(node.get_coord()[0],node.get_parameters_entry(0),"g.")

	# Plot nodes
	for node in node_iterator(grid):
#		plt.plot(node.get_coord()[0],node.get_value(),"ro")
		plt.plot(node.get_coord()[0],0,"wo")
	plt.plot(node.get_coord()[0],0,"wo", label="nodes")

	plt.legend(loc="upper right")
	plt.xlabel("x")
	plt.ylabel("Dx")
#	plt.show()



def plot_Dxx_approximation_1D(grid, parameter):
	""" Plot current approximation of Dxx """

	# Initialisea a figure
	fig = plt.figure("TPSFEM_" + str(parameter.get_dataset()) + "_Dxx_1D")	
	
	ux = parameter.get_function()[2]

	xi = np.linspace(0.0,1.0,100).tolist()
	yi = []
	for i in range(len(xi)):
		yi.append(ux([xi[i]]))

	plt.plot(xi, yi,"k")
	plt.plot(xi, yi, "k", label="true second derivative")
	
	
	approximate_local_Dxx_1D(grid, parameter, 1)

	# Loop through each edge
	for node in node_iterator(grid):
		node_id = node.get_node_id()
		for endpt in endpt_iterator(grid, node.get_node_id()):
			if node_id > endpt:
				end_node = grid.get_node(endpt)

				# Create a list of x coordinates of two nodes
				x = [node.get_coord()[0],grid.get_coord(endpt)[0]]
				y = [node.get_parameters_entry(1),end_node.get_parameters_entry(1)]
				
				# Plot the line between two nodes
				plt.plot(x,y,"g:", linewidth=3.0)
	plt.plot(x,y,"g:", linewidth=3.0, label="Dxx approximation using c")

	approximate_local_Dxx_1D(grid, parameter, 2)

	# Loop through each edge
	for node in node_iterator(grid):
		node_id = node.get_node_id()
		for endpt in endpt_iterator(grid, node.get_node_id()):
			if node_id > endpt:
				end_node = grid.get_node(endpt)

				# Create a list of x coordinates of two nodes
				x = [node.get_coord()[0],grid.get_coord(endpt)[0]]
				y = [node.get_parameters_entry(1),end_node.get_parameters_entry(1)]
				
				# Plot the line between two nodes
				plt.plot(x,y,"r--", linewidth=3.0)
	plt.plot(x,y,"r--", linewidth=3.0, label="Dxx approximation using g")
	
	approximate_local_Dxx_1D(grid, parameter, 3)

	# Loop through each edge
	for node in node_iterator(grid):
		node_id = node.get_node_id()
		for endpt in endpt_iterator(grid, node.get_node_id()):
			if node_id > endpt:
				end_node = grid.get_node(endpt)

				# Create a list of x coordinates of two nodes
				x = [node.get_coord()[0],grid.get_coord(endpt)[0]]
				y = [-node.get_parameters_entry(1),-end_node.get_parameters_entry(1)]
				
				# Plot the line between two nodes
				plt.plot(x,y,"y--", linewidth=2.0)
	plt.plot([0,0],[0,0],"y--", linewidth=2.0, label="Dxx approximation using w")


#	for node in node_iterator(grid):
#		plt.plot(node.get_coord()[0],node.get_value(),"ro")
#		plt.plot(node.get_coord()[0],node.get_parameters_entry(1),"g.")

	# Plot nodes
	for node in node_iterator(grid):
#		plt.plot(node.get_coord()[0],node.get_value(),"ro")
		plt.plot(node.get_coord()[0],0,"wo")
	plt.plot(node.get_coord()[0],0,"wo", label="nodes")

	plt.legend(loc="lower right")
	plt.xlabel("x")
	plt.ylabel("Dxx")
#	plt.show()



def plot_Dxy_approximation_2D(grid, parameter, gradient):
	""" Plot the 2D TPSFEM solution """

	if grid.get_dim() != 2:
		return

#	plt.clf()

	# Initialisea a figure
	fig = plt.figure("Derivative")

	# Find the position of the nodes and the values
	node_x = []
	node_y = []
	node_v = []

	# Add node coordinates and values
	for node in node_iterator(grid):

		coord = node.get_coord()
		node_x.append(coord[0])
		node_y.append(coord[1])
		if gradient == 0 or gradient == 1:
			node_v.append(node.get_parameters_entry(gradient))
		elif gradient == 2:
			node_v.append(node.get_parameters_entry(2)+node.get_parameters_entry(4))

	# Store the results in an array
	node_x = array(node_x)
	node_y = array(node_y)
	node_v = array(node_v)

	# Initialise the figure
	ax = fig.gca(projection ='3d')
	ax = fig.gca()

	# Interpolate the nodes onto a structured mesh
	X, Y = mgrid[node_x.min(): node_x.max():100j,
				 node_y.min(): node_y.max():100j]
	Z = griddata((node_x, node_y), node_v, (X, Y), method ='cubic')

	# Make a surface plot
	surf = ax.plot_surface(X, Y, Z, rstride=1, cstride=1,
		cmap=cm.coolwarm, linewidth=0, antialiased=False)
	
	# Set the z axis limits
	ax.set_zlim(node_v.min(), node_v.max())

	# Make the ticks look pretty
	ax.zaxis.set_major_locator(LinearLocator(10))
	ax.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))

	# Include a colour bar
	fig.colorbar(surf, shrink=0.5, aspect=5)

#	plt.xlim(-1,1)
#	plt.ylim(-1,1)
	# Show the plot
	plt.show()
