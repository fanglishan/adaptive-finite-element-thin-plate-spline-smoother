# -*- coding: utf-8 -*-
"""
@Author: Fang
@Date:   2022-08-15 22:18:01
@Last Modified by:   Fang
@Last Modified time: 2022-10-09 23:39:43

This class contains routines to plot the 2D TPSFEM solution using Plotly library.

"""


from grid.NodeTable import node_iterator
from grid.EdgeTable import endpt_iterator
from adaptive.triangle.GridValue import cal_smoother_2d

import matplotlib.pyplot as plt
import plotly.graph_objects as go


def plot_contour(grid, dataset=0):
	""" Plot 2D TPSFEM contour using Plotly """
	
	fig = plt.figure("Contour map")
	ax = plt.axes()

	v_min = 0.0
	v_max = 1.0
	step = 20
	step_size = abs(v_max-v_min)/(step-1)

	v_x = []
	v_y = []
	v_z = []

	v_z_max = -10000000000000.0
	v_z_min = 10000000000000.0

	"""
	for node in node_iterator(grid):
		coord = node.get_coord()

		v_x.append(coord[0])
		v_y.append(coord[1])
		value = cal_smoother_2d(grid, coord)
		v_z.append(value)
		v_z_max = max(value, v_z_max)
		v_z_min = min(value, v_z_min)
	"""	
	# Get value based on a square mesh
	for j in range(0,step):
		v_x.append(v_min+step_size*j)
		v_y.append(v_min+step_size*j)
		new_list = []
		for i in range(0,step):
			value = cal_smoother_2d(grid, [v_min+step_size*i,v_min+step_size*j])
			if value is None:
				new_list.append(None)
			else:
				new_list.append(value)
				v_z_max = max(value, v_z_max)
				v_z_min = min(value, v_z_min)

		v_z.append(new_list)
	
#	print(v_x,v_y)
#	print(v_z)
	print("max:",v_z_max,", min:",v_z_min)
	colorscale = [[0, 'blue'], [0.5, 'white'], [1, 'red']]
	
	no_contour = 30
	size = abs(v_z_max-v_z_min)/no_contour
#	print(v_z_max,v_z_min,size)
	fig = go.Figure(data =
		  go.Contour(
			z=v_z,
			x=v_x,   # horizontal axis
			y=v_y,   # vertical axis
			line_smoothing=0.85,
			colorscale='balance',
	        contours=dict(
	            start=v_z_min,
	            end=v_z_max,
	            size=size,)
		))
	
#	fig.write_image("images/TPSFEM_"+time.strftime('%y_%m_%d_%H_%M',time.localtime())+".pdf")
	fig.show()


