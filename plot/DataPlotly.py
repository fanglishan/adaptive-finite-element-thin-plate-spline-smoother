# -*- coding: utf-8 -*-
"""
@Author: Fang
@Date:   2023-08-14 12:56:16
@Last Modified by:   Fang
@Last Modified time: 2024-08-06 22:45:25

"""



import plotly.graph_objects as go
import numpy as np


def plot_data_2D(data):

	# Initialise values of 3D coordinate for the data points
	x = []
	y = []
	z = []

	# Add data coordinates and values to the arrays
	for data_point in data:
		x.append(data_point[0])
		y.append(data_point[1])
		z.append(data_point[2])

	fig = go.Figure(data=[go.Scatter3d(x=x, y=y, z=z, 
			 mode='markers',
			marker=dict(size=2, color=z, colorscale='Rainbow',opacity=0.8))])

	# Remove background
#	fig.update_scenes(xaxis_visible=False, yaxis_visible=False,zaxis_visible=False )

	fig.show()