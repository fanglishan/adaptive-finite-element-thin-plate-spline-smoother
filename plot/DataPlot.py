#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-04-16 13:03:48
@Last Modified time: 2024-09-23 09:53:48

This class contains routines to plot data points. The length of all
the data points should be consistent.

"""

# Import libraries
import matplotlib.pyplot as plt
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D
from matplotlib.ticker import LinearLocator, FormatStrFormatter
import numpy as np


def plot_data_1D(data):
	""" Plot 1D data points """

	plt.clf()
	
	# Plot data points as green triangles
	for data_point in data:
#		plt.plot(data_point[0],data_point[1],"g+", markersize=0.1)
		plt.plot(data_point[0],data_point[1],"g+", markersize=1)

#	plt.ylim(-0.2,1.2)


def plot_data_2D(data):
	""" Plot 2D data points """

#	plt.clf()

	# Initialise values of 3D coordinate for the data points
	x_vals = []
	y_vals = []
	z_vals = []

	# Add data coordinates and values to the arrays
	for data_point in data:
		x_vals.append(data_point[0])
		y_vals.append(data_point[1])
		z_vals.append(data_point[2])

	# Initialise a new figure and axis
	fig = plt.figure()
#	ax = Axes3D(fig)
	ax = fig.add_subplot(projection='3d')
	
	# Create the scatter data points
#	points = ax.scatter(x_vals, y_vals, z_vals, marker="o")#, cmap = cm.coolwarm)#, s=10, cmap = cm.coolwarm, marker="o")
#	points = ax.scatter(x_vals, y_vals, z_vals, s=10, color='purple', marker="o")
	ax.scatter(x_vals, y_vals, z_vals, marker=".")

#	plt.colorbar(points)
	# Set the z axis limits
#	ax.set_zlim(min(z_vals), max(z_vals))
#	ax.set_zlim(-0.04, 0.04)


def plot_data_3D(data):
	""" Plot 3D data points """

	# Initialise values of 3D coordinate for the data points
	x_vals = []
	y_vals = []
	z_vals = []

	# Add data coordinates and values to the arrays
	for data_point in data:
		x_vals.append(data_point[0])
		y_vals.append(data_point[1])
		z_vals.append(data_point[2])

	# Initialise a new figure and axis
	fig = plt.figure()
	ax = Axes3D(fig)

	# Create the scatter data points
	points = ax.scatter(x_vals, y_vals, z_vals, s=10, cmap = cm.coolwarm, marker="o")
	
	ax.set_xlim(-1,1)
	ax.set_ylim(-1,1)
	ax.set_zlim(-1,1)




def plot_data(data):
	""" Plot the given data """

	# Print warning message
	if len(data) == 0:
		print("Input data is empty")
		return

	print("Plot data for", len(data), "data points")

	# Plot data
	if len(data[0]) == 2:
		plot_data_1D(data)
	elif len(data[0]) == 3:
		plot_data_2D(data)
	elif len(data[0]) == 4:
		plot_data_3D(data)
	else:
		print("Warning: invalid data dimension")

	plt.show()



def create_uniform_data_2D(n):
	""" Create 2D uniform data """

	# Initialise a list for the data points in the grid 
	data = []

	data_value = 3000
	data_range = 0.999

	# Range of domain
	x_start = 0.4
	y_start = 0.5
	x_end = 0.7
	y_end = 0.8

	# Total length of the domain
	length_x = x_end-x_start
	length_y = y_end-y_start

	if length_x < 0 or length_y < 0:
		print("Warning: data/BuildData2D - negative domain length")

	if data_range > 1.0 or data_range < 0.0:
		print("Warning: data/BuildData2D - invalid proportion of data")

	# Interval length between data points
	x_interval = length_x/(n-1)*data_range
	y_interval = length_y/(n-1)*data_range

	# Start coordinate of the point
	x_coord_start = x_start + length_x*(1-data_range)/2.0
	y_coord_start = y_start + length_y*(1-data_range)/2.0

	# Set initial y coordinate
	y_coord = y_coord_start

	# Iterate through data points in the y direction
	for j in range(0, n):

		# Set initial x coordinate
		x_coord = x_coord_start

		# Iterate through data points in the x direction
		for i in range(0, n):

			# Set x, y coordinates and z value of the data point
			data_point = [x_coord, y_coord, data_value]

			# Update x coordinate
			x_coord += x_interval

			if data_value is False:
				continue
	
			# Add the data point to the list
			data.append(data_point)

		# Update y coordinate
		y_coord += y_interval

	return data


def plot_contour(data):
	""" Plot 2D data points """

	fig = plt.figure("Contour map")
	ax = plt.axes()

	x = []
	y = []
	z = []

	value_min = 10000000000.0
	value_max = -10000000000.0

	for i in range(0, len(data)):
		x.append(data[i][0])
		y.append(data[i][1])
		z.append(data[i][2])

		value_min = min(value_min, data[i][2])
		value_max = max(value_max, data[i][2])
	"""
	# Create data on regions without data points to make blank spaces
	blank_data = create_uniform_data_2D(30)

	dist_tol = 0.02**2

	for point in blank_data:

		added = True
		for data_point in data:
			if (point[0]-data_point[0])**2+(point[1]-data_point[1])**2 < dist_tol:
				added = False
				break
		if added:
			x.append(point[0])
			y.append(point[1])
			z.append(point[2])		
	"""

	print(value_min,value_max)
	# Customise range
#	value_min = 1920
#	value_max = 2110
#	value_min = 1200
#	value_max = 1950

	# Determine the levels of the contour map
	interval = abs(value_min-value_max)

#	levels = np.arange(value_min-interval/10.0,value_max+interval/10.0,interval/20.0)
	levels = np.arange(value_min-interval/30.0,value_max+interval/30.0,interval/30.0)

	# Plot contour map
	ax.tricontour(x, y, z, levels=levels, linewidths=0.001, colors='k')
	cntr = ax.tricontourf(x, y, z, levels=levels, cmap=cm.coolwarm)
#	cntr = ax.tricontourf(x, y, z, levels=levels, cmap="gray")

#	plt.xlim(0.4,0.7)
#	plt.ylim(0.5,0.8)
#	plt.xlim(0.2,0.5)
#	plt.ylim(0.25,0.55)

	fig.colorbar(cntr, ax=ax)
	
	plt.show()




def plot_data_dist_1D(data):
	""" Plot 1D data points """

	# Plot data points as green triangles
	for data_point in data:
		plt.plot(data_point[0],1.0,"b.")



def plot_data_dist_2D(data, plot_directly=True):
	""" Plot 2D data points """

	# Add data coordinates and values to the arrays
	for data_point in data:
		plt.plot(data_point[0],data_point[1],"bo", markersize=1)

#	plt.xlim(0.1,0.9)
#	plt.ylim(0.1,0.9)

	if plot_directly:
		plt.show()


def plot_data_dist(data):
	""" Plot the given data """

	import random

	max_data_size = 10000

	# Print warning message
	if len(data) == 0:
		print("Input data is empty")
		return

	print("Plot data distribution for", len(data), "data points")

	if len(data) > max_data_size:
		data = random.sample(data, max_data_size)
		print("Reduce to", max_data_size, "data points")

	# Plot data
	if len(data[0]) == 2:
		plot_data_dist_1D(data)
	elif len(data[0]) == 3:
		plot_data_dist_2D(data)
	else:
		print("Warning: invalid data dimension")

	plt.xlim(0.15,0.85)
	plt.ylim(0.15,0.85)
	plt.show()



def plot_data_hist(data):

	y_values = []
	for data_point in data:
		y_values.append(data_point[-1])
#	y_values = data[:][-1]

	plt.hist(y_values, bins=100)
	plt.show()


