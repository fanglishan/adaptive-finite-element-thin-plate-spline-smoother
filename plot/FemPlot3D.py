#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2021-07-04 16:54:30
@Last Modified time: 2022-12-06 13:15:23

This class contains modules to plot 3D triangulation.

"""

# Import libraries
from grid.NodeTable import node_iterator
from grid.EdgeTable import endpt_iterator
from grid.Edge import DomainSet, TetraRefineSet
from adaptive.triangle.Triangle import triangle_iterator_ref

import plotly.graph_objects as go
import plotly.express as px
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D
from matplotlib.ticker import LinearLocator, FormatStrFormatter
import numpy as np



def plot_fem_grid_3D_line(grid):
	""" Plot 3D fintie element grid """

	from adaptive.tetrahedron.Tetrahedron import tetrahedron_iterator

	# Edge types and positions
	not_base = TetraRefineSet.not_base_edge
	base1 = TetraRefineSet.base_edge1
	base2 = TetraRefineSet.base_edge2
	base3 = TetraRefineSet.base_edge3
	orient = TetraRefineSet.orient

	intp = DomainSet.interior
	bnd = DomainSet.boundary

	X = []
	Y = []
	Z = []
	color = []
	refine_type = []

	X_point = []
	Y_point = []
	Z_point = []
	color_point = []
	size_point = []
	node_id_list = []

	node_count = 0
	edge_count = 0
	base0_count = 0
	base1_count = 0
	base2_count = 0
	base3_count = 0
	orient_count = 0

	# Loop through each edge once
	for node in node_iterator(grid):

		node_id = node.get_node_id()
		coord = node.get_coord()
		X_point.append(coord[0])
		Y_point.append(coord[1])
		Z_point.append(coord[2])
		node_id_list.append(str(node_id))

		node_count += 1

		if node.get_slave():
			color_point.append("boundary")
			size_point.append(3)
		else:
			color_point.append("interior")
			size_point.append(2)

		for endpt in endpt_iterator(grid, node.get_node_id()):
			if node_id < endpt:
				edge_count += 1

				# Coordinates of two ends
				X += [node.get_coord()[0],grid.get_coord(endpt)[0]]
				Y += [node.get_coord()[1],grid.get_coord(endpt)[1]]
				Z += [node.get_coord()[2],grid.get_coord(endpt)[2]]

				color += [grid.get_refine_type(endpt,node_id),grid.get_refine_type(endpt,node_id)]

				# Plot edges in different colours
				if grid.get_refine_type(node_id, endpt) == not_base:
#					colour += ["not_base", "not_base"]
					base0_count += 1
				elif grid.get_refine_type(node_id, endpt) == base1:
#					colour += ["base1", "base1"]
					base1_count += 1
				elif grid.get_refine_type(node_id, endpt) == base2:
#					colour += ["base2", "base2"]
					base2_count += 1
				elif grid.get_refine_type(node_id, endpt) == base3:
#					colour += ["base3", "base3"]
					base3_count += 1
				elif grid.get_refine_type(node_id, endpt) == orient:
#					colour += ["orient", "orient"]
					orient_count += 1
#				elif grid.get_location(node_id, endpt) == bnd:
#					colour += ["boundary", "boundary"]

#	color = [1]*len(X)
	
	tetra_count = 0
	for tet in tetrahedron_iterator(grid):
		if tet[0].get_node_id() < tet[1].get_node_id() and \
		   tet[1].get_node_id() < tet[2].get_node_id() and \
		   tet[2].get_node_id() < tet[3].get_node_id():
#			print(tet[0].get_node_id(), tet[0].get_coord(), tet[1].get_node_id(), tet[1].get_coord(), tet[2].get_node_id(), tet[2].get_coord())
			tetra_count += 1


	print(node_count, "nodes,", edge_count, "edges,", tetra_count, "tetraherons")
	print(base0_count, "not base,", base1_count, "base 1,", base2_count, "base 2,", base3_count, "base 3,", orient_count, "orient")

#	fig1 = px.line_3d(x=X, y=Y, z=Z)#, color=color)#, text=color)
	
	# Plot 3D lines
	df = pd.DataFrame(dict(
		X = X,
		Y = Y,
		Z = Z,
#		color = color
	))

	print(df)

	fig1 = px.line_3d(df, x='X', y='Y', z='Z')#, color="color")
	
	# Plot 3D points and node ids
	point_df = pd.DataFrame(dict(
		X = X_point,
		Y = Y_point,
		Z = Z_point,
		color = color_point,
		node_id = node_id_list,
		size = size_point
	))	

	fig2 = px.scatter_3d(point_df, x='X', y='Y', z='Z', color="color",text="node_id",size="size")

	fig3 = go.Figure(data=fig1.data+fig2.data)

#	from adaptive.tetrahedron.CheckTetraBase import check_tetra_base

#	check_tetra_base(grid)
	
	fig3.show()



def plot_fem_grid_3D(grid):
	""" Plot 3D fintie element grid """

	fig = go.Figure(data=[
	    go.Mesh3d(
	        # 8 vertices of a cube
	        x=[0, 0, 1, 1, 0, 0, 1, 1],
	        y=[0, 1, 1, 0, 0, 1, 1, 0],
	        z=[0, 0, 0, 0, 1, 1, 1, 1],
	        colorbar_title='z',
	        colorscale=[[0, 'gold'],
	                    [0.5, 'mediumturquoise'],
	                    [1, 'magenta']],
	        # Intensity of each vertex, which will be interpolated and color-coded
	        intensity = np.linspace(0, 1, 14, endpoint=True),
	        intensitymode='cell',
	        # i, j and k give the vertices of triangles
	        i = [7, 0, 0, 0, 4, 4, 6, 6, 4, 0, 3, 2, 0, 0],
	        j = [3, 4, 1, 2, 5, 6, 5, 2, 0, 1, 6, 3, 6, 3],
	        k = [0, 7, 2, 3, 6, 7, 1, 1, 5, 5, 7, 6, 4, 4],
	        name='y',
	        showscale=True
	    )
	])

	fig.show()

	"""
	bnd = DomainSet.boundary

	node_x = []
	node_y = []
	node_z = []

	count = 1
	node_dict = {"a":0}
	for node in node_iterator(grid):
		if node.get_coord()[0] == 0.5 and node.get_coord()[1] == 0.5 and node.get_coord()[2] == 0.5:
			pass
		else:
			node_x.append(node.get_coord()[0])
			node_y.append(node.get_coord()[1])
			node_z.append(node.get_coord()[2])
			node_dict[str(node.get_node_id())] = count
			count += 1

	tri_x = []
	tri_y = []
	tri_z = []

	for tri in triangle_iterator_ref(grid):
		if tri[0].get_node_id()>tri[1].get_node_id() and \
			tri[1].get_node_id()>tri[2].get_node_id():

			if tri[0].get_slave() and tri[1].get_slave() and tri[2].get_slave():
				tri_x.append(node_dict[str(tri[0].get_node_id())])
				tri_y.append(node_dict[str(tri[1].get_node_id())])
				tri_z.append(node_dict[str(tri[2].get_node_id())])

	print(len(node_x),len(node_y),len(node_z),len(tri_x),len(tri_y),len(tri_z))
	print(node_x)
	print(node_y)
	print(node_z)
	print(tri_x)
	print(tri_y)
	print(tri_z)
	# Plot 3D mesh
	fig = go.Figure(
		data=[go.Mesh3d(
	        x=node_x,
	        y=node_y,
	        z=node_z,
	        colorbar_title='z',
	        colorscale=[[0, 'gold'],
	                    [0.5, 'mediumturquoise'],
	                    [1, 'magenta']],
	        # Intensity of each vertex, which will be interpolated and color-coded
	        intensity = np.linspace(0, 1, len(tri_x), endpoint=True),
	        intensitymode='cell',
	        # i, j and k give the vertices of triangles
	        # here we represent the 4 triangles of the tetrahedron surface
	        i=tri_x,
	        j=tri_y,
	        k=tri_z,
	        name='y',
	        showscale=True
	    )
	])

	fig.show()
	"""



def plot_fem_grid_3D_matplotlib(grid):
	""" Plot 3D fintie element grid using Matplotlib """

	marker_size = 10
	interior_edge = "b"

	# Edge types
	base = RefineSet.base_edge
	not_base = RefineSet.not_base_edge
	intp = DomainSet.interior
	bnd = DomainSet.boundary

	# Initialise a new figure and axis
	fig = plt.figure()
	ax = Axes3D(fig)

	# Loop through each edge once
	for node in node_iterator(grid):
		node_id = node.get_node_id()
		for endpt in endpt_iterator(grid, node.get_node_id()):
			if node_id > endpt:

				# Coordinates of two ends
				x = [node.get_coord()[0],grid.get_coord(endpt)[0]]
				y = [node.get_coord()[1],grid.get_coord(endpt)[1]]
				z = [node.get_coord()[2],grid.get_coord(endpt)[2]]

				# Plot the line
				if grid.get_refine_type(node_id, endpt) == base:
					ax.plot(x,y,z,interior_edge,c="yellow")
				elif grid.get_location(node_id, endpt) == bnd:
					ax.plot(x,y,z,interior_edge,c="red")
				else:
					ax.plot(x,y,z,interior_edge,c="blue")

	# Initialise values of 3D coordinate for the nodes
	x_vals = []
	y_vals = []
	z_vals = []

	# Loop through each edge once
	for node in node_iterator(grid):

		coord = node.get_coord()

		x_vals.append(coord[0])
		y_vals.append(coord[1])
		z_vals.append(coord[2])

	# Create the scatter data points
	points = ax.scatter(x_vals, y_vals, z_vals, s=marker_size, cmap = cm.coolwarm, marker="o")

	plt.show()
#	plt.colorbar(points)
	# Set the z axis limits
#	ax.set_zlim(min(z_vals), max(z_vals))
#	ax.set_zlim(-0.04, 0.04)