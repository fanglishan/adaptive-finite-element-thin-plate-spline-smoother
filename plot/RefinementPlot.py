#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-11-10 11:39:45
@Last Modified time: 2022-04-24 11:56:34

This class contains routines to plot the new refinement of
a certain grid. The new nodes will be plotted along with
connected nodes and edges.

"""

# Import libraries
from grid.NodeTable import node_iterator
from grid.EdgeTable import endpt_iterator

import matplotlib.pyplot as plt
import numpy as np



def obtain_new_nodes(grid, node_no):
	""" Obtain a list of new nodes from the refinement """

	new_node_list = []

	# Iterate and add new nodes
	for node in node_iterator(grid):

		if int(str(node.get_node_id())) >= node_no:
			new_node_list.append(node)

	return new_node_list



def plot_refinement_1D(grid, parameter, node_no):
	""" Plot new refinement of a 1D grid """

	plt.clf()

	fig = plt.figure()

	new_node_list = obtain_new_nodes(grid, node_no)

	# Iterate through each node
	for node in new_node_list:
		plt.plot(node.get_coord()[0], 1.0, "ro")



def plot_refinement_2D(grid, parameter, node_no):
	""" Plot new refinement of a 2D grid """
	
	plt.clf()
	
	fig = plt.figure()

	new_node_list = obtain_new_nodes(grid, node_no)

	# Iterate through each edge
	for node in new_node_list:
		node_id = node.get_node_id()
		for endpt in endpt_iterator(grid, node_id):
			if node_id > endpt:

				# Coordinates of two ends
				x = [node.get_coord()[0],grid.get_coord(endpt)[0]]
				y = [node.get_coord()[1],grid.get_coord(endpt)[1]]

				# Plot the line
				line = plt.plot(x,y, "b")

	# Iterate through each node
#	for node in new_node_list:
#		plt.plot(node.get_coord()[0],node.get_coord()[1],"ro")