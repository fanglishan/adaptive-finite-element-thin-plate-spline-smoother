#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-05-07 20:05:38
@Last Modified time: 2022-03-29 11:22:42

This class contains routines to plot the 1D TPSFEM solution.

"""

# Import libraries
from grid.NodeTable import node_iterator
from grid.EdgeTable import endpt_iterator
from grid.Edge import DomainSet

import matplotlib.pyplot as plt
import numpy as np


def plot_tpsfem_solution_1D(grid, region, parameter, print_data=True, solution=1, true_soln=False, show_plot=False):
	""" Plot the 1D TPSFEM solution with data """

	# Parameters

	# Marker for node location
	node_marker = -0.1

	if grid.get_dim() != 1:
		return

	# Initialisea a figure
#	fig = plt.figure(str(parameter.get_dataset()))
	fig = plt.figure()

	if print_data:

		# Region parameters
		mesh = region.get_mesh()
		region_no = region.get_region_no()

		# Plot data
		for i in range(0, region_no[0]):
			height = [0.1,0.2,0.3]
			for data in mesh[i]:
				plt.plot(data[0], data[1], "g.")
	plt.plot(0.0, 0.0, "g.", label="data", markersize=0.01)		

	# Plot true solution
	if true_soln:

		# Model problem function
		if solution == 1:
			u = parameter.get_function()[0]
		elif solution == 2:
			u = parameter.get_function()[1]
		elif solution == 3:
			u = parameter.get_function()[2]

		xi = np.linspace(0.0, 1.0, 500)
		fi = []
		for i in range(0, len(xi)):

			fi.append(u([xi[i]]))
#		plt.plot(xi, fi, "b", label="true second derivatives")

	x=[1,1]
	y=[1,1]
	# Loop through each edge
	for node in node_iterator(grid):
		node_id = node.get_node_id()
		for endpt in endpt_iterator(grid, node.get_node_id()):
			if node_id > endpt:
				end_node = grid.get_node(endpt)

				# Print solution
				if solution == 1:
					value1 = node.get_value()
					value2 = end_node.get_value()

				# Print first derivatives
				elif solution == 2:
					value1 = node.get_d_values()[0]
					value2 = end_node.get_d_values()[0]

				# Print second derivatives
				elif solution == 3:
					value1 = -node.get_d_values()[1]
					value2 = -end_node.get_d_values()[1]

				# Create a list of x coordinates of two nodes
				x = [node.get_coord()[0],grid.get_coord(endpt)[0]]
				y = [value1,value2]
			
				# Plot the line between two nodes
				plt.plot(x,y,"b-")
	plt.plot([0,0],[0,0],"b-", label="TPSFEM")
#				plt.text((x[0]+x[1])/2.0,0.01,grid.get_no_data(endpt, node_id)[0],fontsize="smaller", color="r")
#	plt.plot(x,y,"b-.",label="Dxx approximation")

	"""
	# Plot nodes
	for node in node_iterator(grid):
#		plt.plot(node.get_coord()[0],node.get_value(),"ro")
#		plt.plot(node.get_coord()[0],grid.get_value(node.get_node_id()),"ro")
		if solution == 1:
			plt.plot(node.get_coord()[0],node.get_value(),"wo")
		elif solution == 2:
			plt.plot(node.get_coord()[0],node.get_d_values()[0],"wo")
		elif solution == 3:
			plt.plot(node.get_coord()[0],-node.get_d_values()[1],"wo")
	"""
	"""
	# Plot node ids for smaller domain
	if parameter.get_local():

		for node in node_iterator(grid): 
			# Plot node id near the node
			plt.text(node.get_coord()[0]+0.001,node.get_value()+0.01,node.get_node_id(),fontsize="smaller")
		
			# Plot global node id near the node
#			plt.text(node.get_coord()[0]-0.03,node.get_value(),node.get_global_id(),fontsize="smaller")
	"""
	# Loop over the full nodes in the grid
	for node in node_iterator(grid):
		plt.plot(node.get_coord()[0],node_marker,"b+", markersize=0.4)
#		plt.plot(node.get_coord()[0],node.get_value(),"wo")
	plt.plot(0,0,"b+",label="node location", markersize=0.4)
#	plt.plot(0,node_marker,"wo", label="nodes")
	
	"""
	# Limit the domain of the plot
	if parameter.get_local():

		min_coord = 10000.0
		max_coord = -10000.0

		for node in node_iterator(grid):
			coord = node.get_coord()[0]

			if coord > max_coord:
				max_coord = coord
			if coord < min_coord:
				min_coord = coord

		plt.xlim(min_coord, max_coord)
	"""
	max_value = -10000.0
	max_error = 0
	for node in node_iterator(grid):
		max_value = max(max_value, node.get_value())
		value = np.exp(-50*(node.get_coord()[0]-0.5)**2)
		max_error = max(max_error, abs(node.get_value()-value))

	print("max value", max_value)
	print("max error", max_error)	
	plt.legend()
#	plt.xlim(0.11, 0.17)
#	plt.ylim(-0.2,1.2)
	if show_plot:
		plt.show()



def plot_fem_grid_1D(grid, parameter, mark_node=True):
	""" Plot the FEM grid for 1D TPSFEM """

	plt.clf()
	
	# Initialisea a figure
	fig = plt.figure(str(parameter.get_dataset()))

	# Loop through each edge
	for node in node_iterator(grid):
		node_id = node.get_node_id()
		for endpt in endpt_iterator(grid, node.get_node_id()):
			if node_id > endpt:
				end_node = grid.get_node(endpt)

				# Create a list of x coordinates of two nodes
				x = [node.get_coord()[0],end_node.get_coord()[0]]
				y = [node.get_value(),end_node.get_value()]
			
				# Plot the line between two nodes
				plt.plot(x,y,"b")


	if mark_node:
		
		node_marker = -0.1

		# Loop over the full nodes in the grid
		for node in node_iterator(grid):
			plt.plot(node.get_coord()[0],node_marker,"b+", markersize=10)


def plot_tpsfem_only_1D(grid, parameter):
	""" Plot the FEM nodes for 1D TPSFEM """

	node_marker = -0.1

	# Initialisea a figure
	fig = plt.figure(str(parameter.get_dataset()))

	# Loop over the full nodes in the grid
	for node in node_iterator(grid):
		plt.plot(node.get_coord()[0],node.get_value(),"b.")

	plt.show()