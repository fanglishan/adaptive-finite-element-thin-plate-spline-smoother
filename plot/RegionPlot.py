#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-04-19 09:08:55
@Last Modified time: 2022-03-29 11:22:35

This class contains routines to plot a data region. The data inside
data region should be consistent with the dimension of the region.

"""

# Import libraries
from grid.NodeTable import node_iterator
from grid.EdgeTable import endpt_iterator

import matplotlib.pyplot as plt
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D


def plot_region_1D(region):
	""" Plot a 1D data region """

	# Figure name
	plt.figure("Data region 1D")

	# Region parameters
	mesh = region.get_mesh()
	coord = region.get_coord()
	region_no = region.get_region_no()
	region_size = region.get_region_size()

	# Plot region vertices
	for i in range(0, region_no[0]+1):
		plt.plot(coord[0]+i*region_size[0],0,"ro")

	# Plot data
	for i in range(0, region_no[0]):
		height = [0.1,0.2,0.3]
		for data in mesh[i]:
			plt.plot(data[0], height[int(i%len(height))], "g^")



def plot_region_2D(region, grid):
	""" Plot a 2D data region """

	# Figure name
	plt.figure("Data region 2D")

	# Region parameters
	mesh = region.get_mesh()
	coord = region.get_coord()
	region_no = region.get_region_no()
	region_size = region.get_region_size()

	
	if_plot = False
	# Plot data
	for i in range(0, region_no[0]):
		for j in range(0, region_no[1]):

			for data in mesh[i][j]:
				plt.plot(data[0], data[1], "go", markersize=1)
				
				if not if_plot:
					plt.plot(data[0], data[1], "go", markersize=1, label="data")
					if_plot = True
					
	# Plot region vertices
#	for i in range(0, region_no[0]+1):
#		for j in range(0, region_no[1]+1):
#			plt.plot(coord[0]+i*region_size[0],coord[1]+j*region_size[1],"ro")
	
	if_plot = False
	# Plot region using finite element grid
	# Loop through each edge once
	for node in node_iterator(grid):
		node_id = node.get_node_id()
		for endpt in endpt_iterator(grid, node.get_node_id()):
			if node_id > endpt:

				if abs(node.get_coord()[0]-grid.get_coord(endpt)[0]) < 0.000001 or \
					abs(node.get_coord()[1]-grid.get_coord(endpt)[1]) < 0.000001:

					# Coordinates of two ends
					x = [node.get_coord()[0],grid.get_coord(endpt)[0]]
					y = [node.get_coord()[1],grid.get_coord(endpt)[1]]

					# Plot the line
					line = plt.plot(x,y, "b")

					if not if_plot:
						plt.plot(x,y, "b", label="data region")
						if_plot = True

	plt.xlim(-0.05, 1.05)
	plt.ylim(-0.05, 1.05)
	plt.legend()
"""
	# Figure name
	plt.figure("Data region 2D")

	# Region parameters
	mesh = region.get_mesh()
	coord = region.get_coord()
	region_no = region.get_region_no()
	region_size = region.get_region_size()

	# Plot region vertices
	for i in range(0, region_no[0]+1):
		for j in range(0, region_no[1]+1):
			plt.plot(coord[0]+i*region_size[0],coord[1]+j*region_size[1],"ro")

	# Plot data
	for i in range(0, region_no[0]):
		for j in range(0, region_no[1]):

			for data in mesh[i][j]:
				plt.plot(data[0], data[1], "g^")
"""

def plot_region(region, grid):
	""" Plot a data region """

	# Dimension of data
	dim = region.get_dim()

	# Plot data region
	if dim == 1:
		plot_region_1D(region)
	elif dim == 2:
		plot_region_2D(region, grid)
	else:
		print("Warning: invalid data region dimension")

	plt.show()

