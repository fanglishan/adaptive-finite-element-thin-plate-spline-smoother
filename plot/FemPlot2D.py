#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-04-16 10:13:44
@Last Modified time: 2024-08-07 11:17:53

This class contains routines to plot the FEM grids.

Adapted from Stals' FEM code.

"""

# Import libraries
from grid.NodeTable import node_iterator
from grid.EdgeTable import endpt_iterator
from adaptive.triangle.Triangle import triangle_iterator_ref
from grid.Edge import DomainSet, RefineSet

import matplotlib.pyplot as plt



def replace_id(given_list, old_id, new_id):

	for i in range(0, len(given_list)):
		for j in range(0, len(given_list[i])):
			if given_list[i][j] == old_id:
				given_list[i][j] = new_id


def output_grid_info(grid):

	# Set base as the base edge in RefineSet
	base = RefineSet.base_edge

	# Set not_base as not base edge in RefineSet
	not_base = RefineSet.not_base_edge

	# Set intp as interior in DomainSet
	intp = DomainSet.interior

	# Set bnd as boundary in DomainSet
	bnd = DomainSet.boundary

	vertices = []
	bnd_edges = []
	triangles = []
	base_edges = []

	v_id = []
	v_coord = []
	for node in node_iterator(grid):
		node_id = node.get_node_id()
		v_id.append(int(str(node_id)))
		v_coord.append(node.get_coord())

		for endpt in endpt_iterator(grid, node_id):
			if node_id > endpt:

				if grid.get_location(node_id, endpt) == bnd:
					bnd_edges.append([int(str(node_id)),int(str(endpt))])

				if grid.get_refine_type(node_id, endpt) == base:
					base_edges.append([int(str(node_id)),int(str(endpt))])


	for tri in triangle_iterator_ref(grid):
		if tri[0].get_node_id() < tri[1].get_node_id() and \
		   tri[1].get_node_id() < tri[2].get_node_id():
		   triangles.append([int(str(tri[0].get_node_id())),int(str(tri[1].get_node_id())),int(str(tri[2].get_node_id()))])

	order = False   # Ascending
	id_tuple = zip(v_id, v_coord)
	v_id, v_coord = zip(*sorted(id_tuple, key=lambda id_tuple: id_tuple[0], reverse=order))

	v_id = list(v_id)
	vertices = list(v_coord)

	for i in range(0, len(v_id)):
		replace_id(bnd_edges, v_id[i], i)
		replace_id(triangles, v_id[i], i)
		replace_id(base_edges, v_id[i], i)

	print("vertices =", vertices)
	print("bnd_edges =", bnd_edges)
	print("triangles =", triangles)
	print("base_edges =", base_edges)


def plot_fem_grid_2D(grid, parameter=None, show_plot=False):
	""" Plot the finite element grid """

#	plt.clf()
#	output_grid_info(grid)

	# Initialisea a figure
	if parameter == None:
		fig = plt.figure("TPSFEM_grid_2D")
	else:
		fig = plt.figure(str(parameter.get_dataset())+"_"+str(parameter.get_indicator()))
	
	print("Finite element grid with", grid.get_no_nodes(), "nodes")
	
	# Loop through each edge once
	for node in node_iterator(grid):
		node_id = node.get_node_id()
		for endpt in endpt_iterator(grid, node.get_node_id()):
			if node_id > endpt:

				# Coordinates of two ends
				x = [node.get_coord()[0],grid.get_coord(endpt)[0]]
				y = [node.get_coord()[1],grid.get_coord(endpt)[1]]

				# Plot the line
				line = plt.plot(x,y, linewidth=0.5, color="k")  ## For final grid
#				line = plt.plot(x,y, linewidth=1, color="k")    ## For initial grid

				# Boundary edge with width 3 and colour red
#				if grid.get_location(node_id, endpt) == 2:
#					plt.setp(line,linewidth=3, color="r")
			
				"""	
				# Base edge with width 3 and colour green
				if grid.get_refine_type(node_id, endpt) == 1:
					plt.setp(line,linewidth=1, color="g")

				# Interface base edge with width 2 and colour yellow
#				elif grid.get_refine_type(node_id, endpt) == 2:
#					plt.setp(line,linewidth=1, color="b")

				# Interface base edge with width 2 and colour yellow
				elif grid.get_refine_type(node_id, endpt) == 3:
					plt.setp(line,linewidth=1, color="y")
				
				# Boundary edge with width 1 and colour red
				if grid.get_location(node_id, endpt) == 2:
					plt.setp(line,linewidth=1, color="r")
				"""
	
	# Loop over the nodes in the grid
	for node in node_iterator(grid):

		# Plot boundary nodes as red circles
		if grid.get_slave(node.get_node_id()):
			plt.plot(node.get_coord()[0],node.get_coord()[1],"r")
		
		# Plot interior nodes as blue circles
#		else:
#			plt.plot(node.get_coord()[0],node.get_coord()[1],"b")
	
	
	# Plot node ids
	if grid.get_no_nodes() < 10:
		for node in node_iterator(grid):
			node_id = node.get_node_id()
			plt.text(node.get_coord()[0]-0.0002,node.get_coord()[1]+0.0001,str(node_id),fontsize="smaller")
	
	# Plot refine level and number of neighbouring data points
	if grid.get_no_nodes() < 100 and 1 == 22:
#	if grid.get_no_nodes() < 50:
		for node in node_iterator(grid):
			node_id = node.get_node_id()
			coord = node.get_coord()
			for endpt in endpt_iterator(grid, node_id):
				if node_id > endpt:
					endpt_coord = grid.get_coord(endpt)

#					refine_level = grid.get_refine_level(node_id, endpt)+
#					plt.text((coord[0]+endpt_coord[0])/2.0,(coord[1]+endpt_coord[1])/2.0,refine_level,fontsize="smaller", color="g")

					plt.text((coord[0]+endpt_coord[0])/2.0-0.02,(coord[1]+endpt_coord[1])/2.0,grid.get_no_data(endpt, node_id)[0],fontsize="smaller", color="r")
					plt.text((coord[0]+endpt_coord[0])/2.0+0.02,(coord[1]+endpt_coord[1])/2.0,grid.get_no_data(endpt, node_id)[1],fontsize="smaller", color="r")

#	plt.xlim(0,1)
#	plt.ylim(0,1)
#	plt.xlim(-0.05,1.05)
#	plt.ylim(-0.05,1.05)
	# Show the plot
	if show_plot:
		plt.show()


def plot_fem_grid_data_2D(grid, data, parameter=None):
	""" Plot the finite element grid """

#	plt.clf()
	
	# Initialisea a figure
	fig = plt.figure("TPSFEM_grid_data_2D")

	show_label = True

	if len(data) > 30000:
		import random
		n = 10000
		data = random.sample(data, n)

	for point in data:
		
#		if point[1]>0.7 and point[0]>0.35:
	#		continue
		if show_label:
			plt.plot(point[0], point[1], "bo", markersize=0.2, label="data")
			show_label = False
		else:
			plt.plot(point[0], point[1], "bo", markersize=0.2)

	show_label = True

	# Loop through each edge once
	for node in node_iterator(grid):
		node_id = node.get_node_id()
		for endpt in endpt_iterator(grid, node.get_node_id()):
			if node_id > endpt:

				# Coordinates of two ends
				x = [node.get_coord()[0],grid.get_coord(endpt)[0]]
				y = [node.get_coord()[1],grid.get_coord(endpt)[1]]

				# Plot the line
				line = plt.plot(x,y)
				plt.setp(line,linewidth=0.5, color="k")
				"""
				# Boundary edge with width 3 and colour red
				if grid.get_location(node_id, endpt) == 2:
#					plt.setp(line,linewidth=3, color="r")
					plt.setp(line,linewidth=0.5, color="k")

				# Base edge with width 3 and colour green
#				elif grid.get_refine_type(node_id, endpt) == 1:
#					plt.setp(line,linewidth=1, color="g")

				# Interface base edge with width 2 and colour yellow
#				elif grid.get_refine_type(node_id, endpt) == 3:
#					plt.setp(line,linewidth=1, color="y")
				
				# Interior edge with width 1 and colour blue
				elif grid.get_location(node_id, endpt) == 1:
					if show_label:
						plt.setp(line,linewidth=1, color="k", label="edge")
						show_label = False
					else:
						plt.setp(line,linewidth=1, color="k")
				"""
	show_label = True

	# Loop through each edge once
	for node in node_iterator(grid):
		coord = node.get_coord()
		if grid.get_slave(node.get_node_id()):
#			plt.plot(coord[0], coord[1], "ro")

			plt.plot(coord[0], coord[1], "k")
		else:
			if show_label:
				plt.plot(coord[0], coord[1], "b", label="node")
				show_label = False
			else:
				plt.plot(coord[0], coord[1], "b")
	"""
	# Plot node ids if the grid is small
	if grid.get_no_nodes() < 50:
		for node in node_iterator(grid):
			plt.text(node.get_coord()[0]-0.02,node.get_coord()[1]+0.01,node.get_node_id(),fontsize="smaller")
	
	# Plot refine level and number of neighbouring data points
	if grid.get_no_nodes():

		for node in node_iterator(grid):
			node_id = node.get_node_id()
			coord = node.get_coord()
			for endpt in endpt_iterator(grid, node_id):
				if node_id > endpt:
					endpt_coord = grid.get_coord(endpt)

					refine_level = grid.get_refine_level(node_id, endpt)

#					plt.text((coord[0]+endpt_coord[0])/2.0,(coord[1]+endpt_coord[1])/2.0,refine_level,fontsize="smaller", color="g")

					plt.text((coord[0]+endpt_coord[0])/2.0-0.02,(coord[1]+endpt_coord[1])/2.0,grid.get_no_data(endpt, node_id)[0],fontsize="smaller", color="r")
					plt.text((coord[0]+endpt_coord[0])/2.0+0.02,(coord[1]+endpt_coord[1])/2.0,grid.get_no_data(endpt, node_id)[1],fontsize="smaller", color="r")
	"""

#	plt.legend()
	plt.xlim(-0.05,1.05)
	plt.ylim(-0.05,1.05)
#	plt.savefig(parameter.get_folder()+"grid"+"_"+str(parameter.get_iteration())+ "_" + str(parameter.get_marker()) + '.png')
	plt.show()


