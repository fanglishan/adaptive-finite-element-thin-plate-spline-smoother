#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-05-26 20:31:59
@Last Modified time: 2022-10-09 23:40:21

This class contains routines to plot local approximation of the auxiliary problem
error indicator.

"""

# Import libraries
from grid.NodeTable import node_iterator
from grid.EdgeTable import endpt_iterator
from grid.Edge import DomainSet
from adaptive.line.SetPolynomial import get_solution_polynomial_linear_1D
from data.ObtainData import obtain_data_grid, obtain_data_all

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
from scipy.interpolate import griddata
from numpy import mgrid, array
import numpy as np


def save_auxiliary_approx_1D(grid, region, parameter, node_dict, global_grid=None):
	""" Plot 1D TPSFEM approximation of the auxiliary problem """

#	if not parameter.get_write():
#		return

	if grid.get_dim() != 1:
		print("Warning: EIAuxiliaryPlot - inconsistent dimension")

	plt.clf()

#	fig = plt.figure(str(parameter.get_dataset()))

	show_data = True
	show_soln = False

	show_label = True

	if show_data:
		data = obtain_data_grid(region, grid)
		for data_point in data:
			if show_label:
				plt.plot(data_point[0], data_point[1], "g+", label="data")
				show_label = False
			else:
				plt.plot(data_point[0], data_point[1], "g+")
	# Model problem function
	u = parameter.get_function()[0]

	# Plot true solution
	if show_soln:
		xi = np.linspace(0.0, 1.0, 500)
		fi = []
		for i in range(0, len(xi)):

			fi.append(u([xi[i]]))
		plt.plot(xi, fi, ":")

	# Initialise limit of the local domain
	coord_min = 1000000000000000.0
	coord_max = -1000000000000000.0
	value_min = 1000000000000000.0
	value_max = -1000000000000000.0	

	plot_label_1 = True
	plot_label_2 = True

	# Iterate through each edge once
	for node in node_iterator(grid):
		node_id = node.get_node_id()

		# Find the min and max of local domain
		coord_min = min(coord_min, node.get_coord()[0])
		coord_max = max(coord_max, node.get_coord()[0])
		value_min = min(value_min, node.get_value())
		value_max = max(value_max, node.get_value())
		value_min = min(value_min, node_dict[str(node_id)])
		value_max = max(value_max, node_dict[str(node_id)])

		for endpt in endpt_iterator(grid, node_id):
			if node_id > endpt:
		
				# Plot the local and global approximation
				# Local: blue line
				# Global: red dashed lines
				x = [node.get_coord()[0],grid.get_coord(endpt)[0]]
				y1 = [node.get_value(),grid.get_value(endpt)]
				y2 = [node_dict[str(node_id)], node_dict[str(endpt)]]

				plt.plot(x,y1,"b")#, label="Local approximation")
				plt.plot(x,y2,"r--")#, label="Global approximation")

		if global_grid.is_in(node.get_node_id()):
			plt.plot(node.get_coord()[0],node_dict[str(node_id)],"ro")#, label="Global node")

		plt.plot(node.get_coord()[0],node.get_value(),"bo")#, label="Local node")


	plt.plot(0,0, "ro--", label="Global approximation")
	plt.plot(0,0, "bo-", label="Local approximation")
	"""
	if global_grid is not None:
		# Iterate through each edge once
		for node in node_iterator(grid):
			if global_grid.is_in(node.get_node_id()):
				print node.get_coord()[0],node_dict[str(node_id)]
				plt.plot(node.get_coord()[0],node_dict[str(node_id)],"ro")		
	"""
	# Set the limit for the local domain
	coord_length = abs(coord_max-coord_min)/10.0
	value_length = abs(value_max-value_min)/10.0

	plt.xlim(coord_min-coord_length, coord_max+coord_length)
	plt.ylim(value_min-value_length, value_max+value_length)
#	plt.ylim(-0.005, 0.011)
#	plt.legend(loc="lower right")
	plt.legend()
#	plt.savefig(parameter.get_folder()+"grid"+"_"+str(parameter.get_iteration())+ "_" + str(parameter.get_marker()) + '.png')

	plt.show()







def save_auxiliary_approx_2D(grid, region, parameter, node_dict):
	""" Plot 2D TPSFEM approximation of the auxiliary problem """

#	if not parameter.get_write():
#		return

	if grid.get_dim() != 2:
		print("Warning: EIAuxiliaryPlot - inconsistent dimension")

	data_marker = "."
#	plt.clf()

	fig = plt.figure(str(parameter.get_dataset()), figsize=plt.figaspect(0.5))

	show_data = True

	# Find the position of the nodes and the values
	node_x = []
	node_y = []
	node_v = []
	node_w = []       # Global approximation

	# Add node coordinates and values
	for node in node_iterator(grid):
		"""
		# Limit the solution
		coord = node.get_coord()
		if not (coord[0] >= -0.000000000001 and coord[0] <= 1.000000000001 and \
			coord[1] >= -0.000000000001 and coord[1] <= 1.000000000001):
			continue
		"""
		coord = node.get_coord()
		node_x.append(coord[0])
		node_y.append(coord[1])
		node_v.append(node.get_value())
		node_w.append(float(node_dict[str(node.get_node_id())]))
#		if not grid.get_slave(node.get_node_id()):
#			print node.get_value(), float(node_dict[str(node.get_node_id())])
	
	# Store the results in an array
	node_x = array(node_x)
	node_y = array(node_y)
	node_v = array(node_v)
	node_w = array(node_w)

	# Initialise the figure
#	ax = fig.gca()

	# Interpolate the nodes onto a structured mesh
	X, Y = mgrid[node_x.min(): node_x.max():100j,
				 node_y.min(): node_y.max():100j]
	Z = griddata((node_x, node_y), node_v, (X, Y), method ='linear')
	Z1 = griddata((node_x, node_y), node_w, (X, Y), method ='linear')

	data = []
	if show_data:
		data = obtain_data_grid(region, grid)
		print("# data:", len(data))
		# Initialise values of 3D coordinate for the data points
		x_vals = []
		y_vals = []
		z_vals = []

		if len(data) > 1000:

			downsize = int(len(data)/1000)
			counter = 0
			# Add data coordinates and values to the arrays
			for data_point in data:
				if counter%downsize == 0:
					x_vals.append(data_point[0])
					y_vals.append(data_point[1])
					z_vals.append(data_point[2])

				counter += 1
		else:
			# Add data coordinates and values to the arrays
			for data_point in data:
				x_vals.append(data_point[0])
				y_vals.append(data_point[1])
				z_vals.append(data_point[2])

	# Make a surface plot of both surfaces
	ax = fig.add_subplot(1, 2, 1, projection='3d')
	ax.plot_surface(X, Y, Z1, color='red')

	# Set the z axis limits
	ax.set_zlim(node_w.min(), node_w.max())
	ax.title.set_text('Global approximation')
	# Make the ticks look pretty
#	ax.zaxis.set_major_locator(LinearLocator(10))
#	ax.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))

	# Include a colour bar
#	fig.colorbar(surf, shrink=0.5, aspect=5)

	if show_data:

		# Create the scatter data points
		ax.scatter(x_vals, y_vals, z_vals, cmap=cm.coolwarm, marker=data_marker)

	ax = fig.add_subplot(1, 2, 2, projection='3d')
	ax.plot_surface(X, Y, Z, color='green')

	# Set the z axis limits
	ax.set_zlim(node_w.min(), node_w.max())
	ax.title.set_text('Local approximation')
	# Make the ticks look pretty
#	ax.zaxis.set_major_locator(LinearLocator(10))
#	ax.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))

	# Include a colour bar
#	fig.colorbar(surf, shrink=0.5, aspect=5)

	if show_data:

		# Create the scatter data points
		ax.scatter(x_vals, y_vals, z_vals, cmap=cm.coolwarm, marker=data_marker)

#	plt.savefig(parameter.get_folder()+"grid"+"_"+str(parameter.get_iteration())+ "_" + str(parameter.get_marker()) + '.png')
	
	plt.show()




def save_auxiliary_approx_2D_separate(grid, region, parameter, node_dict):
	""" Plot 2D TPSFEM approximation of the auxiliary problem """

#	if not parameter.get_write():
#		return

	if grid.get_dim() != 2:
		print("Warning: EIAuxiliaryPlot - inconsistent dimension")

	data_marker = "."
#	plt.clf()

	fig = plt.figure(str(parameter.get_dataset()))

	show_data = True

	# Find the position of the nodes and the values
	node_x = []
	node_y = []
	node_v = []
	node_w = []       # Global approximation

	# Add node coordinates and values
	for node in node_iterator(grid):
		"""
		# Limit the solution
		coord = node.get_coord()
		if not (coord[0] >= -0.000000000001 and coord[0] <= 1.000000000001 and \
			coord[1] >= -0.000000000001 and coord[1] <= 1.000000000001):
			continue
		"""
		coord = node.get_coord()
		node_x.append(coord[0])
		node_y.append(coord[1])
		node_v.append(node.get_value())
		node_w.append(float(node_dict[str(node.get_node_id())]))
#		if not grid.get_slave(node.get_node_id()):
#			print node.get_value(), float(node_dict[str(node.get_node_id())])
	
	# Store the results in an array
	node_x = array(node_x)
	node_y = array(node_y)
	node_v = array(node_v)
	node_w = array(node_w)

	# Initialise the figure
#	ax = fig.gca()

	# Interpolate the nodes onto a structured mesh
	X, Y = mgrid[node_x.min(): node_x.max():500j,
				 node_y.min(): node_y.max():500j]
	Z = griddata((node_x, node_y), node_v, (X, Y), method ='linear')
	Z1 = griddata((node_x, node_y), node_w, (X, Y), method ='linear')

	data = []
	if show_data:
		data = obtain_data_grid(region, grid)
		print("# data:", len(data))
		# Initialise values of 3D coordinate for the data points
		x_vals = []
		y_vals = []
		z_vals = []

		if len(data) > 1000:

			downsize = int(len(data)/1000)
			counter = 0
			# Add data coordinates and values to the arrays
			for data_point in data:
				if counter%downsize == 0:
					x_vals.append(data_point[0])
					y_vals.append(data_point[1])
					z_vals.append(data_point[2])

				counter += 1
		else:
			# Add data coordinates and values to the arrays
			for data_point in data:
				x_vals.append(data_point[0])
				y_vals.append(data_point[1])
				z_vals.append(data_point[2])

	# Make a surface plot of both surfaces
	ax = fig.gca()
	
	# Global approximation	
#	ax.plot_surface(X, Y, Z1, color='red', edgecolors="none")
	
	# Local approximation
	ax.plot_surface(X, Y, Z, color='green', edgecolors="none")

	# Set the z axis limits
	ax.set_zlim(node_w.min(), node_w.max())
#	ax.title.set_text('Global approximation')
#	ax.title.set_text('Local approximation')
	# Make the ticks look pretty
#	ax.zaxis.set_major_locator(LinearLocator(10))
#	ax.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))

	# Include a colour bar
#	fig.colorbar(surf, shrink=0.5, aspect=5)
	
	if show_data:
		ax.scatter(x_vals, y_vals, z_vals, cmap=cm.coolwarm, marker=data_marker, label="data")

	"""
	ax = fig.add_subplot(1, 2, 2, projection='3d')
	ax.plot_surface(X, Y, Z, color='green')

	# Set the z axis limits
	ax.set_zlim(node_w.min(), node_w.max())
	ax.title.set_text('Local approximation')
	# Make the ticks look pretty
#	ax.zaxis.set_major_locator(LinearLocator(10))
#	ax.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))

	# Include a colour bar
#	fig.colorbar(surf, shrink=0.5, aspect=5)

	if show_data:

		# Create the scatter data points
		ax.scatter(x_vals, y_vals, z_vals, cmap=cm.coolwarm, marker=data_marker)
	"""
#	plt.legend()
#	plt.savefig(parameter.get_folder()+"grid"+"_"+str(parameter.get_iteration())+ "_" + str(parameter.get_marker()) + '.png')
	
	plt.show()





def save_auxiliary_approx(grid, region, parameter, node_dict):
	""" Save or plot the auxiliary approximation"""

	if grid.get_dim() == 1:
		save_auxiliary_approx_1D(grid, region, parameter, node_dict)

	if grid.get_dim() == 2:
#		save_auxiliary_approx_2D(grid, region, parameter, node_dict)
		save_auxiliary_approx_2D_separate(grid, region, parameter, node_dict)