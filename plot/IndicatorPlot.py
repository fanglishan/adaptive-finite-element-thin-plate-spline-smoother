#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-08-05 19:47:17
@Last Modified time: 2023-03-11 13:17:21

This class contains routines to plot 1D and 2D indicator values.

"""

# Import libraries
from grid.NodeTable import node_iterator
from grid.EdgeTable import endpt_iterator
from grid.Edge import DomainSet
from adaptive.triangle.UniformRefinement import build_base_edges as build_base_edges_2D

import matplotlib.pyplot as plt
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D
import numpy as np


def plot_error_indicator_1D(grid, fig):
	""" Plot error indicators for 1D grids """

	# Loop through each edge
	for node in node_iterator(grid):
		node_id = node.get_node_id()
		for endpt in endpt_iterator(grid, node.get_node_id()):
			if node_id > endpt:
				end_node = grid.get_node(endpt)

				mid_coord = (node.get_coord()[0]+end_node.get_coord()[0])/2.0

				indicator_value = grid.get_error_indicator(node_id, endpt)
				
				plt.plot(mid_coord, indicator_value, "ro")
				
				# plot h
#				plt.plot(mid_coord, abs(node.get_coord()[0]-end_node.get_coord()[0]), "ro")
	plt.xlabel("x")
	plt.ylabel("error indicator")
#	plt.ylim(0,0.003)


def plot_error_indicator_2D(grid, fig):
	""" Plot error indicators for 2D grids """

	ax = Axes3D(fig)

	# Initialise values of 3D coordinate for the data points
	x_vals = []
	y_vals = []
	z_vals = []

	base_edges = build_base_edges_2D(grid)

	# Stats
	v_min = 1000000.0
	v_max = -1000000.0
	v_sum = 0.0
	v_no = 0

	# Indicate error of base edges
	for edge in base_edges:

		# End nodes of the edge
		endpt1 = edge.get_endpt1()
		node1 = grid.get_node(endpt1)
		endpt2 = edge.get_endpt2()
		node2 = grid.get_node(endpt2)

		x_vals.append((node1.get_coord()[0]+node2.get_coord()[0])/2.0)
		y_vals.append((node1.get_coord()[1]+node2.get_coord()[1])/2.0)

		indicator_value = grid.get_error_indicator(endpt1, endpt2)
		z_vals.append(indicator_value)
		
		v_min = min(v_min, indicator_value)
		v_max = max(v_max, indicator_value)
		v_sum += v_max
		v_no += 1

	"""
	print "x_vals =", x_vals
	print "y_vals =", y_vals
	print "z_vals =", z_vals

	print "number:", v_no
	print "min:", v_min
	print "max:", v_max
	print "average", v_sum/v_no
	"""
	# Create the scatter data points
	ax.scatter(x_vals, y_vals, z_vals, cmap=cm.coolwarm)

#	ax.set_zlim(6e-6, 0.00001)


def plot_error_indicator(grid, parameter=None):
	""" Plot the given data """


	dim = grid.get_dim()

	dataset = parameter.get_dataset()
	no_nodes = grid.get_no_nodes()
	indicator = parameter.get_indicator()

	fig = plt.figure(str(dataset)+"_indicator_"+str(indicator)+"_"+str(no_nodes))

	# Plot error indicators
	if dim == 1:
		plot_error_indicator_1D(grid, fig)
	elif dim == 2:
		plot_error_indicator_2D(grid, fig)
#		plot_contour(grid, fig)
	else:
		print("Warning: invalid dimension")

	plt.show()



def plot_error_indicator_list(grid, parameter):
	""" Plot the given data """


	dim = grid.get_dim()
	dataset = parameter.get_dataset()
	no_nodes = grid.get_no_nodes()
	indicator = parameter.get_indicator()

	fig = plt.figure(str(dataset)+"_indicator_"+str(indicator)+"_"+str(no_nodes))

	indicator_list = parameter.get_indicator_list()

	# Plot error indicators
	if dim == 1:
		pass
	elif dim == 2:
		# Initialise values of 3D coordinate for the data points
		x_vals = []
		y_vals = []
		z_vals = []

		for value in indicator_list:
			coord1 = grid.get_coord(value[0])
			coord2 = grid.get_coord(value[1])
			x_vals.append((coord1[0]+coord2[0])/2.0)	
			y_vals.append((coord1[1]+coord2[1])/2.0)
			z_vals.append(value[2])

		ax = Axes3D(fig)
#		ax.scatter(x_vals, y_vals, z_vals, cmap=cm.coolwarm)
		ax.scatter(x_vals, y_vals, z_vals, color='darkblue')

	elif dim == 3:
		pass	
	else:
		print("Warning: invalid dimension")

	plt.show()