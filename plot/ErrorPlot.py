#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-06-22 12:05:30
@Last Modified time: 2022-10-09 23:40:15

This class plots the error of the stored grid against
the exact model problem solution or a fine grid solution.

"""

# Import libraries
from grid.NodeTable import node_iterator
from adaptive.line.GridValue import cal_value_1d, cal_smoother_1d
from adaptive.triangle.GridValue import cal_value_2d, cal_smoother_2d
from plot.TpsfemPlot1D import plot_fem_grid_1D
from plot.TpsfemPlot2D import plot_tpsfem_solution_2D

import matplotlib.pyplot as plt
from matplotlib import cm
import numpy as np
from copy import deepcopy


def plot_error_1D(grid, parameter, use_fine_grid=False):
	""" Plot the error of a 1D TPSFEM solution """

	# Record maximum error
#	max_error = 0.0

	mark_node = False

	grid_error = deepcopy(grid)

	# Plot error against fine grid solution
	if use_fine_grid:

		fine_grid = parameter.get_solution()

		# Estimate error on each node on the fine grid
		for node in node_iterator(grid_error):
			error = abs(node.get_value()-cal_smoother_1d(fine_grid, node.get_coord()))
			node.set_value(error)

#			if error > max_error:
#				max_error = error

#		print("max error:",  max_error)
		
		plot_fem_grid_1D(grid_error, parameter, mark_node)

	# Plot error against exact model function
	else:

		u = parameter.get_function()[0]

		# Estimate error on each node
		for node in node_iterator(grid_error):
			error = abs(node.get_value()-u(node.get_coord()))
			node.set_value(error)

#			if error > max_error:
#				max_error = error

#		print("max error:",  max_error)

		plot_fem_grid_1D(grid_error, parameter, mark_node)



def calculate_error_2D(grid, parameter, use_fine_grid):
	""" Calculate solution of the grid """

	grid_copy = deepcopy(grid)

	# Plot error against fine grid solution
	if use_fine_grid:

		fine_grid = parameter.get_solution()

		# Estimate error on each node on the fine grid
		for node in node_iterator(grid_copy):
			error = abs(node.get_value()-cal_smoother_2d(fine_grid, node.get_coord()))
			node.set_value(error)


	# Plot error against exact model function
	else:

		u = parameter.get_function()[0]

		# Estimate error on each node
		for node in node_iterator(grid_copy):
			error = abs(node.get_value()-u(node.get_coord()))
			node.set_value(error)

	return grid_copy



def plot_error_2D(grid, parameter, use_fine_grid=False):
	""" Plot the error of a 2D TPSFEM solution """

	grid_error = calculate_error_2D(grid, parameter, use_fine_grid)
		
	plot_tpsfem_solution_2D(grid_error, False)



def plot_error_contour_2D(grid, parameter, use_fine_grid=False):
	""" Plot 2D data points """

	grid_error = calculate_error_2D(grid, parameter, use_fine_grid)

	fig = plt.figure("Error contour")
	ax = plt.axes()

	x = []
	y = []
	z = []

	min_value = 10000000000
	max_value = -10000000000

	for node in node_iterator(grid_error):
		coord = node.get_coord()
		value = node.get_value()

		x.append(coord[0])
		y.append(coord[1])
		z.append(value)

		min_value = min(min_value, value)
		max_value = max(max_value, value)

#	print "min error", min_value
#	print "max error", max_value
#	min_value = 0
#	max_value = 0.0655
#	step_size = (max_value-min_value)/100.0
#	levels = np.arange(min_value, max_value, step_size)

#	ax.tricontour(x, y, z, levels=levels, linewidths=0.3, colors='k')
#	cntr = ax.tricontourf(x, y, z, levels=levels, cmap=cm.coolwarm)
	cntr = ax.tricontourf(x, y, z, cmap=cm.coolwarm)

	fig.colorbar(cntr, ax=ax)

	if len(str(parameter.get_dataset())) == 1:
		plt.xlim(0.0, 1.0)
		plt.ylim(0.0, 1.0)
	else:
		plt.xlim(min(x), max(x))
		plt.ylim(min(y), max(y))	