#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-05-29 16:08:49
@Last Modified time: 2024-10-07 16:52:28

This class contains modules to plot radial basis interpolation.

"""

# Import libraries
from rbf.CalcRbf import rbf_calculate, rbf_calculate_deri
from rbf.Kernel import kernel

import plotly.graph_objects as go
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
from matplotlib.pyplot import show, figure
from mpl_toolkits.mplot3d import Axes3D
from scipy.interpolate import griddata
import numpy as np
from numpy import mgrid, array



def plot_rbf_coefficient(coefficient, data_center, b, deriv=0):

	# list of values for TPS
	xi = np.linspace(0.01, 1.0, 100)

	# Initialise a response value list
	fi = []

	# Iterate through coordinates
	for i in range(0,len(xi)):
		
		if abs(xi[i]-0) <= 0.0000000001:
			fi.append(0)

		# Calculate and add TPS smoother approximations
		fi.append(coefficient*kernel([xi[i]],[data_center],b))


	# Plot TPS approximation
	plt.plot(xi, fi)	
#	plt.show()


def plot_rbf_1D(data, rbfi):
	""" Plot 1D radial basis interpolation """
	"""
	# Set number of print points
	if len(data) > 500:
		no_print = len(data)
	else:
		no_print = 500

	# list of values for TPS
	xi = np.linspace(0.0, 1.0, no_print)
	"""
	# Clear previous plots
	plt.clf()
	
	# Iterate through each data point
	for i in range(0,len(data)):
		plt.plot(data[i][0], data[i][1], 'g.')
	
	xi = np.linspace(0, 1, 100)
	yi = rbfi(xi)

	# Plot TPS approximation
	plt.plot(xi, yi, 'b')

	# Plot the figure with title
#	plt.title('Thin plate spline')
#	plt.ylim(-0.2,1.2)
	plt.show()


def plot_rbf_2D(rbf_values, data, d, b, deriv=0):
	""" Plot 2D radial basis interpolation """

	# Find the position of the nodes and the values
	node_x = []
	node_y = []
	node_v = []

	for i in range(len(data)):
		x = data[i][0]
		y = data[i][1]
		node_x.append(x)
		node_y.append(y)
#		node_v.append(rbf_calculate(rbf_values, data, [x,y], d, b))
		node_v.append(rbf_calculate_deri(data, rbf_values, [x,y], d, b, deriv))
#		node_v.append(abs(u(coord)-node.get_value()))

	# Store the results in an array
	node_x = array(node_x)
	node_y = array(node_y)
	node_v = array(node_v)

	# Initialise the figure
	fig = figure()
	ax = fig.gca(projection ='3d')

	# Interpolate the nodes onto a structured mesh
	X, Y = mgrid[node_x.min(): node_x.max():50j,
				 node_y.min(): node_y.max():50j]
	Z = griddata((node_x, node_y), node_v, (X, Y), method ='cubic')

	# Make a surface plot
	surf = ax.plot_surface(X, Y, Z, rstride=1, cstride=1,
		cmap=cm.coolwarm, linewidth=0, antialiased=False)
	
	# Set the z axis limits
#	ax.set_zlim(node_v.min(), node_v.max())
	
	# Make the ticks look pretty
	ax.zaxis.set_major_locator(LinearLocator(10))
	ax.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))

	# Include a colour bar
	fig.colorbar(surf, shrink=0.5, aspect=5)

	# Initialise a new figure and axis
	#fig = plt.figure()
	#ax = Axes3D(fig)
	"""
	# Initialise values of 3D coordinate for the data points
	x_vals = []
	y_vals = []
	z_vals = []

	# Iterate through the data points
	for data_point in data:

		# Add the value on each axis to the corresponding arrays
		x_vals.append(data_point[0])
		y_vals.append(data_point[1])
		z_vals.append(data_point[2])

	# Create the scatter data points
	ax.scatter(x_vals, y_vals, z_vals)
	"""
	# Show the plot
	show()


def plot_rbf_3D(tps_values, data, d, b, deriv=0):
	""" Plot 3D radial basis interpolation """

	"""
	print(rbf_calculate(tps_values, data, [1,0,0], d, b))
	print(rbf_calculate(tps_values, data, [0,0,1], d, b))
	print(rbf_calculate(tps_values, data, [1,0,0], d, b))
	print(rbf_calculate(tps_values, data, [0.707106781,0.707106781,0], d, b))
	print(rbf_calculate(tps_values, data, [0,0.707106781,0.707106781], d, b))
	print(rbf_calculate(tps_values, data, [0.707106781,0,0.707106781], d, b))
	print(rbf_calculate(tps_values, data, [0,0,0], d, b))
	print(rbf_calculate(tps_values, data, [1,1,1], d, b))

		
	"""

	step = 20
	init = -1.2
	end = 1.2

	X = []
	Y = []
	Z = []
	V = []

	interval = (end-init)/float(step)

	for i in range(0, step+1):
		for j in range(0, step+1):
			for k in range(0, step+1):
				coord = [i*interval+init,j*interval+init,k*interval+init]
				X.append(coord[0])
				Y.append(coord[1])
				Z.append(coord[2])
				V.append(rbf_calculate(tps_values, data, coord, d, b))

	fig = go.Figure(data=go.Isosurface(
		x=X, 
		y=Y, 
		z=Z, 
		value=V,
	    isomin=-0.0,
	    isomax=0.0,
	))
	fig.show()


def plot_rbf(data, rbfi):
	""" Plot radial basis interpolation """

	d = len(data[0])-1

	# For 1D RBF
	if d == 1:
		plot_rbf_1D(data, rbfi)

	# For 2D RBF
#	elif d == 2:
#		plot_rbf_2D(rbf_values, data, d, b, deriv)

	# For 3D RBF
#	elif d == 3:
#		plot_rbf_3D(rbf_values, data, d, b, deriv)

	else:
		print("Warning: rbf/RbfPlot - unknown dimension")



def plot_rbf_error_1D(data, rbfi):
	""" Plot 1D radial basis interpolation """

	# Clear previous plots
	plt.clf()
	
	# Iterate through each data point
	for i in range(0,len(data)):
		plt.plot(data[i][0], rbfi(data[i][0])-data[i][1], 'g.')

	# Plot the figure with title
#	plt.title('Thin plate spline')
#	plt.ylim(-0.2,1.2)
	plt.show()


def plot_rbf_error(data, rbfi):
	""" Plot radial basis interpolation """

	d = len(data[0])-1

	# For 1D RBF
	if d == 1:
		plot_rbf_error_1D(data, rbfi)

	# For 2D RBF
#	elif d == 2:
#		plot_rbf_2D(rbf_values, data, d, b, deriv)

	# For 3D RBF
#	elif d == 3:
#		plot_rbf_3D(rbf_values, data, d, b, deriv)

	else:
		print("Warning: rbf/RbfPlot - unknown dimension")


def plot_contour_2D(rbf_values, data, d, b, deriv=0):
	""" Plot 2D radial basis interpolation """

	fig = plt.figure("Contour map")
	ax = plt.axes()

	v_min = 0.0
	v_max = 1.0
	step = 30
	step_size = abs(v_max-v_min)/(step-1)

	v_x = []
	v_y = []
	v_z = []

	v_z_max = -10000000000000.0
	v_z_min = 10000000000000.0

	for j in range(0,step):
		v_x.append(v_min+step_size*j)
		v_y.append(v_min+step_size*j)
		new_list = []
		for i in range(0,step):
#			value = rbf_calculate(rbf_values, data, [v_min+step_size*i,v_min+step_size*j], d, b)
			value = rbf_calculate_deri(data, rbf_values, [v_min+step_size*i,v_min+step_size*j], d, b, deriv)
			new_list.append(value)

			v_z_max = max(value, v_z_max)
			v_z_min = min(value, v_z_min)

		v_z.append(new_list)

#	print(v_x,v_y)
#	print(v_z)

	colorscale = [[0, 'blue'], [0.5, 'green'], [1, 'red']]

	no_contour = 20
	size = abs(v_z_max-v_z_min)/no_contour
#	print(v_z_max,v_z_min,size)
	fig = go.Figure(data =
		  go.Contour(
			z=v_z,
			x=v_x,   # horizontal axis
			y=v_y,   # vertical axis
#			line_smoothing=0.85,
			colorscale=colorscale,#'RdBu',
	        contours=dict(
	            start=v_z_min,
	            end=v_z_max,
	            size=size,)
		))
	
#	fig.write_image("images/TPSFEM_"+time.strftime('%y_%m_%d_%H_%M',time.localtime())+".pdf")
	fig.show()


def plot_contour(rbf_values, data, d, b, deriv=0):
	""" Plot contour map of radial basis interpolation """

	# For 1D RBF
	if d == 1:
#		plot_contour_1D(rbf_values, data, d, b)
		pass
	# For 2D RBF
	elif d == 2:
		plot_contour_2D(rbf_values, data, d, b, deriv)

	# For 3D RBF
	elif d == 3:
#		plot_contour_3D(rbf_values, data, d, b)
		pass
	else:
		print("Warning: rbf/RbfPlot - unknown dimension")


def plot_contour_2D_trial(rbf_values, data, d, b, deriv=0):
	""" Plot 2D radial basis interpolation """

	fig = plt.figure("Contour map")
	ax = plt.axes()

	v_min = 0.0
	v_max = 1.0
	step = 20
	step_size = abs(v_max-v_min)/(step-1)

	v_x = []
	v_y = []
	v_z = []

	v_z_max = -10000000000000.0
	v_z_min = 10000000000000.0

	for j in range(0,step):
		v_x.append(v_min+step_size*j)
		v_y.append(v_min+step_size*j)
		new_list = []
		for i in range(0,step):
			value = rbf_calculate(rbf_values, data, [v_min+step_size*i,v_min+step_size*j], d, b)
#			value = rbf_calculate_deri(data, rbf_values, [v_min+step_size*i,v_min+step_size*j], d, b, deriv)
			new_list.append(value)

			v_z_max = max(value, v_z_max)
			v_z_min = min(value, v_z_min)

		v_z.append(new_list)

	value_min = v_z_min
	value_max = v_z_max

	print(value_min,value_max)

	# Determine the levels of the contour map
	interval = abs(value_min-value_max)*1.2

	levels = np.arange(value_min-interval/10.0,value_max+interval/10.0,interval/30.0)
#	levels = np.arange(value_min,value_max,interval/30.0)
#	levels = np.arange(value_min,value_max,interval/30.0)

	# Plot contour map
	ax.tricontour(v_x, v_y, v_z, levels=levels, linewidths=1, colors='k')
	cntr = ax.tricontourf(x, y, z, levels=levels, cmap=cm.coolwarm)

	fig.colorbar(cntr, ax=ax)

#	plt.xlim(0.4, 0.5)
#	plt.ylim(0.42, 0.52)
	fig.show()

