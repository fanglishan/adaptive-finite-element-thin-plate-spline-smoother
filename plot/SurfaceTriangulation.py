# -*- coding: utf-8 -*-
"""
@Author: Fang
@Date:   2022-09-24 09:42:09
@Last Modified by:   Fang
@Last Modified time: 2024-08-06 22:45:35

"""


from grid.NodeTable import node_iterator
from grid.EdgeTable import endpt_iterator
from adaptive.triangle.GridValue import cal_smoother_2d
from adaptive.triangle.Triangle import triangle_iterator_ref

import plotly.graph_objects as go
import chart_studio.plotly as py
import numpy as np
import matplotlib.cm as cm
from scipy.spatial import Delaunay
import plotly.figure_factory as ff


def dist_origin(x, y, z):
    return np.sqrt((1.0 * x)**2 + (1.0 * y)**2 + (1.0 * z)**2)

def add_uniform_values(grid,x,y,z):

	v_min = 0.0
	v_max = 1.0
	step = 20
	step_size = abs(v_max-v_min)/(step-1)


	for j in range(0,step):
		for i in range(0,step):
			value = cal_smoother_2d(grid, [v_min+step_size*i,v_min+step_size*j])
			if value is not None:
				x.append(v_min+step_size*j)
				y.append(v_min+step_size*j)
				z.append(value)

	return x,y,z


def plot_surface_triangulation(grid):

	no_nodes = grid.get_no_nodes()
	x = [0]*no_nodes
	y = [0]*no_nodes
	z = [0]*no_nodes

	for node in node_iterator(grid):
		index  = int(str(node.get_node_id()))
		x[index] = node.get_coord()[0]
		y[index] = node.get_coord()[1]
		z[index] = node.get_value()


	tri_list = []
	for tri in triangle_iterator_ref(grid):
		tri_list.append([int(str(tri[0].get_node_id())), \
								 int(str(tri[1].get_node_id())), \
								 int(str(tri[2].get_node_id()))])
	x = np.array(x)
	y = np.array(y)
	z = np.array(z)
	simplices = np.array(tri_list)

	fig = ff.create_trisurf(x=x, y=y, z=z,
	                         simplices=simplices,
	                         colormap="Rainbow",
	                         #color_func=dist_origin,
#	                         showbackground=False,
#	                         show_colorbar=False,
	                         title="Surface triangulation", 
	                         aspectratio=dict(x=1, y=1, z=1))

	# Remove background
#	fig.update_scenes(xaxis_visible=False, yaxis_visible=False,zaxis_visible=False )

	fig.show()


#def plot_surface_triangulation_data(grid):
def plot_surface_triangulation_data(x,y,z,tri_list):
	"""
	no_nodes = grid.get_no_nodes()
	x = [0]*no_nodes
	y = [0]*no_nodes
	z = [0]*no_nodes

	for node in node_iterator(grid):
		index  = int(str(node.get_node_id()))
		x[index] = node.get_coord()[0]
		y[index] = node.get_coord()[1]
		z[index] = node.get_value()


	tri_list = []
	for tri in triangle_iterator_ref(grid):
		tri_list.append([int(str(tri[0].get_node_id())), \
								 int(str(tri[1].get_node_id())), \
								 int(str(tri[2].get_node_id()))])

	
	"""
	x = np.array(x)
	y = np.array(y)
	z = np.array(z)
	simplices = np.array(tri_list)

	fig = ff.create_trisurf(x=x, y=y, z=z,
	                         simplices=simplices,
	                         colormap="Rainbow",
#	                         colormap="Portland",
	                         color_func=dist_origin,
	                         showbackground=True,
	                         title="Surface triangulation", 
	                         aspectratio=dict(x=1, y=1, z=0.3))

	N = 100
	go.Figure(data=go.Scattergl(
	    x = np.random.randn(N),
	    y = np.random.randn(N),
	    mode='markers',
	    marker=dict(
	        color=np.random.randn(N),
	        colorscale='Viridis',
	        line_width=1
	    )
	))

	fig.show()


def repo(grid):


	v_x = []
	v_y = []
	v_z = []

	for node in node_iterator(grid):
		coord = node.get_coord()

		v_x.append(coord[0])
		v_y.append(coord[1])
		value = node.get_value()
		v_z.append(value)
#		v_z_max = max(value, v_z_max)
#		v_z_min = min(value, v_z_min)
	"""
	no_nodes = grid.get_no_nodes()
	x = [0]*no_nodes
	y = [0]*no_nodes
	z = [0]*no_nodes

	for node in node_iterator(grid):
		index  = int(str(node.get_node_id()))
		x[index] = node.get_coord()[0]
		y[index] = node.get_coord()[1]
		z[index] = node.get_value()

	x = np.array(x)
	y = np.array(y)
	z = np.array(z)
	tri_list = []
	for tri in triangle_iterator_ref(grid):
		if tri[0].get_node_id() < tri[1].get_node_id() and \
			tri[1].get_node_id() < tri[2].get_node_id():
				tri_list.append([int(str(tri[0].get_node_id())), \
								 int(str(tri[1].get_node_id())), \
								 int(str(tri[2].get_node_id()))])
	simplices = np.array(tri_list)
	"""

	x = np.array(v_x)
	y = np.array(v_y)
	z = np.array(v_z)
	u = np.array(v_x)
	v = np.array(v_y)


	u = np.linspace(0, 1, 20)
	v = np.linspace(0, 1, 20)

	u,v = np.meshgrid(u,v)
	u = u.flatten()
	v = v.flatten()

	x = (3 + (np.cos(v)))*np.cos(u)
	y = (3 + (np.cos(v)))*np.sin(u)
	z = np.sin(v)

	points2D = np.vstack([u,v]).T
	tri = Delaunay(points2D)
	simplices = tri.simplices
	print(x)
	print(y)
	print(z)
	print(simplices)

	fig = ff.create_trisurf(x=x, y=y, z=z,
	                         simplices=simplices,
	                         colormap="Portland",
	                         title="River", aspectratio=dict(x=1, y=1, z=0.3))
	fig.show()


def Mobius_Band():

	u = np.linspace(0, 2*np.pi, 24)
	v = np.linspace(-1, 1, 8)
	u,v = np.meshgrid(u,v)
	u = u.flatten()
	v = v.flatten()

	tp = 1 + 0.5*v*np.cos(u/2.)
	x = tp*np.cos(u)
	y = tp*np.sin(u)
	z = 0.5*v*np.sin(u/2.)
	fig = ff.create_trisurf(x=x, y=y, z=z,
	                         colormap="Portland",
	                         simplices=My_simplices,
	                         title="The Mobius Band")
	fig.show()