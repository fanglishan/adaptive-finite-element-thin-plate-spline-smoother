# -*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang
@Date:   2022-04-03 22:13:43
@Last Modified by:   Fang
@Last Modified time: 2023-05-14 14:26:29

This class contains modules to plot errors of RBF interpolations.

"""

# Import libraries
from rbf.CalcRbf import rbf_calculate, rbf_calculate_deri
from rbf.Kernel import kernel

import plotly.graph_objects as go
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
from matplotlib.pyplot import show, figure
from mpl_toolkits.mplot3d import Axes3D
from scipy.interpolate import griddata
import numpy as np
from numpy import mgrid, array



def plot_rbf_error_2D(rbf_values, sampled_data, data, d, b, f, deriv):
	""" Plot 2D radial basis interpolation """

	# Find the position of the nodes and the values
	node_x = []
	node_y = []
	node_v = []

	for i in range(len(data)):
		x = data[i][0]
		y = data[i][1]
		node_x.append(x)
		node_y.append(y)
		node_v.append(rbf_calculate(rbf_values, data, [x,y], d, b))
#		node_v.append(tps_calculate_deri(sampled_data, rbf_values, [x,y], d, b, deriv))

	# Store the results in an array
	node_x = array(node_x)
	node_y = array(node_y)
	node_v = array(node_v)

	# Initialise the figure
	fig = figure()
	ax = fig.gca(projection ='3d')

	# Interpolate the nodes onto a structured mesh
	X, Y = mgrid[node_x.min(): node_x.max():50j,
				 node_y.min(): node_y.max():50j]
	Z = griddata((node_x, node_y), node_v, (X, Y), method ='cubic')

	# Make a surface plot
	surf = ax.plot_surface(X, Y, Z, rstride=1, cstride=1,
		cmap=cm.coolwarm, linewidth=0, antialiased=False)
	
	# Set the z axis limits
#	ax.set_zlim(node_v.min(), node_v.max())
	
	# Make the ticks look pretty
	ax.zaxis.set_major_locator(LinearLocator(10))
	ax.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))

	# Include a colour bar
	fig.colorbar(surf, shrink=0.5, aspect=5)

	show()


