#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-04-16 10:13:44
@Last Modified time: 2023-05-14 16:49:41

This class contains routines to plot the gradients of the 2D TPSFEM solution.

"""

# Import libraries
from grid.NodeTable import node_iterator
from grid.EdgeTable import endpt_iterator
from grid.Edge import DomainSet
from data.ObtainData import obtain_data_grid

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
from scipy.interpolate import griddata
from numpy import mgrid, array
import numpy as np



def plot_tpsfem_solution_2D(grid, show_plot=False, derivative=1):
	""" Plot the 2D TPSFEM solution """

	if grid.get_dim() != 2:
		return

#	plt.clf()

	# Initialisea a figure
	fig = plt.figure("TPSFEM_solution_2D")

	# Find the position of the nodes and the values
	node_x = []
	node_y = []
	node_v = []

	# Add node coordinates and values
	for node in node_iterator(grid):
		"""
		# Limit the solution
		coord = node.get_coord()
		if not (coord[0] >= -0.000000000001 and coord[0] <= 1.000000000001 and \
			coord[1] >= -0.000000000001 and coord[1] <= 1.000000000001):
			continue
		"""
		coord = node.get_coord()
		node_x.append(coord[0])
		node_y.append(coord[1])

		if derivative == 1:
			node_v.append(node.get_d_values()[0])

		elif derivative == 2:
			node_v.append(node.get_d_values()[1])

		elif derivative == 3:
			node_v.append(-node.get_d_values()[2])

	# Store the results in an array
	node_x = array(node_x)
	node_y = array(node_y)
	node_v = array(node_v)

	# Initialise the figure
	ax = fig.gca()

	# Interpolate the nodes onto a structured mesh
	X, Y = mgrid[node_x.min(): node_x.max():100j,
				 node_y.min(): node_y.max():100j]
	Z = griddata((node_x, node_y), node_v, (X, Y), method ='cubic')

	# Make a surface plot
	surf = ax.plot_surface(X, Y, Z, rstride=1, cstride=1,
		cmap=cm.coolwarm, linewidth=0, antialiased=False)
	
	# Set the z axis limits
	ax.set_zlim(node_v.min(), node_v.max())

	# Make the ticks look pretty
	ax.zaxis.set_major_locator(LinearLocator(10))
	ax.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))

	# Include a colour bar
	fig.colorbar(surf, shrink=0.5, aspect=5)

#	plt.xlim(-1,1)
#	plt.ylim(-1,1)
	# Show the plot
	if show_plot:
		plt.show()



def plot_tpsfem_solution_data_2D(grid, region, show_data=True, show_plot=False):
	""" Plot the 2D TPSFEM solution """

	if grid.get_dim() != 2:
		return

	plt.clf()

	# Initialisea a figure
	fig = plt.figure("TPSFEM_solution_2D")

	# Find the position of the nodes and the values
	node_x = []
	node_y = []
	node_v = []

	# Add node coordinates and values
	for node in node_iterator(grid):
		"""
		# Limit the solution
		coord = node.get_coord()
		if not (coord[0] >= -0.000000000001 and coord[0] <= 1.000000000001 and \
			coord[1] >= -0.000000000001 and coord[1] <= 1.000000000001):
			continue
		"""
		coord = node.get_coord()
		node_x.append(coord[0])
		node_y.append(coord[1])
		node_v.append(node.get_value())

	# Store the results in an array
	node_x = array(node_x)
	node_y = array(node_y)
	node_v = array(node_v)

	# Initialise the figure
	ax = fig.gca()

	# Interpolate the nodes onto a structured mesh
	X, Y = mgrid[node_x.min(): node_x.max():100j,
				 node_y.min(): node_y.max():100j]
	Z = griddata((node_x, node_y), node_v, (X, Y), method ='cubic')

	# Make a surface plot
	surf = ax.plot_surface(X, Y, Z, rstride=1, cstride=1,
		cmap=cm.coolwarm, linewidth=0, antialiased=False)
	
	# Set the z axis limits
	ax.set_zlim(node_v.min(), node_v.max())

	# Make the ticks look pretty
	ax.zaxis.set_major_locator(LinearLocator(10))
	ax.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))

	# Include a colour bar
	fig.colorbar(surf, shrink=0.5, aspect=5)

	if show_data:
		data = obtain_data_grid(region, grid)
		print("# data:", len(data))
		# Initialise values of 3D coordinate for the data points
		x_vals = []
		y_vals = []
		z_vals = []

		# Add data coordinates and values to the arrays
		for data_point in data:
			x_vals.append(data_point[0])
			y_vals.append(data_point[1])
			z_vals.append(data_point[2])

		# Create the scatter data points
		ax.scatter(x_vals, y_vals, z_vals, cmap=cm.coolwarm, marker=".")

	# Show the plot
	if show_plot:
		plt.show()


def plot_gradient_contour(grid,deriv):
	""" Plot 2D data points """

	plt.clf()
	
	fig = plt.figure("Contour map")
	ax = plt.axes()

	x = []
	y = []
	z = []

	value_min = 10000000000.0
	value_max = -10000000000.0

	# Add node coordinates and values
	for node in node_iterator(grid):
		"""
		# Limit the solution
		coord = node.get_coord()
		if not (coord[0] >= -0.000000000001 and coord[0] <= 1.000000000001 and \
			coord[1] >= -0.000000000001 and coord[1] <= 1.000000000001):
			continue
		"""
		coord = node.get_coord()
		x.append(coord[0])
		y.append(coord[1])
		value = node.get_d_values()[deriv]
		z.append(value)

		value_min = min(value_min, value)
		value_max = max(value_max, value)

#	value_min = 1920
#	value_max = 2110

	# Determine the levels of the contour map
	interval = abs(value_min-value_max)

	levels = np.arange(value_min-interval/30.0,value_max+interval/30.0,interval/30.0)

	# Plot contour map
	ax.tricontour(x, y, z, levels=levels, linewidths=0.01, colors='k')
	cntr = ax.tricontourf(x, y, z, levels=levels, cmap=cm.coolwarm)

	fig.colorbar(cntr, ax=ax)

#	if len(str(dataset)) == 1:
#		plt.xlim(0.0, 1.0)
#		plt.ylim(0.0, 1.0)
#	else:
#		plt.xlim(min(x), max(x))
#		plt.ylim(min(y), max(y))	


def plot_contour(grid, dataset=0):
	""" Plot 2D data points """

	plt.clf()
	
	fig = plt.figure("Contour map")
	ax = plt.axes()

	x = []
	y = []
	z = []

	value_min = 10000000000.0
	value_max = -10000000000.0

	# Add node coordinates and values
	for node in node_iterator(grid):
		"""
		# Limit the solution
		coord = node.get_coord()
		if not (coord[0] >= -0.000000000001 and coord[0] <= 1.000000000001 and \
			coord[1] >= -0.000000000001 and coord[1] <= 1.000000000001):
			continue
		"""
		coord = node.get_coord()
		x.append(coord[0])
		y.append(coord[1])
		z.append(node.get_value())

		value_min = min(value_min, node.get_value())
		value_max = max(value_max, node.get_value())

#	value_min = 1920
#	value_max = 2110

	# Determine the levels of the contour map
	interval = abs(value_min-value_max)

	levels = np.arange(value_min-interval/10.0,value_max+interval/10.0,interval/20.0)

	# Plot contour map
	ax.tricontour(x, y, z, levels=levels, linewidths=0.3, colors='k')
	cntr = ax.tricontourf(x, y, z, levels=levels, cmap=cm.coolwarm)

	fig.colorbar(cntr, ax=ax)

	if len(str(dataset)) == 1:
		plt.xlim(0.0, 1.0)
		plt.ylim(0.0, 1.0)
	else:
		plt.xlim(min(x), max(x))
		plt.ylim(min(y), max(y))		