#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-04-22 09:41:36
@Last Modified time: 2024-08-07 13:56:48

This class contains routines to solve the 2D TPSFEM.

"""

# Import libraries
from adaptive.triangle.BuildEquationLinear import build_equation_linear_2D
from parameter.Definition import BoundaryType, IndicatorType, CostType
from tpsfem.BuildBasis import build_node_list
from tpsfem.AddEntry2D import add_alpha_L, add_uniqueness, add_uniqueness_w, add_boundary_effects, set_bnd_c_g
from gcv.GCVRefinement import GCV_refine_2D
from gcv.GCVIndicator import GCV_indicator_2D
from fileio.WriteResult import write_line

from numpy import zeros
from scipy.sparse.linalg import spsolve
from scipy.sparse import lil_matrix, csc_matrix
import matplotlib.pyplot as plt
from matplotlib.pyplot import spy
from timeit import default_timer
from pypardiso import spsolve as pypardiso_spsolve


# Alternate approach that rearranges of the structure of the TPSFEM system
# of equations 2D. Note that the boundary conditions have also been 
# adjusted acoordingly.
# 
# Dirichlet boundary
# [L -G1 -G2   0   ] [c]   [0]   [h4]
# [0  a*L 0   -G1^T] [g1]  [0]   [h2]
# [0  0   a*L -G2^T] [g2]  [0]   [h3]
# [A  0   0    L   ] [w] = [d] - [h1]
#
def obtain_matrix_entries_alternate(grid, node_list, matrix, vector, KTu_vector, parameter):
	""" Obtain matrix entries from the grid except for alpha*L """

	# Numer of interior numbers
	no_nodes = len(node_list)

	# Iterate through columns
	for j in range(0, no_nodes):

		node_id2 = node_list[j].get_node_id()

		# Add values to d vector
		vector[j+3*no_nodes,0] = node_list[j].get_data_load()

		# Add values to K^T*u vector
		KTu_vector[j,0] = node_list[j].get_KTu_load()

		# Iterate through rows
		for i in range(0, no_nodes):

			node_id1 = node_list[i].get_node_id()

			# Check if two nodes have connections
			if grid.is_connection(node_id1, node_id2):

				# Add value to A submatrix
				matrix[i+3*no_nodes,j] = grid.get_matrix_A_value(node_id1, node_id2)

				# Get L matrix value from the existing connections
				L_value = grid.get_matrix_L_value(node_id1, node_id2)

				# Set value for submatrix L at 1,4 and 4,1 at 4*4 matrix
				matrix[i,j] = L_value
				matrix[i+3*no_nodes,j+3*no_nodes] = L_value

				# Alpha values will be updated later and entries are filled
#				matrix[i+no_nodes,j+no_nodes] = alpha*L_value
#				matrix[i+2*no_nodes,j+2*no_nodes] = alpha*L_value

				# Get G matrix value from the existing connections
				G_value = grid.get_matrix_G_value(node_id1, node_id2)

				# Set value for submatrix G1 
				matrix[i,j+no_nodes] = G_value[0]
				matrix[j+no_nodes,i+3*no_nodes] = G_value[0]

				# Set value for submatrix G2
				matrix[i,j+2*no_nodes] = G_value[1]
				matrix[j+2*no_nodes,i+3*no_nodes] = G_value[1]


# System of equations 2D
# [A  0   0    L   ] [c] = [d] - [h1]
# [0  a*L 0   -G1^T] [g1]  [0]   [h2]
# [0  0   a*L -G2^T] [g2]  [0]   [h3]
# [L -G1 -G2   0   ] [w]   [0]   [h4]
#
def obtain_matrix_entries(grid, node_list, matrix, vector, KTu_vector, parameter):
	""" Obtain matrix entries from the grid except for alpha*L """

	# Numer of interior numbers
	no_nodes = len(node_list)

	# Iterate through columns
	for j in range(0, no_nodes):

		node_id2 = node_list[j].get_node_id()

		# Add values to d vector
		vector[j,0] = node_list[j].get_data_load()

		# Add values to K^T*u vector
		KTu_vector[j,0] = node_list[j].get_KTu_load()

		# Iterate through rows
		for i in range(0, no_nodes):

			node_id1 = node_list[i].get_node_id()

			# Check if two nodes have connections
			if grid.is_connection(node_id1, node_id2):

				# Add value to A submatrix
				matrix[i,j] = grid.get_matrix_A_value(node_id1, node_id2)

				# Get L matrix value from the existing connections
				L_value = grid.get_matrix_L_value(node_id1, node_id2)

				# Set value for submatrix L at 1,4 and 4,1 at 4*4 matrix
				matrix[i,j+3*no_nodes] = L_value
				matrix[i+3*no_nodes,j] = L_value
#				matrix[i+no_nodes,j+no_nodes] = alpha*L_value
#				matrix[i+2*no_nodes,j+2*no_nodes] = alpha*L_value

				# Get G matrix value from the existing connections
				G_value = grid.get_matrix_G_value(node_id1, node_id2)

				# Set value for submatrix G1 
				matrix[i+3*no_nodes,j+no_nodes] = G_value[0]
				matrix[j+no_nodes,i+3*no_nodes] = G_value[0]

				# Set value for submatrix G2
				matrix[i+3*no_nodes,j+2*no_nodes] = G_value[1]
				matrix[j+2*no_nodes,i+3*no_nodes] = G_value[1]


def obtain_bnd_node_list(grid, node_list): 

	bnd_index_list = []

	for i in range(0, len(node_list)):

		if grid.get_slave(node_list[i].get_node_id()):
			bnd_index_list.append(i)

	return bnd_index_list


###################################
# System of equations 2D
# 
# Dirichlet boundary
#
# [A  0   0    L   ] [c] = [d] - [h1]
# [0  a*L 0   -G1^T] [g1]  [0]   [h2]
# [0  0   a*L -G2^T] [g2]  [0]   [h3]
# [L -G1 -G2   0   ] [w]   [0]   [h4]
#
# Neumann boundary
#
# [A  0   0    L    0 0 0] [c] = [d] - [h1]
# [0  a*L 0   -G1^T 1 0 0] [g1]  [0]   [h2]
# [0  0   a*L -G2^T 0 1 0] [g2]  [0]   [h3]
# [L -G1 -G2   0    0 0 1] [w1]  [0]   [h4]
# [0  1   0    0    0 0 0] [w2]  [0]   [0]
# [0  0   1    0    0 0 0] [w3]  [0]   [0]
# [0  0   0    1    0 0 0] [w4]  [0]   [0]
#
###################################
def obtain_system_equations(grid, parameter, node_list, solve_system=True, test_derichlet=False):
	""" Construc the stiffness matrix """

	# Number of nodes
	no_nodes = len(node_list)

	bnd_index_list = obtain_bnd_node_list(grid, node_list)

#	print(test_derichlet)
	# Initialise a sparse matrix and a RHS vector

	# For Dirchlet boundary model problem
	if parameter.get_boundary() == BoundaryType.Dirichlet:
		matrix =  lil_matrix((4*no_nodes,4*no_nodes),dtype=float)
		vector = zeros([4*no_nodes, 1])
		KTu_vector = zeros([4*no_nodes, 1])
	
	# For Neumann boundary model problem
	elif parameter.get_boundary() == BoundaryType.Neumann:

		if not test_derichlet:
			matrix =  lil_matrix((4*no_nodes+3,4*no_nodes+3),dtype=float)	
			vector = zeros([4*no_nodes+3, 1])
			KTu_vector = zeros([4*no_nodes+3, 1])
		
		# Set c and g values using Dirichlet boundary conditions
		else:
			no_bnd_nodes = len(bnd_index_list)
#			print(no_bnd_nodes)
			
			matrix_dim = 4*no_nodes+1+3*no_bnd_nodes

			### THIS IMPLEMENTATION IS TO VERIFY SETTING OF DIRICHLET BOUNDARY CONDITIONS
			matrix_dim = 4*no_nodes+4*no_bnd_nodes
			###

			matrix = lil_matrix((matrix_dim,matrix_dim),dtype=float)	
			vector = zeros([matrix_dim, 1])
			KTu_vector = zeros([matrix_dim, 1])

#			print(no_nodes,no_bnd_nodes)
	# Add submatrices from the grid connections
	obtain_matrix_entries(grid, node_list, matrix, vector, KTu_vector, parameter)

	# Add uniqueness for Neumann boundary problems
	if parameter.get_boundary() == BoundaryType.Neumann:
		if not test_derichlet:
			add_uniqueness(matrix, no_nodes)
		else:
#			add_uniqueness_w(matrix, no_nodes)

			### THIS IMPLEMENTATION IS TO VERIFY SETTING OF DIRICHLET BOUNDARY CONDITIONS
			pass
			###


#	print(matrix.shape)
#	print(vector.shape)
	if test_derichlet:
		set_bnd_c_g(grid, parameter, node_list, matrix, vector, bnd_index_list)

	return matrix, vector, KTu_vector



def assign_grid_values(grid, node_list, parameter, tpsfem_solution):
	""" Assign new values to the grid """

	# Length of node list
	no_nodes = len(node_list)
	
	# Smoothing parameter
	alpha = parameter.get_alpha()
	
	grid.set_alpha(alpha)

	# Update node values in the grid
	for i in range(0,len(node_list)):

		# Set node value
		node_list[i].set_value(tpsfem_solution[i])

		# Set derivative values
		node_list[i].set_d_values([tpsfem_solution[i+no_nodes], \
			tpsfem_solution[i+2*no_nodes], tpsfem_solution[i+3*no_nodes]/alpha])



def solve_tpsfem_2D(grid, region, parameter, if_build=True):
	""" Solve TPSFEM with current grid and update the node values """


	test_derichlet = True

	if parameter.get_local():
		test_derichlet = False

	# Record original alpha
	original_alpha = parameter.get_alpha()

	# Cost record
	cost = parameter.get_cost()

	# Minimum alpha
	min_alpha = 1e-10

	# Calculate matrix value and load vector value for each node
	if if_build:
		region.init_usage()
		
		t = default_timer()
		build_equation_linear_2D(grid, region, parameter)

		if not parameter.get_local():
			cost.add_time(default_timer()-t, CostType.build)
			write_line(parameter, "build time: "+str(default_timer()-t), True)
			
	solve_system = False

	# Build the system for the GCV
	obtain_system_again = False

	if parameter.get_boundary() == BoundaryType.Neumann:
		obtain_system_again = True
		parameter.set_boundary(BoundaryType.Dirichlet)

	# Build a list of nodes to reduce matrix band
	node_list = build_node_list(grid, parameter.get_boundary())


	# Build a system of equations without alpha
	matrix, vector, KTu_vector = obtain_system_equations(grid, parameter, node_list, solve_system)

	# Default alpha
	alpha = parameter.get_alpha()


#	if parameter.get_iteration() == 8:
#		parameter.set_alpha(alpha/0.3)

	# If smoothing parameter needs to be calculated
	if parameter.get_run_gcv():

		# Stop decreasing alpha if it is too small
		if alpha <= min_alpha:
			pass

		elif len(node_list) > 1:
			"""
			gcv_parameter = parameter.get_gcv_parameter()

			v_approx = gcv_parameter[0]
			max_iteration = gcv_parameter[1]

			print "iteration:", parameter.get_iteration()
			if parameter.get_iteration() !=0:
				alpha = parameter.get_alpha()
				start_bound = alpha/100.0
				end_bound = alpha*2.0
				gcv_parameter[2] = start_bound
				gcv_parameter[3] = end_bound
				parameter.set_gcv_parameter(gcv_parameter)
			gcv_parameter = parameter.get_gcv_parameter()
			write_line(parameter, "bound:"+str(gcv_parameter[2])+","+str(gcv_parameter[3]), True)

#			GCV_analysis_2D(grid, region, parameter, node_list, v_approx, max_iteration, matrix, vector, KTu_vector)
			alpha = GCV_2D(grid, region, parameter, node_list, \
					v_approx, max_iteration, matrix, vector, KTu_vector,gcv_parameter[2], gcv_parameter[3])

			"""
			if parameter.get_local():
				v_approx = True
				max_iteration = 7
				start_bound = 0.0000001
				end_bound = 0.01
				if parameter.get_iteration() == parameter.get_max_sweep()+1:
					v_approx = True
				else:
					v_approx = False
			else:
				gcv_parameter = parameter.get_gcv_parameter()
				v_approx = gcv_parameter[0]
				max_iteration = gcv_parameter[1]
				start_bound = gcv_parameter[2]
				end_bound = gcv_parameter[3]

			if parameter.get_local():
				
#				GCV_analysis_2D(grid, region, parameter, node_list, v_approx, max_iteration, matrix, vector, KTu_vector)
#				alpha = GCV_2D(grid, region, parameter, node_list, \
#					v_approx, max_iteration, matrix, vector, KTu_vector, start_bound, end_bound)
				alpha = 1e-6
				"""
				alpha = GCV_indicator_2D(grid, region, parameter, node_list, \
					v_approx, max_iteration, matrix, vector, KTu_vector, start_bound, end_bound)
				if alpha is False:
					print("false")
					alpha = 1e-6
				"""
			else:
				print("calculate alpha")
#				GCV_analysis_2D(grid, region, parameter, node_list, v_approx, max_iteration, matrix, vector, KTu_vector)
#				from sys import exit
#				exit()
#				alpha = GCV_refine_2D_condensed(grid, region, parameter, node_list, matrix, vector, KTu_vector, max_iteration, start_bound, end_bound)
	
				t = default_timer()
				alpha = GCV_refine_2D(grid, region, parameter, node_list, matrix, vector, KTu_vector, max_iteration, start_bound, end_bound)
				cost.add_time(default_timer()-t, CostType.gcv)


			
			if parameter.get_local():
				if not alpha == parameter.get_alpha():
					parameter.add_sum(alpha)
					parameter.add_count()
		
		if alpha < min_alpha:
			parameter.set_alpha(min_alpha)
		else:
			parameter.set_alpha(alpha)


	# If the previous system is for the GCV
	if obtain_system_again:
		parameter.set_boundary(BoundaryType.Neumann)
		 
		# Build a list of nodes to reduce matrix band
		node_list = build_node_list(grid, parameter.get_boundary())

		# Build a system of equations without alpha
		matrix, vector, KTu_vector = obtain_system_equations(grid, parameter, node_list, solve_system, test_derichlet)

	# Add L entries multiplied with alpha
	add_alpha_L(grid, node_list, matrix, vector, parameter)

	# Add effects from boundary conditions
	add_boundary_effects(grid, node_list, vector, parameter)


	# Print the structure of the sparse system of equation
	if 1 == 2:
	
		from tpsfem.BuildMatrix import build_A
		A = build_A(grid, parameter, node_list)
		spy(matrix)
		plt.show()
	

	"""
	###############################
	from plot.DataPlot import plot_data_hist
	A_values = []
	for i in range(0, len(node_list)):
		for j in range(0, len(node_list)):

			if matrix[i,j] > 0.0000000001:
				A_values.append([0,matrix[i,j]])
	plot_data_hist(A_values)
#	from sys import exit
#	exit()
	
	##############################
	from tpsfem.BuildMatrix import build_A
	A = build_A(grid, parameter, node_list)
	print(A.todense())
	spy(matrix)
	plt.show()
	"""
#	matrix[abs(matrix) < 1e-14] = 0.0

#	no_nonzero = csc_matrix(matrix).count_nonzero()
#	print("dimension", matrix.shape[0])
#	print("#nonzero", no_nonzero)
#	print("percent", 100.0*no_nonzero/(matrix.shape[0]**2))

	t = default_timer()
	# Solve system using the direct solver
	if parameter.get_local():
		tpsfem_solution = spsolve(csc_matrix(matrix), vector)
	else:
		try:
			tpsfem_solution = pypardiso_spsolve(csc_matrix(matrix), vector)
		except Exception as e:
			print(e)
			tpsfem_solution = spsolve(csc_matrix(matrix), vector)

	if not parameter.get_local():
		cost.add_time(default_timer()-t, CostType.solve)
		write_line(parameter, "solve time: "+str(default_timer()-t), True)
		
#	print(default_timer(), t)
	"""
	scale = 0.2
	if not parameter.get_local():
		print "iteration:", parameter.get_iteration()
		print "alpha:", alpha_list[parameter.get_iteration()]
		parameter.set_alpha(alpha_list[parameter.get_iteration()])
	else:
		parameter.set_alpha(scale*alpha_list[parameter.get_iteration()])
	"""

	assign_grid_values(grid, node_list, parameter, tpsfem_solution)
	
	# Reset smoothing parameters for auxiliary problems
#	if parameter.get_local():
#		parameter.set_alpha(original_alpha)


def calculate_alpha_2D(grid, region, parameter, V_approx):
	""" Calculate smoothing parameter alpha only """

	# Calculate matrix value and load vector value for each node
	region.init_usage()

	solve_system = False

	build_equation_linear_2D(grid, region, parameter)

	# Build a list of nodes to reduce matrix band
	node_list = build_node_list(grid, parameter.get_boundary())

	# Build a system of equations without alpha
	matrix, vector, KTu_vector = obtain_system_equations(grid, parameter, node_list, solve_system)

	# Default alpha
	alpha = parameter.get_alpha()

	if len(node_list) > 1:

		max_iteration = 7
		start_bound = 1e-8
		end_bound = 1e-4


#		GCV_analysis_2D(grid, region, parameter, node_list, v_approx, max_iteration, matrix, vector, KTu_vector)
#		alpha = GCV_2D(grid, region, parameter, node_list, \
#			v_approx, max_iteration, matrix, vector, KTu_vector, start_bound, end_bound)
		alpha = GCV_indicator_2D(grid, region, parameter, node_list, \
				V_approx, max_iteration, matrix, vector, KTu_vector, start_bound, end_bound)
		
		return alpha

	else:
		return False