#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-04-29 15:27:53
@Last Modified time: 2022-03-29 11:24:09

This class contains routines to build a list of nodes for the
system of equations of the TPSFEM. It will include the interior 
nodes for the Dirichlet boundary conditions or all interior and
boundary nodes for the Neumann boundary conditions.

Some helper functions are also included.

"""

# Import libraries
from grid.NodeTable import node_iterator
from grid.EdgeTable import endpt_iterator
from parameter.Definition import BoundaryType, IndicatorType


def obtain_node_index_1D(node_list, node1, node2):
	""" Obtain the index of the given node in the list """

	# Initialise indices of three nodes
	index1 = -1
	index2 = -1

	# Iterate through the node list
	for i in range(0, len(node_list)):

		# Compare the global id of node 1
		if str(node_list[i].get_node_id()) == str(node1.get_node_id()):

			# Set index 1
			index1 = i

		# Compare the global id of node 2
		if str(node_list[i].get_node_id()) == str(node2.get_node_id()):

			# Set index 2
			index2 = i

	return index1, index2


def obtain_node_index_2D(node_list, node1, node2, node3):
	""" Obtain the index of the given node in the list """

	# Initialise indices of three nodes
	index1 = -1
	index2 = -1
	index3 = -1

	# Iterate through the node list
	for i in range(0, len(node_list)):

		# Compare the global id of node 1
		if str(node_list[i].get_global_id()) == str(node1.get_global_id()):

			# Set index 1
			index1 = i

		# Compare the global id of node 2
		if str(node_list[i].get_global_id()) == str(node2.get_global_id()):

			# Set index 2
			index2 = i

		# Compare the global id of node 3
		if str(node_list[i].get_global_id()) == str(node3.get_global_id()):

			# Set index 3
			index3 = i

	return index1, index2, index3


def check_dict(dictionary, node_id):
	""" Check if the given node id is in the dictionary """
	
	# Quit if the dictionary is empty
	if (len(dictionary) == 0):
		return False

	# Check if the node id is in the dictionary
	return str(node_id) in dictionary



def build_node_list(grid, boundary_type):
	""" Build a list of nodes with connected nodes closer to each other """
	
	# The index of checked node for neighouring nodes
	index = 0

	# Initialise a list of nodes
	node_list = []

	# Dictionary of nodes
	node_dict = {}

	# Iterate through nodes in the grid
	for node in node_iterator(grid):

		# Check if the node is interior
		if not check_dict(node_dict,node.get_node_id()):

			# If endpoint is interior or if the boundary is Neumann
			if not grid.get_slave(node.get_node_id()) or boundary_type == BoundaryType.Neumann:

				# Add the node to the dictionary
				node_dict[str(node.get_node_id())] = 0

				# Add the node to the list
				node_list.append(grid.get_node_ref(node.get_node_id()))

		# Iterate through the list
		while index < len(node_list):

			index_node_id = node_list[index].get_node_id()

			# Check every endpoint of the current node
			for endpt_id in endpt_iterator(grid, index_node_id):

				# Check if the neighbour node has been added, interior and 
				# has nonzero connections
				if not check_dict(node_dict,endpt_id) and \
					grid.get_matrix_L_value(index_node_id,endpt_id) != 0:
					
					# If endpoint is interior or if the boundary is Neumann
					if not grid.get_slave(endpt_id) or boundary_type == BoundaryType.Neumann:

						# Add the node to the dictionary
						node_dict[str(endpt_id)] = 0

						# Add the node to the list
						node_list.append(grid.get_node_ref(endpt_id))
			
			# Increase the index count
			index += 1

	return node_list