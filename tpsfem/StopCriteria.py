#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2020-09-04 16:46:40
@Last Modified time: 2022-10-09 23:41:35

This class contains routines to stop the refinement of the program.

"""

# Import libraries
from adaptive.triangle.LocalGrid2D import build_triangle_pair_grid
#from data.DataThinning import count_data_local_grid_2D
from data.ObtainData import obtain_data_grid
from parameter.Definition import ErrorType



def stop_iteration(parameter, error):
	""" Check if the iterative process should stop """

#	return False
	
	# Error tolerance of the TPSFEM
	error_tol = parameter.get_error_tol()

	# Root mean square error in each iteration
	rmse_list = error.get_error_list(ErrorType.rmse)

	# Continue if no error has been estimated
	if len(rmse_list) == 0:
		return False

	# If the current estimated error is smaller than the error
	# tolerance value, stop the refinement process
	if rmse_list[-1] < error_tol:
		return True

	if len(rmse_list) > 2:
		if abs((rmse_list[-2]-rmse_list[-1])/rmse_list[-2]) < 0.1 and \
			abs((rmse_list[-3]-rmse_list[-2])/rmse_list[-3]) < 0.1:
			return True

	return False



def stop_refinement(region, parameter, grid, node1, node2, node3, node4):
	""" Check if refinement of an element should stop """

	# If element-wise stopping criteria is not used, just return False
	return False

	# Error tolerance of the TPSFEM
	error_tol = parameter.get_error_tol()

	local_grid, inner_nodes = \
		build_triangle_pair_grid(grid, node1, node2, node3, node4)

#	local_data_no = count_data_local_grid_2D(grid, local_grid)
	local_data_no = len(obtain_data_grid(region, local_grid))

	if local_data_no < 5:
		return True

	return False
