#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-04-26 09:59:31
@Last Modified time: 2024-04-09 21:36:13

This class contains main routines of iterative adaptive refinement process
of the TPSFEM.

"""

# Import libraries
from grid.NodeTable import node_iterator
from grid.EdgeTable import endpt_iterator
from tpsfem.TpsfemSolver1D import solve_tpsfem_1D
from tpsfem.TpsfemSolver2D import solve_tpsfem_2D
from tpsfem.StopCriteria import stop_iteration
from indicator.ErrorIndicator import indicate_error
from adaptive.line.UniformRefinement import uniform_refinement as uniform_refinement_1D
from adaptive.line.AdaptiveRefinement import adaptive_refinement as adaptive_refinement_1D
from adaptive.triangle.UniformRefinement import uniform_refinement as uniform_refinement_2D
from adaptive.triangle.AdaptiveRefinement import adaptive_refinement as adaptive_refinement_2D
from adaptive.tetrahedron.UniformRefinement import uniform_refinement as uniform_refinement_3D
from estimator.EstimateError import calc_error
from estimator.Error import Error
from estimator.EstimateNorm import estimate_y_norm
from fileio.WriteResult import write_result, write_line
from fileio.GridFunction import write_grid
from fileio.FigureFunction import save_grid_figure, save_error_figure, \
	save_refinement_figure, save_indicator_figure, save_contour_figure, save_regression_figure
from parameter.Definition import ErrorType, CostType, IndicatorType
import plot.FemPlot2D as femPlot2D
import plot.TpsfemPlot1D as tpsfemPlot1D
import plot.TpsfemPlot2D as tpsfemPlot2D
import plot.RegionPlot as regionPlot
import plot.IndicatorPlot as indicatorPlot
import plot.DataErrorPlot as DataErrorPlot

from time import time


def uniform_refinement(grid, region, parameter):
	""" Perform uniform refinement """

	iterations = parameter.get_refine()[1]

	if iterations > 0:
		write_line(parameter, "Uniformly refine " + str(iterations) + " iterations", True)

	# Perform several uniform refinements before adaptive refinement
	if grid.get_dim() == 1:
		grid = uniform_refinement_1D(grid, parameter)
		pass
	elif grid.get_dim() == 2:
		grid = uniform_refinement_2D(grid, parameter)
		"""
		indicator_type = parameter.get_indicator()
		cost = parameter.get_cost()

		parameter.set_indicator(1)
		parameter.set_cost(None)

		for i in range(0,iterations):
			indicator_value = error_indicator(grid, region, parameter)
			grid = adaptive_refinement_2D(grid, region, parameter, True)

		parameter.set_indicator(indicator_type)
		parameter.set_cost(cost)
		"""
	elif grid.get_dim() == 3:
		grid = uniform_refinement_3D(grid, parameter)
	else:
		print("Warning: invalid dimension")

#	femPlot2D.plot_fem_grid_2D(grid, parameter, True)
	
	return grid



def solve_tpsfem(grid, region, parameter):
	""" Solve TPSFEM system of equations """

	write_line(parameter, "Solve TPSFEM", True)

	start_time = time()

	write_line(parameter, "#nodes: "+str(grid.get_no_nodes()), True)

	# Solve the discrete TPS smoother
	if grid.get_dim() == 1:
		solve_tpsfem_1D(grid, region, parameter)
		pass
	elif grid.get_dim() == 2:
		solve_tpsfem_2D(grid, region, parameter)
	else:
		print("Warning: invalid dimension")

	write_line(parameter, "alpha: "+str(grid.get_alpha()), True)

	elapsed_time = time() - start_time

	write_line(parameter, "time: "+str(elapsed_time), True)


def error_indicator(grid, region, parameter):
	""" Indicate error of the grid """

	parameter.set_sum(0.0)
	parameter.set_count(0)

	write_line(parameter, "Indicate error", True)

	start_time = time()

	indicator_list, indicator_max, indicator_one, indicator_two \
		 = indicate_error(grid, region, parameter)

	write_line(parameter, "indicator: " + str(indicator_max) + ", " + \
			str(indicator_one) + ", " + str(indicator_two), True)
	
	if len(indicator_list) != 0:
		print(indicator_max, indicator_one/len(indicator_list), indicator_two/len(indicator_list))

	elapsed_time = time() - start_time

	write_line(parameter, "time: "+str(elapsed_time), True)

	if parameter.get_indicator() == IndicatorType.auxiliary:
		if not parameter.get_count() == 0:
			write_line(parameter, "alpha sum: "+str(parameter.get_sum())+", count: "+str(parameter.get_count()), True)
			write_line(parameter, "average alpha:"+str(parameter.get_sum()/parameter.get_count()), True)

	indicator_value = indicator_two/len(indicator_list)
#	indicatorPlot.plot_error_indicator(grid, parameter)

	return indicator_value


def measure_error(grid, region, parameter, error):
	""" Measure grid error """

	write_line(parameter, "Measure error", True)

	start_time = time()

	calc_h=True
	calc_L=True
	calc_minimiser=False
	calc_regression=True

	# Calculate error using the true solution
	calc_error(grid, parameter, error, region, calc_h, calc_L, calc_minimiser, calc_regression)


	
	error1 = error.get_error_list(ErrorType.two_h)[-1]
	error2 = error.get_error_list(ErrorType.max_norm)[-1]

	
	write_line(parameter, "two h error: "+str(error1), True)
	write_line(parameter, "max error: "+str(error2), True)
	write_line(parameter, "RMSE: "+str(error.get_error_list(ErrorType.rmse)[-1]), True)

	elapsed_time = time() - start_time

	write_line(parameter, "time: "+str(elapsed_time), True)


def output_grid_error(parameter, error):
	""" Print recorded grid errors """

	print("grid error")
	print(error)
	print("")



def adaptive_refinement(grid, region, parameter):
	""" Perform adaptive refinement """

	# Record max node id
	node_no = 0
	if not parameter.get_local():
		node_no = grid._counter

	write_line(parameter, "Refine grid", True)

	start_time = time()
	
	# Record cost
	cost = parameter.get_cost()
	cost.add_time([], CostType.indicate_refine)

	refine_type = 1
	if parameter.get_indicator() == IndicatorType.auxiliary:
		indicator_parameter = parameter.get_indicator_parameter()
		refine_type = indicator_parameter[2]
		indicator_parameter[2] = 1
		parameter.set_indicator_parameter(indicator_parameter)


	if grid.get_dim() == 1:
		grid = adaptive_refinement_1D(grid, region, parameter)
		pass
	elif grid.get_dim() == 2:
		grid = adaptive_refinement_2D(grid, region, parameter)
	else:
		print("Warning: invalid dimension")

	elapsed_time = time() - start_time

	if parameter.get_indicator() == IndicatorType.auxiliary:
		indicator_parameter = parameter.get_indicator_parameter()
		indicator_parameter[2] = refine_type
		parameter.set_indicator_parameter(indicator_parameter)


	write_line(parameter, "time: "+str(elapsed_time), True)

	save_refinement_figure(grid, parameter, node_no)

	return grid


def main_tpsfem(grid, region, parameter, stop_criteria):
	""" Solve and refine the TPSFEM """
	
	# Grid error records
	error = Error()

	alpha_list = []
	indicator_list = []
	max_list = []
	rmse_list = []
	stop_list = []
	y_norm_list = []

	# Parameters
	max_sweep = parameter.get_max_sweep()

	# Perform several uniform refinements before adaptive refinement
	grid = uniform_refinement(grid, region, parameter)
	
	
#	for node in node_iterator(grid):
#		print(node.get_node_id(), node.get_global_id())

	###################
	if parameter.get_dim() == 3:
#		from sys import exit
#		exit()
		return grid
	###################

	# Solve TPSFEM using initial grid
	solve_tpsfem(grid, region, parameter)

	alpha_list.append(parameter.get_alpha())
	
	# Indicate grid error
	indicator_value = error_indicator(grid, region, parameter)
	indicator_list.append(indicator_value)

###################################
#	from plot.IndicatorPlot import plot_error_indicator_list
#	plot_error_indicator_list(grid, parameter)
#	parameter.set_indicator_list([])

	count_1 = 0
	count_2 = 0
	for node in node_iterator(grid):
		node_id = node.get_node_id()
		for endpt in endpt_iterator(grid, node.get_node_id()):
			if node_id > endpt:
				if grid.get_refine_type(node_id,endpt) == 1 or \
					grid.get_refine_type(node_id,endpt) == 3:
					if grid.get_error_indicator(node_id,endpt) == 0.0:
						count_2 += 1
					count_1 += 1

#	print("Edges with full data": count_1, count_2, count_2*1.0/count_1)
#	write_line(parameter, "Edges with full data: "+str(count_1)+", "+str(count_2)+", "+str(count_2*1.0/count_1), True)
	stop_list.append(count_2*1.0/count_1)

	# Measure the error of the current grid
	measure_error(grid, region, parameter, error)

	max_list.append(error.get_error_list(ErrorType.max_norm)[-1])
	rmse_list.append(error.get_error_list(ErrorType.rmse)[-1])

	one_norm, two_norm, max_norm, one_norm_h, two_norm_h = estimate_y_norm(grid, parameter)
	y_norm_list.append(two_norm)

	
	print("Start saving figures")

	write_grid(grid, parameter)
	save_grid_figure(grid, region, parameter)
	save_error_figure(grid, parameter)
#	save_regression_figure(grid, region, parameter)
#	save_indicator_figure(grid, parameter)
	save_contour_figure(grid, region, parameter)
	
	# The number of refinement sweeps
	sweep_count = 1	

	write_line(parameter, "", True)

	# Perform refinements until error tolerance is reached
	while sweep_count <= parameter.get_max_sweep():

		# Terminate iterative process if stopping criteria is met
		if stop_criteria and stop_iteration(parameter, error):
			break

		write_line(parameter, "=== Iteration "+str(sweep_count)+" ===", True)

		parameter.set_iteration(sweep_count)

		parameter.clear_indicator_list()

		# Perform adaptive refinement
		grid = adaptive_refinement(grid, region, parameter)

############################################		
#		from plot.IndicatorPlot import plot_error_indicator_list
#		plot_error_indicator_list(grid, parameter)
#		parameter.set_indicator_list([])

		# Solve TPSFEM using initial grid
		solve_tpsfem(grid, region, parameter)

		alpha_list.append(parameter.get_alpha())
		"""
		# Switch error indicator to get the indicator values for a certain iteration
		if parameter.get_iteration() == parameter.get_max_sweep():
			parameter.set_indicator(4)
			parameter.set_indicator_parameter([1,2,1,0,2, True, True,-1])
			indicator_value = error_indicator(grid, region, parameter)
			indicatorPlot.plot_error_indicator(grid, parameter)

			parameter.set_iteration(parameter.get_iteration()+1)
			parameter.set_indicator_parameter([1,2,1,0,2, True, True,-1])
			indicator_value = error_indicator(grid, region, parameter)
			indicatorPlot.plot_error_indicator(grid, parameter)
		"""


		# Indicate grid error
#		if sweep_count != parameter.get_max_sweep():
		indicator_value = error_indicator(grid, region, parameter)
		indicator_list.append(indicator_value)


############## CHANGE BACK LATER




###################################
#		from plot.IndicatorPlot import plot_error_indicator_list
#		plot_error_indicator_list(grid, parameter)
#		parameter.set_indicator_list([])
		"""
		count_1 = 0
		count_2 = 0
		for node in node_iterator(grid):
			node_id = node.get_node_id()
			for endpt in endpt_iterator(grid, node.get_node_id()):
				if node_id > endpt:
					if grid.get_refine_type(node_id,endpt) == 1 or \
						grid.get_refine_type(node_id,endpt) == 3:
						if grid.get_error_indicator(node_id,endpt) == 0.0:
							count_2 += 1
						count_1 += 1
		"""
#		print("Edges with full data": count_1, count_2, count_2*1.0/count_1)
		write_line(parameter, "Edges with full data: "+str(count_1)+", "+str(count_2)+", "+str(count_2*1.0/count_1), True)
		stop_list.append(count_2*1.0/count_1)

		# Measure the error of the current grid
		measure_error(grid, region, parameter, error)

		max_list.append(error.get_error_list(ErrorType.max_norm)[-1])
		rmse_list.append(error.get_error_list(ErrorType.rmse)[-1])
		
		one_norm, two_norm, max_norm, one_norm_h, two_norm_h = \
			estimate_y_norm(grid, parameter)
		y_norm_list.append(two_norm_h)

#		write_line(parameter, "", True)
		write_result(grid, region, parameter, error)
		
#		write_line(parameter, "", True)
#		write_line(parameter, "indicator_list = "+str(indicator_list), True)
#		write_line(parameter, "rmse_list = "+str(rmse_list), True)
#		write_line(parameter, "stop_list = "+str(stop_list), True)
#		write_line(parameter, "y norm list = "+str(y_norm_list), True)
#		write_line(parameter, "alpha_list = "+str(alpha_list), True)
#		write_line(parameter, "ratio list = "+str(parameter.get_ratios()), True)
		if parameter.get_indicator() == IndicatorType.auxiliary:
			write_line(parameter, "auxiliary_alpha_list = "+str(parameter.get_auxiliary_alpha()), True)

		# Write the current grid into a file
		write_grid(grid, parameter)

		# Save relevant grid information
		save_grid_figure(grid, region, parameter)
#		save_error_figure(grid, parameter)
#		save_regression_figure(grid, region, parameter)
#		save_indicator_figure(grid, parameter)
		save_contour_figure(grid, region, parameter)
		write_line(parameter, "", True)
		
		sweep_count += 1

#	write_line(parameter, "alpha_list = "+str(alpha_list), True)
#	write_line(parameter, "ratio list = "+str(parameter.get_ratios()), True)

#	output_grid_error(parameter, error)
#	write_line(parameter, str(parameter.get_cost()), True)
#	write_result(grid, region, parameter, error)
#	write_line(parameter, "", True)
	write_line(parameter, "rmse_list = "+str(rmse_list), True)
	write_line(parameter, "node_list = "+str(error.get_no_nodes_list()), True)
	
	return grid#, error.get_error_list(ErrorType.two_h)[-1]
