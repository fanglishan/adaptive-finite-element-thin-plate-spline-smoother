#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
@Author: Fang

@Date:   2022-12-19 16:03:53
@Last Modified time: 2022-12-19 16:08:46

This class contains routines to solve the 3D TPSFEM.

"""



