#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-11-15 22:37:09
@Last Modified time: 2022-03-29 11:24:03

This class contains routines to add entries to a given matrix.

"""

# Import libraries
from grid.EdgeTable import endpt_iterator
from parameter.Definition import BoundaryType



def add_alpha_L(grid, node_list, matrix, vector, parameter):
	""" Add L entries multiplied by alpha into the matrix """

	# Numer of interior numbers
	no_nodes = len(node_list)

	# Smoothing parameter
	alpha = parameter.get_alpha()

	# Iterate through columns
	for j in range(0, no_nodes):

		node_id2 = node_list[j].get_node_id()

		# Iterate through rows
		for i in range(0, no_nodes):

			node_id1 = node_list[i].get_node_id()

			# Check if two nodes have connections
			if grid.is_connection(node_id1, node_id2):

				# Get L matrix value from the existing connections
				L_value = grid.get_matrix_L_value(node_id1, node_id2)

				# Set value for submatrix L at 2,2 at 3*3 matrix
				matrix[i+no_nodes,j+no_nodes] = alpha*L_value



def add_boundary_effects(grid, node_list, vector, parameter):
	""" Obtain boundary effects from the grid """

	# For Dirichlet boundary problems
	if not (parameter.get_boundary() == BoundaryType.Dirichlet):
		return

	# Numer of interior numbers
	no_nodes = len(node_list)

	# Smoothing parameter
	alpha = parameter.get_alpha()

	# Iterate through the node list
	for i in range(0,len(node_list)):

		node_id = node_list[i].get_node_id()

		# Iterate through each endpoint of the node
		for endpt in endpt_iterator(grid, node_id):

			# If the endpoint is a boundary point
			if grid.get_slave(endpt):
				
				# Node of the endpoint
				end_node = grid.get_node_ref(endpt)
	
				# Get matrix values from the connections in the grid
				A_value = grid.get_matrix_A_value(node_id, endpt)
				L_value = grid.get_matrix_L_value(node_id, endpt)
				G_value = grid.get_matrix_G_value(node_id, endpt)[0]
				G_T_value = grid.get_matrix_G_value(endpt, node_id)[0]

				# Add boundary conditions for the first subvector
				vector[i,0] -= A_value*end_node.get_value()
				vector[i,0] -= L_value*(alpha*end_node.get_d_values()[1])

				# Add boundary conditions for the second subvector
				vector[i+no_nodes,0] -= (alpha*L_value)*end_node.get_d_values()[0]
				vector[i+no_nodes,0] -= G_T_value*(alpha*end_node.get_d_values()[1])

				# Add boundary conditions for the third subvector
				vector[i+2*no_nodes,0] -= L_value*end_node.get_value()
				vector[i+2*no_nodes,0] -= G_value*end_node.get_d_values()[0]



def add_uniqueness(matrix, no_nodes):
	""" Add uniquess constraints for the Neumann matrix """

	matrix[3*no_nodes, no_nodes:2*no_nodes] = 1      # Sum for g=0
	matrix[no_nodes:2*no_nodes, 3*no_nodes] = 1      # symmetry of g=0
	matrix[3*no_nodes+1, 2*no_nodes:3*no_nodes] = 1  # Sum for w=0
	matrix[2*no_nodes:3*no_nodes, 3*no_nodes+1] = 1  # Symmetry of g=0