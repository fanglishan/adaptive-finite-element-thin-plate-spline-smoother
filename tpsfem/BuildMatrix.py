#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-04-28 10:02:48
@Last Modified time: 2024-08-06 20:59:29

This class contains modules to build sparse matrix in the 
TPSFEM formulation.

"""

# Import libraries
from grid.EdgeTable import endpt_iterator
from grid.NodeTable import node_iterator
from data.ObtainData import obtain_data_all, obtain_data_u_all, obtain_data_grid
from adaptive.line.SetPolynomial import set_polynomial_linear_1D
from adaptive.triangle.SetPolynomial import set_polynomial_linear_2D
from adaptive.triangle.Triangle import triangle_iterator_ref
from tpsfem.BuildBasis import obtain_node_index_1D, obtain_node_index_2D

from numpy import sqrt, zeros
from scipy.sparse import lil_matrix, csc_matrix


def build_y(grid, region, parameter):
	""" Build matrix B and vector y """

	# Print warning message
#	if parameter.get_data_size() == 0:
#		print("Warning: no data point")

	# Obtain data
	if parameter.get_local():
		data = obtain_data_grid(region, grid)
	else:
		data = obtain_data_all(region)

	# Initialise data point value list y
	y = []

	# Set y vector
	for i in range(0, len(data)):
		y.append(data[i][-1])

	return y


def build_BT_y_1D(grid, region, parameter, node_list):
	""" Build matrix B and vector y """

	# Print warning message
	if parameter.get_data_size() == 0:
		print("Waring: no data point")

	data = obtain_data_all(region)

	# Number of unknowns
	no_nodes = len(node_list)

	# Number of data points
	no_data = len(data)

	# Initialise a matrix B transpose
	B_T =  lil_matrix((no_data,no_nodes),dtype=float)

	# Initialise data point value list y
	y = zeros([no_data, 1])

	# Iterate through each edge once
	for node in node_iterator(grid):
		node_id = node.get_node_id()
		for endpt in endpt_iterator(grid, node_id):
			if node_id > endpt:
				end_node = grid.get_node_ref(endpt)

				# Coordinates of two endpoints
				coord1 = node.get_coord()
				coord2 = end_node.get_coord()

				# Polynomials of three vertices
				poly1 = set_polynomial_linear_1D(node, end_node)
				poly2 = set_polynomial_linear_1D(end_node, node)
			
				# Obtain two node index in the list
				index1, index2 = obtain_node_index_1D(node_list, node, end_node)

				# Iterate through each data point
				for i in range(0, no_data):

					# Coodinate of the data point
					coord = data[i][0]

					# Check if the data point is in the triangle
					if poly1.eval(coord) > 0.0 and poly2.eval(coord) > 0.0:

						if index1 >= 0:
							B_T[i, index1] = poly1.eval(coord)

						if index2 >= 0:
							B_T[i, index2] = poly2.eval(coord)
					
	# Set y vector
	for i in range(0, no_data):
		y[i,0] = data[i][-1]

	# Ratio of B matrix
	ratio = 1.0/sqrt(no_data)

	# Iterate through the matrix
	for j in range(0, no_nodes):
		for i in range(0, no_data):
 
 			# Apply the ratio
			B_T[i,j] *= ratio

	return csc_matrix(B_T), y


def build_B_y_2D(grid, region, parameter, node_list):
	""" Build matrix B and vector y """

	# Print warning message
	if parameter.get_data_size() == 0:
		print("Waring: no data point")

	data = obtain_data_all(region)

	# Number of unknowns
	no_nodes = len(node_list)

	# Number of data points
	no_data = len(data)

	# Ratio of B matrix
	ratio = 1.0/sqrt(no_data)

	# Initialise a matrix B transpose
	B =  lil_matrix((no_nodes,no_data),dtype=float)

	# Initialise data point value list y
	y = zeros([no_data, 1])

	# Iterate through each triangle
	for tri in triangle_iterator_ref(grid):
		if tri[0].get_node_id() < tri[1].get_node_id() and \
		   tri[1].get_node_id() < tri[2].get_node_id():

			# Coordinates of three vertices
			coord1 = tri[0].get_coord()
			coord2 = tri[1].get_coord()
			coord3 = tri[2].get_coord()

			# Polynomials of three vertices
			poly1 = set_polynomial_linear_2D(tri[0], tri[1], tri[2])
			poly2 = set_polynomial_linear_2D(tri[1], tri[2], tri[0])
			poly3 = set_polynomial_linear_2D(tri[2], tri[0], tri[1])

			# Obtain three node index in the list
			index1, index2, index3 = \
				obtain_node_index_2D(node_list, tri[0], tri[1], tri[2])

			# If all three vertices of the triangle are boundary nodes
			# Skip this triangle
			if index1 == -1 and index2 == -1 and index3 == -1:
				continue

			# Iterate through each data point
			for i in range(0, no_data):

				# Coodinate of the data point
				coord = data[i][:2]

				# Check if the data point is in the triangle
				if poly1.eval(coord[0], coord[1]) > 0.0 and poly2.eval(coord[0], coord[1]) > 0.0 \
						and poly3.eval(coord[0], coord[1]) > 0.0:

					if index1 >= 0:
						B[index1, i] = poly1.eval(coord[0], coord[1])*ratio

					if index2 >= 0:
						B[index2, i] = poly2.eval(coord[0], coord[1])*ratio

					if index3 >= 0:
						B[index3, i] = poly3.eval(coord[0], coord[1])*ratio
					
	# Set y vector
	for i in range(0, no_data):
		y[i,0] = data[i][-1]

	return csc_matrix(B), y



def build_BT_y_2D(grid, region, parameter, node_list):
	""" Build matrix B and vector y """

	# Print warning message
	if parameter.get_data_size() == 0:
		print("Waring: no data point")


	# Obtain data
	if parameter.get_local():
		data = obtain_data_grid(region, grid)
	else:
		data = obtain_data_all(region)

	# Number of unknowns
	no_nodes = len(node_list)

	# Number of data points
	no_data = len(data)

	# Ratio of B matrix
	ratio = 1.0/sqrt(no_data)

	# Initialise a matrix B transpose
	B_T =  lil_matrix((no_data,no_nodes),dtype=float)

	# Initialise data point value list y
	y = zeros([no_data, 1])

	# Iterate through each triangle
	for tri in triangle_iterator_ref(grid):
		if tri[0].get_node_id() < tri[1].get_node_id() and \
		   tri[1].get_node_id() < tri[2].get_node_id():

			# Coordinates of three vertices
			coord1 = tri[0].get_coord()
			coord2 = tri[1].get_coord()
			coord3 = tri[2].get_coord()

			# Polynomials of three vertices
			poly1 = set_polynomial_linear_2D(tri[0], tri[1], tri[2])
			poly2 = set_polynomial_linear_2D(tri[1], tri[2], tri[0])
			poly3 = set_polynomial_linear_2D(tri[2], tri[0], tri[1])

			# Obtain three node index in the list
			index1, index2, index3 = \
				obtain_node_index_2D(node_list, tri[0], tri[1], tri[2])

			# If all three vertices of the triangle are boundary nodes
			# Skip this triangle
			if index1 == -1 and index2 == -1 and index3 == -1:
				continue

			# Iterate through each data point
			for i in range(0, no_data):

				# Coodinate of the data point
				coord = data[i][:2]

				# Check if the data point is in the triangle
				if poly1.eval(coord[0], coord[1]) > 0.0 and poly2.eval(coord[0], coord[1]) > 0.0 \
						and poly3.eval(coord[0], coord[1]) > 0.0:

					if index1 >= 0:
						B_T[i, index1] = poly1.eval(coord[0], coord[1])*ratio

					if index2 >= 0:
						B_T[i, index2] = poly2.eval(coord[0], coord[1])*ratio

					if index3 >= 0:
						B_T[i, index3] = poly3.eval(coord[0], coord[1])*ratio
					
	# Set y vector
	for i in range(0, no_data):
		y[i,0] = data[i][-1]

	return csc_matrix(B_T), y


def build_BT_y_u_2D(grid, region, parameter, node_list):
	""" Build matrix B and vector y """

	# Print warning message
	if parameter.get_data_size() == 0:
		print("Waring: no data point")

	data, u = obtain_data_u_all(region)

	# Number of unknowns
	no_nodes = len(node_list)

	# Number of data points
	no_data = len(data)

	# Ratio of B matrix
	ratio = 1.0/sqrt(no_data)

	# Initialise a matrix B transpose
	B_T =  lil_matrix((no_data,no_nodes),dtype=float)

	# Initialise data point value list y
	y = zeros([no_data, 1])

	# Iterate through each triangle
	for tri in triangle_iterator_ref(grid):
		if tri[0].get_node_id() < tri[1].get_node_id() and \
		   tri[1].get_node_id() < tri[2].get_node_id():

			# Coordinates of three vertices
			coord1 = tri[0].get_coord()
			coord2 = tri[1].get_coord()
			coord3 = tri[2].get_coord()

			# Polynomials of three vertices
			poly1 = set_polynomial_linear_2D(tri[0], tri[1], tri[2])
			poly2 = set_polynomial_linear_2D(tri[1], tri[2], tri[0])
			poly3 = set_polynomial_linear_2D(tri[2], tri[0], tri[1])

			# Obtain three node index in the list
			index1, index2, index3 = \
				obtain_node_index_2D(node_list, tri[0], tri[1], tri[2])

			# If all three vertices of the triangle are boundary nodes
			# Skip this triangle
			if index1 == -1 and index2 == -1 and index3 == -1:
				continue

			# Iterate through each data point
			for i in range(0, no_data):

				# Coodinate of the data point
				coord = data[i][:2]

				# Check if the data point is in the triangle
				if poly1.eval(coord[0], coord[1]) > 0.0 and poly2.eval(coord[0], coord[1]) > 0.0 \
						and poly3.eval(coord[0], coord[1]) > 0.0:

					if index1 >= 0:
						B_T[i, index1] = poly1.eval(coord[0], coord[1])*ratio

					if index2 >= 0:
						B_T[i, index2] = poly2.eval(coord[0], coord[1])*ratio

					if index3 >= 0:
						B_T[i, index3] = poly3.eval(coord[0], coord[1])*ratio
					
	# Set y vector
	for i in range(0, no_data):
		y[i,0] = data[i][-1]

	return csc_matrix(B_T), y, u


def build_A(grid, parameter, node_list):

	# Numer of interior numbers
	no_nodes = len(node_list)

	# Initialise a matrix B transpose
	A =  lil_matrix((no_nodes,no_nodes),dtype=float)

	# Iterate through each entry
	for j in range(0, no_nodes):
		node_id2 = node_list[j].get_node_id()
		for i in range(0, no_nodes):
			node_id1 = node_list[i].get_node_id()

			# Check if two nodes have connections
			if grid.is_connection(node_id1, node_id2):

				# Add value to A submatrix
				A[i,j] = grid.get_matrix_A_value(node_id1, node_id2)

	return csc_matrix(A)


def build_L(grid, parameter, node_list):
	""" Build matrix L """

	# Numer of interior numbers
	no_nodes = len(node_list)

	# Initialise a matrix B transpose
	L =  lil_matrix((no_nodes,no_nodes),dtype=float)

	# Iterate through each entry
	for j in range(0, no_nodes):
		node_id2 = node_list[j].get_node_id()
		for i in range(0, no_nodes):
			node_id1 = node_list[i].get_node_id()

			# Check if two nodes have connections
			if grid.is_connection(node_id1, node_id2):

				# Add value to A submatrix
				L[i,j] = grid.get_matrix_L_value(node_id1, node_id2)

	return csc_matrix(L)


def build_G_1D(grid, parameter, node_list):
	""" Build matrix G """

	# Numer of interior numbers
	no_nodes = len(node_list)

	# Initialise a matrix B transpose
	G =  lil_matrix((no_nodes,no_nodes),dtype=float)

	# Iterate through each entry
	for j in range(0, no_nodes):
		node_id2 = node_list[j].get_node_id()
		for i in range(0, no_nodes):
			node_id1 = node_list[i].get_node_id()

			# Check if two nodes have connections
			if grid.is_connection(node_id1, node_id2):

				# Add value to -G submatrix
				G_value = grid.get_matrix_G_value(node_id1, node_id2)

				G[i,j] = G_value[0]

	return csc_matrix(G)


def build_G_2D(grid, parameter, node_list):
	""" Build matrix G """

	# Numer of interior numbers
	no_nodes = len(node_list)

	# Initialise a matrix B transpose
	G1 =  lil_matrix((no_nodes,no_nodes),dtype=float)
	G2 =  lil_matrix((no_nodes,no_nodes),dtype=float)

	# Iterate through each entry
	for j in range(0, no_nodes):
		node_id2 = node_list[j].get_node_id()
		for i in range(0, no_nodes):
			node_id1 = node_list[i].get_node_id()

			# Check if two nodes have connections
			if grid.is_connection(node_id1, node_id2):

				# Add value to A submatrix
				G_value = grid.get_matrix_G_value(node_id1, node_id2)

				G1[i,j] = G_value[0]
				G2[i,j] = G_value[1]

	return csc_matrix(G1), csc_matrix(G2)


def build_G_combine_2D(grid, parameter, node_list):
	""" Build matrix G """

	# Numer of interior numbers
	no_nodes = len(node_list)

	# Initialise a matrix B transpose
	G =  lil_matrix((no_nodes,2*no_nodes),dtype=float)

	# Iterate through each entry
	for j in range(0, no_nodes):
		node_id2 = node_list[j].get_node_id()
		for i in range(0, no_nodes):
			node_id1 = node_list[i].get_node_id()

			# Check if two nodes have connections
			if grid.is_connection(node_id1, node_id2):

				# Add value to A submatrix
				G_value = grid.get_matrix_G_value(node_id1, node_id2)

				if abs(G_value[0]) > 0.000000000001:
					G[i,j] = G_value[0]
				if abs(G_value[1]) > 0.000000000001:
					G[i,j+no_nodes] = G_value[1]
				
	return csc_matrix(G)

