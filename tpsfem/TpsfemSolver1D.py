#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-05-07 21:32:36
@Last Modified time: 2023-10-13 00:03:35

This class contains routines to solve the 1D TPSFEM.

"""

# Import libraries
from adaptive.line.BuildEquationLinear import build_equation_linear_1D
from tpsfem.BuildBasis import build_node_list
from tpsfem.AddEntry1D import add_alpha_L, add_uniqueness, add_boundary_effects
from gcv.GCVSaddle1D import GCV_1D#, GCV_analysis_1D
from gcv.GCVRefinement import GCV_refine_1D
from gcv.GCVIndicator import GCV_indicator_1D
from parameter.Definition import BoundaryType, IndicatorType

from numpy import zeros
from scipy.sparse.linalg import spsolve
from scipy.sparse import lil_matrix, csc_matrix



def obtain_matrix_entries(grid, node_list, matrix, vector, KTu_vector, parameter):
	""" Add submatrice entries from grid connections """

	# Numer of interior numbers
	no_nodes = len(node_list)

	# Iterate through columns
	for j in range(0,len(node_list)):

		node_id2 = node_list[j].get_node_id()

		# Add values to vector
		vector[j,0] += node_list[j].get_data_load()

		# Add values to K^T*u vector
		KTu_vector[j,0] = node_list[j].get_KTu_load()

		# Iterate through rows
		for i in range(0,len(node_list)):

			node_id1 = node_list[i].get_node_id()

			# Check if two nodes have connections
			if grid.is_connection(node_id1, node_id2):

				# Add value to A submatrix
				matrix[i,j] = grid.get_matrix_A_value(node_id1, node_id2)

				# Get L matrix value from the existing connections
				L_value = grid.get_matrix_L_value(node_id1, node_id2)

				# Add values to L submatrices
				matrix[i,j+2*no_nodes] = L_value
				matrix[i+2*no_nodes,j] = L_value
#				matrix[i+no_nodes,j+no_nodes] = alpha*L_value

				# Get -G matrix value from the existing connections
				G_value = grid.get_matrix_G_value(node_id1, node_id2)

				# Add values to G submatrices
				matrix[i+2*no_nodes,j+no_nodes] = G_value
				matrix[j+no_nodes,i+2*no_nodes] = G_value



###################################
# System of equations 1D
# 
# Dirichlet boundary
#
# [A  0    L  ] [c] = [d] - [h1]
# [0  a*L -G^T] [g]   [0]   [h2]
# [L -G    0  ] [w]   [0]   [h3]
#
# Neumann boundary
#
# [A  0    L    0 0] [c] = [d] - [h1]
# [0  a*L -G^T  1 0] [g]   [0]   [h2]
# [L -G    0    0 1] [w1]  [0]   [h3]
# [0  1    0    0 0] [w2]  [0]   [0]
# [0  0    1    0 0] [w3]  [0]   [0]
#
###################################
def obtain_system_equations(grid, parameter, node_list, solve_system=True):
	""" Construc the stiffness matrix """

	# Number of nodes
	no_nodes = len(node_list)

	# Number of data points
	no_data = parameter.get_data_size()

	# For Dirchlet boundary model problem
	if parameter.get_boundary() == BoundaryType.Dirichlet:
	
		# Initialise a sparse matrix and a RHS vector
		matrix =  lil_matrix((3*no_nodes,3*no_nodes),dtype=float)
		vector = zeros([3*no_nodes, 1])
		KTu_vector = zeros([3*no_nodes, 1])

	# For Neumann boundary model problem
	elif parameter.get_boundary() == BoundaryType.Neumann:

		# Initialise a sparse matrix and a RHS vector
		matrix =  lil_matrix((3*no_nodes+2,3*no_nodes+2),dtype=float)	
		vector = zeros([3*no_nodes+2, 1])
		KTu_vector = zeros([3*no_nodes+2, 1])

	# Add submatrices from the grid connections
	obtain_matrix_entries(grid, node_list, matrix, vector, KTu_vector, parameter)
 	
	# For Neumann boundary problems
	if parameter.get_boundary() == BoundaryType.Neumann:
		add_uniqueness(matrix, no_nodes)

	return matrix, vector, KTu_vector




def assign_grid_values(grid, node_list, parameter, tpsfem_solution):
	""" Assign new values to the grid """

	# Length of node list
	no_nodes = len(node_list)
	
	# Smoothing parameter
	alpha = parameter.get_alpha()
	
	grid.set_alpha(alpha)

	# Update node values in the grid
	for i in range(0,len(node_list)):

		# Set node value
		node_list[i].set_value(tpsfem_solution[i])

		# Set derivative values
		node_list[i].set_d_values([tpsfem_solution[i+no_nodes], tpsfem_solution[i+2*no_nodes]/alpha])

	
	func = parameter.get_function()


	# Update node values in the grid
	for i in range(0,len(node_list)):

		coord = node_list[i].get_coord()

		# Set node value
		node_list[i].set_value(func[0](coord))

		# Set derivative values
		node_list[i].set_d_values([func[1](coord), func[2](coord)/alpha])
	

def solve_tpsfem_1D(grid, region, parameter, if_build=True):
	""" Solve TPSFEM with current grid and update the node values """

	# Record original alpha
	original_alpha = parameter.get_alpha()

	# Minimum alpha
	min_alpha = 1e-14

	# Calculate matrix value and load vector value for each node
	if if_build:
		region.init_usage()

		build_equation_linear_1D(grid, region, parameter)

	solve_system = False

	# Build the system for the GCV
	obtain_system_again = False

	if parameter.get_boundary() == BoundaryType.Neumann:
		obtain_system_again = True
		parameter.set_boundary(BoundaryType.Dirichlet)

	# Build a list of nodes to reduce matrix band
	node_list = build_node_list(grid, parameter.get_boundary())

	# Build a system of equations without alpha
	matrix, vector, KTu_vector = obtain_system_equations(grid, parameter, node_list, solve_system)

	# Default alpha
	alpha = parameter.get_alpha()

	# If smoothing parameter needs to be calculated
	if parameter.get_run_gcv():

		# Stop decreasing alpha if it is too small
		if alpha <= min_alpha:
			pass
	
		elif len(node_list) > 1:
			
			if parameter.get_local():
				v_approx = True
				max_iteration = 6
			else:
				gcv_parameter = parameter.get_gcv_parameter()
				v_approx = gcv_parameter[0]
				max_iteration = gcv_parameter[1]
				start_bound = gcv_parameter[2]
				end_bound = gcv_parameter[3]
				
			if parameter.get_local():
				alpha = GCV_1D(grid, region, parameter, node_list, \
					v_approx, max_iteration, matrix, vector, KTu_vector)
#				GCV_analysis_1D(grid, region, parameter, node_list, v_approx, max_iteration, matrix, vector, KTu_vector)
			else:
#				GCV_analysis_1D(grid, region, parameter, node_list, v_approx, max_iteration, matrix, vector, KTu_vector)
				alpha = GCV_refine_1D(grid, region, parameter, node_list, \
						matrix, vector, KTu_vector, max_iteration, start_bound, end_bound)

			if parameter.get_local():
				parameter.add_sum(alpha)
				parameter.add_count()

		# Stop decreasing alpha if it is too small
		if alpha < min_alpha:
			parameter.set_alpha(min_alpha)				
		else:
			parameter.set_alpha(alpha)

	# If the previous system is for the GCV
	if obtain_system_again:
		parameter.set_boundary(BoundaryType.Neumann)

		# Build a list of nodes to reduce matrix band
		node_list = build_node_list(grid, parameter.get_boundary())

		# Build a system of equations without alpha
		matrix, vector, KTu_vector = obtain_system_equations(grid, parameter, node_list, solve_system)

	# Add L entries multiplied with alpha
	add_alpha_L(grid, node_list, matrix, vector, parameter)

	# Add effects from boundary conditions
	add_boundary_effects(grid, node_list, vector, parameter)

#	compare_performance_1D(grid, node_list, parameter, matrix, vector)

	# Solve system using the direct solver
	tpsfem_solution = spsolve(csc_matrix(matrix), vector)

	# Assign new grid values
	assign_grid_values(grid, node_list, parameter, tpsfem_solution)

	# Reset smoothing parameters for auxiliary problems
	if parameter.get_local():
		parameter.set_alpha(original_alpha)


def calculate_alpha_1D(grid, region, parameter, V_approx):
	""" Calculate smoothing parameter alpha only """

	# Calculate matrix value and load vector value for each node
	region.init_usage()

	build_equation_linear_1D(grid, region, parameter)

	solve_system = False

	# Build the system for the GCV
	obtain_system_again = False

	# Build a list of nodes to reduce matrix band
	node_list = build_node_list(grid, parameter.get_boundary())

	# Build a system of equations without alpha
	matrix, vector, KTu_vector = obtain_system_equations(grid, parameter, node_list, solve_system)

	# Default alpha
	alpha = parameter.get_alpha()

	if len(node_list) > 1:
			
		max_iteration = 7
		start_bound = 1e-9
		end_bound = 1e-4

		alpha = GCV_indicator_1D(grid, region, parameter, node_list, \
				V_approx, max_iteration, matrix, vector, KTu_vector, start_bound, end_bound)

		return alpha

	else:
		return False