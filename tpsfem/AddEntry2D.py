#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-11-15 22:37:09
@Last Modified time: 2024-01-26 17:34:21

This class contains routines to add entries to a given matrix.

"""

# Import libraries
from grid.EdgeTable import endpt_iterator
from parameter.Definition import BoundaryType
from adaptive.triangle.BuildEquation import set_slave_value


def add_alpha_L(grid, node_list, matrix, vector, parameter):
	""" Add L entries multiplied by alpha into the matrix """

	# Numer of interior numbers
	no_nodes = len(node_list)

	# Smoothing parameter
	alpha = parameter.get_alpha()

	# Iterate through columns
	for j in range(0, no_nodes):

		node_id2 = node_list[j].get_node_id()

		# Iterate through rows
		for i in range(0, no_nodes):

			node_id1 = node_list[i].get_node_id()

			# Check if two nodes have connections
			if grid.is_connection(node_id1, node_id2):

				# Get L matrix value from the existing connections
				L_value = grid.get_matrix_L_value(node_id1, node_id2)

				# Set value for submatrix L at 2,2 and 3,3 at 4*4 matrix
				matrix[i+no_nodes,j+no_nodes] = alpha*L_value
				matrix[i+2*no_nodes,j+2*no_nodes] = alpha*L_value



def add_boundary_effects_alternate(grid, node_list, vector, parameter):
	""" Obtain boundary effects from the grid """

	# For Dirichlet boundary problems
	if not (parameter.get_boundary() == BoundaryType.Dirichlet):
		return
		
#	print("add boundary conditions")

	# Numer of interior numbers
	no_nodes = len(node_list)

	# Smoothing parameter
	alpha = parameter.get_alpha()

	# Iterate through the node list
	for i in range(0,len(node_list)):

		node_id = node_list[i].get_node_id()

		# Iterate through each endpoint of the node
		for endpt in endpt_iterator(grid, node_id):

			# If the endpoint is a boundary point
			if grid.get_slave(endpt):

				# Node of the endpoint
				end_node = grid.get_node_ref(endpt)

				# Get matrix values from the connections in the grid
				A_value = grid.get_matrix_A_value(node_id, endpt)
				L_value = grid.get_matrix_L_value(node_id, endpt)
				G1_value = grid.get_matrix_G_value(node_id, endpt)[0]
				G1_T_value = grid.get_matrix_G_value(endpt, node_id)[0]
				G2_value = grid.get_matrix_G_value(node_id, endpt)[1]
				G2_T_value = grid.get_matrix_G_value(endpt, node_id)[1]

				# Add boundary conditions for the first subvector
#				vector[i,0] -= A_value*end_node.get_value()
#				vector[i,0] -= L_value*(alpha*end_node.get_d_values()[2])
				vector[i,0] -= L_value*end_node.get_value()
				vector[i,0] -= G1_value*end_node.get_d_values()[0]
				vector[i,0] -= G2_value*end_node.get_d_values()[1]

				# Add boundary conditions for the second subvector
				vector[i+no_nodes,0] -= (alpha*L_value)*end_node.get_d_values()[0]
				vector[i+no_nodes,0] -= G1_T_value*(alpha*end_node.get_d_values()[2])

				# Add boundary conditions for the third subvector
				vector[i+2*no_nodes,0] -= (alpha*L_value)*end_node.get_d_values()[1]
				vector[i+2*no_nodes,0] -= G2_T_value*(alpha*end_node.get_d_values()[2])

				# Add boundary conditions for the forth subvector
#				vector[i+3*no_nodes,0] -= L_value*end_node.get_value()
#				vector[i+3*no_nodes,0] -= G1_value*end_node.get_d_values()[0]
#				vector[i+3*no_nodes,0] -= G2_value*end_node.get_d_values()[1]
				vector[i+3*no_nodes,0] -= A_value*end_node.get_value()
				vector[i+3*no_nodes,0] -= L_value*(alpha*end_node.get_d_values()[2])


def add_boundary_effects(grid, node_list, vector, parameter):
	""" Obtain boundary effects from the grid """

	# For Dirichlet boundary problems
	if not (parameter.get_boundary() == BoundaryType.Dirichlet):
		return
		
#	print("add boundary conditions")

	# Numer of interior numbers
	no_nodes = len(node_list)

	# Smoothing parameter
	alpha = parameter.get_alpha()

	# Iterate through the node list
	for i in range(0,len(node_list)):

		node_id = node_list[i].get_node_id()

		# Iterate through each endpoint of the node
		for endpt in endpt_iterator(grid, node_id):

			# If the endpoint is a boundary point
			if grid.get_slave(endpt):

				# Node of the endpoint
				end_node = grid.get_node_ref(endpt)

				# Get matrix values from the connections in the grid
				A_value = grid.get_matrix_A_value(node_id, endpt)
				L_value = grid.get_matrix_L_value(node_id, endpt)
				G1_value = grid.get_matrix_G_value(node_id, endpt)[0]
				G1_T_value = grid.get_matrix_G_value(endpt, node_id)[0]
				G2_value = grid.get_matrix_G_value(node_id, endpt)[1]
				G2_T_value = grid.get_matrix_G_value(endpt, node_id)[1]

				# Add boundary conditions for the first subvector
				vector[i,0] -= A_value*end_node.get_value()
				vector[i,0] -= L_value*(alpha*end_node.get_d_values()[2])

				# Add boundary conditions for the second subvector
				vector[i+no_nodes,0] -= (alpha*L_value)*end_node.get_d_values()[0]
				vector[i+no_nodes,0] -= G1_T_value*(alpha*end_node.get_d_values()[2])

				# Add boundary conditions for the third subvector
				vector[i+2*no_nodes,0] -= (alpha*L_value)*end_node.get_d_values()[1]
				vector[i+2*no_nodes,0] -= G2_T_value*(alpha*end_node.get_d_values()[2])

				# Add boundary conditions for the forth subvector
				vector[i+3*no_nodes,0] -= L_value*end_node.get_value()
				vector[i+3*no_nodes,0] -= G1_value*end_node.get_d_values()[0]
				vector[i+3*no_nodes,0] -= G2_value*end_node.get_d_values()[1]



def add_uniqueness(matrix, no_nodes):
	""" Add uniquess constraints for the Neumann matrix """

	matrix[4*no_nodes, no_nodes:2*no_nodes] = 1      # Sum for g1=0
	matrix[no_nodes:2*no_nodes, 4*no_nodes] = 1      # Symmetry of g1=0
	matrix[4*no_nodes+1, 2*no_nodes:3*no_nodes] = 1  # Sum for g2=0
	matrix[2*no_nodes:3*no_nodes, 4*no_nodes+1] = 1  # Symmetry of g2=0
	matrix[4*no_nodes+2, 3*no_nodes:4*no_nodes] = 1  # Sum for w1=0	
	matrix[3*no_nodes:4*no_nodes, 4*no_nodes+2] = 1  # Symmetry of w1=0


def add_uniqueness_w(matrix, no_nodes):
	""" Add uniquess constraints for w values for the Neumann matrix """

	matrix[4*no_nodes, 3*no_nodes:4*no_nodes] = 1  # Sum for w1=0	
	matrix[3*no_nodes:4*no_nodes, 4*no_nodes] = 1  # Symmetry of w1=0


def set_bnd_c_g(grid, parameter, node_list, matrix, vector, bnd_index_list):

	alpha = parameter.get_alpha()

	no_bnd_nodes = len(bnd_index_list)
	no_nodes = len(node_list)

	space = 4*no_nodes

	set_slave_value(grid, parameter)

	# Iterate through the boundary node list
	for i in range(0,no_bnd_nodes):

		node = node_list[bnd_index_list[i]]

		matrix[i+space, bnd_index_list[i]] = 1
		matrix[i+space+no_bnd_nodes, bnd_index_list[i]+no_nodes] = 1
		matrix[i+space+2*no_bnd_nodes, bnd_index_list[i]+2*no_nodes] = 1
		matrix[bnd_index_list[i], i+space] = 1
		matrix[bnd_index_list[i]+no_nodes,i+space+no_bnd_nodes] = 1
		matrix[bnd_index_list[i]+2*no_nodes,i+space+2*no_bnd_nodes] = 1
#		print(node.get_value())
#		print(node.get_d_values())
		vector[i+space,0] = node.get_value()
		vector[i+space+no_bnd_nodes,0] = node.get_d_values()[0]
		vector[i+space+2*no_bnd_nodes,0] = node.get_d_values()[1]


		### THIS IMPLEMENTATION IS TO VERIFY SETTING OF DIRICHLET BOUNDARY CONDITIONS
		matrix[i+space+3*no_bnd_nodes, bnd_index_list[i]+3*no_nodes] = 1
		matrix[bnd_index_list[i]+3*no_nodes,i+space+3*no_bnd_nodes] = 1
		vector[i+space+3*no_bnd_nodes,0] = node.get_d_values()[2]*alpha
		###