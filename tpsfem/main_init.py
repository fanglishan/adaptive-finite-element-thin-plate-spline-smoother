#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-04-16 10:13:44
@Last Modified time: 2023-03-09 16:25:44

This host class will initialise parameters that is set in the main class. Most of them 
will be stored in a parameter variable to be used in workers. It will also build an 
initial grid and solve the TPSFEM. The grid will be divided into several sub-grids 
and sent to workers. The final grid will be sent back from workers to combine into the 
final grid in the host processor.

"""

# Import libraries
from grid.Grid import Grid
from build.InitialGrid import build_initial_grid
from data.RegionFunction import build_region
from data.BuildDataset import build_dataset
from data.DatasetManager import obtain_dataset
from fileio.WriteResult import create_folder, initialise_file
from estimator.ManageSolution import obtain_soln
from estimator.MeasureCost import measure_time
from parameter.Definition import BoundaryType, ModelProblem2D
import plot.FemPlot2D as femPlot2D
import plot.TpsfemPlot1D as tpsfemPlot1D
import plot.TpsfemPlot2D as tpsfemPlot2D
import plot.RegionPlot as regionPlot
from plot.FemPlot3D import plot_fem_grid_3D, plot_fem_grid_3D_line

import os
from numpy import sqrt, cbrt


def set_data_stat(data, parameter):
	""" Add statistics of the dataset """

	data_mean = 0.0

	for data_point in data:
		data_mean += data_point[-1]

	parameter.add_stat(data_mean/len(data))



def set_region_no(parameter, d, dataset, data_size):

	region_no = 0

	data_index = abs(dataset)*-1

	# Adjust region depending on the size of the datasets
	if data_index == ModelProblem2D.magnet:  # 735,700 data points (2D)
		region_no = 200
			
	elif data_index == ModelProblem2D.bone:  # 179,830 data points (2D)
		region_no = 100

	elif data_index == ModelProblem2D.lake:  # 22,731 data points (2D)
		region_no = 40

	elif data_index == ModelProblem2D.lena:  # 262,144 data points (2D)
		region_no = 150

	elif data_index == ModelProblem2D.crater:  # 12,936,068 data points (2D)
		region_no = 350

	elif data_index == ModelProblem2D.ironmine:  # 210,688 data points (2D)
		region_no = 120

	elif data_index == ModelProblem2D.mountain:  # 1,011,468 data points (2D)
		region_no = 250

	elif data_index == ModelProblem2D.burney:  # 1,048,576 data points (2D)
		region_no = 250

	elif data_index == ModelProblem2D.canyon:  # 22,104 data points (2D)
		region_no = 40

	elif data_index == ModelProblem2D.river:  # 895,748 data points (2D)
		region_no = 220

	elif data_index == ModelProblem2D.beach:  # 254 data points (2D)
		region_no =	10


	elif d == 1:
		region_no = int(data_size/30)

		if region_no < 20:
			region_no = 20

	elif d == 2:
		data_level = int(sqrt(data_size))
		region_no = int(data_level/7)

		if region_no < 10:
			region_no = 10

	elif d == 3:
		data_level = int(cbrt(data_size))
		region_no = int(data_level/5)

		if region_no < 10:
			region_no = 10

	else:
		print("Warning: invalid dimension")
		
	parameter.set_region_no(region_no)

	return region_no



def obtain_data(parameter, dataset, path):

	# Obtain data
	if os.path.isfile(path) or len(str(dataset)) < 3:
		data = obtain_dataset(dataset, parameter.get_subset())
	else:
		data = build_dataset(dataset)

	data_size = len(data)
	parameter.set_data_size(data_size)

	# Add some statistics of the dataset
	set_data_stat(data, parameter)


	if (len(str(dataset)) > 3 and str(dataset) == "2") or len(str(dataset)) < 3:
		x1_max = -100000000000.0
		x1_min = 100000000000.0
		x2_max = -100000000000.0
		x2_min = 100000000000.0
		y_max = -100000000000.0
		y_min = 100000000000.0
		y_average = 0
		for point in data:
			x1_max = max(point[0], x1_max)
			x1_min = min(point[0], x1_min)
			x2_max = max(point[1], x2_max)
			x2_min = min(point[1], x2_min)
			y_max = max(point[2], y_max)
			y_min = min(point[2], y_min)
			y_average += point[2]
		if len(data) != 0:
			y_average = y_average/len(data)

		print("Dataset:", dataset, ", #points", len(data))
		print("x1:", x1_min, "to", x1_max)
		print("x2:", x2_min, "to", x2_max)
		print("y:", y_min, "to", y_max)
		print("y average:", y_average)
		print("")

	return data, data_size


def main_init(parameter, n, grid_type):
	""" Initialise the TPSFEM program """

	# Create a folder to store results 
	if parameter.get_write():
		print(parameter.get_folder())
		create_folder("./" + parameter.get_folder())

	# Parameters
	d = parameter.get_dim()
	dataset = parameter.get_dataset()
	region_no = parameter.get_region_no()

	# Adjust size of L shape domain
	if str(abs(dataset)) == '12' and grid_type != 1:
		parameter.set_uni_refine(2)

	# Create a an initial FEM n*n grid
	grid = build_initial_grid(parameter, n, grid_type)

#	plot_fem_grid_3D(grid)
#	plot_fem_grid_3D_line(grid)

	# Obtain data
	path = "dataset/"+str(dataset)+".csv"
	data, data_size = obtain_data(parameter, dataset, path)

	# Set number of data subdivision regions
	region_no = set_region_no(parameter, d, dataset, data_size)

	# Build a data region
	region = build_region(data, d, region_no, grid)

#	tpsfemPlot1D.plot_tpsfem_solution_1D(grid, region, parameter)
#	regionPlot.plot_region(region)

	initialise_file(parameter)

	return grid, region