#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-05-08 00:09:05
@Last Modified time: 2022-10-09 23:26:42

This class contains 1D routines for the norm-based error indicator.

"""

# Import libraries
from grid.NodeTable import node_iterator
from grid.EdgeTable import endpt_iterator
from parameter.Definition import BoundaryType
from indicator.triangle.EINorm2D import set_average_entry, set_absolute_parameters
from indicator.EINormPlot import plot_Dx_approximation_1D, plot_Dxx_approximation_1D


def initialise_node_parameters(local_grid):
	""" Initialise parameters in each node """

	for node in node_iterator(local_grid):

		# The parameters store Dx, Dxx
		node.set_parameters([[],[]])


def approximate_local_Dxx_1D(local_grid, parameter, Dx_source):
	""" Approximate Dxx values for each interior nodes """

	# Initialise the parameters variable to store Dx,Dy 
	# approximations from different intervals
	initialise_node_parameters(local_grid)

	# Approximate gx using c values
	if Dx_source == 1:

		# Iterate through each edge
		for node in node_iterator(local_grid):
			node_id = node.get_node_id()
			for endpt in endpt_iterator(local_grid, node_id):
				if node_id > endpt:
					end_node = local_grid.get_node_ref(endpt)

					Dx = (end_node.get_value()-node.get_value())/(end_node.get_coord()[0]-node.get_coord()[0])

					node.add_parameters_entry(Dx, 0)
					end_node.add_parameters_entry(Dx, 0)

		# Get average of gradients for each node
		set_average_entry(local_grid, 0)

	# Approximate gx using g values
	elif Dx_source == 2:

		for node in node_iterator(local_grid):

			d_values = node.get_d_values()

			node.set_parameters_entry(d_values[0], 0)

	# Update Dx of the boundary nodes
	if parameter.get_boundary() == BoundaryType.Dirichlet:

		Dx = parameter.get_function()[1]

		for node in node_iterator(local_grid):

			if local_grid.get_slave(node.get_node_id()):
				node.set_parameters_entry(Dx(node.get_coord()), 0)


	# Approximate Dxx using gradients of each node
	if Dx_source == 1 or Dx_source == 2:

		# Iterate through each edge
		for node in node_iterator(local_grid):
			node_id = node.get_node_id()
			for endpt in endpt_iterator(local_grid, node_id):
				if node_id > endpt:
					end_node = local_grid.get_node_ref(endpt)

					Dxx = (end_node.get_parameters_entry(0)-node.get_parameters_entry(0))/(end_node.get_coord()[0]-node.get_coord()[0])

					node.add_parameters_entry(Dxx, 1)
					end_node.add_parameters_entry(Dxx, 1)

		# Get average of gradients for each node
		set_average_entry(local_grid, 1)


	# Using w values
	elif Dx_source == 3:

		# Iterate through each node
		for node in node_iterator(local_grid):

			# Set w/alpha as double derivatives
			dxx_abs = abs(node.get_d_values()[1])
			
			node.set_parameters_entry(dxx_abs, 1)

	# Update Dxx of boundary nodes
	if parameter.get_boundary() == BoundaryType.Dirichlet:

		Dx = parameter.get_function()[2]

		for node in node_iterator(local_grid):
			if local_grid.get_slave(node.get_node_id()):
				node.set_parameters_entry(Dx(node.get_coord()), 1)

#	plot_Dx_approximation_1D(local_grid, parameter)
	plot_Dxx_approximation_1D(local_grid, parameter)


def calculate_Dxx_norm_1D(local_grid, node1, node2, quad_rule):
	""" Calculate integral norm of the interval """

	node_1 = local_grid.get_node_ref(node1.get_node_id())
	node_2 = local_grid.get_node_ref(node2.get_node_id())

	h = abs(node_1.get_coord()[0] - node_2.get_coord()[0])
	
	# Calculate integral of Dxx
	int_Dxx = h*abs((node_1.get_parameters()[1] + node_2.get_parameters()[1])/2.0)	

	return int_Dxx