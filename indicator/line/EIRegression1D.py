#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-08-12 21:47:53
@Last Modified time: 2022-10-09 23:34:29

This class contains routines to measures the regression error of the error of the 
1D approximation.

"""

# Import libaries
from adaptive.line.LocalGrid1D import build_local_grid_1D
from data.ObtainData import obtain_data_grid
from adaptive.line.GridValue import cal_smoother_1d
from indicator.EISmoother import smooth_data_1D
import plot.TpsfemPlot1D as tpsfemPlot1D
from plot.DataPlot import plot_data 

from numpy import sqrt
from copy import deepcopy


def calculate_error(local_grid, point):

	return point[-1]-cal_smoother_1d(local_grid, [point[0]])


def MAE(local_grid, data):
	""" Calculate mean absolute error (MAE) """

	if len(data) == 0:
		return 0.0

	sum = 0.0

	for point in data:

		sum += abs(calculate_error(local_grid, point))

	return sum/len(data)



def RMSE(local_grid, data):
	""" Calculate root mean square error (RMSE) """

	if len(data) == 0:
		return 0.0

	sum = 0.0

	for point in data:

		sum += (calculate_error(local_grid, point))**2

	return sqrt(sum/len(data))


def MAPE(local_grid, data):
	""" Calculate mean absolute percentage error (MAPE) """

	if len(data) == 0:
		return 0.0

	sum = 0.0

	for point in data:

		sum += abs(calculate_error(local_grid, point)/point[-1])

	return sum/len(data)


def MPE(local_grid, data):
	""" Calculate mean percentage error (MPE) """

	if len(data) == 0:
		return 0.0
		
	sum = 0.0

	for point in data:

		sum += (calculate_error(local_grid, point))/point[-1]

	return abs(sum/len(data))



def MASE(local_grid, data):
	""" Calculate mean absolute scaled error (MASE) """

	if len(data) == 0:
		return 0.0
		
	sum = 0.0

	denominator = 0.0

	for i in range(1, len(data)):
		denominator += abs(data[i][-1]-data[i-1][-1])

	denominator = denominator/(len(data)-1)	

	for point in data:
		sum += abs(abs(calculate_error(local_grid, point))/denominator)

	return abs(sum/len(data))



def add_scaling(node1, node2):

	return abs(node1.get_coord()[0]-node2.get_coord()[0])


def indicate_error_regression_1D(grid, region, node1, node2, \
	parameter, no_layers, refine_choice, error_type, smoother_type):
	""" Indicate error of the 1D grid """

	# Number of uniform refinement on the local grid
	uni_refine = 0

	# Build a local refined grid
	local_grid, mid_id = build_local_grid_1D(grid, region, node1, node2, \
				parameter, no_layers, refine_choice, uni_refine)

	# Obtain data in the local grid
	data = obtain_data_grid(region, local_grid)

	if len(data) < 2:
		return 0.0

	# Reduce effects of noise
	data = smooth_data_1D(data, smoother_type)

	indicator = 0

	# 1-Mean absolute error (MAE)
	if error_type == 1:
		indicator = MAE(local_grid, data)

	# 2-Root mean square error (RMSE)
	elif error_type == 2:
		indicator = RMSE(local_grid, data)

	# 3-Mean absolute percentage error (MAPE)
	elif error_type == 3:
		indicator = MAPE(local_grid, data)

	# 4-Mean percentage error (MPE)
	elif error_type == 4:
		indicator = MPE(local_grid, data)

	# 5-Mean absolute scaled error (MASE)
	elif error_type == 5:
		indicator = MASE(local_grid, data)

#	indicator *= 1.0*add_scaling(node1, node2)

	return indicator
	