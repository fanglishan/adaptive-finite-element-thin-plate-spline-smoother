#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-05-05 21:47:53
@Last Modified time: 2022-10-09 23:34:46

This class contains routines to build a 2D local grid with refinement at the base edge. 
The local TPSFEM will be solved and the energy norm of the difference between the improved 
approximation and original interpolation will be calculated.

"""

# Import libaries
from grid.NodeTable import node_iterator
from grid.EdgeTable import endpt_iterator
from adaptive.line.SetPolynomial import get_solution_polynomial_linear_1D
from adaptive.line.IntervalIntegrateQuadratic import quadratic_integrate
from adaptive.line.LocalGrid1D import build_local_grid_1D
from tpsfem.TpsfemSolver1D import solve_tpsfem_1D
from data.CheckData import check_data
import plot.TpsfemPlot1D as tpsfemPlot1D
import plot.EIAuxiliaryPlot as auxiliaryPlot

from numpy import sqrt
from copy import deepcopy


def calc_error_norm(local_grid, interval_list, node_dict, form):
	""" Calculate norms in the refined triangle pair """

	error_norm = 0.0

	for interval in interval_list:
		

		node1 = local_grid.get_node_ref(interval[0])
		node2 = local_grid.get_node_ref(interval[1])

		error1 = node_dict[str(interval[0])]-node1.get_value()
		error2 = node_dict[str(interval[1])]-node2.get_value()

		poly = get_solution_polynomial_linear_1D(node1, node2, error1, error2)

		if form == 1:
			error_norm += quadratic_integrate(poly, poly, node1, node2)

		elif form == 2:
			error_norm += abs(node1.get_coord()[0]-node2.get_coord()[0])*(poly.get_x()**2)

		# ==========May also try L max norm =============
	
	error_norm = sqrt(error_norm)
	
	return error_norm


def create_node_value_dict(local_grid):
	""" Record the value of each node """

	# Create a dictionary
	node_dict = {"key":0}

	# Use node id as keys to find values
	for node in node_iterator(local_grid):
		node_dict[str(node.get_node_id())] = node.get_value()
		
	return node_dict



def find_intervals_inside(local_grid, node1, node2):
	""" Find intervals between node 1 and node 2 """
	
	interval_list = []

	node1_coord = node1.get_coord()[0]
	node2_coord = node2.get_coord()[0]

	if node1_coord > node2_coord:
		temp = node1_coord
		node1_coord = node2_coord
		node2_coord = temp

	node1_coord -= 0.000000000001
	node2_coord += 0.000000000001

	for node in node_iterator(local_grid):
		node_id = node.get_node_id()

		coord1 = node.get_coord()[0]

		for endpt in endpt_iterator(local_grid, node_id):
			if node_id > endpt:
				end_node = local_grid.get_node_ref(endpt)

				coord2 = end_node.get_coord()[0]

				if coord1 >= node1_coord and coord2 >= node1_coord and \
					coord1 <= node2_coord and coord2 <= node2_coord:
					interval_list.append([node_id, endpt])

	return interval_list



def solve_local_tpsfem_1D(grid, region, node1, node2, parameter, \
	boundary, no_layers, refine_choice, uni_refine, form, min_data):
	""" Solve a local TPSFEM to obtain a better approximation """

	# Set boundary for local TPSFEM problem
	parameter.set_boundary(boundary)

	# Set error indicator as 0 when no data inside the element
	# This will stop error indicators from refining this region
	data_number = check_data(region, [node1, node2])
	if data_number < min_data:
		return 0

	# Build a local refined grid
	local_grid, mid_id = build_local_grid_1D(grid, region, node1, node2, \
				parameter, no_layers, refine_choice, uni_refine)

	# Record original values
	node1_copy = deepcopy(node1)
	node2_copy = deepcopy(node2)
	mid_node_copy = deepcopy(local_grid.get_node_ref(mid_id))

#	if_plot = True

#	tpsfemPlot1D.plot_tpsfem_solution_1D(local_grid, region, parameter, True, 1, False, if_plot)
	
	################ Implementation ##################
	# Neumann boundary won't work when no data is in the local domain
	# (or when the number of data is very small, not stable)
	# either stop refining (works in some cases)
	# or use Dirichlet boundary

	node_dict = create_node_value_dict(local_grid)

	# Solve the local TPSFEM
	solve_tpsfem_1D(local_grid, region, parameter)

	interval_list = find_intervals_inside(local_grid, node1, node2)

#	tpsfemPlot1D.plot_tpsfem_solution_1D(local_grid, region, parameter)
#	plot_local_tpsfem_1D(local_grid, region, parameter, grid, mid_id, node1, node2)
#	tpsfemPlot1D.plot_tpsfem_solution_1D(local_grid, region, parameter, True, 1, False, if_plot)


#	if parameter.get_marker() == 14:
#	auxiliaryPlot.save_auxiliary_approx_1D(local_grid, region, parameter, node_dict, grid)

	# calculate error norm
	error_norm = calc_error_norm(local_grid, interval_list, node_dict, form)

	# Restore original triangle values
	node1.set_value(node1_copy.get_value())
	node1.set_d_values(node1_copy.get_d_values())
	node2.set_value(node2_copy.get_value())
	node2.set_d_values(node2_copy.get_d_values())

	return error_norm