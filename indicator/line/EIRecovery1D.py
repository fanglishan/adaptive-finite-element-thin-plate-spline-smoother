#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-05-13 21:41:28
@Last Modified time: 2022-10-09 23:34:16

This class contains the routines related to the recovery-based error indicator for 1D problems.

"""

# Import libraries
from grid.EdgeTable import endpt_iterator
from grid.NodeTable import node_iterator
from adaptive.line.SetPolynomial import set_polynomial_linear_1D, get_solution_polynomial_linear_1D
from adaptive.line.IntervalIntegrateLinear import linear_integrate, linear_integrate_single
from adaptive.line.IntervalIntegrateQuadratic import quadratic_integrate, quadratic_integrate_single
from tpsfem.BuildBasis import check_dict
from tpsfem.BuildBasis import obtain_node_index_1D
from indicator.EIRecoveryPlot import plot_processed_gradient, plot_tpsfem_approximation
from parameter.Definition import BoundaryType

from numpy import zeros
from numpy.linalg import norm, solve


def set_average_entry(grid, index):
	""" Obtain and set average value of one parameter """

	# Iterate through each node
	for node in node_iterator(grid):

		# The list on given index
		dx_values = node.get_parameters()[index]
		
		# Compute the average value
		if len(dx_values) > 0:
			entry = sum(dx_values)/len(dx_values)
		# Return 0 if list is empty
		else:
			entry = 0.0

		node.set_parameters_entry(entry, index)


def initialise_gradient(grid, Dx_boundary):
	""" Initialise current gradients """

	# Approximate gx using c values
	if Dx_boundary == 1:

		# Initialise current gradient and improved gradient
		for node in node_iterator(grid):
			node.set_parameters([[],0.0])

		# Iterate through each edge
		for node in node_iterator(grid):
			node_id = node.get_node_id()
			for endpt in endpt_iterator(grid, node_id):
				if node_id > endpt:
					end_node = grid.get_node_ref(endpt)

					Dx = (end_node.get_value()-node.get_value())/(end_node.get_coord()[0]-node.get_coord()[0])

					node.add_parameters_entry(Dx, 0)
					end_node.add_parameters_entry(Dx, 0)

		# Get average of gradients for each node
		set_average_entry(grid, 0)


	# Approximate gx using g values
	elif Dx_boundary == 2:

		for node in node_iterator(grid):
			d_values = node.get_d_values()
			node.set_parameters([d_values[0], 0])




def build_system(grid, parameter, node_list, Dx_approx):
	""" Build the system of equations """

	no_nodes = len(node_list)

	# Initialise a matrix and a vector
	matrix = zeros([no_nodes, no_nodes])
	vector = zeros([no_nodes, 1])

	# Iterate through each edge once
	for node1 in node_iterator(grid):
		node_id1 = node1.get_node_id()
		for node_id2 in endpt_iterator(grid, node_id1):
			if node_id1 > node_id2:
				node2 = grid.get_node_ref(node_id2)

				# Obtain index of the nodes
				index1, index2 = obtain_node_index_1D(node_list, node1, node2)

				if index1 == -1 and index2 == -1:
					continue

				# Compute polynomial of two nodes
				poly1 = set_polynomial_linear_1D(node1, node2)
				poly2 = set_polynomial_linear_1D(node2, node1)

				# Obtain current gradients as Dirichlet boundaries
				g1 = node1.get_parameters_entry(0)
				g2 = node2.get_parameters_entry(0)

				# Calculate matrix entries
				stiffness1 = quadratic_integrate(poly1, poly1, node1, node2)
				stiffness2 = quadratic_integrate(poly1, poly2, node1, node2)
				stiffness3 = quadratic_integrate(poly2, poly2, node1, node2)

				# Assign entries and add boundary conditions
				if index1 != -1:
					matrix[index1][index1] += stiffness1
				if index2 != -1:
					matrix[index2][index2] += stiffness3

				if index1 != -1 and index2 != -1:
					matrix[index1][index2] += stiffness2
					matrix[index2][index1] += stiffness2
				elif index1 != -1:
					vector[index1] -= stiffness2*g2
				elif index2 != -1:
					vector[index2] -= stiffness2*g1

				# Compute vector entries

				# Approximate gx using c values
				if Dx_approx == 1:

					# Gradient of the current TPSFEM
					g = (node1.get_value()-node2.get_value())/\
						 (node1.get_coord()[0]-node2.get_coord()[0])

					if index1 != -1:
						vector[index1,0] += g*linear_integrate_single(poly1, node1, node2)

					if index2 != -1:
						vector[index2,0] += g*linear_integrate_single(poly2, node1, node2)

				# Approximate gx using g values
				elif Dx_approx == 2:

					gradient_poly = get_solution_polynomial_linear_1D(node1, node2, g1, g2)

					if index1 != -1:
						vector[index1,0] += quadratic_integrate(poly1, gradient_poly, node1, node2)

					if index2 != -1:
						vector[index2,0] += quadratic_integrate(poly2, gradient_poly, node1, node2)

	return matrix, vector


def build_node_list(grid, boundary):
	""" Build a list of nodes """

	# Initialise a list of nodes
	node_list = []

	# Dictionary of nodes
	node_dict = {}

	# Iterate through nodes in the grid
	for node in node_iterator(grid):

		# Check if the node is interior
		if not check_dict(node_dict,node.get_node_id()):

			# If endpoint is interior or if the boundary is Neumann
			if not grid.get_slave(node.get_node_id()) or boundary == BoundaryType.Neumann:

				# Add the node to the dictionary
				node_dict[str(node.get_node_id())] = 0

				# Add the node to the list
				node_list.append(grid.get_node_ref(node.get_node_id()))

	return node_list


def process_gradient(grid, parameter, boundary, Dx_approx):
	""" Post-process gradient to improve them """

	# Build a list of interior nodes
	node_list = build_node_list(grid, boundary)

	# Build a system of equations
	matrix, vector = build_system(grid, parameter, node_list, Dx_approx)

	# Solve the system using sparse solver
	tpsfem_solution = solve(matrix, vector)

	# Save improved gradients
	for i in range(0, len(node_list)):
		node_list[i].set_parameters_entry(tpsfem_solution[i,0], 1)

	# Set boundary values for the "improved" gradients
	if boundary == BoundaryType.Dirichlet:
		for node in node_iterator(grid):
			if grid.get_slave(node.get_node_id()):
				node.set_parameters_entry(node.get_parameters_entry(0), 1)


def estimate_error(grid, original_node1, original_node2):
	""" Estimate error for 1D elements in the interval """

	# Get two nodes in the local grid
	node1 = grid.get_node_ref(original_node1.get_node_id())
	node2 = grid.get_node_ref(original_node2.get_node_id())

	# Gradient of the current interval
	gradient = (node2.get_value()-node1.get_value())/(node2.get_coord()[0]-node1.get_coord()[0])

	# Polynomial of improved gradient minus current gradient
	poly = get_solution_polynomial_linear_1D(node1, node2, \
					node1.get_parameters_entry(1)-gradient, \
					node2.get_parameters_entry(1)-gradient)
	
	# A quadratic polynomial
	quad_poly = poly*poly

	# Compute the error estimate
	error_estimate = quadratic_integrate_single(quad_poly, node1, node2)

	return error_estimate


def recovery_based_1D(grid, node1, node2, parameter, boundary, \
					Dx_boundary, Dx_approx, improve_choice):
	""" Estimate error by recovering gradients """

	if improve_choice == 1:
		# Initialise current gradients
		initialise_gradient(grid, Dx_boundary)

		# Postprocess gradients
		process_gradient(grid, parameter, boundary, Dx_approx)
	
	elif improve_choice == 2 or improve_choice == 3:

		if improve_choice == 2:
			Dx_boundary = 1

		elif improve_choice == 3:
			Dx_boundary = 2
		else:
			print("Warning: indicator/EIRecovery1D - unknown parameter")

		# Initialise current gradients
		initialise_gradient(grid, Dx_boundary)

		for node in node_iterator(grid):
			node.set_parameters_entry(node.get_parameters_entry(0),1)

	else:
		print("Warning: indicator/EIRecovery1D - unknown parameter")

	# Estimate error
	error_estimate = estimate_error(grid, node1, node2)

	
	# Plots for articles
#	plot_processed_gradient(grid, parameter)
#	plot_tpsfem_approximation(grid, parameter)

	return error_estimate