#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-05-05 21:47:53
@Last Modified time: 2022-10-09 23:34:40

This class contains routines to build a 2D local grid with refinement at the base edge. 
The local TPSFEM will be solved and the energy norm of the difference between the improved 
approximation and original interpolation will be calculated.

"""

# Import libaries
from grid.EdgeTable import endpt_iterator
from adaptive.line.BuildEquationLinear import build_equation_linear_1D
from adaptive.line.SetPolynomial import get_solution_polynomial_linear_1D
from adaptive.line.IntervalIntegrateQuadratic import quadratic_integrate
from adaptive.line.LocalGrid1D import build_local_grid_1D
from tpsfem.TpsfemSolver1D import obtain_system_equations
from tpsfem.BuildBasis import build_node_list
from tpsfem.AddEntry1D import add_alpha_L, add_uniqueness, add_boundary_effects
import plot.TpsfemPlot1D as tpsfemPlot1D

from numpy import sqrt, zeros
from copy import deepcopy



def calculate_residual(grid, region, node1, node2, parameter, \
					boundary, no_layers, refine_choice):
	""" Calculate residual of the interval """

	# Parameters
	alpha = parameter.get_alpha()

	# Number of uniform refinement
	uni_refine = 0

	# Set boundary for local TPSFEM problem
	parameter.set_boundary(boundary)

	# Build a local refined grid
	local_grid, mid_id = build_local_grid_1D(grid, region, node1, node2, \
						parameter, no_layers, refine_choice, uni_refine)

	# Record original values
	node1_copy = deepcopy(node1)
	node2_copy = deepcopy(node2)
	mid_node_copy = deepcopy(local_grid.get_node_ref(mid_id))

#	tpsfemPlot1D.plot_tpsfem_solution_1D(local_grid, region, parameter, True, 1, False, True)

	# Build equation for the refined intervals
	build_equation_linear_1D(local_grid, region, parameter)

	# Build a list of nodes and build local system
	node_list = build_node_list(local_grid, boundary)
	matrix, vector, u_vector = obtain_system_equations(local_grid, parameter, node_list, False)

	# Add L entries multiplied with alpha
	add_alpha_L(local_grid, node_list, matrix, vector, parameter)

	# Add effects from boundary conditions
	add_boundary_effects(local_grid, node_list, vector, parameter)

	# Construct current approximation
	no_nodes = len(node_list)
	approx = zeros([3*no_nodes, 1])

	for i in range(0, no_nodes):
		approx[i,0] += node_list[i].get_value()
		d_values = node_list[i].get_d_values()
		approx[i+no_nodes,0] += d_values[0]
		approx[i+2*no_nodes,0] += d_values[1]*alpha

	# Calculate residual for nodes
	vector_residual = matrix*approx - vector

	# Obtain residual of each node
	node_id1 = node1.get_node_id()
	node_id2 = node2.get_node_id()
	index1 = -1
	index2 = -1 
	index3 = -1
	for i in range(0, len(node_list)):
		if node_list[i].get_node_id() == node_id1:
			index1 = i
		if node_list[i].get_node_id() == node_id2:
			index2 = i
		if node_list[i].get_node_id() == mid_id:
			index3 = i
	
	# Assign residual to each node
	# Boundary node has 0 residual
	node1_res = 0.0
	node2_res = 0.0
	node3_res = 0.0
	if index1 != -1:
		node1_res = vector_residual[index1,0]
	if index2 != -1:
		node2_res = vector_residual[index2,0]
	if index3 != -1:
		node3_res = vector_residual[index3,0]

	# Build surface using residual values
	poly1 = get_solution_polynomial_linear_1D(node1_copy, mid_node_copy, node1_res, node3_res)
	poly2 = get_solution_polynomial_linear_1D(node2_copy, mid_node_copy, node2_res, node3_res)

	# ||R||_{L2}^{2}
	R_norm = 0.0
	
	# Calculate residual norm for each interval
	R_norm += quadratic_integrate(poly1, poly1, node1_copy, mid_node_copy)
	R_norm += quadratic_integrate(poly2, poly2, node2_copy, mid_node_copy)

	# Restore original node values (just in case)
	node1.set_value(node1_copy.get_value())
	node1.set_d_values(node1_copy.get_d_values())
	node2.set_value(node2_copy.get_value())
	node2.set_d_values(node2_copy.get_d_values())

	return R_norm


def calculate_jump(grid, node1, node2):
	""" Calculate the jump term """

	# Node IDs of two nodes
	node_id1 = node1.get_node_id()
	node_id2 = node2.get_node_id()

	# Gradient and length of node 1 to node 2
	Dx = (node1.get_value()-node2.get_value())/(node1.get_coord()[0]-node2.get_coord()[0])
#	length = abs(node1.get_coord()[0]-node2.get_coord()[0])

	# Gradient and length of node 1 and node2 to the other node
	Dx1 = 0.0
	Dx2 = 0.0
#	length1 = 0.0
#	length2 = 0.0

	for endpt in endpt_iterator(grid, node_id1):
		if endpt != node_id2:
			end_node = grid.get_node_ref(endpt)
			Dx1 = (end_node.get_value()-node1.get_value())/(end_node.get_coord()[0]-node1.get_coord()[0])
#			length1 = abs(end_node.get_coord()[0]-node1.get_coord()[0])

	for endpt in endpt_iterator(grid, node_id2):
		if endpt != node_id1:
			end_node = grid.get_node_ref(endpt)
			Dx2 = (end_node.get_value()-node2.get_value())/(end_node.get_coord()[0]-node2.get_coord()[0])
#			length2 = abs(end_node.get_coord()[0]-node2.get_coord()[0])

	# Calculate J norm
	J_norm = 0.0

	if Dx1 == 0.0:
		J_norm += abs(Dx-Dx1)**2
	if Dx2 == 0.0:
		J_norm += abs(Dx-Dx2)**2

	return J_norm



def estimate_error_1D(grid, region, node1, node2, parameter, \
					boundary, no_layers, refine_choice, c1, c2):
	""" Solve a local TPSFEM to obtain a better approximation """

	R_norm = calculate_residual(grid, region, node1, node2, parameter, \
					boundary, no_layers, refine_choice)

	J_norm = calculate_jump(grid, node1, node2)
	h_K = abs(node1.get_coord()[0]-node2.get_coord()[0])

	error_norm = h_K**2*c1*R_norm + h_K*c2*J_norm
#	error_norm = h_K**2*c1*R_norm
#	error_norm = h_K*c2*J_norm

	error_norm = sqrt(error_norm)

	return error_norm