#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-08-12 21:47:53
@Last Modified time: 2022-10-09 23:34:01

This class contains routines to indicate errors using GCV.

"""

# Import libaries
from grid.Edge import DomainSet, RefineSet
from adaptive.line.BuildEquationLinear import build_equation_linear_1D
from adaptive.line.LocalGrid1D import build_local_grid_1D
from gcv.GCV1D import prep
from plot.DataPlot import plot_data 
from data.ObtainData import obtain_data_grid
from tpsfem.TpsfemSolver1D import build_node_list
import plot.TpsfemPlot1D as tpsfemPlot1D
#from GCV.FunctionV1D import V, V_approx

from scipy.optimize import minimize_scalar
from numpy import sqrt
from copy import deepcopy





def run_GCV(grid, region, parameter, choice):
	""" Find optimal alpha for the grid """

	# Build a list of nodes
	node_list = build_node_list(grid, parameter.get_boundary())

	# Obtain required matrices
	K, KT, KTK, L, y, KTy, yTK, yTy, KTu, uTK = prep(grid, region, parameter, node_list)

	alpha = parameter.get_alpha()

	max_iteration=20

	start_bound=1e-9

	end_bound=1e-5


	if choice == 1:

		from GCV.VTemp import V

		v_value, numerator, denominator = V(alpha, K, KT, KTK, L, y)

		indicator = v_value
#		indicator = numerator
#		indicator = denominator

	elif choice == 2:

		from GCV.VTemp import V_approx

		v_value, numerator, denominator = V_approx(alpha, K, KT, KTK, L, y, KTy, yTK, yTy, KTu, uTK)

		indicator = v_value

	elif choice == 3:

		from GCV.FunctionV1D import V, V_approx

		result = minimize_scalar(V_approx, bounds=(start_bound, end_bound), method='bounded', \
			args=(K, KT, KTK, L, y, KTy, yTK, yTy, KTu, uTK), \
			options={'xatol':1e-10, 'maxiter':max_iteration})
	
		indicator = result.x

	elif choice == 4:

		from GCV.FunctionV1D import V, V_approx

		result = minimize_scalar(V, bounds=(start_bound, end_bound), method='bounded', \
			args=(K, KT, KTK, L, y), \
			options={'xatol':1e-10, 'maxiter':max_iteration})

		indicator = result.x

	return abs(indicator)


def indicate_error_gcv_1D(grid, region, node1, node2, parameter):
	""" Indicate error of the 1D grid """

	boundary = 1
	no_layers = 2
	refine_choice = 1
	uni_refine = 1
	choice = 4

	# Set boundary for local TPSFEM problem
	parameter.set_boundary(boundary)

	# Build a local refined grid
	local_grid, mid_id = build_local_grid_1D(grid, node1, node2, \
				parameter, no_layers, refine_choice, uni_refine)

	data = obtain_data_grid(region, local_grid)

	if len(data) < 1:
		return 0

	build_equation_linear_1D(local_grid, region, parameter)

	indicator = run_GCV(local_grid, region, parameter, choice)

	indicator /= abs(node1.get_coord()[0]-node2.get_coord()[0])
#	plot_data(data)


	return indicator
	