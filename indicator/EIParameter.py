# -*- coding: utf-8 -*-
"""
@Author: Fang
@Date:   2024-03-16 13:31:21
@Last Modified by:   Fang
@Last Modified time: 2024-03-16 13:36:55

This class contains parameters for the error indicators, which affect their
performance during adaptive refinement.

"""

from parameter.Definition import IndicatorType


def get_indicator_param(indicator):
	""" Set Error indicator parameters """

	# Indicator parameters
	indicator_parameter = []

	# 0-boundary type, 1-number of layers, 2-refine choice, 3-number of uniform refinement, 4-error estimate form, 5-run gcv, 6-subset data, 7-min data
	# Refine choice 2 and 3 leads to the same result
	if indicator == IndicatorType.auxiliary:
		indicator_parameter = [1,2,2,0,2, False, False,-1]
	# 0-boundary type, 1-number of layers, 2-refine choice, 3-source of Dx, 4-ways to improve Dx
	elif indicator == IndicatorType.recovery:
		indicator_parameter = [1,2,0,1,1]
	# 0-number of layers, 1-refine choice, 2-source of Dx, 
	# 3-quadrature rule
	elif indicator == IndicatorType.norm:
		indicator_parameter = [2,0,1,1]
	# 0-boundary type, 1-number of layers, 2-refine choice, 3-ratio, 4-run gcv(EIRecovery1D)
	elif indicator == IndicatorType.residual:
		indicator_parameter = [1,2,1,0.0, False]

	# 0-number of layers, 1-refine choice                 
	elif indicator == IndicatorType.GCV:              ### Depreciated
		indicator_parameter = [2, 1]

	# 0-number of layers, 1-refine choice, 2-error type, 3-smoother type
	elif indicator == IndicatorType.regression:       ### Depreciated
		indicator_parameter = [1, 0, 2, 2]

	return indicator_parameter