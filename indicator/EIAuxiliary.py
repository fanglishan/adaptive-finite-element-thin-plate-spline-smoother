#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-05-05 21:53:41
@Last Modified time: 2024-04-09 06:40:24

This error indicator approximates the error by solving auxiliaryproblems. An auxiliary 
problem is a TPSFEM smoother in a local domain, which can be a triangle pair. The local 
smoother shouldhave a better approximation after the local domain is refined with the 
same order polynomial. This error indicator works for both Dirichlet and Neumann boundary 
conditions.

"""

# Import libraries
from grid.Edge import DomainSet
from adaptive.triangle.TriangleFunction import get_mid_coord, calc_tri_pair_area, find_triangle_pairs
from parameter.Definition import BoundaryType, CostType
from indicator.line.EIAuxiliary1D import solve_local_tpsfem_1D
from indicator.triangle.EIAuxiliary2D import solve_local_tpsfem_2D
import indicator.EIWeight as EIWeight
from tpsfem.StopCriteria import stop_refinement
from fileio.WriteResult import write_line

from timeit import default_timer



def indicate_edge_error(grid, region, edge, parameter):
	""" Indicate the true error of the given edge """

	# Set parameter mode to local
	parameter.set_local(True)
	
	# Record original parameters
	origin_boundary = parameter.get_boundary()
	origin_data_size = parameter.get_data_size()
	original_alpha = parameter.get_alpha()
	original_run_gcv = parameter.get_run_gcv()
	original_error_tol = parameter.get_error_tol()

	# Parameters
	indicator_parameter = parameter.get_indicator_parameter()

	# Boundary of local TPSFEM: 1-Dirichlet, 2-Neumann
	boundary = indicator_parameter[0]

	# number of layers of the local grid
	no_layers = indicator_parameter[1]

	# Refine choice: 0-no refine, 1-refine base edge
	# 2-refine edge only and build equation in local grid (during adaptive refinement)
	# 3-refine edge only and dont build equation in local grid (during pure error indication)
	refine_choice = indicator_parameter[2]

	# Number of uniform refinement
	uni_refine = indicator_parameter[3]

	# Error indicator formulation
	# 1-energy norm of the difference between local and global approximation
	# 2-error estimate from Babuska in babuvvska1978error 
	form = indicator_parameter[4]

	# Whether to run GCV
	run_gcv = indicator_parameter[5]

	# Whether to subset data
	subset_data = indicator_parameter[6]

	# Minimum data required eligble for refinement
	min_data = indicator_parameter[7]

	# End nodes of the edge
	endpt1 = edge.get_endpt1()
	node1 = grid.get_node(endpt1)
	endpt2 = edge.get_endpt2()
	node2 = grid.get_node(endpt2)

	# Update GCV setting
	parameter.set_run_gcv(run_gcv)

	# Update error tolerance for local grid refinement
	parameter.set_error_tol(-10000000.0)

	# Set smoothing parameter
	if parameter.get_current_auxiliary_alpha() != 0:
		parameter.set_alpha(parameter.get_current_auxiliary_alpha())
	else:
		# If no alpha is found, use the default alpha
		parameter.set_alpha(1e-6) 

	# Initialise edge error
	edge_error = 0.0

	# For 1D grid
	if grid.get_dim() == 1:
		edge_error = solve_local_tpsfem_1D(grid, region, node1, node2, parameter, 
				boundary, no_layers, refine_choice, uni_refine, form, min_data)

	# For 2D grid
	elif grid.get_dim() == 2 and EIWeight.check_boundary_edge(grid, edge):
		edge_error = 0.0

	elif grid.get_dim() == 2:
		"""
		# Check if the edge is on the boundary for a Dirichlet problem
		if grid.get_location(endpt1, endpt2) == DomainSet.boundary and \
		   parameter.get_boundary() == BoundaryType.Dirichlet:

			# Use boundary function to estimate the true solution
			true_soln = parameter.get_function()[0](get_mid_coord(grid, node1, node2))
			
			# Obtain the interpolated midpoint value
			approx_value = (node1.get_value()+node2.get_value())/2.

			# Calculate error energy norm
			area = calc_tri_pair_area(grid, node1, node2, endpt1, endpt2)
			edge_error = abs(approx_value-true_soln)/3.0*area

		else:
		"""
		node3, node4 = find_triangle_pairs(grid, endpt1, endpt2)

		if stop_refinement(region, parameter, grid, node1, node2, node3, node4):
			edge_error = 0.0
		
		else:
			edge_error = solve_local_tpsfem_2D(grid, region, node1, node2, parameter, \
				boundary, no_layers, refine_choice, uni_refine, form, min_data)

		# For print error indicators
#		print parameter.get_alpha()
#		edge_error = parameter.get_alpha()

	# For unlisted dimensions
	else:
		print("Warning: invalid dimension")
	
	# Set error indicator of the edge
	grid.set_error_indicator(edge.get_endpt1(),edge.get_endpt2(),edge_error)
	grid.set_error_indicator(edge.get_endpt2(),edge.get_endpt1(),edge_error)

	# Reset parameter mode to global
	parameter.set_local(False)

	# Restore original parameters
	parameter.set_boundary(origin_boundary)
	parameter.set_data_size(origin_data_size)
	parameter.set_alpha(original_alpha)
	parameter.set_run_gcv(original_run_gcv)
	parameter.set_error_tol(original_error_tol)

	# Reset main grid counter to avoid potential error
	grid.reset_counter()

	edge_error = EIWeight.add_data_weights(grid, edge, parameter, edge_error)
	
	parameter.add_indicator_list([edge.get_endpt1(),edge.get_endpt2(),edge_error])
	
	return edge_error


def indicate_error(grid, region, base_edges, parameter):
	""" Indicate the true error of the grid """

	# Whether to plot the distribution of number of data points
	plot_data_dist = False

	# Initialise the error list for error indicators
	error_list = []

	# Cost record
	cost = parameter.get_cost()

	# Switch to a more efficient route
#	indicator_parameter = parameter.get_indicator_parameter()
#	if indicator_parameter[2] == 2 and grid.get_dim() == 2:
#		indicator_parameter[2] = 3
#	parameter.set_indicator_parameter(indicator_parameter)
	
	if plot_data_dist:
		parameter.init_stat()

	iteration = parameter.get_iteration()
	cost.add_time([], CostType.indicate)

	write_line(parameter, "number of base edges: "+str(len(base_edges)), True)
	time_list = []

	# Indicate error of base edges
	for edge in base_edges:

		t = default_timer()
		# Indicate the error of the edge
		edge_error_indicator = indicate_edge_error(grid, region, edge, parameter)
		cost.append_time(default_timer()-t, CostType.indicate, iteration)
		time_list.append(default_timer()-t)

		# Add the new error to the list
		error_list.append(edge_error_indicator)

	write_line(parameter, "error indicator average: "+str(sum(time_list)/len(time_list)), True)

	if plot_data_dist:
		stat = parameter.get_stat()
		print(stat)
		import matplotlib.pyplot as plt
		plt.hist(stat, bins=100)
		plt.show()

	# Switch it back
#	indicator_parameter = parameter.get_indicator_parameter()
#	if indicator_parameter[2] == 3 and grid.get_dim() == 2:
#		indicator_parameter[2] = 2
#	parameter.set_indicator_parameter(indicator_parameter)

	return error_list
