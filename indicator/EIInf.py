#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-04-22 23:08:37
@Last Modified time: 2022-03-29 11:17:38

This class contains the error indicator returns a large number for all 
base edges. The adaptive refinement will become uniform refinement.

"""


def indicate_edge_error(grid, region, edge, parameter):
	""" Indicate a high error of given edge """
	
	default_error = 1000000000000

	# Set error indicator of the edge
	grid.set_error_indicator(edge.get_endpt1(), edge.get_endpt2(), default_error)
	grid.set_error_indicator(edge.get_endpt2(), edge.get_endpt1(), default_error)

	return default_error


def indicate_error(grid, region, base_edges, parameter):
	""" Indicate a very high error of the grid """

	# Initialise a list for error indicators
	error_list = []

	# Indicate error of base edges
	for edge in base_edges:

		# Indicate the error of the edge
		edge_error_indicator = indicate_edge_error(grid, region, edge, parameter)

		# Add the new error to the list
		error_list.append(edge_error_indicator)


	return error_list
