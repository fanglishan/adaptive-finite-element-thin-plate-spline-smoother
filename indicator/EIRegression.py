#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-08-12 21:53:41
@Last Modified time: 2024-04-11 10:56:09

This error indicator measures the regression error of the error of the current approximation.

"""

# Import libraries
from adaptive.triangle.TriangleFunction import get_mid_coord, calc_tri_pair_area
from parameter.Definition import BoundaryType
from indicator.line.EIRegression1D import indicate_error_regression_1D
from indicator.triangle.EIRegression2D import indicate_error_regression_2D
from fileio.WriteResult import write_line

from timeit import default_timer


def indicate_edge_error(grid, region, edge, parameter):
	""" Indicate the true error of the given edge """
	
	# Set parameter mode to local
	parameter.set_local(True)

	# Parameters
	indicator_parameter = parameter.get_indicator_parameter()

	# number of layers of the local grid
	no_layers = indicator_parameter[0]

	# Refine choice: 0-no refine, 1-refine base edge
	refine_choice = indicator_parameter[1]

	# Refine choice:
	# 1-Mean absolute error (MAE)
	# 2-Root mean square error (RMSE)
	# 3-Mean absolute percentage error (MAPE)
	# 4-Mean percentage error (MPE)
	error_type = indicator_parameter[2]

	# Smoother type:
	# 1-
	smoother_type = indicator_parameter[3]

	# End nodes of the edge
	endpt1 = edge.get_endpt1()
	node1 = grid.get_node(endpt1)
	endpt2 = edge.get_endpt2()
	node2 = grid.get_node(endpt2)

	# Initialise edge error
	edge_error = 0.0

	# For 1D grid
	if grid.get_dim() == 1:
		edge_error = indicate_error_regression_1D(grid, region, node1, \
		node2, parameter, no_layers, refine_choice, error_type, smoother_type)

	# For 2D grid
	elif grid.get_dim() == 2:
		edge_error = indicate_error_regression_2D(grid, region, node1, \
		node2, parameter, no_layers, refine_choice, error_type, smoother_type)
		
	# For unlisted dimensions
	else:
		print("Warning: invalid dimension")

	# Set error indicator of the edge
	grid.set_error_indicator(edge.get_endpt1(),edge.get_endpt2(),edge_error)
	grid.set_error_indicator(edge.get_endpt2(),edge.get_endpt1(),edge_error)

	parameter.set_local(False)

	# Reset main grid counter to avoid potential error
	grid.reset_counter()

	parameter.add_indicator_list([edge.get_endpt1(),edge.get_endpt2(),edge_error])
	
	return edge_error


def indicate_error(grid, region, base_edges, parameter):
	""" Indicate the true error of the grid """

	# Initialise the error list for error indicators
	error_list = []

	write_line(parameter, "number of base edges: "+str(len(base_edges)), True)
	time_list = []

	# Indicate error of base edges
	for edge in base_edges:
		t = default_timer()
		# Indicate the error of the edge
		edge_error_indicator = indicate_edge_error(grid, region, edge, parameter)

		time_list.append(default_timer()-t)
		# Add the new error to the list
		error_list.append(edge_error_indicator)

	write_line(parameter, "error indicator average: "+str(sum(time_list)/len(time_list)), True)

	return error_list
