#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-05-05 21:53:41
@Last Modified time: 2024-04-10 21:27:31

This error indicator estimate the error using the residual of 
the elements.

"""

# Import libraries
from adaptive.triangle.TriangleFunction import get_mid_coord, calc_tri_pair_area, find_triangle_pairs
from parameter.Definition import BoundaryType
from indicator.line.EIResidual1D import estimate_error_1D
from indicator.triangle.EIResidual2D import estimate_error_2D
import indicator.EIWeight as EIWeight
from tpsfem.StopCriteria import stop_refinement
from parameter.Definition import CostType
from fileio.WriteResult import write_line

from timeit import default_timer
from numpy import pi


def indicate_edge_error(grid, region, edge, parameter):
	""" Indicate the true error of the given edge """
	
	# Set parameter mode to local
	parameter.set_local(True)
	
	# Record original parameters
	origin_boundary = parameter.get_boundary()
	origin_data_size = parameter.get_data_size()

	# Parameters
	indicator_parameter = parameter.get_indicator_parameter()
	
	# Boundary of local TPSFEM: 1-Dirichlet, 2-Neumann
	boundary = indicator_parameter[0]

	# number of layers of the local grid
	no_layers = indicator_parameter[1]

	# Refine choice: 0-no refine, 1-refine base edge
	refine_choice = indicator_parameter[2]

	# proportion of residual and jump
	c1 = 1.0/(2*pi**2)#indicator_parameter[3]
	c2 = 1.0/(2*pi**2)#1.0-c1

	# Whether to run GCV
	run_gcv = indicator_parameter[4]
	
	# End nodes of the edge
	endpt1 = edge.get_endpt1()
	node1 = grid.get_node(endpt1)
	endpt2 = edge.get_endpt2()
	node2 = grid.get_node(endpt2)

	# Initialise edge error
	edge_error = 0.0

	# For 1D grid
	if grid.get_dim() == 1:
		edge_error = estimate_error_1D(grid, region, node1, \
			node2, parameter, boundary, no_layers, refine_choice, c1, c2)

	# For 2D grid
	elif grid.get_dim() == 2 and EIWeight.check_boundary_edge(grid, edge):
		edge_error = 0.0

	elif grid.get_dim() == 2:
			
		node3, node4 = find_triangle_pairs(grid, endpt1, endpt2)

		if stop_refinement(region, parameter, grid, node1, node2, node3, node4):
			edge_error = 0.0
		
		else:
			edge_error = estimate_error_2D(grid, region, node1, \
				node2, parameter, boundary, no_layers, refine_choice, c1, c2)

	# For unlisted dimensions
	else:
		print("Warning: invalid dimension")
	

	# Set error indicator of the edge
	grid.set_error_indicator(edge.get_endpt1(),edge.get_endpt2(),edge_error)
	grid.set_error_indicator(edge.get_endpt2(),edge.get_endpt1(),edge_error)

	# Reset parameter mode to global
	parameter.set_local(False)

	# Restore original parameters
	parameter.set_boundary(origin_boundary)
	parameter.set_data_size(origin_data_size)

	# Reset main grid counter to avoid potential error
	grid.reset_counter()

	edge_error = EIWeight.add_data_weights(grid, edge, parameter, edge_error)
	
	return edge_error


def indicate_error(grid, region, base_edges, parameter):
	""" Indicate the true error of the grid """

	# Initialise the error list for error indicators
	error_list = []

	# Cost record
	cost = parameter.get_cost()

	iteration = parameter.get_iteration()
	cost.add_time([], CostType.indicate)

	write_line(parameter, "number of base edges: "+str(len(base_edges)), True)
	time_list = []

	# Indicate error of base edges
	for edge in base_edges:

		t = default_timer()
		# Indicate the error of the edge
		edge_error_indicator = indicate_edge_error(grid, region, edge, parameter)
		cost.append_time(default_timer()-t, CostType.indicate, iteration)
		time_list.append(default_timer()-t)

		# Add the new error to the list
		error_list.append(edge_error_indicator)

	write_line(parameter, "error indicator average: "+str(sum(time_list)/len(time_list)), True)


	return error_list
