#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-05-26 15:26:29
@Last Modified time: 2022-10-09 23:37:47

This class contains routines to plot outcomes of the norm-based error indicator.

"""

# Import libraries
from grid.NodeTable import node_iterator
from grid.EdgeTable import endpt_iterator
from adaptive.line.SetPolynomial import get_solution_polynomial_linear_1D

import matplotlib.pyplot as plt
import numpy as np


def plot_Dx_approximation_1D(grid, parameter):


	# Initialisea a figure
	fig = plt.figure("TPSFEM_" + str(parameter.get_dataset()) + "_norm_1D")	

	ux = parameter.get_function()[1]

	xi = np.linspace(0.0,1.0,100).tolist()
	yi = []
	for i in range(len(xi)):
		yi.append(ux([xi[i]]))

	plt.plot(xi, yi,"k")
	plt.plot(xi, yi, "k", label="true derivative")

	# Iterate through each edge once
	for node in node_iterator(grid):
		node_id = node.get_node_id()
		for endpt in endpt_iterator(grid, node_id):
			if node_id > endpt:
				end_node = grid.get_node_ref(endpt)

				soln_poly = get_solution_polynomial_linear_1D(node, \
					end_node, node.get_value(), end_node.get_value())
				gx = soln_poly.get_x()

				x = [node.get_coord()[0],end_node.get_coord()[0]]
				y = [gx, gx]
				plt.plot(x, y,"b--")
	plt.plot(x, y,"b--", label="solution derivative")

	# Loop through each edge
	for node in node_iterator(grid):
		node_id = node.get_node_id()
		for endpt in endpt_iterator(grid, node.get_node_id()):
			if node_id > endpt:
				end_node = grid.get_node(endpt)

				# Create a list of x coordinates of two nodes
				x = [node.get_coord()[0],grid.get_coord(endpt)[0]]
				y = [node.get_parameters_entry(0),end_node.get_parameters_entry(0)]
				
				# Plot the line between two nodes
				plt.plot(x,y,"g-.")
	plt.plot(x,y,"g-.", label="Dx approximation",linewidth=3.0)

	for node in node_iterator(grid):
#		plt.plot(node.get_coord()[0],node.get_value(),"ro")
		plt.plot(node.get_coord()[0],node.get_parameters_entry(0),"g.")

	# Plot nodes
	for node in node_iterator(grid):
#		plt.plot(node.get_coord()[0],node.get_value(),"ro")
		plt.plot(node.get_coord()[0],0,"wo")
	plt.plot(node.get_coord()[0],0,"wo", label="nodes")

	plt.legend(loc="upper right")
	plt.xlabel("x")
	plt.ylabel("Dx")
	plt.show()


def plot_Dxx_approximation_1D(grid, parameter):
	""" Plot current approximation of Dxx """

	# Initialisea a figure
	fig = plt.figure("TPSFEM_" + str(parameter.get_dataset()) + "_norm_1D")	

	ux = parameter.get_function()[2]

	xi = np.linspace(0.0,1.0,100).tolist()
	yi = []
	for i in range(len(xi)):
		yi.append(ux([xi[i]]))

	plt.plot(xi, yi,"k")
	plt.plot(xi, yi, "k", label="true second derivative")

	# Iterate through each edge once
	for node in node_iterator(grid):
		node_id = node.get_node_id()
		for endpt in endpt_iterator(grid, node_id):
			if node_id > endpt:
				end_node = grid.get_node_ref(endpt)

				soln_poly = get_solution_polynomial_linear_1D(node, \
					end_node, node.get_parameters_entry(0), end_node.get_parameters_entry(0))
				gxx = soln_poly.get_x()

				x = [node.get_coord()[0],end_node.get_coord()[0]]
				y = [gxx, gxx]
				plt.plot(x, y,"b--")
	plt.plot(x, y,"b--", label="solution second derivative")

	# Loop through each edge
	for node in node_iterator(grid):
		node_id = node.get_node_id()
		for endpt in endpt_iterator(grid, node.get_node_id()):
			if node_id > endpt:
				end_node = grid.get_node(endpt)

				# Create a list of x coordinates of two nodes
				x = [node.get_coord()[0],grid.get_coord(endpt)[0]]
				y = [node.get_parameters_entry(1),end_node.get_parameters_entry(1)]
				
				# Plot the line between two nodes
				plt.plot(x,y,"g-.",linewidth=3.0)
	plt.plot(x,y,"g-.", label="Dxx approximation")

	for node in node_iterator(grid):
#		plt.plot(node.get_coord()[0],node.get_value(),"ro")
		plt.plot(node.get_coord()[0],node.get_parameters_entry(1),"g.")

	# Plot nodes
	for node in node_iterator(grid):
#		plt.plot(node.get_coord()[0],node.get_value(),"ro")
		plt.plot(node.get_coord()[0],0,"wo")
	plt.plot(node.get_coord()[0],0,"wo", label="nodes")

	plt.legend(loc="upper right")
	plt.xlabel("x")
	plt.ylabel("Dxx")
	plt.show()
