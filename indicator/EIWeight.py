# -*- coding: utf-8 -*-
"""
@Author: Fang
@Date:   2022-05-19 16:40:33
@Last Modified by:   Fang
@Last Modified time: 2022-12-18 16:13:47

This class contains routines to modify error indicator values according to the
location of its edge or the number of data points in the neighbouring elements.

"""


from grid.Edge import Edge, DomainSet

from numpy import sqrt, exp


def check_boundary_edge(grid, edge):
	""" Check if one edge is on the boundary """

	# Current implementation does not need to set error indicator values
	# for edges on boundary as zero as weights that are data dependent
	# will automatically reduces refinement on regions without data points
	return False


	#################################
	endpt1 = edge.get_endpt1()
	endpt2 = edge.get_endpt2()

	if edge.get_location() != grid.get_location(edge.get_endpt1(),edge.get_endpt2()):
		print("Warning: inconsistent location type")

	if edge.get_location() == DomainSet.boundary:
		return True
	else:
		return False




def scale_no_data(grid, edge, parameter, edge_error):
	""" scale error using number of data points in triangle pair """

	min_data = 1

	no_data_list = grid.get_no_data(edge.get_endpt1(),edge.get_endpt2())

	no_data = sum(no_data_list)

	return edge_error*sqrt(min_data+no_data)



def scale_A(grid, edge, parameter, edge_error):
	""" scale error using A matrix entry value of two nodes """

	endpt1 = edge.get_endpt1()
	endpt2 = edge.get_endpt2()

	A_ave = (grid.get_matrix_A_value(endpt1, endpt1) + grid.get_matrix_A_value(endpt2, endpt2))/2.0

	coord1 = grid.get_coord(endpt1)
	coord2 = grid.get_coord(endpt2)
	dist = sqrt((coord1[0]-coord2[0])**2+(coord1[1]-coord2[1])**2)

	return edge_error*A_ave/dist



def scale_d_alpha(grid, edge, parameter, edge_error):
	""" scale error using d_X and alpha directly """

	from data.CalculateD import calculate_d

	endpt1 = edge.get_endpt1()
	endpt2 = edge.get_endpt2()

	# Calculate d explicitly

	# OR approximate it (assume uniform data)
	d = calculate_d(grid, endpt1, endpt2, parameter)

	alpha = parameter.get_alpha()
#	print("d^4:", d**4, ", alpha:", alpha)
	return edge_error/(d**4+alpha)


def sigmoid(x):
	return 1.0/(1.0+exp(-x))


def scale_active(grid, edge, parameter, edge_error):
	""" scale error using an activation function  """

	from data.CalculateD import calculate_d

	endpt1 = edge.get_endpt1()
	endpt2 = edge.get_endpt2()

	alpha = parameter.get_alpha()

	d = calculate_d(grid, endpt1, endpt2, parameter)

	coord1 = grid.get_coord(endpt1)
	coord2 = grid.get_coord(endpt2)
	h = sqrt((coord1[0]-coord2[0])**2 + (coord1[1]-coord2[1])**2)

	# Only apply weights if h is smaller than d_X
	if h < d:
		return edge_error
	else:
#		print(h/d, "weight:", sigmoid(h/d))
		return edge_error*sigmoid(h/d)


def add_data_weights(grid, edge, parameter, edge_error):
	""" Add weights to error indicator values using data info """

	weight = parameter.get_weight()

	# 0 weight means no weight

	if weight == 1:
		edge_error = scale_no_data(grid, edge, parameter, edge_error)
	elif weight == 2:
		edge_error = scale_A(grid, edge, parameter, edge_error)
	elif weight == 3:
		edge_error = scale_d_alpha(grid, edge, parameter, edge_error)
	elif weight == 4:
		edge_error = scale_active(grid, edge, parameter, edge_error)

	return edge_error

#	return edge_error
#	return edge_error*sqrt(min_data+no_data)
#	return edge_error*A_ave
#	return edge_error*A_ave/dist

#	return A_ave
#	return sqrt(min_data+no_data)
#	return edge_error*no_data
#	return edge_error*sqrt(no_data)
