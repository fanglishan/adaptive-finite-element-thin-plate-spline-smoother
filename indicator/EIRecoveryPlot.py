#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-05-14 00:01:11
@Last Modified time: 2022-10-09 23:37:53

This class contains routines to plot outcomes of the recovery-based error indicator.

"""

# Import libraries
from grid.NodeTable import node_iterator
from grid.EdgeTable import endpt_iterator
from adaptive.line.SetPolynomial import get_solution_polynomial_linear_1D

import matplotlib.pyplot as plt
import numpy as np


def plot_processed_gradient(grid, parameter):
	""" Plot processed gradient vs approximated gradient """

	# Initialisea a figure
	fig = plt.figure("TPSFEM_" + str(parameter.get_dataset()) + "_recovery_1D")

	# Plot piecewise constant gradients
	for node in node_iterator(grid):
		node_id = node.get_node_id()
		for endpt in endpt_iterator(grid, node_id):
			if node_id > endpt:
				end_node = grid.get_node_ref(endpt)

				soln_poly = get_solution_polynomial_linear_1D(node, \
					end_node, node.get_value(), end_node.get_value())
				gx = soln_poly.get_x()

				x = [node.get_coord()[0],end_node.get_coord()[0]]
				y = [gx, gx]
				plt.plot(x, y,"k")
	
	# Plot true gradient
	ux = parameter.get_function()[1]

	xi = np.linspace(0.0,1.0,100).tolist()
	yi = []
	for i in range(len(xi)):
		yi.append(ux([xi[i]]))

	plt.plot(xi, yi,"b:")	

	# Plot averaged gradients
	for node in node_iterator(grid):
		node_id = node.get_node_id()
		for endpt in endpt_iterator(grid, node_id):
			if node_id > endpt:
				end_node = grid.get_node_ref(endpt)
				x = [node.get_coord()[0],end_node.get_coord()[0]]
#				y = [node.get_d_values()[0], end_node.get_d_values()[0]]
				y = [node.get_parameters_entry(0), end_node.get_parameters_entry(0)]
				plt.plot(x, y,"g--")				

	# Plot g values
	for node in node_iterator(grid):	

		plt.plot(node.get_coord(), node.get_d_values()[0], "y^")
	
	# Plot improved gradients
	for node in node_iterator(grid):
		node_id = node.get_node_id()
		for endpt in endpt_iterator(grid, node_id):
			if node_id > endpt:
				end_node = grid.get_node_ref(endpt)

				x = [node.get_coord()[0],end_node.get_coord()[0]]
				y = [node.get_parameters_entry(1), end_node.get_parameters_entry(1)]
#				plt.plot(x, y,"r-")				


	for node in node_iterator(grid):	

		plt.plot(node.get_coord(), node.get_parameters_entry(1), "r*")
#		print(node.get_coord()[0], grid.get_slave(node.get_node_id()), node.get_parameters_entry(0), node.get_parameters_entry(1))

	plt.show()



def plot_processed_gradient(grid, parameter):
	""" Plot processed gradient vs approximated gradient """

	# Initialisea a figure
	fig = plt.figure("TPSFEM_" + str(parameter.get_dataset()) + "_recovery_1D")

	# Plot piecewise constant gradients
	for node in node_iterator(grid):
		node_id = node.get_node_id()
		for endpt in endpt_iterator(grid, node_id):
			if node_id > endpt:
				end_node = grid.get_node_ref(endpt)

				soln_poly = get_solution_polynomial_linear_1D(node, \
					end_node, node.get_value(), end_node.get_value())
				gx = soln_poly.get_x()

				x = [node.get_coord()[0],end_node.get_coord()[0]]
				y = [gx, gx]
				plt.plot(x, y,"k")
	
	# Plot true gradient
	ux = parameter.get_function()[1]

	xi = np.linspace(0.0,1.0,100).tolist()
	yi = []
	for i in range(len(xi)):
		yi.append(ux([xi[i]]))

	plt.plot(xi, yi,"b:")	

	# Plot averaged gradients
	for node in node_iterator(grid):
		node_id = node.get_node_id()
		for endpt in endpt_iterator(grid, node_id):
			if node_id > endpt:
				end_node = grid.get_node_ref(endpt)
				x = [node.get_coord()[0],end_node.get_coord()[0]]
#				y = [node.get_d_values()[0], end_node.get_d_values()[0]]
				y = [node.get_parameters_entry(0), end_node.get_parameters_entry(0)]
				plt.plot(x, y,"g--")				

	# Plot g values
	for node in node_iterator(grid):	

		plt.plot(node.get_coord(), node.get_d_values()[0], "y^")
	
	# Plot improved gradients
	for node in node_iterator(grid):
		node_id = node.get_node_id()
		for endpt in endpt_iterator(grid, node_id):
			if node_id > endpt:
				end_node = grid.get_node_ref(endpt)

				x = [node.get_coord()[0],end_node.get_coord()[0]]
				y = [node.get_parameters_entry(1), end_node.get_parameters_entry(1)]
#				plt.plot(x, y,"r-")				


	for node in node_iterator(grid):	

		plt.plot(node.get_coord(), node.get_parameters_entry(1), "r*")
#		print(node.get_coord()[0], grid.get_slave(node.get_node_id()), node.get_parameters_entry(0), node.get_parameters_entry(1))

	plt.show()



def plot_tpsfem_approximation(grid, parameter):
	""" Plot some plots for the thesis """

	# Previous plots are made with 10 nodes in the grid with sine function

	# Parameters

	# Marker for node location
	node_marker = -0.1

	if grid.get_dim() != 1:
		return
	
	# Initialisea a figure
	fig = plt.figure("TPSFEM_solution_1D")

	# Model problem function
	true_soln = parameter.get_function()[0]

	# Plot true solution
	xi = np.linspace(0.0, 1.0, 500)
	fi = []
	for i in range(0, len(xi)):
		fi.append(true_soln([xi[i]]))
	plt.plot(xi, fi, "k", label="model problem")


#	for node in node_iterator(grid):
#		node.set_value(true_soln(node.get_coord()))

	# Loop through each edge
	for node in node_iterator(grid):
		node_id = node.get_node_id()
		for endpt in endpt_iterator(grid, node.get_node_id()):
			if node_id > endpt:
				end_node = grid.get_node(endpt)

				value1 = node.get_value()
				value2 = end_node.get_value()

				# Create a list of x coordinates of two nodes
				x = [node.get_coord()[0],grid.get_coord(endpt)[0]]
				y = [value1,value2]
			
				# Plot the line between two nodes
				plt.plot(x,y,"b--")
	plt.plot(x,y,"b--", label="TPSFEM")

	for node in node_iterator(grid):

		x = [node.get_coord()[0],node.get_coord()[0]]
		y = [node.get_value(), 0.0]
			
		# Plot the line between two nodes
		plt.plot(x,y,"b:")	

	plt.plot([0,1],[0,0],"b:")
	# Plot nodes
	for node in node_iterator(grid):
#		plt.plot(node.get_coord()[0],node.get_value(),"ro")
		plt.plot(node.get_coord()[0],grid.get_value(node.get_node_id()),"wo")
		plt.plot(node.get_coord()[0],0,"wo")

	plt.plot(node.get_coord()[0],grid.get_value(node.get_node_id()),"wo", label="nodes")
		# Plot node id near the node
#		plt.text(node.get_coord()[0]+0.02,node.get_value(),node.get_node_id(),fontsize="smaller")
		
		# Plot global node id near the node
#		plt.text(node.get_coord()[0]-0.03,node.get_value(),node.get_global_id(),fontsize="smaller")
	
	# Loop over the full nodes in the grid
#	for node in node_iterator(grid):
#		plt.plot(node.get_coord()[0],node_marker,"b+")


	plt.legend(loc="upper right")

#	plt.xlim(0.3, 0.7)
#	plt.ylim(0.8, 1.05)
	plt.xlabel("x")
	plt.ylabel("y")
	plt.show()
	

# Marker for node location
	node_marker = -0.1

	if grid.get_dim() != 1:
		return

	# Initialisea a figure
	fig = plt.figure("TPSFEM_solution_1D")
	
	# Plot shades
	"""
	trianglex = [0.333333333333, 0.222222222222, 0.333333333333, 0.333333333333] 
	triangley = [1.5, 2.0312511512668108, 2.0312511512668108, 1.5]    
	plt.fill(trianglex, triangley)
	"""

	# Model problem function
	true_soln = parameter.get_function()[1]

	# Plot true solution
	xi = np.linspace(0.0, 1.0, 500)
	fi = []
	for i in range(0, len(xi)):
		fi.append(true_soln([xi[i]]))
	plt.plot(xi, fi, "k", label="gradient")

	# Loop through each edge
	for node in node_iterator(grid):
		node_id = node.get_node_id()
		for endpt in endpt_iterator(grid, node.get_node_id()):
			if node_id > endpt:
				end_node = grid.get_node(endpt)

				Dx = (end_node.get_value()-node.get_value())/(end_node.get_coord()[0]-node.get_coord()[0])

				# Create a list of x coordinates of two nodes
				x = [node.get_coord()[0],grid.get_coord(endpt)[0]]
				y = [Dx, Dx]

				# Plot the line between two nodes
				plt.plot(x,y,"b--")
	plt.plot(x,y,"b--", label="gradient approximation")


		# Plot node id near the node
#		plt.text(node.get_coord()[0]+0.02,node.get_value(),node.get_node_id(),fontsize="smaller")
		
		# Plot global node id near the node
#		plt.text(node.get_coord()[0]-0.03,node.get_value(),node.get_global_id(),fontsize="smaller")
	
	# Loop over the full nodes in the grid
#	for node in node_iterator(grid):
#		plt.plot(node.get_coord()[0],node_marker,"b+")

#	for node in node_iterator(grid):
#		node.set_value(true_soln(node.get_coord()))

	# Loop through each edge
	for node in node_iterator(grid):
		node_id = node.get_node_id()
		for endpt in endpt_iterator(grid, node.get_node_id()):
			if node_id > endpt:
				end_node = grid.get_node(endpt)

				# Create a list of x coordinates of two nodes
				x = [node.get_coord()[0],grid.get_coord(endpt)[0]]
				y = [node.get_parameters_entry(1),end_node.get_parameters_entry(1)]
			
				# Plot the line between two nodes
				plt.plot(x,y,"r:",linewidth=2.0)
	plt.plot(x,y,"r:", label="improved gradient")


	# Initialise current gradient and improved gradient
	for node in node_iterator(grid):
		node.set_parameters([[],0.0])

	# Iterate through each edge
	for node in node_iterator(grid):
		node_id = node.get_node_id()
		for endpt in endpt_iterator(grid, node_id):
			if node_id > endpt:
				end_node = grid.get_node_ref(endpt)

				Dx = (end_node.get_value()-node.get_value())/(end_node.get_coord()[0]-node.get_coord()[0])

				node.add_parameters_entry(Dx, 0)
				end_node.add_parameters_entry(Dx, 0)
	
	for node in node_iterator(grid):
		parameters = node.get_parameters()[0]
		if len(parameters) == 2:

			x = [node.get_coord()[0],node.get_coord()[0]]
			y = [parameters[0], parameters[1]]

			# Plot the line between two nodes
			plt.plot(x,y,"b:")	

	# Plot nodes
	for node in node_iterator(grid):
#		plt.plot(node.get_coord()[0],node.get_value(),"ro")
		plt.plot(node.get_coord()[0],0,"wo")
	plt.plot(node.get_coord()[0],0,"wo", label="nodes")

	plt.legend(loc="upper right")
#	plt.xlim(0.3, 0.7)
#	plt.ylim(-3, 3)
	plt.xlabel("x")
	plt.ylabel("dy/dx")
	plt.show()