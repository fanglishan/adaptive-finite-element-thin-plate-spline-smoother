#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-08-26 15:47:25
@Last Modified time: 2022-04-09 21:17:03

This class contains routines to smooth input data.

"""


# Import libraries
from rbf.RbfSolver import rbf
from rbf.CalcRbf import rbf_calculate



def smooth_tps_1D(data):

	d = 1

	# Smoothing parameter
	alpha = 0.0001


	tps_values = rbf(data, alpha, d)

	smooth_data = []

	for data_point in data:

		value = rbf_calculate(tps_values, data, [data_point[0]], d)

		smooth_data.append([data_point[0], value])

	return smooth_data


def take_x(elem):
	return elem[0]


def smooth_data_1D(data, smoother_type):

#	coord = (data[0][0] + data[1][0])/2.0
#	value = (data[0][1] + data[1][1])/2.0
#	smooth_data.append([coord, value])

	# No smoothing
	if smoother_type == 0:
		
		# Sort data
		data.sort(key=take_x)

		return data

	# Smoothing using the TPS
	elif smoother_type == 1:
		return smooth_tps_1D(data)

	# Smoothing by averaging the values
	elif smoother_type == 2:

		data.sort(key=take_x)
#		print data
		smooth_data = []

		for i in range(1, len(data)-1):

			coord = data[i][0]
			value = (data[i-1][1]+4*data[i][1] + data[i+1][1])/6.0
			smooth_data.append([coord, value])

		return smooth_data

	# Smoothing by a moving average of four nearest neighbour
	elif smoother_type == 3:

		data.sort(key=take_x)
#		print data
		smooth_data = []

		for i in range(0, len(data)):
			coord = data[i][0]

			start_index = max(0, i-2)
			end_index = min(len(data)-1,i+2)

			value = 0.0
			for j in range(start_index, end_index+1):
				value += data[j][1]
			value = value/(end_index-start_index+1)
			
			smooth_data.append([coord, value])

		return smooth_data

	else:
		print("Warning: invalid smoother type")



def smooth_data_2D(data):

	smooth_data = []

	return smooth_data