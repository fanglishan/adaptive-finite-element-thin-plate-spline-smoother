#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-08-12 21:47:53
@Last Modified time: 2022-10-09 23:33:21

This class contains routines to 

"""

# Import libaries
from adaptive.triangle.Triangle import triangle_iterator, neighbour_triangles_iterator_ref
from adaptive.triangle.BuildEquationLinear import build_equation_linear_2D
from adaptive.triangle.LocalGrid2D import build_local_grid_2D
from adaptive.triangle.TriangleFunction import find_triangle_pairs
from tpsfem.TpsfemSolver2D import solve_tpsfem_2D, build_node_list
from parameter.Definition import BoundaryType
from data.ObtainData import obtain_data_grid
from gcv.GCV2D import GCV_2D
from gcv.FunctionV2D import V, V_approx
from gcv.GCV2D import prep
import plot.FemPlot2D as femPlot2D
import plot.TpsfemPlot2D as tpsfemPlot2D

from numpy import sqrt
from copy import deepcopy


def run_GCV(grid, region, parameter, choice):
	""" Find optimal alpha for the grid """

	# Build a list of nodes
	node_list = build_node_list(grid, parameter.get_boundary())

	# Obtain required matrices
	K, KT, KTK, L, y, KTy, yTK, yTy, KTu, uTK = prep(grid, region, parameter, node_list)

	alpha = parameter.get_alpha()

	max_iteration=5

	start_bound=1e-9

	end_bound=1e-5


	if choice == 1:

		v_value, numerator, denominator = V(alpha, K, KT, KTK, L, y)

		return -v_value

	elif choice == 2:

		v_value, numerator, denominator = V_approx(alpha, K, KT, KTK, L, y, KTy, yTK, yTy, KTu, uTK)

		return -v_value

	elif choice == 3:

		result = minimize_scalar(V_approx, bounds=(start_bound, end_bound), method='bounded', \
			args=(K, KT, KTK, L, y, KTy, yTK, yTy, KTu, uTK), \
			options={'xatol':1e-10, 'maxiter':max_iteration})
	
		return -result.x

	elif choice == 4:

		result = minimize_scalar(V, bounds=(start_bound, end_bound), method='bounded', \
			args=(K, KT, KTK, L, y), \
			options={'xatol':1e-10, 'maxiter':max_iteration})

		return -result.x


def indicate_error_gcv_2D(grid, region, node1, node2, parameter):
	""" Indicate error of the 2D grid """

	boundary = 1
	no_layers = 2
	refine_choice = 1

	choice = 1

	# Set boundary for local TPSFEM problem
	parameter.set_boundary(boundary)

	node3, node4 = find_triangle_pairs(grid, node1.get_node_id(), node2.get_node_id())

	# Build a local refined grid
	local_grid, mid_id = build_local_grid_2D(grid, node1, node2, \
				node3, node4, parameter, no_layers, refine_choice)



	data = obtain_data_grid(region, local_grid)

	if len(data) < 1:
		return 0

	build_equation_linear_2D(local_grid, region, parameter)

	indicator = run_GCV(local_grid, region, parameter, choice)


#	plot_data(data)


	return indicator