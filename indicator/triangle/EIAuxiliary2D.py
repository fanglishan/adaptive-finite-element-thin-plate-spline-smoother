#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-05-05 21:47:53
@Last Modified time: 2022-10-09 23:42:33

This class contains routines to build a 2D local grid with refinement at the base edge. 
The local TPSFEM will be solved and the energy norm of the difference between the improved 
approximation and original interpolation will be calculated.

"""

# Import libaries
from grid.NodeTable import node_iterator
from adaptive.triangle.Triangle import triangle_iterator, neighbour_triangles_iterator_ref
from adaptive.triangle.SetPolynomial import get_solution_polynomial_linear_2D, set_polynomial_linear_2D
from adaptive.triangle.TriangleIntegrateLinear import calc_area#, linear_integrate_single
from adaptive.triangle.TriangleIntegrateQuadratic import quadratic_integrate
from adaptive.triangle.LocalGrid2D import build_local_grid_2D
from adaptive.triangle.TriangleFunction import find_triangle_pairs
from tpsfem.TpsfemSolver2D import solve_tpsfem_2D
#from parameter.Definition import BoundaryType
from data.ObtainData import obtain_data_grid, obtain_data
from data.CheckData import check_data
import plot.FemPlot2D as femPlot2D
import plot.TpsfemPlot2D as tpsfemPlot2D
import plot.EIAuxiliaryPlot as auxiliaryPlot

from numpy import sqrt
from copy import deepcopy


def calc_error_norm(local_grid, tri_list, node_dict, form):
	""" Calculate norms in the refined triangle pair """

	error_norm = 0.0

	for tri in tri_list:

		node1 = local_grid.get_node_ref(tri[0])
		node2 = local_grid.get_node_ref(tri[1])
		node3 = local_grid.get_node_ref(tri[2])

		# Calculate and set error at three vertices
		error1 = node_dict[str(tri[0])]-node1.get_value()
		error2 = node_dict[str(tri[1])]-node2.get_value()
		error3 = node_dict[str(tri[2])]-node3.get_value()

		# Obtain piecewise flat surface and integrate
		poly = get_solution_polynomial_linear_2D(node1, node2, node3, error1, error2, error3)

		# Calculate the energy norm of the difference 
		if form == 1:
			error_norm += quadratic_integrate(poly, poly, node1, node2, node3)

		# Calculate the error estimate (energy norm)
		elif form == 2:
			coord1 = node1.get_coord()
			coord2 = node2.get_coord()
			coord3 = node3.get_coord()

			error_norm += calc_area(coord1, coord2, coord3)* (poly.get_x()**2+poly.get_y()**2)

	# The error norm is the total sqrt of L2 norm of the
	# difference between interpolated solution and the 
	# local solution
	error_norm = sqrt(error_norm)

	return error_norm



def create_node_value_dict(local_grid):
	""" Record the value of each node """

	# Create a dictionary
	node_dict = {"key":0}

	# Use node id as keys to find values
	for node in node_iterator(local_grid):
		node_dict[str(node.get_node_id())] = node.get_value()
		
	return node_dict



def is_in_triangle(local_grid, tri_list, node1, node2, node3, tol=-0.00001):
	""" Obtain a set of data points for triangle 1-2-3 """

	# Polynomials of the triangle
	poly1 = set_polynomial_linear_2D(node1, node2, node3)
	poly2 = set_polynomial_linear_2D(node2, node3, node1)
	poly3 = set_polynomial_linear_2D(node3, node1, node2)

	# Iterate through each triangle
	for tri in triangle_iterator(local_grid):
		if tri[0].get_node_id() < tri[1].get_node_id() and \
			tri[1].get_node_id() < tri[2].get_node_id():
		
			# Coordinates of the triangle
			coord1x1 = tri[0].get_coord()[0]
			coord1x2 = tri[0].get_coord()[1]
			coord2x1 = tri[1].get_coord()[0]
			coord2x2 = tri[1].get_coord()[1]
			coord3x1 = tri[2].get_coord()[0]
			coord3x2 = tri[2].get_coord()[1]

			# Check if the node is in the triangle 1-2-3
			if poly1.eval(coord1x1, coord1x2)>=tol and \
			   poly2.eval(coord1x1, coord1x2)>=tol and \
			   poly3.eval(coord1x1, coord1x2)>=tol and \
			   poly1.eval(coord2x1, coord2x2)>=tol and \
			   poly2.eval(coord2x1, coord2x2)>=tol and \
			   poly3.eval(coord2x1, coord2x2)>=tol and \
			   poly1.eval(coord3x1, coord3x2)>=tol and \
			   poly2.eval(coord3x1, coord3x2)>=tol and \
			   poly3.eval(coord3x1, coord3x2)>=tol: 

			   	tri_list.append([tri[0].get_node_id(), \
			   		tri[1].get_node_id(), tri[2].get_node_id()])
			


def find_triangles_inside(local_grid, node1, node2, node3, node4, mid_id):
	""" Find triangles inside the triangles 1-2-3 and 1-2-4 """
	
	tri_list = []

	# In case the new middle node was not added correctly
	if mid_id is None:
		if node3 is not None:
			tri_list.append([node1.get_node_id(), node2.get_node_id(), node3.get_node_id()])
		if node4 is not None:
			tri_list.append([node1.get_node_id(), node2.get_node_id(), node4.get_node_id()])
		return tri_list

	# Otherwise identify triangles inside
	if node3 is not None:
		is_in_triangle(local_grid, tri_list, node1, node2, node3)
	
	if node4 is not None:
		is_in_triangle(local_grid, tri_list, node1, node2, node4)

	return tri_list


def tri_pair_data_no(region, node1, node2, node3, node4):
	""" Check whether there are data in the triangle pair """

	data_sum = 0

	# Count the number of data points in triangle 1-2-3
	if node3 is not None:

		no_data = check_data(region, [node1, node2, node3])

		data_sum += no_data

	# Count the number of data points in triangle 1-2-4
	if node4 is not None:

		no_data = check_data(region, [node1, node2, node4])

		data_sum += no_data

	return data_sum



def solve_local_tpsfem_2D(grid, region, node1, node2, parameter, \
	boundary, no_layers, refine_choice, uni_refine, form, min_data):
	""" Solve a local TPSFEM to obtain a better approximation """

	# Reset refine choice if uniform refinement is required
	if uni_refine != 0:
		refine_choice = 1

	# Set boundary for local TPSFEM problem
	parameter.set_boundary(boundary)

	endpt1 = node1.get_node_id()
	endpt2 = node2.get_node_id()
	
	# Obtain other vertices of the triangle pairs
	node3, node4 = find_triangle_pairs(grid, endpt1, endpt2)
	
	# Set error indicator as 0 when no data inside the triangle pair
	# This will stop error indicators from refining this region
	data_number = tri_pair_data_no(region, node1, node2, node3, node4)
	if data_number < min_data:
		return 0

	# Build a local refined grid
	local_grid, mid_id = build_local_grid_2D(grid, region, node1, node2, \
			node3, node4, parameter, no_layers, refine_choice, uni_refine)
	
	################ Implementation ##################
	# Neumann boundary won't work when no data is in the local domain
	# (or when the number of data is very small, not stable)
	# either stop refining (works in some cases)
	# or use Dirichlet boundary

	node_dict = create_node_value_dict(local_grid)

	# Do not build if the triangle if the triangle pair is
	# only split into two (connections should have been updated)
	if_build = True

	if refine_choice == 0 or refine_choice == 3:
		if_build = False

#	if parameter.get_iteration() > 0:
#		femPlot2D.plot_fem_grid_2D(local_grid, parameter, True)

	# Solve the local TPSFEM
	solve_tpsfem_2D(local_grid, region, parameter, if_build)

#	if parameter.get_marker() == 1:
#		data = obtain_data_grid(region, local_grid)
#		femPlot2D.plot_fem_grid_data_2D(local_grid, data, parameter)

	tri_list = find_triangles_inside(local_grid, node1, node2, node3, node4, mid_id)
	
#	for i in range(0, len(tri_list)):
#		print tri_list[i][0], tri_list[i][1], tri_list[i][2]
	
	# calculate error norm
	error_norm = calc_error_norm(local_grid, tri_list, node_dict, form)
	"""
	if error_norm > 100:
		print error_norm
		femPlot2D.plot_fem_grid_2D(local_grid, parameter, True)
		tpsfemPlot2D.plot_tpsfem_solution_data_2D(grid, region, True, True)
	"""
#	if parameter.get_iteration() > 0:
#		femPlot2D.plot_fem_grid_2D(local_grid, parameter, True)

#	print error_norm
	"""
	for node in node_iterator(local_grid):
		if node.get_value() < 1000:
			print "!!!!!!!!!!!!!!!!!", node.get_value()
			for node in node_iterator(local_grid):
				print node.get_node_id(), node.get_value()
	"""
#			femPlot2D.plot_fem_grid_2D(local_grid, parameter, True)
#	for node in node_iterator(local_grid):
#		print node.get_coord(), node.get_value(), node.get_d_values()
#	auxiliaryPlot.save_auxiliary_approx(local_grid, region, parameter, node_dict)

	return error_norm