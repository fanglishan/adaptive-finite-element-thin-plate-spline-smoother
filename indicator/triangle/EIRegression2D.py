#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-08-12 21:47:53
@Last Modified time: 2024-04-11 13:51:29

This class contains routines to measures the regression error of the error of the 
2D approximation.

"""

# Import libaries
from adaptive.triangle.LocalGrid2D import build_local_grid_2D
from adaptive.triangle.TriangleFunction import find_triangle_pairs
import plot.TpsfemPlot2D as tpsfemPlot2D
from plot.DataPlot import plot_data 
from plot.FemPlot2D import plot_fem_grid_data_2D
from data.ObtainData import obtain_data_grid
from adaptive.triangle.GridValue import cal_smoother_2d

from numpy import sqrt
from copy import deepcopy


def calculate_error(local_grid, point):

	return point[-1]-cal_smoother_2d(local_grid, point[0:2])


def MAE(local_grid, data):
	""" Calculate mean absolute error (MAE) """

	sum = 0.0

	for point in data:

		error = calculate_error(local_grid, point)

		if error != 10000000.0:
			sum += abs(error)

	return sum/len(data)



def RMSE(local_grid, data):
	""" Calculate root mean square error (RMSE) """

	sum = 0.0

	for point in data:

		error = calculate_error(local_grid, point)

		if error != 10000000.0:
			sum += error**2

	return sqrt(sum/len(data))



def MAPE(local_grid, data):
	""" Calculate mean absolute percentage error (MAPE) """

	sum = 0.0

	for point in data:

		error = calculate_error(local_grid, point)

		if error != 10000000.0:
			sum += abs(error/point[-1])

	return sum/len(data)



def MPE(local_grid, data):
	""" Calculate mean percentage error (MPE) """

	sum = 0.0

	for point in data:

		error = calculate_error(local_grid, point)

		if error != 10000000.0:
			sum += error/point[-1]

	return abs(sum/len(data))


def add_scaling(node1, node2):

	return abs(node1.get_coord()[0]-node2.get_coord()[0])


def indicate_error_regression_2D(grid, region, node1, node2, \
	parameter, no_layers, refine_choice, error_type, smoother_type):
	""" Indicate error of the 2D grid """

	# Obtain other vertices of the triangle pairs
	node3, node4 = find_triangle_pairs(grid, node1.get_node_id(), node2.get_node_id())

	# Build a local refined grid
	local_grid, mid_id = build_local_grid_2D(grid, region, node1, node2, node3, node4, parameter, no_layers, refine_choice)

	
	data = obtain_data_grid(region, local_grid)

#	plot_fem_grid_data_2D(local_grid, data)

	if len(data) < 1:
		return 0

	indicator = 0

	
	# 1-Mean absolute error (MAE)
	if error_type == 1:
		indicator = MAE(local_grid, data)

	# 2-Root mean square error (RMSE)
	elif error_type == 2:
		indicator = RMSE(local_grid, data)

	# 3-Mean absolute percentage error (MAPE)
	elif error_type == 3:
		indicator = MAPE(local_grid, data)

	# 4-Mean percentage error (MPE)
	elif error_type == 4:
		indicator = MPE(local_grid, data)

#	indicator *= add_scaling(node1, node2)
	
	return indicator
	