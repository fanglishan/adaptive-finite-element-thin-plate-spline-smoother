#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-05-13 21:41:28
@Last Modified time: 2024-04-11 14:21:59

This class contains the routines related to the recovery-based error indicator for 2D problems.

"""

# Import libraries
from grid.NodeTable import node_iterator
from adaptive.triangle.Triangle import triangle_iterator, triangle_iterator_ref
from adaptive.triangle.SetPolynomial import set_polynomial_linear_2D, get_solution_polynomial_linear_2D
from adaptive.triangle.TriangleIntegrateLinear import calc_area, linear_integrate_single
from adaptive.triangle.TriangleIntegrateQuadratic import quadratic_integrate, quadratic_integrate_single
from tpsfem.BuildBasis import check_dict
from tpsfem.BuildBasis import obtain_node_index_2D
from parameter.Definition import BoundaryType
import plot.FemPlot2D as femPlot2D

from numpy.linalg import norm, solve, lstsq
from numpy import sqrt, zeros
from timeit import default_timer


def set_average_entry(grid, index):
	""" Obtain and set average value of one parameter """

	# Iterate through each node
	for node in node_iterator(grid):

		# The list on given index
		dx_values = node.get_parameters()[index]
		
		# Compute the average value
		if len(dx_values) > 0:
			entry = sum(dx_values)/len(dx_values)
		# Return 0 if list is empty
		else:
			entry = 0.0

		node.set_parameters_entry(entry, index)


def initialise_gradient(grid, Dx_boundary):
	""" Initialise current gradients as Dirichlet boundary conditions """

	# Four parameters of each node contains current
	# 0-Dx, 1-current Dy, 2-improved Dx, 3-improved Dy

	# Approximate gx using c values
	if Dx_boundary == 1:

		# Initialise current gradient and improved gradient
		for node in node_iterator(grid):
			node.set_parameters([[], [], 0.0, 0.0])

		# Loop over the triangles once
		for tri in triangle_iterator_ref(grid):
			if tri[0].get_node_id() < tri[1].get_node_id() and \
				tri[1].get_node_id() < tri[2].get_node_id():

				# Compute gradients of each triangular surface
				poly = get_solution_polynomial_linear_2D(tri[0], \
					tri[1], tri[2], tri[0].get_value(), \
					tri[1].get_value(), tri[2].get_value())

				Dx = poly.dx().get_const()
				Dy = poly.dy().get_const()

				# Add gradients to each node
				tri[0].add_parameters_entry(Dx, 0)
				tri[0].add_parameters_entry(Dy, 1)
				tri[1].add_parameters_entry(Dx, 0)
				tri[1].add_parameters_entry(Dy, 1)
				tri[2].add_parameters_entry(Dx, 0)
				tri[2].add_parameters_entry(Dy, 1)

		# Get average of gradients for each node
		set_average_entry(grid, 0)
		set_average_entry(grid, 1)


	# Approximate gx using g values
	elif Dx_boundary == 2:

		# Set current Dx and Dy for each node using g values
		for node in node_iterator(grid):
			d_values = node.get_d_values()
			node.set_parameters([d_values[0], d_values[1], 0.0, 0.0])



def calculate_matrix_entries(matrix, vector1, vector2, node1, node2, \
				node3, poly1, poly2, poly3, index1, index2, index3):
	""" Calculate entries of the matrix """

	# Current gradient: 0-Dx, 1-Dy
	Dx1 = node1.get_parameters_entry(0)
	Dx2 = node2.get_parameters_entry(0)
	Dx3 = node3.get_parameters_entry(0)
	Dy1 = node1.get_parameters_entry(1)
	Dy2 = node2.get_parameters_entry(1)
	Dy3 = node3.get_parameters_entry(1)

	# Calculate stiffness values between nodes
	stiff1 = quadratic_integrate(poly1, poly1, node1, node2, node3)
	stiff2 = quadratic_integrate(poly2, poly2, node1, node2, node3)
	stiff3 = quadratic_integrate(poly3, poly3, node1, node2, node3)
	stiff4 = quadratic_integrate(poly1, poly2, node1, node2, node3)
	stiff5 = quadratic_integrate(poly2, poly3, node1, node2, node3)
	stiff6 = quadratic_integrate(poly3, poly1, node1, node2, node3)

	# Add stiffness values to the matrix

	# Add diagonal entries
	if index1 != -1:
		matrix[index1][index1] += stiff1
	if index2 != -1:
		matrix[index2][index2] += stiff2
	if index3 != -1:
		matrix[index3][index3] += stiff3

	# Add matrix values or boundary conditions
	if index1 != -1 and index2 != -1:
		matrix[index1][index2] += stiff4
		matrix[index2][index1] += stiff4
	elif index1 != -1:
		vector1[index1,0] -= Dx2*stiff4
		vector2[index1,0] -= Dy2*stiff4
	elif index2 != -1:
		vector1[index2,0] -= Dx1*stiff4
		vector2[index2,0] -= Dy1*stiff4

	if index2 != -1 and index3 != -1:
		matrix[index2][index3] += stiff5
		matrix[index3][index2] += stiff5
	elif index2 != -1:
		vector1[index2,0] -= Dx3*stiff5
		vector2[index2,0] -= Dy3*stiff5
	elif index3 != -1:
		vector1[index3,0] -= Dx2*stiff5
		vector2[index3,0] -= Dy2*stiff5

	if index3 != -1 and index1 != -1:
		matrix[index3][index1] += stiff6
		matrix[index1][index3] += stiff6
	elif index3 != -1:
		vector1[index3,0] -= Dx1*stiff6
		vector2[index3,0] -= Dy1*stiff6
	elif index1 != -1:
		vector1[index1,0] -= Dx3*stiff6
		vector2[index1,0] -= Dy3*stiff6


def calculate_vector_entries(vector1, vector2, node1, node2, node3, \
			poly1, poly2, poly3, index1, index2, index3, Dx_approx):
	""" Calculate entries of the vector """

	# Use gradients from c values in RHS vector
	if Dx_approx == 1:

		# Calculate Dx and Dy of the current approximation
		poly = get_solution_polynomial_linear_2D(node1, node2, node3, \
				node1.get_value(), node2.get_value(), node3.get_value())

		Dx = poly.get_x()
		Dy = poly.get_y()

		# Calculate and add RHS entries
		if index1 != -1:
			vector1[index1,0] += Dx*linear_integrate_single(poly1, node1, node2, node3)
			vector2[index1,0] += Dy*linear_integrate_single(poly1, node1, node2, node3)
				
		if index2 != -1:
			vector1[index2,0] += Dx*linear_integrate_single(poly2, node1, node2, node3)
			vector2[index2,0] += Dy*linear_integrate_single(poly2, node1, node2, node3)

		if index3 != -1:
			vector1[index3,0] += Dx*linear_integrate_single(poly3, node1, node2, node3)
			vector2[index3,0] += Dy*linear_integrate_single(poly3, node1, node2, node3)

	# Use gradients from g values in RHS vector
	elif Dx_approx == 2:

		# Polynomial of surface of gradients Dx and Dy using g1 and g2
		poly_Dx = get_solution_polynomial_linear_2D(node1, node2, node3, \
			node1.get_parameters_entry(0), node2.get_parameters_entry(0), node3.get_parameters_entry(0))

		poly_Dy = get_solution_polynomial_linear_2D(node1, node2, node3, \
			node1.get_parameters_entry(1), node2.get_parameters_entry(1), node3.get_parameters_entry(1))

		# Calculte RHS functions for two vectors
		if index1 != -1:
			vector1[index1,0] += quadratic_integrate(poly1, poly_Dx, node1, node2, node3)
			vector2[index1,0] += quadratic_integrate(poly1, poly_Dy, node1, node2, node3)

		if index2 != -1:
			vector1[index2,0] += quadratic_integrate(poly2, poly_Dx, node1, node2, node3)
			vector2[index2,0] += quadratic_integrate(poly2, poly_Dy, node1, node2, node3)

		if index3 != -1:
			vector1[index3,0] += quadratic_integrate(poly3, poly_Dx, node1, node2, node3)
			vector2[index3,0] += quadratic_integrate(poly3, poly_Dy, node1, node2, node3)



def build_system(grid, parameter, node_list, Dx_approx):
	""" Build the system of equations """

	no_nodes = len(node_list)

	# Matrix and two vectors for Dx and Dy
	matrix =  zeros([no_nodes, no_nodes])
	vector2 = zeros([no_nodes, 1])
	vector1 = zeros([no_nodes, 1])

	# Loop over the triangles once
	for tri in triangle_iterator_ref(grid):
		if tri[0].get_node_id() < tri[1].get_node_id() and \
			tri[1].get_node_id() < tri[2].get_node_id():

			# Obtain index of the nodes
			index1, index2, index3 = \
				obtain_node_index_2D(node_list, tri[0], tri[1], tri[2])

			if index1 == -1 and index2 == -1 and index3 == -1:
				continue

			# Compute gradients of each triangular surface
			poly1 = set_polynomial_linear_2D(tri[0], tri[1], tri[2])
			poly2 = set_polynomial_linear_2D(tri[1], tri[2], tri[0])
			poly3 = set_polynomial_linear_2D(tri[2], tri[0], tri[1])

			# Calculate entries for the system of equations
			calculate_matrix_entries(matrix, vector1, vector2, tri[0], tri[1], tri[2], poly1, poly2, poly3, index1, index2, index3)

			calculate_vector_entries(vector1, vector2, tri[0], tri[1], tri[2], poly1, poly2, poly3, index1, index2, index3, Dx_approx)

	return matrix, vector1, vector2



def build_node_list(grid, boundary):
	""" Build a list of nodes """

	# Initialise a list of nodes
	node_list = []

	# Dictionary of nodes
	node_dict = {}

	# Iterate through nodes in the grid
	for node in node_iterator(grid):

		# Check if the node is interior
		if not check_dict(node_dict,node.get_node_id()):

			# If endpoint is interior or if the boundary is Neumann
			if not grid.get_slave(node.get_node_id()) or boundary == BoundaryType.Neumann:

				# Add the node to the dictionary
				node_dict[str(node.get_node_id())] = 0

				# Add the node to the list
				node_list.append(grid.get_node_ref(node.get_node_id()))

	return node_list


def process_gradient(grid, parameter, boundary, Dx_approx):
	""" Post-process gradient to improve them """

	# Build a list of interior nodes
	node_list = build_node_list(grid, boundary)

	# Build a system of equations
	matrix, vector1, vector2 = build_system(grid, parameter, node_list, Dx_approx)

	# Solve the system to obtain improved Dx and Dy
	try:
		tpsfem_solution1 = solve(matrix, vector1)
		tpsfem_solution2 = solve(matrix, vector2)
	# In case of singular matrices
	except Exception as e:
#		print("Error caught", e)
		tpsfem_solution1 = lstsq(matrix, vector1)[0]
		tpsfem_solution2 = lstsq(matrix, vector2)[0]

	"""
	except:
		for i in range(0,len(node_list)):
			print(node_list[i].get_node_id(),node_list[i].get_coord())
		print(matrix)
		femPlot2D.plot_fem_grid_2D(grid, parameter, True)
	"""

	# Save improved gradients
	for i in range(0, len(node_list)):
		node_list[i].set_parameters_entry(tpsfem_solution1[i,0], 2)
		node_list[i].set_parameters_entry(tpsfem_solution2[i,0], 3)

	# Set boundary values for the "improved" gradients
	if boundary == BoundaryType.Dirichlet:
		for node in node_iterator(grid):
			if grid.get_slave(node.get_node_id()):
				node.set_parameters_entry(node.get_parameters_entry(0), 2)
				node.set_parameters_entry(node.get_parameters_entry(1), 3)
	


def estimate_triangle_error(node1, node2, node3):
	""" Estimate error in the given triangle """

	# Obtain triangle surface and current gradients
	poly = get_solution_polynomial_linear_2D(node1, node2, node3, \
			node1.get_value(), node2.get_value(), node3.get_value())
	Dx = poly.get_x()
	Dy = poly.get_y()

	# Square of the result of the improved solution minus current solution
	Dx_poly = get_solution_polynomial_linear_2D(node1, node2, node3, \
		node1.get_parameters_entry(2)-Dx, node2.get_parameters_entry(2)-Dx, node3.get_parameters_entry(2)-Dx)

	Dy_poly = get_solution_polynomial_linear_2D(node1, node2, node3, \
		node1.get_parameters_entry(3)-Dy, node2.get_parameters_entry(3)-Dy, node3.get_parameters_entry(3)-Dy)

	Dx_poly_product = Dx_poly*Dx_poly
	Dy_poly_product = Dy_poly*Dy_poly

	# Integration over current triangle
	error_x = quadratic_integrate_single(Dx_poly_product, node1, node2, node3)
	error_y = quadratic_integrate_single(Dy_poly_product, node1, node2, node3)

	return error_x, error_y


def estimate_error(grid, original_node1, original_node2, original_node3, original_node4):
	""" Estimate error in the interval """

	# Get two nodes in the local grid
	node1 = grid.get_node_ref(original_node1.get_node_id())
	node2 = grid.get_node_ref(original_node2.get_node_id())

	error_estimate = 0.0

	error_x_list = []
	error_y_list = []

	# Estimate error in triangle 1-2-3
	if original_node3 is not None:

		node3 = grid.get_node_ref(original_node3.get_node_id())

		error_x, error_y = estimate_triangle_error(node1, node2, node3)
		
		error_x_list.append(error_x)
		error_y_list.append(error_y)

	# Estimate error in triangle 1-2-4
	if original_node4 is not None:

		node4 = grid.get_node_ref(original_node4.get_node_id())

		error_x, error_y = estimate_triangle_error(node1, node2, node4)
		
		error_x_list.append(error_x)
		error_y_list.append(error_y)

	# Calculate error norm of the triangle pair
	if len(error_x_list) > 0:

		error_list = []

		for i in range(0, len(error_x_list)):
			error_list.append(sqrt(error_x_list[i]**2+error_y_list[i]**2))

		# The error indicator is the sum of the triangle pair
		error_estimate = sum(error_list)#/len(error_list)

	return error_estimate



def recovery_based_2D(grid, node1, node2, node3, node4, parameter, boundary, Dx_boundary, Dx_approx):
	""" Estimate error for 2D elements by recovering gradients """
	
#	t = default_timer()
	# Initialise current gradients
	initialise_gradient(grid, Dx_boundary)
#	print("initialise gradient:", default_timer()-t)

#	t = default_timer()
	# Postprocess gradients
	process_gradient(grid, parameter, boundary, Dx_approx)
#	print("process gradient:", default_timer()-t)

#	t = default_timer()
	# Estimate error
	error_estimate = estimate_error(grid, node1, node2, node3, node4)
#	print("estimate error:", default_timer()-t)
	"""
	n1 = grid.get_node_ref(node1.get_node_id())
	n2 = grid.get_node_ref(node2.get_node_id())
	print grid.get_slave(node1.get_node_id()), n1.get_parameters()
	print grid.get_slave(node2.get_node_id()), n2.get_parameters()
	if node3 is not None:
		n3 = grid.get_node_ref(node3.get_node_id())
		print grid.get_slave(node3.get_node_id()), n3.get_parameters()
	if node4 is not None:
		n4 = grid.get_node_ref(node4.get_node_id())
		print grid.get_slave(node4.get_node_id()), n4.get_parameters()		
	print error_estimate
	print ""
	"""
#	femPlot2D.plot_fem_grid_2D(grid, True)

	return error_estimate