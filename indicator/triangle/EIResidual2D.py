#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-05-05 21:47:53
@Last Modified time: 2022-10-09 23:33:50

This class contains routines to build a 2D local grid with refinement at the base edge. 
The local TPSFEM will be solved and the energy norm of the difference between the improved 
approximation and original interpolation will be calculated.

"""

# Import libaries
from grid.Edge import DomainSet
from adaptive.triangle.Triangle import triangle_iterator, neighbour_triangles_iterator_ref
from adaptive.triangle.SetPolynomial import get_solution_polynomial_linear_2D
from adaptive.triangle.TriangleIntegrateQuadratic import quadratic_integrate
from adaptive.triangle.LocalGrid2D import build_local_grid_2D
from adaptive.triangle.TriangleFunction import find_triangle_pairs, get_h_2d, cal_distance
from adaptive.triangle.BuildEquationLinear import build_equation_linear_2D
from tpsfem.TpsfemSolver2D import obtain_system_equations
from tpsfem.BuildBasis import build_node_list
from tpsfem.AddEntry2D import add_alpha_L, add_uniqueness, add_boundary_effects
from parameter.Definition import BoundaryType
import plot.FemPlot2D as femPlot2D
import plot.TpsfemPlot2D as tpsfemPlot2D

from numpy import sqrt, zeros
from copy import deepcopy


def calculate_residual(node1, node2, node3, parameter, \
					node_list, vector_residual):
	""" Calculate residual of the triangle """

	# Obtain residual of each node
	node_id1 = node1.get_node_id()
	node_id2 = node2.get_node_id()
	node_id3 = node3.get_node_id()

	# Find the index of triangle vertices
	index1 = -1
	index2 = -1 
	index3 = -1

	# Only interior nodes will be considered, New mid node 
	# should be set as an interior even when it is at the
	# boundary. This makes its residual nonzero
	for i in range(0, len(node_list)):
		if node_list[i].get_node_id() == node_id1:
			if not node_list[i].get_slave():
				index1 = i
		if node_list[i].get_node_id() == node_id2:
			if not node_list[i].get_slave():
				index2 = i
		if node_list[i].get_node_id() == node_id3:
			if not node_list[i].get_slave():
				index3 = i

	# Assign residual of the c value to each node
	# Dirichlet boundary node has 0 residual
	node1_res = 0.0
	node2_res = 0.0
	node3_res = 0.0
	if index1 != -1:
		node1_res = vector_residual[index1,0]
	if index2 != -1:
		node2_res = vector_residual[index2,0]
	if index3 != -1:
		node3_res = vector_residual[index3,0]
#	print node1_res, node2_res, node3_res

	# Calculate residual norm 
	poly = get_solution_polynomial_linear_2D(node1, \
		node2, node3, node1_res, node2_res, node3_res)

	R_norm = quadratic_integrate(poly, poly, node1, node2, node3)

	return R_norm


def calculate_jump(grid, node1, node2, original_boundary):
	""" Calculate inter-element jump of an edge """

	# Find the triangle pair from the grid
	# This will not work if it is given local grid
	node3, node4 = find_triangle_pairs(grid, node1.get_node_id(), node2.get_node_id())

	# Jump for a single triangle
	if node3 is None or node4 is None:

		# The jump at the Dirichlet boundary is zero
		if original_boundary == BoundaryType.Dirichlet:
			J_norm = 0.0

		# The jump at the Neumann boundary is from one triangle
		# For TPSFEM boundary force is assumed to be zero
		elif original_boundary == BoundaryType.Neumann:

			if node3 is None and node4 is not None:
				print("Warning: indicator/EIResidual2D - incorrect triangle pair")
				return 0.0

			# Vertice values
			value1 = node1.get_value()
			value2 = node2.get_value()
			value3 = node3.get_value()

			# Current approximation
			poly = get_solution_polynomial_linear_2D(node1, \
				node2, node3, value1, value2, value3)

			coord1 = node1.get_coord()
			coord2 = node2.get_coord()
		
			# Calculate unit outward normal vector
			if abs(coord2[0]-coord1[0]) <= 1e-14:
				n_x = 1.0    # Vertical edge
				n_y = 0.0    # Horizontal normal
			elif abs(coord2[1]-coord1[1]) <= 1e-14:
				n_x = 0.0    # Horizontal edge
				n_y = 1.0    # Vertical normal
			else:
				# normal vector is horizontal to the edge
				slope = (coord2[1]-coord1[1])/(coord2[0]-coord1[0])
				n_x = 1
				n_y = -1/slope

				# Normalise the normal
				length = sqrt(n_x**2+n_y**2)
				n_x = n_x/length
				n_y = n_y/length

			# Unit normal of the other triangle is the negative
			# of the other unit normal
			J = (poly.get_x()*n_x+poly.get_y()*n_y)
			
			# The jump is distributed to two triangles sharing this edge
	#		J = J/2.0
			# The division is achived by the coefficient c2

			# Line integral of the jump
			J_norm = cal_distance(node1, node2)*J**2

		else:
			print("Warning: indicator/EIResidual2D - invalid boundary")

	# Jump for a triangle pair
	else:
		# Vertice values
		value1 = node1.get_value()
		value2 = node2.get_value()
		value3 = node3.get_value()
		value4 = node4.get_value()

		# Current approximation
		poly1 = get_solution_polynomial_linear_2D(node1, \
			node2, node3, value1, value2, value3)

		poly2 = get_solution_polynomial_linear_2D(node1, \
			node2, node4, value1, value2, value4)

		coord1 = node1.get_coord()
		coord2 = node2.get_coord()
	
		# Calculate unit outward normal vector
		if abs(coord2[0]-coord1[0]) <= 1e-14:
			n_x = 1.0    # Vertical edge
			n_y = 0.0    # Horizontal normal
		elif abs(coord2[1]-coord1[1]) <= 1e-14:
			n_x = 0.0    # Horizontal edge
			n_y = 1.0    # Vertical normal
		else:
			# normal vector is horizontal to the edge
			slope = (coord2[1]-coord1[1])/(coord2[0]-coord1[0])
			n_x = 1
			n_y = -1/slope

			# Normalise the normal
			length = sqrt(n_x**2+n_y**2)
			n_x = n_x/length
			n_y = n_y/length

		# Unit normal of the other triangle is the negative
		# of the other unit normal
		J = (poly1.get_x()*n_x+poly1.get_y()*n_y)-(poly2.get_x()*n_x+poly2.get_y()*n_y)
	
		# The jump is distributed to two triangles sharing this edge
#		J = J/2.0
		# The division is achived by the coefficient c2

		# Line integral of the jump
		J_norm = cal_distance(node1, node2)*J**2

	return J_norm


def estimate_error_tri(grid, parameter, node1, node2, node3, mid_node, \
				node_list, vector_residual, original_boundary, c1, c2):
	""" Estimate error of a triangle """

	R_norm = 0.0

	# Calculate residual norm of triangle 1-3-mid
	R_norm += calculate_residual(node1, node3, mid_node, \
				parameter, node_list, vector_residual)

	# Calculate residual norm of triangle 2-3-mid
	R_norm += calculate_residual(node2, node3, mid_node, \
				parameter, node_list, vector_residual)

	# Calculate jump at three edges
	J12 = calculate_jump(grid, node1, node2, original_boundary)
	J13 = calculate_jump(grid, node1, node3, original_boundary)
	J23 = calculate_jump(grid, node2, node3, original_boundary)

	J_norm = J12 + J13 + J23

	h_K = get_h_2d(node1, node2, node3)

	# Estimate error with given coefficients
	error_norm = c1*(h_K**2)*R_norm + c2*h_K*J_norm	
#	print R_norm, J_norm, c1*(h_K**2)*R_norm, c2*h_K*J_norm
	return error_norm


def estimate_error_2D(grid, region, node1, node2, parameter, \
				boundary, no_layers, refine_choice, c1, c2):
	""" Estimate error using residual and traction jump """
	
	# Parameters
	alpha = parameter.get_alpha()

	# Boundary of the TPSFEM (used to calculate jump)
	original_boundary = parameter.get_boundary()

	# Set boundary for local TPSFEM problem
	parameter.set_boundary(boundary)

	# Obtain other vertices of the triangle pairs
	node3, node4 = find_triangle_pairs(grid, node1.get_node_id(), node2.get_node_id())

	# Build a local refined grid
	local_grid, mid_id = build_local_grid_2D(grid, region, node1, node2, \
				node3, node4, parameter, no_layers, refine_choice)
	mid_node = local_grid.get_node_ref(mid_id)

#	femPlot2D.plot_fem_grid_2D(local_grid, parameter, True)

	endpt1 = node1.get_node_id()
	endpt2 = node2.get_node_id()

	# When the new node is a slave node in this error indicator
	# It will still be in the node list and residual will not be zero
#	if grid.get_location(endpt1, endpt2) == DomainSet.boundary and \
#		   parameter.get_boundary() == BoundaryType.Dirichlet:
#		mid_node.set_slave(True)
#		local_grid.set_slave(mid_id, True)

	# Build a list of triangles near the new node
	tri_list = []
	for tri in neighbour_triangles_iterator_ref(local_grid, mid_node):
		if tri[1].get_node_id() > tri[2].get_node_id():
			tri_list.append(deepcopy(tri))

	# Build equation for the refined intervals
	build_equation_linear_2D(local_grid, region, parameter)

	# Build a list of nodes and build local system
	node_list = build_node_list(local_grid, boundary)

#	matrix, vector = obtain_system_equations(local_grid, parameter, node_list, False)
	matrix, vector, KTu_vector = obtain_system_equations(local_grid, parameter, node_list, False)

	# Add L entries multiplied with alpha
	add_alpha_L(local_grid, node_list, matrix, vector, parameter)

	# Add effects from boundary conditions
	add_boundary_effects(local_grid, node_list, vector, parameter)

	# Construct current approximation
	no_nodes = len(node_list)

	if boundary == BoundaryType.Dirichlet:
		approx = zeros([4*no_nodes, 1])
	else:
		approx = zeros([4*no_nodes+3, 1])

	for i in range(0, no_nodes):
		approx[i,0] += node_list[i].get_value()
		d_values = node_list[i].get_d_values()
		approx[i+no_nodes,0] += d_values[0]
		approx[i+2*no_nodes,0] += d_values[1]
		approx[i+3*no_nodes,0] += d_values[2]*alpha

	# Calculate residual for nodes
	vector_residual = matrix*approx - vector

	# Error norm for each triangle
	error_norm_1 = 0.0
	error_norm_2 = 0.0

	# Calculate error for triangle 1-2-3
	if node3 is not None:
		error_norm_1 = estimate_error_tri(grid, parameter, node1, node2, \
							node3, mid_node, node_list, vector_residual, \
							original_boundary, c1, c2)

	# Calculate error for triangle 1-2-4
	if node4 is not None:
		error_norm_2 = estimate_error_tri(grid, parameter, node1, node2, \
							node4, mid_node, node_list, vector_residual, \
							original_boundary, c1, c2)


	if node3 is not None and node4 is not None:
		error_norm = (error_norm_1+error_norm_2)/2.0

	if node3 is None or node4 is None:
		error_norm = max(error_norm_1, error_norm_2)

	error_norm = sqrt(error_norm)

	# Restore original triangle values (just in case)
	for tri in tri_list:
		for i in range(0, 3):
			node_id = tri[i].get_node_id()
			if node_id.get_no() != -10:
				if grid.is_in(node_id):
					node = grid.get_node_ref(node_id)
					node.set_value(tri[i].get_value())
					node.set_d_values(tri[i].get_d_values())

	return error_norm