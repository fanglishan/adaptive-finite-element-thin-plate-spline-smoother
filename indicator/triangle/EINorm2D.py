#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-05-05 22:56:23
@Last Modified time: 2022-10-09 23:32:47

This class contains 2D routines for the norm-based error indicator.

"""

# Import libraries
from grid.NodeTable import node_iterator
from grid.EdgeTable import endpt_iterator
from adaptive.triangle.Triangle import triangle_iterator, triangle_iterator_ref
from adaptive.triangle.SetPolynomial import get_solution_polynomial_linear_2D
from adaptive.triangle.TriangleIntegrateLinear import calc_area

from numpy import array


def set_average_entry(local_grid, index):
	""" Obtain and set average value of one parameter """

	# Iterate through each node
	for node in node_iterator(local_grid):

		# The list on given index
		dx_values = node.get_parameters()[index]
		
		# Compute the average value
		if len(dx_values) > 0:
			entry = sum(dx_values)/len(dx_values)
		# Return 0 if list is empty
		else:
			entry = 0.0

		node.set_parameters_entry(entry, index)


def set_absolute_parameters(local_grid, index):

	# Iterate through each node
	for node in node_iterator(local_grid):

		# The list on given index
		entry = node.get_parameters_entry(index)		

		node.set_parameters_entry(abs(entry), index)


def initialise_node_parameters(local_grid):
	""" Initialise parameters in each node """

	for node in node_iterator(local_grid):

		# The parameters store Dx, Dy, Dxx, Dxy, Dyy
		node.set_parameters([[],[],[],[],[]])


def approximate_local_Dxx_2D(local_grid, Dx_source):
	""" Approximate Dxx values for each interior nodes """

	# Initialise the parameters variable to store Dx,Dy 
	# approximations from different intervals
	initialise_node_parameters(local_grid)

	# Approximate gx and gy using c values
	if Dx_source == 1:			

		# Loop over the triangles once
		for tri in triangle_iterator_ref(local_grid):
			if tri[0].get_node_id() < tri[1].get_node_id() and \
				tri[1].get_node_id() < tri[2].get_node_id():

				# Compute gradients of each triangular surface
				poly = get_solution_polynomial_linear_2D(tri[0], \
					tri[1], tri[2], tri[0].get_value(), \
					tri[1].get_value(), tri[2].get_value())

				gx = poly.dx().get_const()
				gy = poly.dy().get_const()

				# Add gradients to each node
				tri[0].add_parameters_entry(gx, 0)
				tri[0].add_parameters_entry(gy, 1)
				tri[1].add_parameters_entry(gx, 0)
				tri[1].add_parameters_entry(gy, 1)
				tri[2].add_parameters_entry(gx, 0)
				tri[2].add_parameters_entry(gy, 1)

		# Get average of gradients for each node
		set_average_entry(local_grid, 0)
		set_average_entry(local_grid, 1)


	# Set gx and gy using g values
	elif Dx_source == 2:
		for node in node_iterator(local_grid):

			d_values = node.get_d_values()

			node.set_parameters_entry(d_values[0], 0)
			node.set_parameters_entry(d_values[1], 1)


	# Approximate Dxx, Dxy and Dyy using gradients of each node
	if Dx_source == 1 or Dx_source == 2:

		# Loop over the triangles
		for tri in triangle_iterator_ref(local_grid):

			# Each triangle will be evaluated only once
			if tri[0].get_node_id() < tri[1].get_node_id() and \
				tri[1].get_node_id() < tri[2].get_node_id():

				# Find the surface of Dx
				poly = get_solution_polynomial_linear_2D(tri[0], \
					tri[1], tri[2], tri[0].get_parameters_entry(0), \
					tri[1].get_parameters_entry(0), tri[2].get_parameters_entry(0))

				# Dxx
				gx = poly.dx().get_const()
				# Dxy
				gy = poly.dy().get_const()

				tri[0].add_parameters_entry(gx, 2)
				tri[0].add_parameters_entry(gy, 3)
				tri[1].add_parameters_entry(gx, 2)
				tri[1].add_parameters_entry(gy, 3)
				tri[2].add_parameters_entry(gx, 2)
				tri[2].add_parameters_entry(gy, 3)

				# Find the surface of Dy
				poly = get_solution_polynomial_linear_2D(tri[0], \
					tri[1], tri[2], tri[0].get_parameters_entry(1), \
					tri[1].get_parameters_entry(1), tri[2].get_parameters_entry(1))

				# Dyx
				gx = poly.dx().get_const()
				# Dyy
				gy = poly.dy().get_const()

				tri[0].add_parameters_entry(gx, 3)
				tri[0].add_parameters_entry(gy, 4)
				tri[1].add_parameters_entry(gx, 3)
				tri[1].add_parameters_entry(gy, 4)
				tri[2].add_parameters_entry(gx, 3)
				tri[2].add_parameters_entry(gy, 4)

		# Get average of gradients for each node
		set_average_entry(local_grid, 2)
		set_average_entry(local_grid, 3)
		set_average_entry(local_grid, 4)

		# Make sure the Dxx, Dxy and Dyy are non negative
		set_absolute_parameters(local_grid, 2)
		set_absolute_parameters(local_grid, 3)
		set_absolute_parameters(local_grid, 4)

	# Approximate Dxx+Dyy using w values
	if Dx_source == 3:

		for node in node_iterator(local_grid):

			d_values = node.get_d_values()

			node.set_parameters_entry(abs(d_values[2]), 2)
			node.set_parameters_entry(0.0, 3)
			node.set_parameters_entry(0.0, 4)		


def calculate_integral_triangle(node1, node2, node3, index):
	""" Calculate the integral value for given triangle """
	
	# Check the coordinates are two dimensional
	assert node1.get_dim() == 2 and node2.get_dim() == 2 \
		and node3.get_dim() == 2, \
			"the triangle coordinates must be two dimensional"

	poly = get_solution_polynomial_linear_2D(node1, node2, node3, \
			node1.get_parameters()[index], node2.get_parameters()[index], \
			node3.get_parameters()[index])

	# Get the coordinates of the 3 vertices
	coord1 = array(node1.get_coord())
	coord2 = array(node2.get_coord())
	coord3 = array(node3.get_coord())
	
	# Find the point at the centre of the triangle
	centre_point = (coord1+coord2+coord3)/3.0
	x = centre_point[0]
	y = centre_point[1]

	# Apply the quadrature rule    
	return poly.eval(x, y)*calc_area(coord1, coord2, coord3)


def calculate_Dxx_norm_2D(local_grid, node1, node2, node3, node4, quad_rule):
	""" Calculate integral norm of the triangle pair"""

	node1_ref = local_grid.get_node_ref(node1.get_node_id())
	node2_ref = local_grid.get_node_ref(node2.get_node_id())

	# Integral value of the triangle pair
	int_tri1 = 0.0
	int_tri2 = 0.0

	# Calculate in triangle 1-2-3
	if node3 is not None:
		node3_ref = local_grid.get_node_ref(node3.get_node_id())

		int_Dxx1 = calculate_integral_triangle(node1_ref, node2_ref, node3_ref, 2)
		int_Dxy1 = calculate_integral_triangle(node1_ref, node2_ref, node3_ref, 3)
		int_Dyy1 = calculate_integral_triangle(node1_ref, node2_ref, node3_ref, 4)

		# The error indicator of the triangle is the max of them
		int_tri1 = max(int_Dxx1, int_Dxy1, int_Dyy1)

	# Calculate in triangle 1-2-4
	if node4 is not None:
		node4_ref = local_grid.get_node_ref(node4.get_node_id())

		int_Dxx2 = calculate_integral_triangle(node1_ref, node2_ref, node4_ref, 2)
		int_Dxy2 = calculate_integral_triangle(node1_ref, node2_ref, node4_ref, 3)
		int_Dyy2 = calculate_integral_triangle(node1_ref, node2_ref, node4_ref, 4)

		# The error indicator of the triangle is the max of them
		int_tri2 = max(int_Dxx2, int_Dxy2, int_Dyy2)

	if int_tri1 < 0 or int_tri2 < 0:
		print(int_tri1, int_tri2)

	if node3 is not None and node4 is not None:
		# Takes the sum instead of the average
		indicator = (int_tri1+int_tri2)#/2.0
	elif node3 is None and node4 is None:
		print("Warning: indicator/EINorm2D - not a triangle")
	else:
		indicator = max(int_tri1,int_tri2)

	return indicator