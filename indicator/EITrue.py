#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-04-22 23:08:37
@Last Modified time: 2022-10-09 23:31:46

This error indicator computes the energy norm of the error between the
current approximation and the reference solution. The reference solution
can be the true model problem function or an approximation with very 
fine grid. This error indicator is for testing purpose.

"""

# Import libraries
from adaptive.triangle.TriangleFunction import find_triangle_pairs, build_centre_point
from adaptive.triangle.TriangleIntegrateLinear import calc_area


def calculate_error(parameter, coord, value):

	u = parameter.get_function()[0]

	return abs(u(coord)-value)


def indicate_edge_error(grid, region, edge, parameter):
	""" Indicate the true error of the given edge """

	endpt1 = edge.get_endpt1()
	endpt2 = edge.get_endpt2()

	# Indicate error for 1D grid
	if parameter.get_dim() == 1:

		# Coordinate of endpoints
		coord1 = grid.get_coord(endpt1)
		coord2 = grid.get_coord(endpt2)

		# Interpolated value of endpoints
		value1 = grid.get_value(endpt1)
		value2 = grid.get_value(endpt2)

		# Calculate energy norm of the error
		edge_error = abs(coord1[0]-coord2[0])*(calculate_error(parameter, coord1, value1)+calculate_error(parameter, coord2, value2))/2.0

	# Indicate error for 1D grid
	if parameter.get_dim() == 2:

		# End nodes of the edge
		node1 = grid.get_node(endpt1)
		node2 = grid.get_node(endpt2)

		coord1 = node1.get_coord()
		coord2 = node2.get_coord()

		# Obtain other vertices of the triangle pairs
		node3, node4 = find_triangle_pairs(grid, endpt1, endpt2)

		edge_error = 0.0

		if node3 is not None:

			centre_point = build_centre_point(grid, node1, node2, node3)

			coord3 = node3.get_coord()
			centre_coord = centre_point.get_coord()
			centre_value = centre_point.get_value()

			edge_error += calc_area(coord1, coord2, coord3)*\
				calculate_error(parameter, centre_coord, centre_value)

		if node4 is not None:

			centre_point = build_centre_point(grid, node1, node2, node4)

			coord4 = node4.get_coord()
			centre_coord = centre_point.get_coord()
			centre_value = centre_point.get_value()

			edge_error += calc_area(coord1, coord2, coord4)*\
				calculate_error(parameter, centre_coord, centre_value)
	
	# Set error indicator of the edge
	grid.set_error_indicator(edge.get_endpt1(),edge.get_endpt2(),edge_error)
	grid.set_error_indicator(edge.get_endpt2(),edge.get_endpt1(),edge_error)

	return edge_error


def indicate_error(grid, region, base_edges, parameter):
	""" Indicate the true error of the grid """

	# Initialise the error list for error indicators
	error_list = []

	# Indicate error of base edges
	for edge in base_edges:

		# Indicate the error of the edge
		edge_error_indicator = indicate_edge_error(grid, region, edge, parameter)

		# Add the new error to the list
		error_list.append(edge_error_indicator)


	return error_list
