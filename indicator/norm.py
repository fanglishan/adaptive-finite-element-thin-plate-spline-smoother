#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-04-16 10:13:44
@Last Modified time: 2022-03-29 11:20:05

This class contains modules that calculates the L norms.

"""

# Import libraries
from scipy import linalg
from numpy import inf
from math import sqrt


def L_norm(input_list, norm_type=2):
	""" calculate different norms """

	if len(input_list) == 0:
		print("Warning: no input for the L norm")
		
	# Find absolute value for each element
	abs_list = [abs(x) for x in input_list]

	norm = 0.0
	
	# L-infinity norm
	if norm_type == 0:
		norm = max(abs_list)

	# L-one norm
	elif norm_type == 1:
		norm = sum(abs_list)

	# L-two norm
	elif norm_type == 2:
		for x in abs_list:
			norm += x**2 
		norm = sqrt(norm)

	return norm