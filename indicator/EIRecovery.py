#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-05-05 21:53:41
@Last Modified time: 2024-04-11 14:22:14

This class contains implements the recovery-based error indicator.

"""

# Import libraries
from grid.NodeTable import node_iterator
from adaptive.triangle.TriangleFunction import find_triangle_pairs
from adaptive.line.LocalGrid1D import build_local_grid_1D
from adaptive.triangle.LocalGrid2D import build_local_grid_2D
from indicator.line.EIRecovery1D import recovery_based_1D
from indicator.triangle.EIRecovery2D import recovery_based_2D
import indicator.EIWeight as EIWeight
from data.ObtainData import obtain_data, obtain_data_grid
from tpsfem.StopCriteria import stop_refinement
from parameter.Definition import CostType
from fileio.WriteResult import write_line

from timeit import default_timer



def indicate_edge_error(grid, region, edge, parameter):
	""" Indicate the true error of the given edge """
	
	# Set parameter mode to local
	parameter.set_local(True)

	# Record original parameters
	origin_boundary = parameter.get_boundary()

	# Parameters
	indicator_parameter = parameter.get_indicator_parameter()
	
	# Boundary of local TPSFEM: 1-Dirichlet, 2-Neumann
	# Only Dirichlet boundary is supported right now
	boundary = indicator_parameter[0]

	# number of layers of the local grid
	no_layers = indicator_parameter[1]

	# Refine choice: 0-no refine, 1-refine base edge
	refine_choice = indicator_parameter[2]

	# Current gradient used as boundary conditions
	# 1-gradients of c, 2-g values
	Dx_choice = indicator_parameter[3]

	# Current gradient used in RHS equation
	# 1-gradients of c, 2-g values
	Dx_approx = Dx_choice

	# Improve choice: 1-solve system
	# 2-use averaged gradients, 3-g values
	improve_choice = indicator_parameter[4]

	# Number of uniform refinement on the local grid
	uni_refine = 0

	# Node 1 of the edge
	endpt1 = edge.get_endpt1()
	node1 = grid.get_node(endpt1)

	# Node 2 of the edge
	endpt2 = edge.get_endpt2()
	node2 = grid.get_node(endpt2)

	# Initialise edge error
	edge_error = 0.0
#	print(refine_choice)
	# For 1D grid
	if grid.get_dim() == 1:

		# Build a local 1D grid
		local_grid, mid_id = build_local_grid_1D(grid, region, node1, node2, \
					parameter, no_layers, refine_choice, uni_refine)

		# Plot 
#		edge_error = recovery_based_1D(grid, node1, node2, \
#					parameter, boundary, Dx_choice, Dx_approx, improve_choice)
#		from sys import exit
#		exit()

		edge_error = recovery_based_1D(local_grid, node1, node2, \
			parameter, boundary, Dx_choice, Dx_approx, improve_choice)
		
		###
		"""
		region.init_usage()
		data = obtain_data_grid(region, local_grid)
		data_size = len(data)

		coord_min = 10000000.0
		coord_max = -10000000.0
		for node in node_iterator(local_grid):
			coord = node.get_coord()[0]
			coord_min = min(coord_min, coord)
			coord_max = max(coord_max, coord)
		length = abs(coord_min-coord_max)

		h_length = abs(node1.get_coord()[0]-node2.get_coord()[0])
		if data_size == 0:
			d_4 = length**4
		else:
			d_4 = (length/(data_size+1))**4
		d_4 = max(d_4, h_length**4)
		print data_size, length
		print edge_error, d_4
		print ""
		error = min(edge_error, d_4)

		edge_error = error
		"""

	# For 2D grid
	elif grid.get_dim() == 2 and EIWeight.check_boundary_edge(grid, edge):
		edge_error = 0.0

	elif grid.get_dim() == 2:
		
#		# Test using A values as error indicator values directly for comparison
#		return grid.get_matrix_A_value(endpt1, endpt1) + grid.get_matrix_A_value(endpt2, endpt2)

		# Obtain other vertices of the triangle pairs
		node3, node4 = find_triangle_pairs(grid, endpt1, endpt2)

#		if stop_refinement(region, parameter, grid, node1, node2, node3, node4):
#			edge_error = 0.0

#		else:
		if 1 == 1:

#			t = default_timer()
			# Build a local 2D grid
			local_grid, mid_id = build_local_grid_2D(grid, region, node1, node2, \
						node3, node4, parameter, no_layers, refine_choice)
#			print("build local grid:", default_timer()-t)

#			t = default_timer()
			# Estimate edge error
			edge_error = recovery_based_2D(local_grid, node1, node2, \
				node3, node4, parameter, boundary, Dx_choice, Dx_choice)
	#		print("recover gradient:", default_timer()-t)

	# For unlisted dimensions
	else:
		print("Warning: indicator/EIRecovery - invalid dimension")

	# Set error indicator of the edge
	grid.set_error_indicator(edge.get_endpt1(),edge.get_endpt2(),edge_error)
	grid.set_error_indicator(edge.get_endpt2(),edge.get_endpt1(),edge_error)

	parameter.set_local(False)
	
	# Restore original parameters
	parameter.set_boundary(origin_boundary)

	# Reset main grid counter to avoid potential error
	grid.reset_counter()

	edge_error = EIWeight.add_data_weights(grid, edge, parameter, edge_error)
	
	parameter.add_indicator_list([edge.get_endpt1(),edge.get_endpt2(),edge_error])
	
	return edge_error


def indicate_error(grid, region, base_edges, parameter):
	""" Indicate the true error of the grid """

	# Initialise the error list for error indicators
	error_list = []

	# Cost record
	cost = parameter.get_cost()

	iteration = parameter.get_iteration()
	cost.add_time([], CostType.indicate)
	
	write_line(parameter, "number of base edges: "+str(len(base_edges)), True)
	time_list = []

	# Indicate error of base edges
	for edge in base_edges:

		t = default_timer()
		# Indicate the error of the edge
		edge_error_indicator = indicate_edge_error(grid, region, edge, parameter)
		cost.append_time(default_timer()-t, CostType.indicate, iteration)
		time_list.append(default_timer()-t)
#		print("indicator cost:",default_timer()-t)
#		print("")
		# Add the new error to the list
		error_list.append(edge_error_indicator)

	write_line(parameter, "error indicator average: "+str(sum(time_list)/len(time_list)), True)

	return error_list
