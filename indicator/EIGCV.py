#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-08-12 21:53:41
@Last Modified time: 2022-10-09 23:26:11

This error indicator uses the GCV to indicate the size of the error in the given element.

"""

# Import libraries
from parameter.Definition import BoundaryType
from indicator.line.EIGCV1D import indicate_error_gcv_1D
from indicator.triangle.EIGCV2D import indicate_error_gcv_2D


def indicate_edge_error(grid, region, edge, parameter):
	""" Indicate the true error of the given edge """
	
	# Set parameter mode to local
	parameter.set_local(True)
	
	# Record original parameters
	origin_boundary = parameter.get_boundary()
	origin_data_size = parameter.get_data_size()
	original_alpha = parameter.get_alpha()
	original_run_gcv = parameter.get_run_gcv()

	# Parameters
#	indicator_parameter = parameter.get_indicator_parameter()

	# Boundary of local TPSFEM: 1-Dirichlet, 2-Neumann
#	boundary = indicator_parameter[0]

	# number of layers of the local grid
#	no_layers = indicator_parameter[1]

	# Refine choice: 0-no refine, 1-refine base edge
#	refine_choice = indicator_parameter[2]

	# Error indicator formulation
	# 1-energy norm of the difference between local and global approximation
	# 2-error estimate from Babuska in babuvvska1978error 
#	form = indicator_parameter[3]

	# Whether to run GCV
#	run_gcv = indicator_parameter[4]

	# End nodes of the edge
	endpt1 = edge.get_endpt1()
	node1 = grid.get_node(endpt1)
	endpt2 = edge.get_endpt2()
	node2 = grid.get_node(endpt2)

	# Initialise edge error
	edge_error = 0.0

	# For 1D grid
	if grid.get_dim() == 1:
		edge_error = indicate_error_gcv_1D(grid, region, node1, node2, parameter)

	# For 2D grid
	elif grid.get_dim() == 2:
		edge_error = indicate_error_gcv_2D(grid, region, node1, node2, parameter)

	# For unlisted dimensions
	else:
		print("Warning: invalid dimension")
	
	# Set error indicator of the edge
	grid.set_error_indicator(edge.get_endpt1(),edge.get_endpt2(),edge_error)
	grid.set_error_indicator(edge.get_endpt2(),edge.get_endpt1(),edge_error)

	# Reset parameter mode to global
	parameter.set_local(False)

	# Restore original parameters
	parameter.set_boundary(origin_boundary)
	parameter.set_data_size(origin_data_size)
	parameter.set_alpha(original_alpha)
	parameter.set_run_gcv(original_run_gcv)

	# Reset main grid counter to avoid potential error
	grid.reset_counter()

	return edge_error


def indicate_error(grid, region, base_edges, parameter):
	""" Indicate the true error of the grid """

	# Initialise the error list for error indicators
	error_list = []

	# Indicate error of base edges
	for edge in base_edges:

		# Indicate the error of the edge
		edge_error_indicator = indicate_edge_error(grid, region, edge, parameter)

		# Add the new error to the list
		error_list.append(edge_error_indicator)


	return error_list
