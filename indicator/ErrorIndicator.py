#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-04-16 10:13:44
@Last Modified time: 2023-02-20 22:58:56

This class contains routines to call the corresponding error indicator.

"""

# Import libraries
from adaptive.line.UniformRefinement import build_base_edges as build_base_edges_1D
from adaptive.triangle.UniformRefinement import build_base_edges as build_base_edges_2D
from parameter.Definition import IndicatorType, CostType
from indicator.norm import L_norm
import indicator.EIInf as EIInfinity
import indicator.EITrue as EITrueSoln
import indicator.EINorm as EINorm
import indicator.EIAuxiliary as EIAuxiliary
import indicator.EIResidual as EIResidual
import indicator.EIRecovery as EIRecovery
import indicator.EIGCV as EIGCV
import indicator.EIRegression as EIRegression
import indicator.EIWeight as EIWeight
from gcv.GCVAuxiliary import compute_gcv_auxiliary

from timeit import default_timer


def indicate_edge_error(grid, region, edge, parameter):
	""" Choose the error indicator function and pass the parameters """

	# Parameters
	indicator_type = parameter.get_indicator()
	cost = parameter.get_cost()
	iteration = parameter.get_iteration()

	edge_error = 0.0

	# Set error indicator as 0 if it is on the boundary
	if grid.get_dim() == 2 and indicator_type != 1 and EIWeight.check_boundary_edge(grid, edge):
		return 0.0

	t = default_timer()

	# Return infinity for the edge => uniform refine
	if indicator_type == IndicatorType.infinity:
		edge_error = EIInfinity.indicate_edge_error(grid, region, edge, parameter)

	# Indicate error using true solution
	elif indicator_type == IndicatorType.true:
		edge_error = EITrueSoln.indicate_edge_error(grid, region, edge, parameter)

	# Indicate error using the maximum norm of Dxx
	elif indicator_type == IndicatorType.norm:
		edge_error = EINorm.indicate_edge_error(grid, region, edge, parameter)

	# Indicate error by solving auxiliary problems
	elif indicator_type == IndicatorType.auxiliary:
		edge_error = EIAuxiliary.indicate_edge_error(grid, region, edge, parameter)

	# Indicate error using the residual
	elif indicator_type == IndicatorType.residual:
		edge_error = EIResidual.indicate_edge_error(grid, region, edge, parameter)

	# Indicate error using the post-process gradients
	elif indicator_type == IndicatorType.recovery:
		edge_error = EIRecovery.indicate_edge_error(grid, region, edge, parameter)

	# Indicate error using GCV
	elif indicator_type == IndicatorType.GCV:
		edge_error = EIGCV.indicate_edge_error(grid, region, edge, parameter)

	# Indicate error using regression error
	elif indicator_type == IndicatorType.regression:
		edge_error = EIRegression.indicate_edge_error(grid, region, edge, parameter)

	else:
		print("Warning: invalid indicator")

	if cost is not None:
		cost.append_time(default_timer()-t, CostType.indicate_refine, iteration-1)



#	parameter.add_indicator_list([edge.get_endpt1(),edge.get_endpt2(),edge_error])

	return edge_error


def indicate_error(grid, region, parameter):
	""" Choose the error indicator function and pass the parameters """

	# Indicator type
	indicator_type = parameter.get_indicator()

	# Obtain a list of base edges
	if grid.get_dim() == 1:
		base_edges = build_base_edges_1D(grid)

	elif grid.get_dim() == 2:
		base_edges = build_base_edges_2D(grid)

	else:
		print("Warning: invalid dimension")

	"""
	alpha_list = []
	no_alpha = 10
	for i in range(0, 20):
		alpha = compute_gcv_auxiliary(grid, region, base_edges, parameter, no_alpha)
		alpha_list.append(alpha)
	print "10"
	print alpha_list
	"""

	if indicator_type == 0:
		indicator_type == 1
		parameter.set_indicator(indicator_type)

	# Set all indicators infinity => uniform refine
	if indicator_type == IndicatorType.infinity:
		error_indicator_list = EIInfinity.indicate_error(grid, \
				region, base_edges, parameter)

	# Indicate error using true solution
	elif indicator_type == IndicatorType.true:
		error_indicator_list = EITrueSoln.indicate_error(grid, \
				region, base_edges, parameter)

	# Indicate error using the maximum norm of Dxx
	elif indicator_type == IndicatorType.norm:
		error_indicator_list = EINorm.indicate_error(grid, \
				region, base_edges, parameter)

	# Indicate error by solving auxiliary problems
	elif indicator_type == IndicatorType.auxiliary:
		boundary_type = parameter.get_boundary()
		parameter.set_boundary(1)

		cost = parameter.get_cost()
		t = default_timer()
		compute_gcv_auxiliary(grid, region, base_edges, parameter)
		cost.add_time(default_timer()-t, CostType.gcv_auxiliary)
		
		parameter.set_boundary(boundary_type)

		error_indicator_list = EIAuxiliary.indicate_error(grid, region, base_edges, parameter)

	# Indicate error using the residual
	elif indicator_type == IndicatorType.residual:
		error_indicator_list = EIResidual.indicate_error(grid, region, base_edges, parameter)

	# Indicate error using the post-process gradients
	elif indicator_type == IndicatorType.recovery:
		error_indicator_list = EIRecovery.indicate_error(grid, region, base_edges, parameter)

	# Indicate error using the GCV
	elif indicator_type == IndicatorType.GCV:
		error_indicator_list = EIGCV.indicate_error(grid, region, base_edges, parameter)

	# Indicate error using regression error
	elif indicator_type == IndicatorType.regression:
		error_indicator_list = EIRegression.indicate_error(grid, region, base_edges, parameter)

	else:
		print("Warning: invalid indicator")
		return

	# Norm of the error indicator values
#	error_norm = L_norm(error_indicator_list, parameter.get_norm())
	if len(error_indicator_list) == 0:
		error_L_max = 0
		error_L_one = 0
		error_L_two = 0
	else: 
		error_L_max = L_norm(error_indicator_list, 0)
		error_L_one = L_norm(error_indicator_list, 1)
		error_L_two = L_norm(error_indicator_list, 2)

#	print(error_indicator_list)

#	return error_indicator_list, error_norm
	return error_indicator_list, error_L_max, error_L_one, error_L_two
