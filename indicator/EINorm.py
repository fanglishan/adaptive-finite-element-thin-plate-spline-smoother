#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-05-05 21:53:41
@Last Modified time: 2024-04-10 21:27:26

This error indicator computes the energy norm of the L max norm of the Dxx of 
the current solution. Regions with higher Dxx have higher rates of change and 
require finer elements to achieve a higher accuracy.

"""

# Import libraries
from indicator.line.EINorm1D import approximate_local_Dxx_1D, calculate_Dxx_norm_1D
from indicator.triangle.EINorm2D import approximate_local_Dxx_2D, calculate_Dxx_norm_2D
import indicator.EIWeight as EIWeight
from adaptive.triangle.TriangleFunction import find_triangle_pairs
from adaptive.line.LocalGrid1D import build_local_grid_1D
from adaptive.triangle.LocalGrid2D import build_local_grid_2D
from tpsfem.StopCriteria import stop_refinement
from plot.EINormPlot import plot_Dxy_approximation_2D
from parameter.Definition import CostType
#from estimator.MeasureCost import measure_time
from fileio.WriteResult import write_line

from numpy import sqrt
from timeit import default_timer


def approximate_Dxx_1D(local_grid, node1, node2, parameter, Dx_source, refine_choice, quad_rule):
	""" Approximate Dxx for an edge from a 1D grid """

	# Approximate Dxx in each node on the local grid
	approximate_local_Dxx_1D(local_grid, parameter, Dx_source)

	# Approximat the integral using Dxx
	indicator = calculate_Dxx_norm_1D(local_grid, node1, node2, quad_rule)

	return indicator


def approximate_Dxx_2D(local_grid, node1, node2, parameter, Dx_source, refine_choice, quad_rule):
	""" Approximate Dxx for an edge from a 2D grid """

	# Approximate Dxx in each node on the local grid
	approximate_local_Dxx_2D(local_grid, Dx_source)

	# Determine the triangle pair vertices in the local grid
	node3, node4 = find_triangle_pairs(local_grid, node1.get_node_id(), node2.get_node_id())
	
	# Approximat the integral using Dxx, Dxy and Dyy
	indicator = calculate_Dxx_norm_2D(local_grid, node1, node2, node3, node4, quad_rule)

	return indicator



def indicate_edge_error(grid, region, edge, parameter):
	""" Indicate the true error of the given edge """
	
	# Set parameter mode to local
	parameter.set_local(True)

	# Parameters
	indicator_parameter = parameter.get_indicator_parameter()

	# number of layers of the local grid
	no_layers = indicator_parameter[0]

	# Refine choice: 0-no refine, 1-refine base edge
	refine_choice = indicator_parameter[1]

	# source of Dx: 1-c values, 2-g values, 3-w values
	Dx_source = indicator_parameter[2]

	# quadrature rule: 1-midpoint, 2-trapezoid, 3-simpson
	quad_rule = indicator_parameter[3]

	# No refinement for local grids of norm-based error indicator
	uni_refine = 0

	# Node 1 of the edge
	endpt1 = edge.get_endpt1()
	node1 = grid.get_node(endpt1)

	# Node 2 of the edge
	endpt2 = edge.get_endpt2()
	node2 = grid.get_node(endpt2)

	# Initialise edge error
	edge_error = 0.0

	# For 1D grid
	if grid.get_dim() == 1:

		# Build a local 1D grid
		local_grid, mid_id = build_local_grid_1D(grid, region, node1, node2, \
					parameter, no_layers, refine_choice, uni_refine)
#		# Approximate integral of maximum Dxx
#		edge_error = approximate_Dxx_1D(grid, node1, node2, \
#						parameter, Dx_source, refine_choice, quad_rule)
#		import sys
#		sys.exit()
		# Approximate integral of maximum Dxx
		edge_error = approximate_Dxx_1D(local_grid, node1, node2, \
						parameter, Dx_source, refine_choice, quad_rule)

		"""
		error_tol = 1e-8
		interval = abs(node1.get_coord()[0]-node2.get_coord()[0])
		K = abs(abs(node1.get_d_values()[0]-node2.get_d_values()[0])/interval)
		n = sqrt(K*(interval**3)/(24.0*error_tol))
		print n
		edge_error = n
		"""
	# For 2D grid
	elif grid.get_dim() == 2 and EIWeight.check_boundary_edge(grid, edge):
		edge_error = 0.0

	elif grid.get_dim() == 2:

		# Obtain other vertices of the triangle pairs
		node3, node4 = find_triangle_pairs(grid, endpt1, endpt2)
		
		if stop_refinement(region, parameter, grid, node1, node2, node3, node4):
			edge_error = 0.0

		else:
			# Build a local 2D grid
			local_grid, mid_id = build_local_grid_2D(grid, region, node1, node2, \
							node3, node4, parameter, no_layers, refine_choice)

			# Approximate integral of maximum Dxx
			edge_error = approximate_Dxx_2D(local_grid, node1, node2, \
							parameter, Dx_source, refine_choice, quad_rule)
			"""
			edge_error = approximate_Dxx_2D(grid, node1, node2, \
							parameter, Dx_source, refine_choice, quad_rule)			
			plot_Dxy_approximation_2D(grid, parameter, 0)
			plot_Dxy_approximation_2D(grid, parameter, 1)
			plot_Dxy_approximation_2D(grid, parameter, 2)
			"""
	# For unlisted dimensions
	else:
		print("Warning: indicator/EINorm - invalid dimension")

	# Set error indicator of the edge
	grid.set_error_indicator(edge.get_endpt1(),edge.get_endpt2(),edge_error)
	grid.set_error_indicator(edge.get_endpt2(),edge.get_endpt1(),edge_error)

	parameter.set_local(False)
	
	# Reset main grid counter to avoid potential error
	grid.reset_counter()

	edge_error = EIWeight.add_data_weights(grid, edge, parameter, edge_error)
	
	return edge_error


def indicate_error(grid, region, base_edges, parameter):
	""" Indicate the true error of the grid """

	# Initialise the error list for error indicators
	error_list = []

	# Cost record
	cost = parameter.get_cost()

	iteration = parameter.get_iteration()
	cost.add_time([], CostType.indicate)

	write_line(parameter, "number of base edges: "+str(len(base_edges)), True)
	time_list = []

	# Indicate error of base edges
	for edge in base_edges:

		t = default_timer()
		# Indicate the error of the edge
		edge_error_indicator = indicate_edge_error(grid, region, edge, parameter)
		cost.append_time(default_timer()-t, CostType.indicate, iteration)

		time_list.append(default_timer()-t)

		# Add the new error to the list
		error_list.append(edge_error_indicator)

	write_line(parameter, "error indicator average: "+str(sum(time_list)/len(time_list)), True)


	return error_list
