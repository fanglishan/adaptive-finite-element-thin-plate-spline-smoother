"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-04-16 17:10:41
@Last Modified time: 2024-09-28 23:45:53

This class creates datasets and store them in the data folder.

Dataset name
1:dimension
2:model problem
3:distribution
4:noise level
5:data size

"""

# Import libraries
from fileio.DataFile import read_file, write_file
from data.DatasetManager import obtain_dataset, set_model, set_noise, set_size, set_distribution, set_noise_distribution
from data.BuildData1D import build_data_1D
from data.BuildData2D import build_data_2D
from data.BuildData3D import build_data_3D
from plot.DataPlot import plot_data, plot_contour, plot_data_dist, plot_data_hist
import plot.DataPlotly as dataPlotly

def build_dataset(dataset):
	""" Build a dataset """

	data = []
	
	# Dimension of data
	d = int(str(dataset)[0])

	model_problem = int(str(dataset)[1:3])
	
	# Number of data points (in each direction)
	n = set_size(dataset)

	# Model problem
	u = set_model(dataset)

	# Data distribution
	distribution = set_distribution(dataset)

	# Noise distribution
	noise_dist = set_noise_distribution(dataset)

	# Noise size
	noise_size = set_noise(dataset)

	# outlier_size
	outlier_no = 0
	outlier_size = 0.0

	if d == 1:
		data = build_data_1D(model_problem, n, u, noise_dist, noise_size, distribution)

	elif d == 2:
		data = build_data_2D(model_problem, n, u, noise_dist, noise_size, distribution)

	elif d == 3:
		data = build_data_3D(model_problem, n, u, noise_dist, noise_size, distribution)


	write_file(data, str(dataset))

	return data


def print_info(data):

	x1_max = -100000000000.0
	x1_min = 100000000000.0
	x2_max = -100000000000.0
	x2_min = 100000000000.0
	y_max = -100000000000.0
	y_min = 100000000000.0
	y_average = 0
	for point in data:
		x1_max = max(point[0], x1_max)
		x1_min = min(point[0], x1_min)
		x2_max = max(point[1], x2_max)
		x2_min = min(point[1], x2_min)
		y_max = max(point[2], y_max)
		y_min = min(point[2], y_min)
		y_average += point[2]
	if len(data) != 0:
		y_average = y_average/len(data)

	print("#data:", len(data))
	print("x1:", x1_min, "to", x1_max)
	print("x2:", x2_min, "to", x2_max)
	print("y:", y_min, "to", y_max)
	print("y average:", y_average)
	print("")
#Dataset name
#1:dimension
#2:model problem
#3:distribution
#4:noise level
#5:data size

# 1D Model problem
# zero=0, linear=1, quadratic=2, cubic=3, exp=4
# sine=5, osci_sine=6, grad_exp=7, steep_exp=8
# exp_fluc=9, exp_mul=10, frac=11, shift=12, runge=13
# blocks=14, bumps=15, Heavisine=16, Doppler=17, Mexican_hat=18

# 2D Model problem
# zero=0, linear=1, quadratic=2, cubic=3, exp=4
# sine=5, osci_sine=6, grad_exp=7, steep_exp=8
# exp_fluc=9, exp_mul=10, sin_mul=11, hole=12
# franke=13, tanh=14, cone=15, peak=16

# Real-world data sets
# 1-magnetic data, 2-bone data, 3-lake data, 4-lena data
# 5-crater data, 6-iron mine, 7-iron mountain, 8-Aeromagnetic Burney
# 9-Grapevine Canyon, 10-river mouth, 11-black beach

# Data distribution
# 1-uniform, 2-uniform with a hole, 3-uniformly random
# 4-normally random, 5-line data

# Noise distribution
# 1-uniform, 2-normal

# Noise level
# uniform:     0-0.0, 1-0.001, 2-0.005, 3-0.01, 4-0.03, 
#              5-0.05, 6-0.06, 7-0.08, 8-0.09, 9-0.1
# normal (sd): 0-0.0, 1-0.005, 2-0.01, 3-0.02, 4-0.05, 
#              5-0.07, 6-0.1, 7-0.15, 8-0.2, 9-0.25

# Data size 
# 0-3, 1-10, 2-20, 3-50, 4-100, 5-250
# 6-500, 7-750, 8-1000, 9-1500, 10-2000

#dataset = 2181204
#dataset = 3012201
#dataset = 10
dataset = 5

#if len(str(dataset)) > 3:
#	build_dataset(dataset)
	
# Whether two subset data
data = obtain_dataset(dataset, True)

plot_after = True
#plot_after = False

#if len(str(dataset)) < 3:
#print_info(data)


if plot_after:
	plot_data(data)
#	dataPlotly.plot_data_2D(data)
#	plot_contour(data)
#	plot_data_dist(data)
	pass






