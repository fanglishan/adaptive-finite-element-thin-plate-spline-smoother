#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-06-13 10:13:44
@Last Modified time: 2022-12-13 09:40:11

This class plots a TPSFEM solution from a stored grid in a file.

"""

# Import libraries
from build.CopyGrid import copy_grid
from grid.NodeTable import node_iterator
from adaptive.triangle.GridValue import cal_smoother_2d
from function.FunctionManager import obtain_model_function
from parameter.Parameter import Parameter
from parameter.ModelProblemFunction import find_model_problem
from estimator.Error import Error
from fileio.GridFunction import read_grid



def plot_contour(grid_1, grid_2):
	""" Plot 2D TPSFEM contour """

	import matplotlib.pyplot as plt
	from mpl_toolkits.mplot3d import Axes3D
	from matplotlib import cm
	from matplotlib.ticker import LinearLocator, FormatStrFormatter
	from scipy.interpolate import griddata
	from numpy import mgrid, array
	import numpy as np
	import time

#	plt.clf()
	
	fig = plt.figure("Contour map")
	ax = plt.axes()

	x = []
	y = []
	z = []

	u = []
	v = []

	gran = 100.0

	for i in range(1,int(gran),1):
		for j in range(1,int(gran),1):
			u.append(i/gran)
			v.append(j/gran)

	for node in node_iterator(grid_1):
		coord = node.get_coord()
		u.append(coord[0])
		v.append(coord[1])

	for i in range(0, len(u)):
		if not (u[i] < 0.5 and v[i] > 0.5):
			x.append(u[i])
			y.append(v[i])


	value_min = 10000000000.0
	value_max = -10000000000.0

	for i in range(0, len(x)):
		coord = [x[i], y[i]]


		value1 = cal_smoother_2d(grid_1, coord)
		value2 = cal_smoother_2d(grid_2, coord)
		diff = value1-value2

		z.append(diff)

		value_min = min(value_min, diff)
		value_max = max(value_max, diff)

	value_min = -48.0
	value_max = 75
	print(value_min,value_max)

	# Determine the levels of the contour map
	interval = abs(value_min-value_max)*1.2

	levels = np.arange(value_min-interval/10.0,value_max+interval/10.0,interval/30.0)
#	levels = np.arange(value_min,value_max,interval/30.0)
#	levels = np.arange(value_min,value_max,interval/30.0)

	# Plot contour map
	ax.tricontour(x, y, z, levels=levels, linewidths=0.2, colors='k')
	cntr = ax.tricontourf(x, y, z, levels=levels, cmap=cm.coolwarm)
#	cntr = ax.tricontourf(x, y, z, levels=levels, cmap="gray")
#	cntr = ax.tricontourf(x, y, z, cmap=cm.coolwarm)
#	cntr = ax.tricontourf(x, y, z, cmap="gray")

	fig.colorbar(cntr, ax=ax)

#	plt.xlim(0.4, 0.5)
#	plt.ylim(0.42, 0.52)
#	plt.savefig("figure_3.png")

#	plt.xlim(0.4, 0.5)
#	plt.ylim(0.42, 0.52)
#	plt.savefig("tpsfem_5.png")
#	plt.show()
	"""
	if len(str(dataset)) == 1:
		plt.xlim(0.0, 1.0)
		plt.ylim(0.0, 1.0)
	else:
		plt.xlim(min(x), max(x))
		plt.ylim(min(y), max(y))
	"""
	plt.xlim(0.0, 1.0)
	plt.ylim(0.0, 1.0)
	plt.show()


# Obtain grid for error estimation
#path = "result/" + "result_1_6_20_02_01_11_02" + "/"
#filename = "1_5677"#path[14:19]
#filename = "1_177"
path = "result/coast_final_grids/" 
#path = "result/" + "result_5_4_21_03_20_14_11" + "/"
#filename = path[14:21]
#filename = "1081200"
filename1 = "12_6416"
filename2 = "12_5818"
#filename = "9_7254"
#filename = "9_226"
#filename = "5_8321"
#filename = "2135204_6592"

d = 2


grid1 = read_grid(filename1, path)
grid2 = read_grid(filename2, path)


plot_contour(grid1, grid2)