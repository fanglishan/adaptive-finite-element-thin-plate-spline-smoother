#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2021-08-07 19:41:30
@Last Modified time: 2023-05-26 00:15:50

This class contains routines to sample a subset from the original large
data set. The routines to extract knots from data using quad-tree were
developed by Linda Stals and added to in program on 2020/08/07.

"""


from random import sample
from numpy import zeros, array



def get_knot_set_level(x, y, z, level):
	"""
	Viewing the data set as a quad-tree, extract a set of knots from the lake 
	data for the current level
	
	Input: x, y = the x and y coordinates of the data
		   z = one dim variable containing the data value
		   level = level in quad tree

	Output: x_l, y_l = x and y coordinates of knots from current level
			z_l = one dim variable containing the knot value
	"""              
	
	from random import randint, seed

	# set the seed so each run picks the same values
	
	seed(1)

	# find the number of data points
	
	m0 = x.shape[0]
	
	# if current quadrants does not contain any data, return empty sets
	
	if m0 == 0:
		x_l = []
		y_l = []
		z_l = []
		return x_l, y_l, z_l
			
	# if at bottom level of tree, pick one value from the quadrant
	
	if level == 0:
		i = randint(0, m0-1)
		x_l = [x[i]]
		y_l = [y[i]]
		z_l = [z[i]]
		
	# else subdivide data into quadrants and find the knots in each quadrant
	
	else:
		
		# find the domain 
		
		min_x = min(x)
		max_x = max(x)
		min_y = min(y)
		max_y = max(y)
		
		# split the domain at these points
		
		q_x = (min_x+max_x)/2.0
		q_y = (min_y+max_y)/2.0
		
		# find one quadrant and extract the knots
		
		index_1 = x < q_x
		x_1 = x[index_1]
		y_1 = y[index_1]
		z_1 = z[index_1]
		
		index_2 = y_1 < q_y
		x_2 = x_1[index_2]
		y_2 = y_1[index_2]
		z_2 = z_1[index_2]
		x_l, y_l, z_l = get_knot_set_level(x_2, y_2, z_2, level-1)
		 
		# find the second quadrant and extract the knots
		
		index_2 = y_1 >= q_y
		x_2 = x_1[index_2]
		y_2 = y_1[index_2]
		z_2 = z_1[index_2]
		x_q, y_q, z_q = get_knot_set_level(x_2, y_2, z_2, level-1)
		x_l = x_l+x_q
		y_l = y_l+y_q
		z_l = z_l+z_q    

		# find the third quadrant and extract the knots
		
		index_1 = x >= q_x
		x_1 = x[index_1]
		y_1 = y[index_1]
		z_1 = z[index_1]
		
		index_2 = y_1 < q_y
		x_2 = x_1[index_2]
		y_2 = y_1[index_2]
		z_2 = z_1[index_2]
		x_q, y_q, z_q = get_knot_set_level(x_2, y_2, z_2, level-1)
		x_l = x_l + x_q
		y_l = y_l + y_q
		z_l = z_l + z_q

		# find the final quadrant and extract the knots
		
		index_2 = y_1 >= q_y
		x_2 = x_1[index_2]
		y_2 = y_1[index_2]
		z_2 = z_1[index_2]
		x_q, y_q, z_q = get_knot_set_level(x_2, y_2, z_2, level-1)
		x_l = x_l + x_q
		y_l = y_l + y_q
		z_l = z_l + z_q    
		
	return x_l, y_l, z_l


def get_knot_set(X, f, m0):
	"""
	Extract a set of knots from the lake data
	
	Input: X = two dim variable containing the x and y coordinates of the data
		   f = one dim variable containing the data value
		   m0 = an upper bound on the number of knots points (as a result of
		   rounding etc, the final number of knots may be less)
			
	Output: X_knot = two dim variable containing the x and y coordinates of 
			the knots
			f = one dim variable containing the knot value
	"""              
	from numpy import zeros
	from numpy import sqrt
	from math import log2
	
	# find, approx, the number of knots
	
	m0_s = int(sqrt(m0))
	no_levels = int(log2(m0_s))
	print ('no_levels', m0, m0_s, no_levels)
	
	# get the knots as a list
	
	x_l, y_l, z_l = get_knot_set_level(X[:, 0], X[:, 1], f[:, 0], no_levels)
	m0 = len(x_l)
	
	# convert the results into an array
	
	X_knot = zeros((m0, 2))
	f_knot = zeros((m0, 1))
	X_knot[:, 0] = x_l
	X_knot[:, 1] = y_l
	f_knot[:, 0] = z_l
			
	return X_knot, f_knot



def extract_quadtree(data, no_m0):
	""" Extract data using quad-tree (Stals) """

	x = []
	y = []
	z = []
	for point in data:
		x.append(float(point[0]))
		y.append(float(point[1]))
		z.append(float(point[2]))
	x = array(x)
	y = array(y)
	z = array(z)

	X = zeros((len(x), 2))
	f = zeros((len(z), 1))
	X[:, 0] = x
	X[:, 1] = y
	f[:, 0] = z

	# m0 is the number of levels and no_basis approx 2**no_m0
	X_knot, f_knot = get_knot_set(X, f, 2**no_m0)

#	print(X_knot.shape, X_knot.shape)
	data_knot = []
	for i in range(X_knot.shape[0]):
		data_knot.append([X_knot[i,0],X_knot[i,1],f_knot[i,0]])

	print("Extract",len(data_knot),"data points")
	
	return data_knot


def exclude_data(data):
	""" Exclude data points in some regions """

	new_data = []

	for point in data:

		if not (point[0] > -1.9 and point[0] < 1.9 and point[1] > -1.9 and point[1] < 1.9):

			new_data.append(point)

	return new_data


def sample_basis(data, no_basis):
	""" Sample a given number of basis from data """
	
	data_size = len(data)
	if data_size <= no_basis:
		return data
	else:
		return sample(data, k=no_basis)
	
"""
#	data = exclude_data(data)

	no_m0 = 11

	# Extract data using quad-tree
	# Currently only work for 2D predictor values
	data = extract_quadtree(data, no_m0)

	if len(data) <= no_basis:
		return data
	else:
		return sample(data, k=no_basis)
"""
