# -*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2024 Fang

@Author: Fang
@Date:   2024-10-06 23:14:25
@Last Modified by:   Fang
@Last Modified time: 2024-10-07 15:43:54

This class contains routines for interpolating scattered data using scipy
rbf interpolator.

"""

import rbf.RBFKernel as rbfKernel

from scipy.interpolate import Rbf
from numpy import zeros, log, sqrt


def build_RBF(data,kernel,alpha,epsilon):

	# epsilon is not utilised so far

	# Determine kernel
	if kernel == 1:
		func = 'multiquadric'
	elif kernel == 2:
		func = 'inverse'
	elif kernel == 3:
		func = 'gaussian'
	elif kernel == 4:
		func = 'linear'
	elif kernel == 5:
		func = 'cubic'
	elif kernel == 6:
		func = 'quintic'
	elif kernel == 7:
		func = 'thin_plate'
	elif kernel == 8:
		func = rbfKernel.tps2
	elif kernel == 9:
		func = rbfKernel.wendland1
	elif kernel == 10:
		func = rbfKernel.wendland2
	elif kernel == 11:
		func = rbfKernel.wendland3

	# Process data
	x = []
	y = []
	for point in data:
		x.append(point[0])
		y.append(point[-1])

	rbfi = Rbf(x,y,function=func,smooth=alpha)

	return rbfi