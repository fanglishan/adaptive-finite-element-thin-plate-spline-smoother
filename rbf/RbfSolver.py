#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2021-08-01 20:43:47
@Last Modified time: 2024-10-06 22:43:03

This class contains modules that solves the RBF system.

"""

# Import libraries
from rbf.Kernel import kernel

from numpy import zeros, log, sqrt
from numpy.linalg import solve
from scipy.sparse import lil_matrix, csr_matrix, csc_matrix
from scipy.sparse.linalg import spsolve
from timeit import default_timer



def rbf(data, alpha, d, b, calc_stat):
	""" Build radial basis interpolation """

	deriv = 0

	# Length of dataset
	n = len(data)

	# dimension of matrix
	# A:n*n; P:n*(d+1)
	dimension = n+d+1

	# Initialise the matrix and vector
	if b == 1 or b == 9:
		matrix = zeros(shape=(dimension,dimension))
	else:
		matrix = lil_matrix((dimension,dimension),dtype=float)
	vector = zeros(shape=(dimension,1))

	# Iterate through submatrix A
	# j are columns and i are rows
	for j in range(0,n):
		for i in range(0,n):

			# Add smoothing effects in diagonal entries
			if i == j:
				matrix[i,i] = n*alpha
			# Calculate entries for radial basis functions
			else:
				matrix[i,j] = kernel(data[i][:-1],data[j][:-1], b, deriv)

	# submatrix for low order polynomial
	# const - x1 - x2 - x3
	for i in range(0,n):
		
		# Set P and P^T submatrix
		matrix[i,n] = 1
		matrix[n,i] = 1

		# Set P and P^T polynomial coefficient 
		for k in range(0, d):
			matrix[i,n+k+1] = data[i][k]
			matrix[n+k+1,i] = data[i][k]

	# Add vector entries
	for i in range(0, n):
		vector[i][0] = data[i][-1]
	
	if calc_stat:
		# Calculate sparsity
		tol = 1e-8
		count = 0.0
		for i in range(0, n):
			for j in range(0, n):
				if abs(matrix[i,j]) < tol:
					matrix[i,j] = 0
					count += 1
				
	print("Dimension:", dimension)
	print("Sparsity(%):", "{:.2f}".format((count/(dimension**2)*100)))

#	print("Solve system")

	t = default_timer()
	# Solve RBF system
	if b == 1 or b == 9:
		rbf_coef = solve(matrix, vector).transpose()[0]
	else:
		rbf_coef = spsolve(csc_matrix(matrix), vector)
#	print(rbf_coef)
#	print("Solve time:", default_timer()-t)

	return rbf_coef

