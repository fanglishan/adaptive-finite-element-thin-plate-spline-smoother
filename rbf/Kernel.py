#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2021-08-02 11:12:55
@Last Modified time: 2024-10-07 13:26:32

This class contains functions that define kernels of radial basis functions.
 
"""

# Import libraries
from numpy import zeros, log, sqrt
import numpy as np


def norm(x_i, x_j):
	""" Calculate Euclidean norm """

	value = 0.0
	# Add up norm in each dimension
	for i in range(0,len(x_i)):
		value += (x_i[i]-x_j[i])**2
			
	return sqrt(value)


def tps(r):
	""" Thin plate spline r^2*log(r) """
	return r**2*log(r)

def tps_x(r,x_i,x_j):
	""" Derivative wrt x1 of TPS """
#	return sqrt(r)*(2.0*log(r)+1.0)*(x_i[0]-x_j[0])
	return (x_i[0]-x_j[0])+2.0*(x_i[0]-x_j[0])*log(r)


def tps_y(r,x_i,x_j):
	""" Derivative wrt x2 of TPS """
#	return sqrt(r)*(2.0*log(r)+1.0)*(x_i[1]-x_j[1])
	return (x_i[1]-x_j[1])+2.0*(x_i[1]-x_j[1])*log(r)

def tps_xxyy(r,x_i,x_j):
	""" Derivative (second-order) of higher-order TPS """
	return 4.0*log(r) + 4.0


def tps2(r):
	""" Thin plate spline (higher-order) r^4*log(r) """
#	if r == 0:
#		return 0
	v = r**4*log(r)
	return np.nan_to_num(v)

def tps2_x(r,x_i,x_j):
	""" Derivative wrt x1 of higher-order TPS """
	return (x_i[0]-x_j[0])*r**2*(4*log(r)+1)

def tps2_y(r,x_i,x_j):
	""" Derivative wrt x2 of higher-order TPS """
	return (x_i[1]-x_j[1])*r**2*(4*log(r)+1)

def tps2_xxyy(r,x_i,x_j):
	""" Derivative (second-order) of higher-order TPS 2 """
	return 8*r**2*(2*log(r)+1)


def triharmonic(r):
	""" Triharmonic r^3"""
	return r**3


def wendland1(r,r_s):
	""" Wendland_1 (1−r)^2 """
	r = r/r_s
	if r <= 1.0:
		return (1.0-r)**2
	else:
		return 0.0

def wendland1_x(r,r_s,x_i,x_j):
	""" Wendland_1 wrt x1 """
	r = r/r_s
	if r <= 1.0:
		return 2*(r-1.0)*(x_i[0]-x_j[0])/r
	else:
		return 0.0

def wendland1_y(r,r_s,x_i,x_j):
	""" Wendland_1 wrt x2 """
	r = r/r_s
	if r <= 1.0:
		return 2*(r-1.0)*(x_i[1]-x_j[1])/r
	else:
		return 0.0

def wendland1_xxyy(r,r_s,x_i,x_j):
	""" Wendland_1 second-order derivative """
	r = r/r_s
	if r <= 1.0:
		return 2.0*(r-1)/r+2.0
	else:
		return 0.0


def wendland2(r,r_s):
	""" Wendland_2 (1−r)^4*(4r+1) """
	r = r/r_s
	if r <= 1.0:
		return (1.0-r)**4*(4*r+1.0)
	else:
		return 0.0

def wendland2_x(r,r_s,x_i,x_j):
	""" Wendland_2 wrt x1 """
	r = r/r_s
	if r <= 1.0:
		return -20.0*(1.0-r)**3*(x_i[0]-x_j[0])
	else:
		return 0.0

def wendland2_y(r,r_s,x_i,x_j):
	""" Wendland_2 wrt x2 """
	r = r/r_s
	if r <= 1.0:
		return -20.0*(1.0-r)**3*(x_i[1]-x_j[1])
	else:
		return 0.0

def wendland2_xxyy(r,r_s,x_i,x_j):
	""" Wendland_2 second-order derivative """
	r = r/r_s
	if r <= 1.0:
		return 4.0*(1.0-r)**4/r-32.0*(1.0-r)**3-4.0*(1.0-r)**3*(4.0*r+1.0)/r+12.0*(1.0-r)**2*(4.0*r+1.0)
	else:
		return 0.0


def wendland3(r,r_s):
	""" Wendland_3 (1−r)^6*(35**2+18r+3) """
	r = r/r_s
	if r <= 1.0:
		return (1.0-r)**6*(35.0*r**2+18.0*r+3.0)
	else:
		return 0.0

def wendland3_x(r,r_s,x_i,x_j):
	""" Wendland_3 (1−r)^6*(35**2+18r+3) """

	r = r/r_s

	if r <= 1.0:
		return (1.0-r)**5*(-6*(35.0*r**2+18.0*r+3.0)+(1.0-r)*(70.0*r+18.0))*(x_i[0]-x_j[0])/sqrt(r)
	else:
		return 0.0

def wendland3_y(r,r_s,x_i,x_j):
	""" Wendland_3 (1−r)^6*(35**2+18r+3) """

	r = r/r_s

	if r <= 1.0:
		return (1.0-r)**5*(-6*(35.0*r**2+18.0*r+3.0)+(1.0-r)*(70.0*r+18.0))*(x_i[1]-x_j[1])/sqrt(r)
	else:
		return 0.0


def kernel(x_i, x_j, b, deriv):
	""" Manage kernels of radial basis functions"""

	# Derivative dxy
	u = 0     # 0-c
	dx = 1    # 1-g_1
	dy = 2    # 2-g_2
	dxxyy = 3    # 2-w*-alpha

	# Validate dimension of two coordinates
	if len(x_i) != len(x_j):
		print("Warning: tps/Kernel")

	if x_i == x_j:
		return 0.0

	# Radius of support for functions with compact support
	r_s = 0.2

	# Euclidean distance between i and j
	r = norm(x_i,x_j)

	# Calculate radial basis function
	if b == 1:
		if len(x_i) == 1:
			if deriv == u:
				return tps(r)
			elif deriv == dx:
				return tps_x(r,x_i,x_j)
			elif deriv == dxxyy:
				return tps_xxyy(r,x_i,x_j)
			
		elif len(x_i) == 2:
#			if r < 1e-14:
#				return 0.0
			if deriv == u:
				return tps(r)
			elif deriv == dx:
				return tps_x(r,x_i,x_j)
			elif deriv == dy:
				return tps_y(r,x_i,x_j)
			elif deriv == dxxyy:
				return tps_xxyy(r,x_i,x_j)

		elif len(x_i) == 3:
			return triharmonic(r)

	elif b == 2:
#		if r < 1e-14:
#			return 0.0
		if deriv == u:
			return tps2(r)
		elif deriv == dx:
			return tps2_x(r,x_i,x_j)
		elif deriv == dy:
			return tps2_y(r,x_i,x_j)
		elif deriv == dxxyy:
			return tps2_xxyy(r,x_i,x_j)

	elif b == 3:
		if deriv == u:
			return wendland1(r,r_s)
		elif deriv == dx:
			return wendland1_x(r,r_s,x_i,x_j)
		elif deriv == dy:
			return wendland1_y(r,r_s,x_i,x_j)
		elif deriv == dxxyy:
			return wendland1_xxyy(r,x_i,x_j)

	elif b == 4:
		if deriv == u:
			return wendland2(r,r_s)
		elif deriv == dx:
			return wendland2_x(r,r_s,x_i,x_j)
		elif deriv == dy:
			return wendland2_y(r,r_s,x_i,x_j)
		elif deriv == dxxyy:
			return wendland2_xxyy(r,x_i,x_j)

	elif b == 5:
		if deriv == u:
			return wendland3(r,r_s)
		elif deriv == dx:
			return wendland3_x(r,r_s,x_i,x_j)
		elif deriv == dy:
			return wendland3_y(r,r_s,x_i,x_j)

	else:
		print("Unknown kernel")


