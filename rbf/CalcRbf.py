# -*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang
@Date:   2022-04-06 22:31:44
@Last Modified by:   Fang
@Last Modified time: 2023-05-14 16:25:05

This class contains modules to calculate RBF interpolated values.

"""

# Import libraries
from rbf.Kernel import kernel


def rbf_calculate(rbf_values, data, coord, d, b):
	""" Calculate predicted value using RBF kernels """

	deriv = 0

	# Initialise a predicted value
	predicted_value = 0.

	# Add up effects from each radial basis
	for i in range(0,len(data)):
		predicted_value += rbf_values[i]*kernel(data[i][:-1],coord, b, deriv)

	# Add polynomial coefficient x_i*coordinate value
	for i in range(0, d):
		predicted_value += rbf_values[-d+i]*coord[i]

	# Add the polynomial const value
	predicted_value += rbf_values[-d-1]
	
	if isinstance(predicted_value, list):
		return predicted_value[0]
	else:
		return predicted_value


def rbf_calculate_deri(data, rbf, coord, d, b, deriv):
	""" Calculate 2D TPS u, ux or uy values """

	# Initialise a predicted value
	value = 0.

	# Derivative dxy
	u = 0       # 0-c
	dx = 1      # 1-g_1
	dy = 2      # 2-g_2
	dxxyy = 3   # 3-w

	# Add up effects from each radial basis
	for i in range(0,len(data)):
		value += rbf[i]*kernel(data[i][:-1],coord, b, deriv)

	if deriv == u:
		# Add polynomial coefficient x_i*coordinate value
		for i in range(0, d):
			value += rbf[-d+i]*coord[i]
		# Add the polynomial const value
		value += rbf[-d-1]

	elif deriv == dx:
		value += rbf[-d]

	elif deriv == dy:
		value += rbf[-d+1]

	if isinstance(value, list):
		return value[0]
	else:
		return value