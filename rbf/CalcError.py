#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2021-08-28 10:54:40
@Last Modified time: 2023-05-14 17:07:37

Calculate errors for the RBFs.

"""

# Import libraries
from rbf.CalcRbf import rbf_calculate, rbf_calculate_deri

from numpy import sqrt



def calc_errors(data, sampled_data, rbf_values, d, b):
	""" Calculate regression errors of RBF interpolation """

	output_results = False

	# Types of regression errors
	RMSE = 0.0
	MAE = 0.0
	ME = 0.0
	MAX = 0.0
	MIN = 0.0


	for i in range(len(data)):
		# Estimate data error
		estimation = rbf_calculate(rbf_values, sampled_data, data[i][:-1], d, b)
		error = estimation - data[i][-1]

		RMSE += error**2
		MAE += abs(error)
		ME += error

		MAX = max(MAX, error)
		MIN = min(MIN, error)

	# Normalise errors
	data_count = len(data)

	RMSE = sqrt(RMSE/data_count)
	MAE = MAE/data_count
	ME = ME/data_count

	if output_results:
		print("RMSE:", RMSE)
		print("MAE:", MAE)
		print("ME:", ME)
		print("MAX:", MAX)
		print("MIN:", MIN)

	return RMSE, MAE, ME, MAX, MIN


def calc_errors_tps_deri(data, sampled_data, rbf_values, d, b, deriv, f):
	""" Calculate errors of TPS's u, ux or uy approximations """

	output_results = False

	# Types of regression errors
	RMSE = 0.0
#	MAE = 0.0
#	ME = 0.0
#	MAX = 0.0
#	MIN = 0.0
	diff = []

	for i in range(len(data)):
		# Estimate data error
		estimation = rbf_calculate_deri(sampled_data, rbf_values, data[i][:-1], d, b, deriv)

#		estimation = rbf_calculate(rbf_values, sampled_data, data[i][:-1], d, b)
#		print(data[i][-1], f(data[i][:-1]))
		if deriv == 0 or deriv == 3:
			error = estimation-f(data[i][:-1])
		else:
			error = estimation+f(data[i][:-1])

		diff.append([data[i][0],data[i][1],error])

		RMSE += error**2
#		MAE += abs(error)
#		ME += error

#		MAX = max(MAX, error)
#		MIN = min(MIN, error)
	
	from plot.DataPlot import plot_contour, plot_data
#	plot_data(diff)
#	plot_contour(diff)

	# Normalise errors
	data_count = len(data)

	RMSE = sqrt(RMSE/data_count)
#	MAE = MAE/data_count
#	ME = ME/data_count

	if output_results:
		print("RMSE:", RMSE)
#		print("MAE:", MAE)
#		print("ME:", ME)
#		print("MAX:", MAX)
#		print("MIN:", MIN)

#	return RMSE, MAE, ME, MAX, MIN
	return RMSE
