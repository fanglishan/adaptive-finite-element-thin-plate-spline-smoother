# -*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2024 Fang

@Author: Fang
@Date:   2024-10-07 15:48:10
@Last Modified by:   Fang
@Last Modified time: 2024-10-07 16:18:01

This class estimates errors of a RBF interpolant.

"""

import numpy as np


def estimate_errors(data, rbfi):
	""" Calculate regression errors of RBF interpolation """

	output_results = False

	# Normalise errors
	data_count = len(data)

	# Types of regression errors
	RMSE = 0.0
	MAE = 0.0
	ME = 0.0
	MAX = 0.0
	MIN = 0.0

	for i in range(0, data_count):
		error = (rbfi(data[i][:-1]) - data[i][-1])[0]

		RMSE += error**2
		MAE += abs(error)
		ME += error
		MAX = max(MAX, error)
		MIN = min(MIN, error)

	RMSE = np.sqrt(RMSE/data_count)
	MAE = MAE/data_count
	ME = ME/data_count

	if output_results:
		print("RMSE:", RMSE)
		print("MAE:", MAE)
		print("ME:", ME)
		print("MAX:", MAX)
		print("MIN:", MIN)

	return RMSE, MAE, ME, MAX, MIN