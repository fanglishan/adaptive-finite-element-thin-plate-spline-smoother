# -*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2024 Fang

@Author: Fang
@Date:   2024-10-07 14:00:22
@Last Modified by:   Fang
@Last Modified time: 2024-10-07 15:48:35

This class contains functions that define kernels of radial basis functions.

"""

import numpy as np


# radius of support for CSRBFs
r_s = 0.1


def limit_radius(r,v):
	s = r > 1.0
	for i in range(0,s.shape[0]):
		for j in range(0,s.shape[1]):
			if s[i,j]:
				v[i,j] = 0
	return v


def tps2(r):
	""" Thin plate spline (higher-order) r^4*log(r) """
	v = r**4*np.log(r)
	return np.nan_to_num(v)


def wendland1(r):
	""" Wendland_1 (1−r)^2 """
	r = r/r_s
	v = (1.0-r)**2

	return limit_radius(r,v)


def wendland2(r):
	""" Wendland_2 (1−r)^4*(4r+1) """
	r = r/r_s
	v = (1.0-r)**4*(4*r+1.0)

	return limit_radius(r,v)


def wendland3(r):
	""" Wendland_3 (1−r)^6*(35**2+18r+3) """
	r = r/r_s
	v =  (1.0-r)**6*(35.0*r**2+18.0*r+3.0)

	return limit_radius(r,v)