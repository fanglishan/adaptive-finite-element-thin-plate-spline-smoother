#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-04-21 23:24:32
@Last Modified time: 2023-09-05 23:17:35

This class contains routines to select data in elements from
a data region.

"""

# Import libraries
from grid.NodeTable import node_iterator
from grid.EdgeTable import endpt_iterator
from adaptive.triangle.Triangle import triangle_iterator_ref
from data.RegionFunction import obtain_region_index
from data.SelectRegion import select_region_2D
from data.DataThinning import subset_data_1D, subset_data_2D
from adaptive.triangle.SetPolynomial import set_polynomial_linear_2D


def choose_data_1d(data, data_usage, node1, node2):
	""" Obtain a set of data points for interval 1-2 """
	
	# Initialise a data list
	chosen_data = []

	# Iterate through each data point in the interval and add to the list
	for i in range(0, len(data)):
		if data_usage[i]:
			continue

		point = data[i][0]
		x1 = node1.get_coord()[0]
		x2 = node2.get_coord()[0]

		# Add data points that are in the interval
		if ((point >= x1) and (point <= x2)) or \
			((point >= x2) and (point <= x1)):
			chosen_data.append(data[i])
			data_usage[i] = True
				
	return chosen_data


def choose_data_2d(data, data_usage, node1, node2, node3, tol=0.0):
	""" Obtain a set of data points for triangle 1-2-3 """

	# Initialise a data list and corresponding u list
	chosen_data = []

	# Polynomials of the triangle
	poly1 = set_polynomial_linear_2D(node1, node2, node3)
	poly2 = set_polynomial_linear_2D(node2, node3, node1)
	poly3 = set_polynomial_linear_2D(node3, node1, node2)

	# Iterate through each data point in the triangle and add to the list
	for i in range(0, len(data)):
		if data_usage[i]:
			continue

		x1 = data[i][0]
		x2 = data[i][1]

		# Add data points that are in the triangle
		if poly1.eval(x1, x2)>=tol and poly2.eval(x1, x2)>=tol and poly3.eval(x1, x2)>=tol:
			chosen_data.append(data[i])
			data_usage[i] = True
			
	return chosen_data



def choose_data_u_1d(data, u_vector, data_usage, node1, node2, data_ratio=1.0):
	""" Obtain a set of data points for interval 1-2 """
	
	# Initialise a data list
	chosen_data = []
	chosen_u = []
	
	# Iterate through each data point in the interval and add to the list
	for i in range(0, len(data)):

		if data_usage[i]:
			continue

		if subset_data_1D(data, data_ratio):
			continue

		point = data[i][0]
		x1 = node1.get_coord()[0]
		x2 = node2.get_coord()[0]

		# Add data points that are in the interval
		if ((point >= x1) and (point <= x2)) or \
			((point >= x2) and (point <= x1)):
			chosen_data.append(data[i])
			chosen_u.append(u_vector[i])
			data_usage[i] = True
				
	return chosen_data, chosen_u


def choose_data_u_2d(data, u_vector, data_usage, node1, node2, \
									node3, tol=0.0, data_ratio=1.0):
	""" Obtain a set of data points for triangle 1-2-3 """

	# Initialise a data list and corresponding u list
	chosen_data = []
	chosen_u = []

	# Polynomials of the triangle
	poly1 = set_polynomial_linear_2D(node1, node2, node3)
	poly2 = set_polynomial_linear_2D(node2, node3, node1)
	poly3 = set_polynomial_linear_2D(node3, node1, node2)

	# Iterate through each data point in the triangle and add to the list
	for i in range(0, len(data)):
		
		if data_usage[i]:
			continue

		if subset_data_2D(data, data_ratio):
			continue

		x1 = data[i][0]
		x2 = data[i][1]

		# Add data points that are in the triangle
		if poly1.eval(x1, x2)>=tol and poly2.eval(x1, x2)>=tol and poly3.eval(x1, x2)>=tol:
			chosen_data.append(data[i])
			chosen_u.append(u_vector[i])
			data_usage[i] = True
			
	return chosen_data, chosen_u


def obtain_data(region, element, parameter=None):
	""" Find the data points in a given element """

	# Parameters
	dim = region.get_dim()
	mesh = region.get_mesh()
	u_mesh = region.get_u_mesh()
	usage = region.get_usage()
	region_coord = region.get_coord()
	region_no = region.get_region_no()
	region_size = region.get_region_size()

	data_ratio = 1.0
	# Obtain data ratio
	if parameter is None:
		data_ratio = 1.0
	elif parameter.get_local():
		data_ratio = parameter.get_data_ratio()
#		data_ratio = 0.01#parameter.get_data_ratio()

#	print(parameter is None)
#	print(data_ratio)
	
	# Extra parameters
	tol = 0.0

	# Initialise max and min coordinates
	max_coord = [-100000000.0]*dim
	min_coord = [100000000.0]*dim

	# Check dimension consistency
	if not (region.get_dim() == 2 and len(element) == 3) and \
	   not (region.get_dim() == 1 and len(element) == 2):
		print("Warning: data/ObtainData - inconsistent dimension")
		return

	# Element is assumed to be a list of nodes
	for node in element:
		coord = node.get_coord()
		for i in range(0, len(coord)):

			# Compare and replace max and min coordinates
			if coord[i] > max_coord[i]:
				max_coord[i] = coord[i]
			if coord[i] < min_coord[i]:
				min_coord[i] = coord[i]

	# Find element of the given coordiantes
	max_index = obtain_region_index(region, max_coord)
	min_index = obtain_region_index(region, min_coord)
#	print(max_index,min_index)
	
	for i in range(0, len(max_index)):
		if max_index[i] < region_no[i]:
			max_index[i] += 1

	chosen_data = []
	chosen_u = []

	if dim == 1:

		# Nodes
		node1 = element[0]
		node2 = element[1]

		# Iterate through the selected mesh sections
		for i in range(min_index[0], max_index[0]):


			data = mesh[i]
			u_vector = u_mesh[i]
			data_usage = usage[i]
				
			local_data, local_u = choose_data_u_1d(data, u_vector, data_usage, node1, node2, data_ratio)

			chosen_data += local_data
			chosen_u += local_u

	elif dim == 2:

		# Nodes
		node1 = element[0]
		node2 = element[1]
		node3 = element[2]

		# Iterate through the selected mesh sections
		for i in range(min_index[0], max_index[0]):
			for j in range(min_index[1], max_index[1]):

				# Determine if the element intersect the region
				intersect = select_region_2D(element, region_coord, region_size, i, j)

				if not intersect:
					continue

				data = mesh[i][j]
				data_usage = usage[i][j]
				u_vector = u_mesh[i][j]

				local_data, local_u = choose_data_u_2d(data, u_vector, \
						data_usage, node1, node2, node3, tol, data_ratio)
				chosen_data += local_data
				chosen_u += local_u

	return chosen_data, chosen_u


def obtain_data_list(data, element):
	""" Find the data points in a given element """

	# 1D domain
	if len(element) == 2:

		# Nodes
		node1 = element[0]
		node2 = element[1]

		data_usage = [False]*len(data)
				
		chosen_data = choose_data_1d(data, data_usage, node1, node2)

	# 2D domain
	elif len(element) == 3:

		# Nodes
		node1 = element[0]
		node2 = element[1]
		node3 = element[2]
		
		data_usage = [False]*len(data)
				
		chosen_data = choose_data_2d(data, data_usage, node1, node2, node3)

	return chosen_data


def obtain_data_u_list(data, u_vector, element):
	""" Find the data points and u values in a given element """

	# 1D domain
	if len(element) == 2:

		# Nodes
		node1 = element[0]
		node2 = element[1]

		data_usage = [False]*len(data)
				
		chosen_data, chosen_u = choose_data_u_1d(data, u_vector, data_usage, node1, node2)

	# 2D domain
	elif len(element) == 3:

		# Nodes
		node1 = element[0]
		node2 = element[1]
		node3 = element[2]
		
		data_usage = [False]*len(data)
				
		chosen_data, chosen_u = choose_data_u_2d(data, u_vector, data_usage, node1, node2, node3)

	return chosen_data, chosen_u



def obtain_data_grid(region, grid):
	""" Obtain all the data in the grid """

	# Parameters
	dim = grid.get_dim()

	if dim != region.get_dim():
		print("Warning: data/ObtainData - invalid dimension")

	region.init_usage()

	chosen_data = []
	u = []

	if dim == 1:
		for node in node_iterator(grid):
			node_id = node.get_node_id()
			for endpt in endpt_iterator(grid, node_id):
				if node_id > endpt:
					end_node = grid.get_node_ref(endpt)

					local_data, local_u = obtain_data(region, [node, end_node])
					chosen_data += local_data

	elif dim == 2:

		for tri in triangle_iterator_ref(grid):
			if tri[0].get_node_id() < tri[1].get_node_id() and \
				tri[1].get_node_id() < tri[2].get_node_id():	

				# Do not count if vertices of a triangle are all at boundary
				if grid.get_slave(tri[0].get_node_id()) and \
					grid.get_slave(tri[1].get_node_id()) and \
					grid.get_slave(tri[2].get_node_id()):
					continue

				local_data, local_u = obtain_data(region, tri)
				chosen_data += local_data

	return chosen_data


def obtain_data_all(region):
	""" Obtain all data in the region """

	data = []

	# Mesh parameters
	dim = region.get_dim()
	region_no = region.get_region_no()
	mesh = region.get_mesh()

	# Add all 1D data points
	if dim == 1:
		for i in range(0, region_no[0]):
			data += mesh[i]

	# Add all 2D data points
	elif dim == 2:
		for i in range(0, region_no[0]):
			for j in range(0, region_no[1]):
				data += mesh[i][j]

	return data


def obtain_data_u_all(region):
	""" Obtain all data in the region """

	data = []
	u = []

	# Mesh parameters
	dim = region.get_dim()
	region_no = region.get_region_no()
	mesh = region.get_mesh()
	u_mesh = region.get_u_mesh()

	# Add all 1D data points
	if dim == 1:
		for i in range(0, region_no[0]):
			data += mesh[i]
			u += u_mesh[i]

	# Add all 2D data points
	elif dim == 2:
		for i in range(0, region_no[0]):
			for j in range(0, region_no[1]):
				data += mesh[i][j]
				u += u_mesh[i][j]
				
	return data,u