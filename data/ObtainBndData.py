# -*- coding: utf-8 -*-
"""
@Author: Fang
@Date:   2022-10-06 10:30:59
@Last Modified by:   Fang
@Last Modified time: 2022-10-06 17:56:32

This class contains routines to randomly select a number of data points 
from elements near boundaries of the domain.

"""




def check_element(point, node1, node2, node3, tol=0.0):
	""" Check if a data point is in the element """

	from triangle.SetPolynomial import set_polynomial_linear_2D

	# Polynomials of the triangle
	poly1 = set_polynomial_linear_2D(node1, node2, node3)
	poly2 = set_polynomial_linear_2D(node2, node3, node1)
	poly3 = set_polynomial_linear_2D(node3, node1, node2)

	x1 = point[0]
	x2 = point[1]

	if poly1.eval(x1, x2)>=tol and poly2.eval(x1, x2)>=tol and poly3.eval(x1, x2)>=tol:
		return True
	else:
		return False


def is_near_boundary(point, grid):
	""" Check if a data point is near boundary """

	from triangle.Triangle import triangle_iterator_ref

	for tri in triangle_iterator_ref(grid):
		if tri[0].get_node_id() < tri[1].get_node_id() and \
			tri[1].get_node_id() < tri[2].get_node_id():	

	#		print(tri[0].get_coord(),tri[1].get_coord(),tri[2].get_coord())
#			print(grid.get_slave(tri[0].get_node_id()),grid.get_slave(tri[2].get_node_id()),grid.get_slave(tri[2].get_node_id()))


			if grid.get_slave(tri[0].get_node_id()) or \
				grid.get_slave(tri[1].get_node_id()) or \
				grid.get_slave(tri[2].get_node_id()):
#				print(check_element(point, tri[0], tri[1], tri[2]))
				if check_element(point, tri[0], tri[1], tri[2]):
					return True
#				return check_element(point, tri[0], tri[1], tri[2])
	return False



def boundary_element_data(data, grid):
	""" Find data points in elements near boundaries """

	from triangle.Triangle import triangle_iterator_ref
	from data.CheckData import check_data_2D

	boundary_data = []

	for tri in triangle_iterator_ref(grid):
		if tri[0].get_node_id() < tri[1].get_node_id() and \
			tri[1].get_node_id() < tri[2].get_node_id():	

			if grid.get_slave(tri[0].get_node_id()) or \
				grid.get_slave(tri[1].get_node_id()) or \
				grid.get_slave(tri[2].get_node_id()):

				new_data = check_data_2D(data, tri[0], tri[1], tri[2], tol=0.0)

				boundary_data = boundary_data + new_data

	return boundary_data



def obtain_local_data():
	""" Obtain a small number of local data near a coordinate """

	pass

def obtain_boundary_data(data, grid, no_sample):
	""" Obtain a chosen number of data points lying in elements in boundary """

	from random import sample

	boundary_data = boundary_element_data(data, grid)

#	boundary_data =[]
#	# Select data from near boundaries
#	for point in data:
#		if is_near_boundary(point, grid):
#			boundary_data.append(point)

	print("Collect", len(boundary_data) ,"data points near boundary")

	# Randomly select a number of data points
	if len(boundary_data) > no_sample:
		chosen_data = sample(boundary_data, no_sample)
	else:
		print("Warning: fewer boundary data than expected!")
		chosen_data = boundary_data
	
	return chosen_data
