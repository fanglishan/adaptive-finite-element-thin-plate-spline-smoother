#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2020-02-28 23:26:46
@Last Modified time: 2022-10-09 23:40:39

This class contains routines to select regions that overlap 
elements, which might contain data points inside the element.
Current implementation should be sufficient for the rectangular
regions and elements that are not long nor thin. 

Sutherland–Hodgman algorithm is another choice. But it is intended
for more complicated polygons and a bit of overshoot here.

"""

# Import libraries
from adaptive.triangle.SetPolynomial import set_polynomial_linear_2D


def is_in_region(node1_x1, node1_x2, region_x1, region_x2, region_size):
	""" Determine if the node is in the region """

	if node1_x1 > region_x1 and node1_x1 < (region_x1 + region_size[0]) and \
	   node1_x2 > region_x2 and node1_x2 < (region_x2 + region_size[1]):
	   	return True



def is_in_triangle(poly1, poly2, poly3, x1, x2):
	""" Determine if the node is in the triangle """

	if poly1.eval(x1, x2)>=0.0 and poly2.eval(x1, x2)>=0.0 and poly3.eval(x1, x2)>=0.0:
		return True



def select_region_2D(element, region_coord, region_size, index1, index2):
	""" Select regions that intersect triangles """

	# Coordinate of the bottom left vertice of the region
	region_x1 = region_coord[0]+ region_size[0]*index1
	region_x2 = region_coord[1]+ region_size[1]*index2

	# Coordinates of vertices of the triangle
	node1_x1 = element[0].get_coord()[0]
	node1_x2 = element[0].get_coord()[1]
	node2_x1 = element[1].get_coord()[0]
	node2_x2 = element[1].get_coord()[1]
	node3_x1 = element[2].get_coord()[0]
	node3_x2 = element[2].get_coord()[1]

	poly1 = set_polynomial_linear_2D(element[0], element[1], element[2])
	poly2 = set_polynomial_linear_2D(element[1], element[2], element[0])
	poly3 = set_polynomial_linear_2D(element[2], element[0], element[1])

	# Check if a vertice of triangle is in the region

	# Vertice 1 of the triangle
	if is_in_region(node1_x1, node1_x2, region_x1, region_x2, region_size):
		return True

	# Vertice 2 of the triangle
	if is_in_region(node2_x1, node2_x2, region_x1, region_x2, region_size):
		return True

	# Vertice 3 of the triangle
	if is_in_region(node3_x1, node3_x2, region_x1, region_x2, region_size):
		return True

	# Check if a vertice of region is in the triangle

	# Vertice 1 (bottom left)
	if is_in_triangle(poly1, poly2, poly3, region_x1, region_x2):
		return True

	# Vertice 2 (bottom right)
	if is_in_triangle(poly1, poly2, poly3, region_x1+region_size[0], region_x2):
		return True

	# Vertice 3 (top left)
	if is_in_triangle(poly1, poly2, poly3, region_x1, region_x2+region_size[1]):
		return True

	# Vertice 4 (top right)
	if is_in_triangle(poly1, poly2, poly3, region_x1+region_size[0], region_x2+region_size[1]):
		return True

	# Check if the centre point of triangle is in the region
	centre_x1 = (node1_x1+node2_x1+node3_x1)/3.0
	centre_x2 = (node1_x2+node2_x2+node3_x2)/3.0
	if is_in_region(centre_x1, centre_x2, region_x1, region_x2, region_size):
#		print "Warning: it happens (data/SelectRegion)"
#		print node1_x1, node1_x2, node2_x1, node2_x2, node3_x1,\
#					node3_x2, region_x1, region_x2, region_size
		return True

	return False