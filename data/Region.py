#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-04-17 00:39:23
@Last Modified time: 2022-11-03 11:26:20

The Region class contains data points and necessary parameters to define
the range of the regions.

"""

# Import libraries
from parameter.RandomFunction import random_pos_neg_one


class Region:
	""" A region that contains data points """

	# Dimension of data
	_dim = 1

	# Data mesh
	_mesh = []

	# Mesh for u vectors
	_u_mesh = []

	# Signal for data usage
	_usage = []

	# Start coordinates
	_coord = []

	# Number of regions in each direction
	_region_no = []

	# Size of regions in each direction
	_region_size = []

	# Number of data points
	_data_size = 0
	
	# Minimum and maximum y values
	_y_min_max = [0.0, 0.0]


	def __init__(self, dim=1, mesh=[], usage=[], coord=[], region_no=[], \
			region_size=[], data_size=0, u_mesh=[], y_min_max=[]):
		self._dim = dim
		self._mesh = mesh
		self._u_mesh = u_mesh
		self._usage = usage
		self._coord = coord
		self._region_no = region_no
		self._region_size = region_size
		self._data_size = data_size
		self._y_min_max = y_min_max

	def set_dim(self, dim):
		""" Set data dimension """
		self._dim = dim

	def set_mesh(self, mesh):
		""" Set data mesh """
		self._mesh = mesh

	def set_u_mesh(self, u_mesh):
		""" Set mesh for u vector """
		self._u_mesh = u_mesh

	def init_u_mesh(self):
		""" Initialise u vector mesh """
		u_mesh = []

		if self._dim == 1:
			for i in range(0, self._region_no[0]):
				u_element = []
				for j in range(0, len(self._mesh[i])):
					u_element.append(random_pos_neg_one())
				u_mesh.append(u_element)

		elif self._dim == 2:
			for i in range(0, self._region_no[0]):
				u_mesh.append([])
				for j in range(0, self._region_no[1]):
					u_element = []
					for k in range(0, len(self._mesh[i][j])):
						u_element.append(random_pos_neg_one())

					u_mesh[i].append(u_element)

		elif self._dim == 3:
			for i in range(0, self._region_no[0]):
				u_mesh.append([])
				for j in range(0, self._region_no[1]):
					u_mesh[i].append([])
					for k in range(0, self._region_no[2]):
						u_mesh[i][j].append([])
						for l in range(0, len(self._mesh[i][j][k])):
							u_mesh[i][j][k].append(random_pos_neg_one())


		self._u_mesh = u_mesh

	def set_usage(self, usage):
		""" Set usage of data points """
		self._usage = usage

	def init_usage(self):
		""" Initialise usage of data points """
		usage = []

		if self._dim == 1:
			for i in range(0, self._region_no[0]):
				usage.append([False]*len(self._mesh[i]))

		elif self._dim == 2:
			for i in range(0, self._region_no[0]):
				usage.append([])
				for j in range(0, self._region_no[1]):
					usage[i].append([False]*len(self._mesh[i][j]))

		elif self._dim == 3:
			for i in range(0, self._region_no[0]):
				usage.append([])
				for j in range(0, self._region_no[1]):
					usage[i].append([])
					for k in range(0, self._region_no[2]):
						usage[i][j].append([False]*len(self._mesh[i][j][k]))

		self._usage = usage

	def set_coord(self, coord):
		""" Set start coordinates """
		self._coord = coord

	def set_region_no(self, region_no):
		""" Set number of regions """
		self._region_no = region_no

	def set_region_size(self, region_size):
		""" Set size of regions """
		self._region_size = region_size

	def set_data_size(self, data_size):
		""" Set number of data points """
		self._data_size = data_size

	def set_y_min_max(self, y_min_max):
		""" Set min and max of y values """
		self._y_min_max = y_min_max

	def get_dim(self):
		""" Get data dimension """
		return self._dim

	def get_mesh(self):
		""" Get data mesh """
		return self._mesh

	def get_u_mesh(self):
		""" Get mesh for u vector """
		return self._u_mesh

	def get_usage(self):
		""" Get usage of data points """
		return self._usage

	def get_coord(self):
		""" Get start coordinates """
		return self._coord

	def get_region_no(self):
		""" Get number of regions """
		return self._region_no

	def get_region_size(self):
		""" Get size of regions """
		return self._region_size

	def get_data_size(self):
		""" Get number of data points """
		return self._data_size

	def get_y_min_max(self):
		""" Get min and max of y values """
		return self._y_min_max

	def get_y_min(self):
		""" Get min of y values """
		return self._y_min_max[0]

	def get_y_max(self):
		""" Get max of y values """
		return self._y_min_max[1]