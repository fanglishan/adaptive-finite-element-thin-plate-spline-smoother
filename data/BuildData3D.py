#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2021-07-31 21:33:28
@Last Modified time: 2022-03-29 11:00:58

This class contains rountines for building 3D datasets. 

"""

# Import libraries
from random import uniform
from math import pi, sin, cos, acos


def create_cube_data(model_problem, n, u, noise_dist, noise, distribution):
	""" Create randomly distributed data in a cube """

	data = []
	n = n**2

	for i in range(0, n):
		x = uniform(0,1)
		y = uniform(0,1)
		z = uniform(0,1)
		v = 0.0
		data.append([x,y,z,v])

	return data


def create_sphere_data(model_problem, n, u, noise_dist, noise, distribution):
	""" Create randomly distributed data on a sphere """

	data = []
	n = n**2

	for i in range(0, n):
		theta = 2*pi*uniform(0,1)
		phi = acos(1-2*uniform(0,1))
		x = sin(phi)*cos(theta)
		y = sin(phi)*sin(theta)
		z = cos(phi)
		v = 0.0
		data.append([x,y,z,v])
		data.append([x*0.95,y*0.95,z*0.95,-1.0])
		data.append([x*1.05,y*1.05,z*1.05,1.0])

	return data


def build_data_3D(model_problem, n, u, noise_dist, noise, distribution):


	if distribution == 1:
		data = create_cube_data(model_problem, n, u, noise_dist, noise, distribution)
	elif distribution == 2:
		data = create_sphere_data(model_problem, n, u, noise_dist, noise, distribution)

	return data