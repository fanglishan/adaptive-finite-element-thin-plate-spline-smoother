#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2020-02-28 16:46:58
@Last Modified time: 2022-10-09 23:31:13

This class contains modules to select a subset of data based on a ratio. 
They are designed to reduced the costs of build equation, which can be 
applied to the whole domain or local domains in the auxiliary problems.

The ratio can be set mannually or determined by the number of data points
in the domain. Functions that record and update the approx number of data
points in each triangle are also stored in this class. The numbers are stored
in each edge star, one for left neighouring triangle and one for the right.

"""

# Import libraries
from grid.NodeTable import node_iterator
from grid.EdgeTable import endpt_iterator
from adaptive.triangle.Triangle import triangle_iterator, triangle_iterator_ref
from parameter.Definition import RefineSet

from random import random


def subset_data_1D(data, ratio):
	""" Ignore a portion of data for the TPSFEM """

	# The ratio is the portion of data that will be used for the TPSFEM
	# 1.0 - all data will be used
	# 0.0 - no data will be used
	if ratio < 0.0 or ratio > 1.0:
		ratio = 1.0

	# Run random function to determine if the data point is taken
	if random() > ratio:
		return True
	else:
		return False


def subset_data_2D(data, ratio):
	""" Ignore a portion of data for the TPSFEM """

	# The ratio is the portion of data that will be used for the TPSFEM
	# 1.0 - all data will be used
	# 0.0 - no data will be used
	if ratio < 0.0 or ratio > 1.0:
		ratio = 1.0

	# Run random function to determine if the data point is taken
	if random() > ratio:
		return True
	else:
		return False



def count_data_1D(grid):
	""" Count the number of data points in a 1D grid """
	
	data_sum = 0

	# Iterate through each node 
	for node in node_iterator(grid):
		node_id1 = node.get_node_id()
		for node in node_iterator(grid):
			node_id2 = node.get_node_id()

			if grid.is_edge(node_id1, node_id2):
				if node_id1 < node_id2:

					data_sum += grid.get_no_data(node_id1, node_id2)[0]

	return data_sum


def count_data_local_grid_1D(grid, local_grid):
	""" Count the number of data points in a 1D grid """
	
	data_sum = 0

	# Iterate through each node 
	for node in node_iterator(local_grid):
		node_id1 = node.get_node_id()
		for node in node_iterator(local_grid):
			node_id2 = node.get_node_id()

			if grid.is_edge(node_id1, node_id2):
				if node_id1 < node_id2:

					data_sum += grid.get_no_data(node_id1, node_id2)[0]

	return data_sum


def determine_side(grid, node_id1, node_id2, node_id3):
	""" Determine if the centre point is left """

	# Coordinates of nodes and centre point
	coord1 = grid.get_coord(node_id1)
	coord2 = grid.get_coord(node_id2)
	coord3 = grid.get_coord(node_id3)
	centre_coord = [(coord1[0]+coord2[0]+coord3[0])/3.0, (coord1[1]+coord2[1]+coord3[1])/3.0]

	# Positive d means node 3 is right, otherwise left
	d = (centre_coord[0]-coord1[0])*(coord2[1]-coord1[1]) - \
		(centre_coord[1]-coord1[1])*(coord2[0]-coord1[0])

	if d < 0:
		return True
	else:
		return False



def count_data_2D(grid):
	""" Count the number of data points in a 2D grid """

	data_sum = 0

	# Iterate through each triangle once
	for tri in triangle_iterator_ref(grid):
		
		node_id1 = tri[0].get_node_id()
		node_id2 = tri[1].get_node_id()
		node_id3 = tri[2].get_node_id()

		if node_id1 < node_id2 and node_id2 < node_id3:

			# Read recorded number of data points in each edge
			# Use the node ids to determine the side of triangle
			if determine_side(grid, node_id1, node_id2, node_id3):
				no_data1 = grid.get_no_data(node_id1, node_id2)[0]
				no_data2 = grid.get_no_data(node_id2, node_id3)[0]
				no_data3 = grid.get_no_data(node_id1, node_id3)[1]
			else:
				no_data1 = grid.get_no_data(node_id1, node_id2)[1]
				no_data2 = grid.get_no_data(node_id2, node_id3)[1]
				no_data3 = grid.get_no_data(node_id1, node_id3)[0]

			# Take an average to approximate number of data points
			data_sum += int((no_data1+no_data2+no_data3)/3.0)

	return data_sum



def count_data_local_grid_2D(grid, local_grid):
	""" Count the number of data points in a 2D grid """

	data_sum = 0

	# Iterate through each triangle once
	for tri in triangle_iterator_ref(local_grid):
		
		node_id1 = tri[0].get_node_id()
		node_id2 = tri[1].get_node_id()
		node_id3 = tri[2].get_node_id()

		# If the vertices of the triangle are boundary nodes, skip it
		if local_grid.get_slave(node_id1) and local_grid.get_slave(node_id2) \
			and local_grid.get_slave(node_id3):
			if (not grid.get_slave(node_id1)) or (not grid.get_slave(node_id2)) \
					or (not grid.get_slave(node_id3)):
				continue

		if node_id1 < node_id2 and node_id2 < node_id3:

			# Read recorded number of data points in each edge
			# Use the node ids to determine the side of triangle
			if determine_side(local_grid, node_id1, node_id2, node_id3):
				no_data1 = grid.get_no_data(node_id1, node_id2)[0]
				no_data2 = grid.get_no_data(node_id2, node_id3)[0]
				no_data3 = grid.get_no_data(node_id1, node_id3)[1]
			else:
				no_data1 = grid.get_no_data(node_id1, node_id2)[1]
				no_data2 = grid.get_no_data(node_id2, node_id3)[1]
				no_data3 = grid.get_no_data(node_id1, node_id3)[0]

			# Take an average to approximate number of data points
			data_sum += int((no_data1+no_data2+no_data3)/3.0)

	return data_sum



def determine_data_ratio_1D(grid, region, parameter, data_sum):
	""" Calculate an ratio for subseting data """

	data_ratio = 1.0

	if data_sum > 500:
		data_ratio = 1000.0/data_sum
		
	parameter.set_data_ratio(data_ratio)


def determine_data_ratio_2D(grid, region, parameter):
	""" Calculate an ratio for subseting data """

	print("Warning: data/DataThining - Execute data thinning now")

	# Total number of data points in the domain
	data_sum = count_data_2D(grid)

	data_ratio = 1.0

	if data_sum > 1000:
		data_ratio = 1000.0/data_sum

	"""
	if data_sum > 10000:
		data_ratio  = 0.1
	elif data_sum > 1000:
		data_ratio  = 0.5
	else:
		data_ratio = 1.0
	"""

	parameter.set_data_ratio(data_ratio)


def record_data_1D(grid, node_id1, node_id2, no_data):
	""" Record the number of data in the element """

	# Update info in the grid
	if node_id1 < node_id2:
		grid.set_no_data(node_id1, node_id2, [no_data])
	else:
		grid.set_no_data(node_id2, node_id1, [no_data])




def record_data_2D(grid, tri, no_data):
	""" Record the number of data near the edge """

	# Node ids of these two nodes are in ascending order
	node_id1 = tri[0].get_node_id()
	node_id2 = tri[1].get_node_id()
	node_id3 = tri[2].get_node_id()

	# Get current number of data recorded in eache dege
	no_data1 = grid.get_no_data(node_id1, node_id2)
	no_data2 = grid.get_no_data(node_id2, node_id3)
	no_data3 = grid.get_no_data(node_id1, node_id3)

	# Update the data in the triangle to all three edges
	if determine_side(grid, node_id1, node_id2, node_id3):
		no_data1[0] = no_data
		no_data2[0] = no_data
		no_data3[1] = no_data
	else:
		no_data1[1] = no_data
		no_data2[1] = no_data
		no_data3[0] = no_data

	# Update info in the grid
	grid.set_no_data(node_id1, node_id2, no_data1)
	grid.set_no_data(node_id2, node_id3, no_data2)
	grid.set_no_data(node_id1, node_id3, no_data3)


def record_no_data_edge(grid, node_id1, node_id2, node_id3):
	""" Record number of data in triangle 1-2-3 and 1-2-4"""

	# Record the number of data points
	no_data = grid.get_no_data(node_id1, node_id2)

#	return no_data

	# Return the number of data in the correct order
	if determine_side(grid, node_id1, node_id2, node_id3):
		return no_data
	
	else:
		return [no_data[1], no_data[0]]



def update_no_data_edge(grid, no_data_1, node_id1, node_id2, mid_id):
	""" Update the number of data points for certain edge """

	# Find the right order of node id 1 and 2
	if node_id1 < node_id2:
		id1 = node_id1
		id2 = node_id2
	else:
		id1 = node_id2
		id2 = node_id1

	# Get the current number of data
	no_data = grid.get_no_data(id1, id2)

	# Determine the side of the triangle and set number of data
	# And update them in the right position
	if determine_side(grid, id1, id2, mid_id):
		no_data[0] = no_data_1
		grid.set_no_data(id1, id2, no_data)
	else:
		no_data[1] = no_data_1
		grid.set_no_data(id1, id2, no_data)



def update_no_data(grid, no_data, mid_id, node_id1, node_id2, node_id3=None, node_id4=None):
	""" Update the number of data points in the refined triangle pair """

	# New number of data points
	# We assume the data points are evely distributed and this new 
	# number is the approximate value, which is sufficient in most cases
	no_data_1 = int(no_data[0]/2)   # Number of data in triangle 1-2-3
	no_data_2 = int(no_data[1]/2)   # Number of data in triangle 1-2-4

	# Update splitted edge
	if determine_side(grid, node_id1, node_id2, node_id3):
		grid.set_no_data(node_id1, mid_id, [no_data_1, no_data_2])
		grid.set_no_data(node_id2, mid_id, [no_data_2, no_data_1])
	else:
		grid.set_no_data(node_id1, mid_id, [no_data_2, no_data_1])
		grid.set_no_data(node_id2, mid_id, [no_data_1, no_data_2])

	# Update existing edges on the boundary of the triangle pair
	if node_id3 is not None:
		update_no_data_edge(grid, no_data_1, node_id1, node_id3, mid_id)
		update_no_data_edge(grid, no_data_1, node_id2, node_id3, mid_id)

	if node_id4 is not None:

		update_no_data_edge(grid, no_data_2, node_id1, node_id4, mid_id)
		update_no_data_edge(grid, no_data_2, node_id2, node_id4, mid_id)

	# Update new edges

	grid.set_no_data(mid_id, node_id1, [no_data_2,no_data_1])
	grid.set_no_data(mid_id, node_id2, [no_data_1,no_data_2])

	if node_id3 is not None:
		grid.set_no_data(mid_id, node_id3, [no_data_1,no_data_1])

	if node_id4 is not None:
		grid.set_no_data(mid_id, node_id4, [no_data_2,no_data_2])