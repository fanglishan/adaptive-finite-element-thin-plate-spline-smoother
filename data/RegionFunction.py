#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-04-18 11:31:49
@Last Modified time: 2023-06-03 23:36:00

This class contains data region related routines. It assumes the dimensions
of all the data points are consistent.

"""

# Import libraries
from data.Region import Region
from grid.NodeTable import node_iterator

from copy import deepcopy


def obtain_region_index(region, coord):
	""" Find the region in the mesh for the given coord """

	# Region parameters
	region_coord = region.get_coord()
	region_no = region.get_region_no()
	region_size = region.get_region_size()

	# Deterimine the index of the region
	index = []
	for i in range(0, len(coord)):

		position = int((coord[i]-region_coord[i])/region_size[i])

		# Print warning message
		if position < 0 or position >= region_no[i]:
			print("Warning: data/Region Function - data out of domain")
			return [-1]

		index.append(position)

	return index


def add_mesh_data(data, region):
	""" Assign data points to the mesh """

	# Region parameters
	dim = region.get_dim()
	mesh = region.get_mesh()

	y_min = 1000000000.0
	y_max = -1000000000.0

	# Iterate through each data point
	for point in data:

		y_min = min(y_min,point[-1])
		y_max = max(y_max,point[-1])

		# Determine region index for the coordinate
		coord = point[:-1]
		index = obtain_region_index(region, coord)

		# Skip this point if region is not found
		if index[0] == -1:
			continue

		# Add data point to the mesh
		if dim == 1:
			mesh[index[0]].append(point)
		elif dim == 2:
			mesh[index[0]][index[1]].append(point)
		elif dim == 3:
			mesh[index[0]][index[1]][index[2]].append(point)

	region.set_y_min_max([y_min,y_max])


def initialise_mesh(dim, region_no):
	""" Initialise a data mesh """

	# 1D data mesh
	if dim == 1:
		mesh = [[] for x in range(region_no[0])]

		# Reset lists
		for i in range(region_no[0]):
			mesh[i] = []

	# 2D data mesh
	elif dim == 2:
		mesh = [[[]]*region_no[1] for x in range(region_no[0])]

		# Reset lists
		for i in range(region_no[0]):
			for j in range(region_no[1]):
				mesh[i][j] = []

	# 3D data mesh
	elif dim == 3:
		mesh = [[[[] for x in range(region_no[2])]]*region_no[1] for x in range(region_no[0])]
		
		# Reset lists
		for i in range(region_no[0]):
			for j in range(region_no[1]):
				for k in range(region_no[2]):
					mesh[i][j][k] = []	

	else:
		print("Warning: data/Region Function - invalid dimennsion")
		return

	return mesh


def grid_minmax_coord(grid):
	""" Obtain minimum and maximum coordinate of the grid """

	dim = grid.get_dim()

	min_coord = [0.0]*dim
	max_coord = [0.0]*dim

	# Initialise the min and max coordinates
	for node in node_iterator(grid):

		coord = node.get_coord()

		min_coord = deepcopy(coord)
		max_coord = deepcopy(coord)
		break

	# Find the min and max coordinates
	for node in node_iterator(grid):

		coord = node.get_coord()

		for i in range(0, dim):

			if coord[i] < min_coord[i]:
				min_coord[i] = coord[i]
			if coord[i] > max_coord[i]:
				max_coord[i] = coord[i]

	return min_coord, max_coord


def validate_u(region):
	""" Ensure the quality of u vector """

	u_mesh = region.get_u_mesh()

	# Initialise number of positive and negative u values
	no_pos = 0
	no_neg = 0

	# Count the number of positive and negative u values
	if region.get_dim() == 1:
		for i in range(0, len(u_mesh)):
			for j in range(0, len(u_mesh[i])):
				if u_mesh[i][j] > 0.0:
					no_pos += 1
				else:
					no_neg += 1

	elif region.get_dim() == 2:
		for i in range(0, len(u_mesh)):
			for j in range(0, len(u_mesh[i])):
				for k in range(0, len(u_mesh[i][j])):
					if u_mesh[i][j][k] > 0.0:
						no_pos += 1
					else:
						no_neg += 1

	elif region.get_dim() == 3:
		for i in range(0, len(u_mesh)):
			for j in range(0, len(u_mesh[i])):
				for k in range(0, len(u_mesh[i][j])):
					for l in range(0, len(u_mesh[i][j][k])):
						if u_mesh[i][j][k][l] > 0.0:
							no_pos += 1
						else:
							no_neg += 1

	else:
		print("Warning: unexpected dimension")

	no_data = no_pos+no_neg

	# Return False if number of positive and negative
	# are not balanced
	if no_data < 4:
		if no_pos == 1 or no_pos == 2:
			return True
		else:
			print("portion of positive:", 1.0*no_pos/no_data)
			return False

	elif no_data == 4:
		if no_pos == 2:
			return True
		else:
			print("portion of positive:", 1.0*no_pos/no_data)
			return False

	elif no_data < 10:
		if no_pos == 4 or no_pos == 5:
			return True
		else:
			print("portion of positive:", 1.0*no_pos/no_data)
			return False

	else:
		if 1.0*no_pos/no_data > 0.55 or 1.0*no_pos/no_data < 0.45:
			print("portion of positive:", 1.0*no_pos/no_data)
			return False
		else:
			return True

	return True


def build_region(data, d, no_region, grid=None):
	""" Build a new data region """

	# Check empty dataset
	if len(data) == 0:
		print("Warning: data/Region Function - empty dataset")
		return
	
	region_no = [no_region]*d

	# Dimension of data
	dim = len(data[0])-1

	# Check input
	if d != dim:
		print("Warning: data/Region Function - invalid number of regions")
		return

	# Initialise a data mesh
	mesh = initialise_mesh(dim, region_no)

	# Set start and end coordinates if it is not specified
	if grid is None:
		min_coord = [0.0]*dim
		max_coord = [1.0]*dim

	else:
		min_coord, max_coord = grid_minmax_coord(grid)

	# Add small distance to avoid overlapping
	for i in range(0, dim):
		min_coord[i] -= 0.0001
		max_coord[i] += 0.0001
		
	# Set region sizes
	region_size = []
	for i in range(0, dim):
		region_size.append((max_coord[i]-min_coord[i])/region_no[i])

	# Build a data region
	region = Region(dim, mesh, [], min_coord, region_no, region_size, len(data), [])

	# Assign data to the data mesh
	add_mesh_data(data, region)

	# Create a u vector mesh corresponding to the data mesh
	region.init_u_mesh()

	if len(data) <= 500:
		check = False
		while not check:
			check = validate_u(region)

			if not check:
				print("Regenerate u values")
				region.init_u_mesh()

	# Initialise usage of data mesh
#	region.init_usage()

	return region