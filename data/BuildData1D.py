#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-04-16 22:30:35
@Last Modified time: 2023-10-12 23:47:42

This class contains rountines for building 1D datasets. 

"""

# Import libraries
from parameter.Definition import NoiseDist, ModelProblem1D

from numpy import random


def add_noise(noise_dist, noise_size):
	""" Add noise to data values """

	mu = 0.0

	if noise_size == 0.0:
		return 0.0

	# Possible scaling of noise can be added here
	scale = 1.0

	# Uniformly distributed noise 
	if noise_dist == NoiseDist.uniform:
		return random.uniform(-noise_size, noise_size)

	# Normally distributed noise
	elif noise_dist == NoiseDist.normal:
		return random.normal(mu, noise_size)


def create_uniform_data_1D(model_problem, n, u, start, end, data_range, noise_dist, noise_size):
	""" Create 1D uniform data """

	# Initialise a list for the data points in the grid 
	data = []

	# Total length of the domain
	length = end-start
	if length < 0:
		print("Warning: data/BuildData1D - negative domain length")

	if data_range > 1.0 or data_range < 0.0:
		print("Warning: data/BuildData1D - invalid proportion of data")

	# Interval length between data points
	interval = length/(n-1)*data_range

	# Coordinate of the point
	coord = start + length*(1-data_range)/2.0

	# Iterate through data points in the x direction
	for i in range(0, n):

		data_value = u([coord]) + add_noise(noise_dist, noise_size)

		# Set x, y coordinates and z value of the data point
		data_point = [coord, data_value]

		# Add the data point to the list
		data.append(data_point)

		# Update x coordinate
		coord += interval

	return data


def create_uniform_data_hole_1D(model_problem, n, u, start, end, data_range, noise_dist, noise_size):
	""" Create 1D uniform data with a data free region """
	
	# Create uniformly distributed data points
	uniform_data = create_uniform_data_1D(model_problem, n, u, start, end, data_range, noise_dist, noise_size)

	# Hole coordinates
	# x_start, x_end
	if model_problem == 8:
		holes = [[0.4, 0.6]]
	elif model_problem == 11:
		holes = [[-1.2, -1.0], [-0.3, -0.05], [0.4, 0.6], [1.0, 1.2]]

	# Initialise a list for data
	data = []

	# Iterate through each data point
	for i in range(0, len(uniform_data)):
		inside = False
		for j in range(0, len(holes)):
			if uniform_data[i][0] > holes[j][0] and uniform_data[i][0] < holes[j][1]:
				inside = True
		if not inside:
			data.append(uniform_data[i])
	
	return data



def create_irregular_data_1D(model_problem, distribution, n, u, \
				start, end, data_range, noise_dist, noise_size):
	""" Create 1D irregular data """

	# Initialise a list for the data points in the grid 
	data = []

	# Total length of the domain
	length = end-start
	if length < 0:
		print("Warning: data/BuildData1D - negative domain length")

	if data_range > 1.0 or data_range < 0.0:
		print("Warning: data/BuildData1D - invalid proportion of data")

	# Distance to boundary
	dist_bound = length*(1-data_range)/2.0

	# Start and end of the data
	start += dist_bound
	end -= dist_bound

	# Data coordinates are uniformly distributed
	if distribution == 3:

		for i in range(0, n):

			coord = random.uniform(start, end)

			data_value = u([coord]) + add_noise(noise_dist, noise_size)

			# Set x, y coordinates and z value of the data point
			data_point = [coord, data_value]

			data.append(data_point)

	# Data coordinates are normaly distributed
	elif distribution == 4:

		# Mean and stand deviation of the Gaussian distribution
		mean = (start+end)/2.0
		sd = 0.15

		for i in range(0, n):

			coord = random.normal(mean, sd)

			# Make sure coord is in the domain
			while coord <= start or coord >= end:
				coord = random.normal(mean, sd)

			data_value = u([coord]) + add_noise(noise_dist, noise_size)

			# Set x, y coordinates and z value of the data point
			data_point = [coord, data_value]

			data.append(data_point)

	# Create a number of outliers
	for i in range(0, no_outlier):

		# Randomly choose one data point
		data_point = random.choice(data)

		# Recompute the data value
		data_point[1] = u([data_point[0]]) + random.uniform(-size_outlier, size_outlier)

	return data



def build_data_1D(model_problem, n, u, noise_dist, noise_size, distribution):
	""" Build 1D data """

	# The range of domain
	if model_problem == ModelProblem1D.frac:
		start = -2.0
		end = 2.0
	elif model_problem == ModelProblem1D.shift or model_problem == ModelProblem1D.runge \
		or model_problem == ModelProblem1D.exp_fluc:
		start = -1.0
		end = 1.0
	else:
		start = 0.0
		end = 1.0

#	start = -0.2
#	end = 1.2

	# The proportion of data inside domain
	data_range = 0.95

	# Create uniform data
	if distribution == 1:
		data = create_uniform_data_1D(model_problem, n, u, start, end, data_range, noise_dist, noise_size)

	# Create uniform data with gap in the middle
	elif distribution == 2:
		data = create_uniform_data_hole_1D(model_problem, n, u, start, end, data_range, noise_dist, noise_size)

	# Create irregularly distributed data (uniform or normal)
	elif distribution == 3 or distribution == 4:
		data = create_irregular_data_1D(model_problem, distribution, n, u, \
								start, end, data_range, noise_dist, noise)
	
	return data
