# -*- coding: utf-8 -*-
"""
@Author: Fang
@Date:   2022-12-10 13:40:56
@Last Modified by:   Fang
@Last Modified time: 2022-12-11 15:14:32

This function calculates d (or d_X), which measures distance from any point
to a data point of the observed data. This parameter is significant as it
is critical in error convergence of the TPSFEM (See Roberts' paper).

Current implementations assume data points inside the triangle pair is 
uniformly distributed.

"""



def calculate_d(grid, endpt1, endpt2, parameter):
	""" Calculate dX in a triangle pair """

	from adaptive.triangle.TriangleFunction import find_triangle_pairs, calc_tri_pair_area
	from numpy import sqrt


	no_data_list = grid.get_no_data(endpt1, endpt2)

	# Obtain total number of data in this triangle pair
	no_data = sum(no_data_list)

	d = 0
#	print(no_data)
	if no_data == 0:
		coord1 = grid.get_coord(endpt1)
		coord2 = grid.get_coord(endpt2)
		d = sqrt((coord1[0]-coord2[0])**2 + (coord1[1]-coord2[1])**2)

	else:
#		node3, node4 = find_triangle_pairs(grid, endpt1, endpt2)

		area = calc_tri_pair_area(grid, grid.get_node(endpt1), grid.get_node(endpt2), endpt1, endpt2)

		d = sqrt(area/no_data)
		"""
		coord1 = grid.get_coord(endpt1)
		coord2 = grid.get_coord(endpt2)
		default = sqrt((coord1[0]-coord2[0])**2 + (coord1[1]-coord2[1])**2)
		print("d:", d, ", default:", default)
		"""
	return d


