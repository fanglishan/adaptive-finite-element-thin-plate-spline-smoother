#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2021-08-05 14:36:43
@Last Modified time: 2022-03-29 11:01:02

This class contains modules to build data sets in different dimensions.

"""

# Import libraries
from data.DatasetManager import set_model, set_noise, set_size, set_distribution, set_noise_distribution
from data.BuildData1D import build_data_1D
from data.BuildData2D import build_data_2D
from data.BuildData3D import build_data_3D
from fileio.DataFile import write_file


def build_dataset(dataset):
	""" Build a dataset """

	data = []
	
	# Dimension of data
	d = int(str(dataset)[0])

	model_problem = int(str(dataset)[1:3])
	
	# Number of data points (in each direction)
	n = set_size(dataset)

	# Model problem
	u = set_model(dataset)

	# Data distribution
	distribution = set_distribution(dataset)

	# Noise distribution
	noise_dist = set_noise_distribution(dataset)

	# Noise size
	noise_size = set_noise(dataset)

	# outlier_size
	outlier_no = 0
	outlier_size = 0.0

	if d == 1:
		data = build_data_1D(model_problem, n, u, noise_dist, noise_size, distribution)

	elif d == 2:
		data = build_data_2D(model_problem, n, u, noise_dist, noise_size, distribution)

	elif d == 3:
		data = build_data_3D(model_problem, n, u, noise_dist, noise_size, distribution)


	write_file(data, str(dataset))

	return data