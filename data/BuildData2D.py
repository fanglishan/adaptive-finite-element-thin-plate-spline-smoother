#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-01-22 18:07:35
@Last Modified time: 2023-03-11 13:12:38

This class contains rountines for building 2D datasets. 

"""

# Import libraries
from parameter.Definition import NoiseDist, ModelProblem2D

from numpy import random, sqrt



def add_noise(noise_dist, noise_size):
	""" Add noise to data values """

	if noise_size == 0.0:
		return 0.0

	mu = 0.0

	# Uniformly distributed noise 
	# noise size is the range of noise
	if noise_dist == NoiseDist.uniform:
		return random.uniform(-noise_size, noise_size)

	# Normally distributed noise
	# noise size is the standard deviation
	elif noise_dist == NoiseDist.normal:
		return random.normal(mu, noise_size)	



def create_response(u, model_problem, x_coord, y_coord, noise_dist, noise_size):
	"""
	if model_problem == ModelProblem2D.hole:
		L1_norm = abs(x_coord-0.5) + abs(y_coord-0.5)
		if L1_norm < 0.25 or L1_norm > 0.5:     # original one from Linda
#		if L1_norm < 0.25 or L1_norm > 0.45:
			return False
	"""
	return u([x_coord, y_coord]) + add_noise(noise_dist, noise_size)


def create_uniform_data_2D(model_problem, n, u, domain, data_range, noise_dist, noise_size):
	""" Create 2D uniform data """

	# Initialise a list for the data points in the grid 
	data = []

	# Range of domain
	x_start = domain[0]
	y_start = domain[1]
	x_end = domain[2]
	y_end = domain[3]

	# Total length of the domain
	length_x = x_end-x_start
	length_y = y_end-y_start

	if length_x < 0 or length_y < 0:
		print("Warning: data/BuildData2D - negative domain length")

	if data_range > 1.0 or data_range < 0.0:
		print("Warning: data/BuildData2D - invalid proportion of data")

	# Interval length between data points
	x_interval = length_x/(n-1)*data_range
	y_interval = length_y/(n-1)*data_range

	# Start coordinate of the point
	x_coord_start = x_start + length_x*(1-data_range)/2.0
	y_coord_start = y_start + length_y*(1-data_range)/2.0

	# Set initial y coordinate
	y_coord = y_coord_start

	# Iterate through data points in the y direction
	for j in range(0, n):

		# Set initial x coordinate
		x_coord = x_coord_start

		# Iterate through data points in the x direction
		for i in range(0, n):

			data_value = create_response(u, model_problem, x_coord, y_coord, noise_dist, noise_size)

			# Set x, y coordinates and z value of the data point
			data_point = [x_coord, y_coord, data_value]

			# Update x coordinate
			x_coord += x_interval

			if data_value is False:
				continue
	
			# Add the data point to the list
			data.append(data_point)

		# Update y coordinate
		y_coord += y_interval

	return data


def create_uniform_data_hole_2D(model_problem, n, u, domain, data_range, noise_dist, noise, hole):
	""" Create 1D uniform data with a data free region """

	# Create uniformly distributed data points
	uniform_data = create_uniform_data_2D(model_problem, n, u, domain, data_range, noise_dist, noise)


	# Hole coordinates
	# x_start, y_start, x_end, y_end
	if model_problem == 7:
		holes = [[0.4, 0.4, 0.6, 0.6]]

	elif model_problem == 8:
		holes = [[0.4, 0.4, 0.6, 0.6]]

	elif model_problem == 13:
		holes = [[-0.6, -0.8, -0.4, -0.6], [0.4, 0.4, 0.6, 0.6],[-0.5, 0.3, -0.3, 0.5], [-0.3, 0.1, -0.1, 0.3], [-0.1, -0.1, 0.1, 0.1], [0.4, -0.6, 0.6, -0.4]]

	elif model_problem == 16 or model_problem == 17:
		holes = [[-2.0, 1.5, -1.5, 2.0], [1.5, 0.0, 2.0, 0.5], [0.0, 0.0, 0.5, 0.5], [-2.0, 0.0, -1.5, 0.5], [1.0, 1.5, 1.5, 2.0], [-0.5, 1.0, 0.0, 1.5], [1.5, -2.0, 2.0, -1.5],[0.5, -1.0, 1.0, -0.5],[1.0, -1.5, 1.5, -1.0],[-1.5, -2.0, -0.5, -1.5]]

	else:
		holes = []

	# Initialise a list for data
	data = []

	# Iterate through each data point
	for i in range(0, len(uniform_data)):
		inside = False
		for j in range(0, len(holes)):
			if uniform_data[i][0] > holes[j][0] and uniform_data[i][0] < holes[j][2] and \
				uniform_data[i][1] > holes[j][1] and uniform_data[i][1] < holes[j][3]:
				inside = True
		if not inside:
			data.append(uniform_data[i])
	
	return data



def create_uniform_data_L_2D(model_problem, n, u, domain, data_range, noise_dist, noise):
	""" Create 2D uniform data with a L shaped data free region """

	# Create uniformly distributed data points
	uniform_data = create_uniform_data_2D(model_problem, n, u, domain, data_range, noise_dist, noise)

	# Initialise a list for data
	data = []

	holes = [[0.0, 0.4, 0.6, 1.0]]
	# Iterate through each data point
	for i in range(0, len(uniform_data)):
		inside = False
		for j in range(0, len(holes)):
			if uniform_data[i][0] > holes[j][0] and uniform_data[i][0] < holes[j][2] and \
				uniform_data[i][1] > holes[j][1] and uniform_data[i][1] < holes[j][3]:
				inside = True
		if not inside:
			data.append(uniform_data[i])
	
	return data


def create_irregular_data_2D(model_problem, distribution, n, u, domain, \
								data_range, noise_dist, noise_size):
	""" Create 2D uniform data """

	# Range of domain
	x_start = domain[0]
	y_start = domain[1]
	x_end = domain[2]
	y_end = domain[3]

	if data_range > 1.0 or data_range < 0.0:
		print("Warning: data/BuildData2D - invalid proportion of data")

	# Distance to boundary
	dist_x = (x_end-x_start)*(1-data_range)/2.0
	dist_y = (y_end-y_start)*(1-data_range)/2.0

	# Start coordinate of the point
	x_start += dist_x
	y_start += dist_y
	x_end -= dist_x
	y_end -= dist_y

	# Initialise a list for the data points in the grid 
	data = []

	# Data coordinates are uniformly distributed
	if distribution == 3:

		# Iterate through each dimension
		for j in range(0, n):
			for i in range(0, n):

				# Update y coordinate
				x_coord = random.uniform(x_start, x_end)
				y_coord = random.uniform(y_start, y_end)

				data_value = u([x_coord, y_coord]) + add_noise(noise_dist, noise_size)
				
				# Set x, y coordinates and z value of the data point
				data_point = [x_coord, y_coord, data_value]

				data.append(data_point)

	# Data coordinates are normaly distributed
	elif distribution == 4:

		# Centre of the domain
		x_mean = (x_start + x_end)/2.0
		y_mean = (y_start + y_end)/2.0

		# Standard deviation of the normal distribution
		if model_problem == ModelProblem2D.franke or model_problem == ModelProblem2D.tanh or model_problem == ModelProblem2D.cone:
#			sd = 0.5
			sd = 0.3
		elif model_problem == ModelProblem2D.peak:
			x_mean = 2
			sd = 1.2
			sd = 1.0
		elif model_problem == ModelProblem2D.lpeak:
			x_mean = 2.5
			sd = 1.5
		else:
			sd = 0.2

		# Iterate through each dimension
		for j in range(0, n):
			for i in range(0, n):

				# Update y coordinate
				x_coord = random.normal(x_mean, sd)
				y_coord = random.normal(y_mean, sd)

				# Make sure coord is in the domain
				while x_coord <= x_start or x_coord >= x_end:
					x_coord = random.normal(x_mean, sd)
				while y_coord <= y_start or y_coord >= y_end:
					y_coord = random.normal(y_mean, sd)

				data_value = u([x_coord, y_coord]) + add_noise(noise_dist, noise_size)
				
				# Set x, y coordinates and z value of the data point
				data_point = [x_coord, y_coord, data_value]

				data.append(data_point)

	return data



def create_line_data_2D(model_problem, n, u, domain, data_range, noise_dist, noise_size):
	""" Create 2D uniform data """

	# Range of domain
	x_start = domain[0]
	y_start = domain[1]
	x_end = domain[2]
	y_end = domain[3]

	# Total length of the domain
	length_x = x_end-x_start
	length_y = y_end-y_start

	if length_x < 0 or length_y < 0:
		print("Warning: data/BuildData2D - negative domain length")

	if data_range > 1.0 or data_range < 0.0:
		print("Warning: data/BuildData2D - invalid proportion of data")

	print(n)

	if n <= 20:
		ratio = 1.5
	elif n <= 20:
		ratio = 2.2
	elif n <= 50:
		ratio = 4.5
	elif n <= 100:
		ratio = 6.66
		ratio = 6.66665
	elif n <= 250:
		ratio = 6.7
	elif n <= 1000:
		ratio = 6.85
	else:
		ratio = 8

	x_n = int(n*ratio)
	y_n = int(n/ratio)
	print(x_n, y_n)

	# Interval length between data points
	x_interval = length_x*data_range/(x_n-1)
	y_interval = length_y*data_range/(y_n-1)
	print(x_interval, y_interval, y_interval/x_interval)

	# Start coordinate of the point
	x_coord_start = x_start + length_x*(1-data_range)/2.0
	y_coord_start = y_start + length_y*(1-data_range)/2.0

	# Initialise a list for the data points in the grid 
	data = []

	# Set initial y coordinate
	y_coord = y_coord_start

	# Iterate through data points in the y direction
	for j in range(0, y_n):

		# Set initial x coordinate
		x_coord = x_coord_start

		# Iterate through data points in the x direction
		for i in range(0, x_n):

			data_value = u([x_coord, y_coord]) + add_noise(noise_dist, noise_size)
			
			# Set x, y coordinates and z value of the data point
			data_point = [x_coord, y_coord, data_value]

			# Add the data point to the list
			data.append(data_point)

			# Update x coordinate
			x_coord += x_interval

		# Update y coordinate
		y_coord += y_interval

	return data



def shrink_domain(domain, n):
	""" Shrink the domain of data is few data points are included """

	shrink_ratio = 0.5

	if n < 10:

		x1_interval = abs(domain[0]-domain[2])
		x1_centre = (domain[0] + domain[2])/2.0
		x2_interval = abs(domain[0]-domain[2])
		x2_centre = (domain[0] + domain[2])/2.0

		new_x1_start = x1_centre-x1_interval/2.0*shrink_ratio
		new_x1_end = x1_centre+x1_interval/2.0*shrink_ratio
		new_x2_start = x2_centre-x2_interval/2.0*shrink_ratio
		new_x2_end = x2_centre+x2_interval/2.0*shrink_ratio

	new_domain = [new_x1_start, new_x2_start, new_x1_end, new_x2_end]
	print(new_domain)

	return new_domain


def build_data_2D(model_problem, n, u, noise_dist, noise, distribution):
	""" Build 2D data """

	# The proportion of data inside domain
	data_range = 0.8

	# The range of domain
	# x1 start, x2 start, x1 end, x2 end
	if model_problem == ModelProblem2D.franke or model_problem == ModelProblem2D.tanh or model_problem == ModelProblem2D.cone:
		domain = [-1.0, -1.0, 1.0, 1.0]
	elif model_problem == ModelProblem2D.peak:
		domain = [-3.0, -3.0, 3.0, 3.0]
	elif model_problem == ModelProblem2D.lpeak:
		domain = [-4.0, -4.0, 4.0, 4.0]
	else:
		domain = [0.0, 0.0, 1.0, 1.0]

	if n < 10:
		# The range of domain
		# x1 start, x2 start, x1 end, x2 end
		if model_problem == ModelProblem2D.franke or model_problem == ModelProblem2D.tanh or model_problem == ModelProblem2D.cone:
			domain = [-0.5, -0.5, 0.5, 0.5]
		else:
			domain = [0.25, 0.25, 0.75, 0.75]
		data_range = 1.0

#	domain = shrink_domain(domain, n)

	# The range of data free region
	hole = [0.4, 0.4, 0.6, 0.6]

	# Create uniform data
	if distribution == 1:
		data = create_uniform_data_2D(model_problem, n, u, domain, data_range, noise_dist, noise)

	# Create uniform data with gap in the middle
	elif distribution == 2:
		data = create_uniform_data_hole_2D(model_problem, n, u, domain, data_range, noise_dist, noise, hole)

	# Create irregularly distributed data
	elif distribution == 3 or distribution == 4:
		data = create_irregular_data_2D(model_problem, distribution, n, u, domain, data_range, noise_dist, noise)

	# Create uniform data distributed in lines (different densities in x and y directions)
	elif distribution == 5:
		data = create_line_data_2D(model_problem, n, u, domain, data_range, noise_dist, noise)

	elif distribution == 6:
		data = create_uniform_data_L_2D(model_problem, n, u, domain, data_range, noise_dist, noise)

	return data