#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-04-16 23:35:59
@Last Modified time: 2022-11-05 11:04:51

This class contains routines to extract information from dataset
names.

Dataset index
0:dimension
1-2:model problem
3:data distribution
4:noise distribution
6:noise level
5:data size

"""

# Import libraries
from fileio.DataFile import read_file
from function.FunctionManager import obtain_model_function
from parameter.Definition import ModelProblem1D, ModelProblem2D
from parameter.Definition import NoiseDist
#from buildDataset import build_dataset

import os


def obtain_dimension(dataset):
	""" Return the dimension of dataset """

	# Dimension
	d = int(str(dataset)[0])

	# Print error message
	if d != 1 and d != 2 and d!= 3:
		print("Warning: data/DatasetManager - unknown dataset")

	return d


def set_model(dataset):
	""" set the model problem of the dataset """

	d = int(str(dataset)[0])	
	model_problem = int(str(dataset)[1:3])

	return obtain_model_function(model_problem, d)[0]


def set_distribution(dataset):
	""" set the model problem of the dataset """

	return int(str(dataset)[3])


def set_noise_distribution(dataset):
	""" set the noise distribution of the dataset """

	return int(str(dataset)[4])


def set_noise(dataset):
	""" set the noise size of the dataset """
	
	# Find noise level from dataset name
	noise_dist = int(str(dataset)[4])
	noise_level = int(str(dataset)[5])

	# Noise level is the range of noise
	uniform_noise_levels = [0.0, 0.001, 0.005, 0.01, 0.03, \
							0.05, 0.06, 0.08, 0.09, 0.1]

	# Noise level is the standard deviation of noise
	normal_noise_levels = [0.0, 0.005, 0.01, 0.02, 0.05, \
						   0.07, 0.1, 0.15, 0.2, 0.25]
		
	if noise_level < 0 or noise_level > 10:
		print("Warning: data/DatasetManager - invalid noise level")
		return		

	# Uniformly distributed noise
	if noise_dist == NoiseDist.uniform:
		return uniform_noise_levels[noise_level]


	# Normally distributed noise
	elif noise_dist == NoiseDist.normal:
		return normal_noise_levels[noise_level]

	else:
		print("Warning: data/DatasetManager - invalid noise distribution")
		return


def set_size(dataset):
	""" set the data size of the dataset """

	# Find data size from dataset name
	size = int(str(dataset)[6])

	data_sizes = [3, 10, 20, 50, 100, 250, 500, 750, 1000, 1500, 2000]
	
	# Return corresponding dataset size
	if size >= 0  and size < 10:
		data_size = data_sizes[size]
	else:
		print("Warning: data/DatasetManager - invalid data size")
		return

	d = int(str(dataset)[0])
	if d == 1:
		data_size *= data_size

	return data_size


def obtain_dataset(dataset, subset=False):
	""" Return the required dataset """

	# Print error message
	if dataset == 0:
		print("Warning: data/DatasetManager - invalid dataset")


	if len(str(dataset)) < 3:
		data_index = abs(dataset)*-1

		# Read Obgoola magnetic data
		if data_index == ModelProblem2D.magnet:
			data_path = "magnet"

		# Read bone data
		elif data_index == ModelProblem2D.bone:
			data_path = "bone"

		# Read lake data
		elif data_index == ModelProblem2D.lake:
			data_path = "lake"

		# Read lena data
		elif data_index == ModelProblem2D.lena:
			data_path = "lena"

		# Read crater lake data
		elif data_index == ModelProblem2D.crater:
			data_path = "crater"

		# Read iron mine data
		elif data_index == ModelProblem2D.ironmine:
			data_path = "ironmine"

		# Read iron mountain data
		elif data_index == ModelProblem2D.mountain:
			data_path = "mountain"

		# Read Aeromagnetic Burney data
		elif data_index == ModelProblem2D.burney:
			data_path = "burney"

		# Read Grapevine Canyon data
		elif data_index == ModelProblem2D.canyon:
			data_path = "canyon"

		# Read Unalakleet river mouth data
		elif data_index == ModelProblem2D.river:
			data_path = "river"

		# Read Black Beach data
		elif data_index == ModelProblem2D.beach:
			data_path = "beach"

		# Read Coastal region data
		elif data_index == ModelProblem2D.coast:
			data_path = "coast"

		elif isinstance(dataset, str):
			data_path = dataset

		if subset:
			data_path = data_path + "_subset"
		
		data = read_file(data_path)
	
	# Read other 
	else:
		data = read_file(str(dataset))

	return data
