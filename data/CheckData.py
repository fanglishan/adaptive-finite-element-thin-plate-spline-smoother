#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2020-02-01 19:17:27
@Last Modified time: 2022-10-09 23:41:03

This class contains routine to check the number of data points 
in the given element.

"""

# Import libraries
from grid.NodeTable import node_iterator
from grid.EdgeTable import endpt_iterator
from adaptive.triangle.Triangle import triangle_iterator_ref
from data.RegionFunction import obtain_region_index
from adaptive.triangle.SetPolynomial import set_polynomial_linear_2D



def check_data_1D(data, node1, node2):
	""" Obtain a set of data points for interval 1-2 """
	
	# Initialise a data list
	chosen_data = []

	# Iterate through each data point in the interval and add to the list
	for i in range(0, len(data)):

		point = data[i][0]
		x1 = node1.get_coord()[0]
		x2 = node2.get_coord()[0]

		# Add data points that are in the interval
		if ((point >= x1) and (point <= x2)) or \
			((point >= x2) and (point <= x1)):
			chosen_data.append(data[i])
				
	return chosen_data


def check_data_2D(data, node1, node2, node3, tol=0.0):
	""" Obtain a set of data points for triangle 1-2-3 """

	# Initialise a data list and corresponding u list
	chosen_data = []

	# Polynomials of the triangle
	poly1 = set_polynomial_linear_2D(node1, node2, node3)
	poly2 = set_polynomial_linear_2D(node2, node3, node1)
	poly3 = set_polynomial_linear_2D(node3, node1, node2)

	# Iterate through each data point in the triangle and add to the list
	for i in range(0, len(data)):

		x1 = data[i][0]
		x2 = data[i][1]

		# Add data points that are in the triangle
		if poly1.eval(x1, x2)>=tol and poly2.eval(x1, x2)>=tol and poly3.eval(x1, x2)>=tol:
			chosen_data.append(data[i])
			
	return chosen_data



def check_data(region, element):
	""" Check the number of data points in the element """

	# Parameters
	dim = region.get_dim()
	mesh = region.get_mesh()
	region_no = region.get_region_no()

	# Initialise max and min coordinates
	max_coord = [-100000000.0]*dim
	min_coord = [100000000.0]*dim

	# Check dimension consistency
	if not (region.get_dim() == 2 and len(element) == 3) and \
	   not (region.get_dim() == 1 and len(element) == 2):
		print("Warning: data/CheckData - inconsistent dimension")
		return

	# Element is assumed to be a list of nodes
	for node in element:
		coord = node.get_coord()
		for i in range(0, len(coord)):

			# Compare and replace max and min coordinates
			if coord[i] > max_coord[i]:
				max_coord[i] = coord[i]
			if coord[i] < min_coord[i]:
				min_coord[i] = coord[i]

	# Find element of the given coordiantes
	max_index = obtain_region_index(region, max_coord)
	min_index = obtain_region_index(region, min_coord)

	for i in range(0, len(max_index)):
		if max_index[i] < region_no[i]:
			max_index[i] += 1

	chosen_data = []

	if dim == 1:

		# Nodes
		node1 = element[0]
		node2 = element[1]

		# Iterate through the selected mesh sections
		for i in range(min_index[0], max_index[0]):

			data = mesh[i]
				
			local_data = check_data_1D(data, node1, node2)

			chosen_data += local_data

	elif dim == 2:

		# Nodes
		node1 = element[0]
		node2 = element[1]
		node3 = element[2]

		# Iterate through the selected mesh sections
		for i in range(min_index[0], max_index[0]):
			for j in range(min_index[1], max_index[1]):
				
				data = mesh[i][j]

				local_data = check_data_2D(data, node1, node2, node3)

				chosen_data += local_data

	return len(chosen_data)

