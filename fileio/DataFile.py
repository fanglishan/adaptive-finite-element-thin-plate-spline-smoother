#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-04-16 14:40:17
@Last Modified time: 2023-07-26 14:02:14

This class contains functions to write or read from data files (csv).

"""

# Import libaries
import csv


def write_file(data, filename, folder="dataset/"):
	""" Write data into a file in .csv """

	import os

	# Access data file
	path = os.path.dirname(os.path.abspath(__file__))
	parent_path = os.path.abspath(os.path.join(path, os.pardir))
	parent_path = os.path.abspath(os.path.join(parent_path, os.pardir))
	path = parent_path + '\\dataset\\' + filename + ".csv"

 	# Open data file
	data_file = open(path, 'w')

	# Write data
	with data_file:
		writer = csv.writer(data_file)
		writer.writerows(data)
	 
	print("Dataset", filename, "written")
	print("")


def read_file(filename, folder="dataset/"):
	""" Read data from a csv file """

	# Initialise a list for data points
	data = []

	import os

	# Access data file (parent of parent)
	path = os.path.dirname(os.path.abspath(__file__))
	parent_path = os.path.abspath(os.path.join(path, os.pardir))
	parent_path = os.path.abspath(os.path.join(parent_path, os.pardir))
	path = parent_path + '\\dataset\\' + filename + ".csv"

	with open(path) as csvfile:

		# Read numeric fields
		reader = csv.reader(csvfile, quoting=csv.QUOTE_NONNUMERIC)

		# Read each row as a list
		for row in reader:
			if row != []:
				data.append(row)

	return data

