#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-06-13 11:04:52
@Last Modified time: 2022-03-29 11:08:33

This class contains routines to write or read fine grid solution. 
This class is different from the GridFunction as it reads and writes
more information from the grid. The grid read from a file cannot be 
used in the further adaptive refinement process.

"""

# Import libaries
from grid.NodeTable import node_iterator
from grid.EdgeTable import endpt_iterator
from grid.Grid import Grid
from grid.GlobalNodeID import GlobalNodeID
from build.BuildSquare import add_edge
from function.FunctionStore import zero

import csv



def write_soln(grid, filename, folder_name="solution/", label=None):
	""" Write a grid into a file in .csv """

	if label is None:
		source_file = folder_name + filename + ".csv"
	else:
		source_file = folder_name + filename + "_" + label + ".csv"

	# Open a csv file
	smoother_file = open(source_file, 'w')

	grid_info = []

	# Grid dimension
	d = grid.get_dim()

	# Add dimension of the grid
	grid_info.append([d])

	# Add number of nodes
	grid_info.append([grid.get_no_nodes()])

	# Add smoothing parameter alpha
	grid_info.append([grid.get_alpha()])

	# Add nodes id, coordinate and value
	for node in node_iterator(grid):

		row = []

		# Add node id
		row.append(str(node.get_node_id()))

		# Add coordinate
		for i in range(0, d):
			row.append(node.get_coord()[i])

		# Add value
		row.append(node.get_value())

		grid_info.append(row)

	# Add edges (two node id)
	for node in node_iterator(grid):
		node_id = node.get_node_id()
		for endpt in endpt_iterator(grid, node_id): 
			if node_id > endpt:

				grid_info.append([str(node_id), str(endpt)])

	# Write data
	with smoother_file:
		writer = csv.writer(smoother_file)
		writer.writerows(grid_info)

	print("Fine grid solution added")


def read_soln(filename, folder="solution/"):
	""" Read a grid from a csv file """

	# Initialise a list for data points
	grid = Grid()

	grid_info = []

	# Open data file
	with open(folder+filename+".csv") as csvfile:

		# Read numeric fields
		reader = csv.reader(csvfile, quoting=csv.QUOTE_NONNUMERIC)

		# Read each row as a list
		for row in reader:	
			grid_info.append(row)

	# Dimension of the grid
	d = int(grid_info[0][0])

	grid.set_dim(d)

	# Number of nodes
	m = int(grid_info[1][0])

	# Smoothing parameter alpha
	alpha = float(grid_info[2][0])

	grid.set_alpha(alpha)

	# Set default global id (carry no meaning)
	global_id = GlobalNodeID(0,0)

	# Go through each node
	for i in range(0, m):

		row_number = i+3

		node_id = grid_info[row_number][0]

		# Read coord
		coord = []
		for j in range(1, d+1):
			coord.append(grid_info[row_number][j])

		value = grid_info[row_number][d+1]

		# Set up a node
		node = grid.create_node(global_id, coord, False, value)

		# Modify node id number
		node_id1 = node.get_node_id()
		node_id1.set_no(int(node_id))
		node.set_node_id(node_id1)

		grid.add_node(node)

	# The rest are edges
	no_edges = len(grid_info)-2-m

	row_number = m + 3

	# Add each edge to the grid
	for i in range(0, no_edges-1):
		node1 = grid.get_node_ref(str(int(grid_info[row_number][0])))
		node2 = grid.get_node_ref(str(int(grid_info[row_number][1])))

		add_edge(grid, node1.get_node_id(), node2.get_node_id(), None, None, zero)

		row_number += 1


	return grid