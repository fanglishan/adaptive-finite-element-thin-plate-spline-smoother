#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-02-22 08:36:33
@Last Modified time: 2024-04-13 15:11:20

This class contains routines to write the results of the TPSFEM
solution, including errors and grids to a file.

"""

# Import libraries
from fileio.FigureFunction import save_grid_figure, save_indicator_figure
from fileio.GridFunction import write_grid

import time
import matplotlib.pyplot as plt
import os


def create_folder(directory):
	""" Creata a folder """

	try:
		if not os.path.exists(directory):
			os.makedirs(directory)
	except OSError:
		print("Error: Creating directory:" +  directory)


def initialise_file(parameter):

	# Check whether needs to write results
	if not parameter.get_write():
		return

	# Write results to a file
	file_name = parameter.get_folder() + parameter.get_folder()[7:-1] + ".txt"
#	file_name = parameter.get_folder() + "result" + ".txt"

	text_file = open(file_name, "w+")

	text_file.write(str(parameter))
	
	text_file.write("")

	text_file.close()


def write_result(grid, region, parameter, error):
	""" Write the results of the TPSFEM program """

	# Check whether needs to write results
	if not parameter.get_write():
		return

	# Write results to a file
	file_name = parameter.get_folder() + parameter.get_folder()[7:-1] + ".txt"
	text_file = open(file_name, "a")

	text_file.write("== error ==\n")
	text_file.write(str(error))
	text_file.write("\n")

	text_file.close()


	# Save the plot of the TPSFEM grid
#	save_grid_figure(grid, region, parameter)

	# Save the plot of error indication
#	save_indicator_figure(grid, parameter)

	# Write details of the grid
#	write_grid(grid, parameter, False)

	# Write the grid
#	write_file(grid, str(parameter.get_dataset()), \
#		"smoother/", str(parameter.get_indicator()))

	print("Results written")



def write_line(parameter, info, plotInfo=True):
	""" Write a single line to the result file """

	# Print the info
	if plotInfo:
		print(info)
		
	# Check whether needs to write results
	if not parameter.get_write():
		return

	# Open the result file
	file_name = parameter.get_folder() + parameter.get_folder()[7:-1] + ".txt"
	text_file = open(file_name, "a")

	text_file.write(info+"\n")

	text_file.close()