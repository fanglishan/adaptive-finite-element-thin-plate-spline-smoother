#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-04-20 21:57:17
@Last Modified time: 2022-11-01 15:02:40

This class contains routines to save a figure to a file.

"""

# Import libraries
import plot.FemPlot2D as femPlot2D
import plot.TpsfemPlot1D as tpsfemPlot1D
import plot.TpsfemPlot2D as tpsfemPlot2D
import plot.ErrorPlot as errorPlot
import plot.DataErrorPlot as dataErrorPlot
import plot.RefinementPlot as refinementPlot
import plot.IndicatorPlot as indicatorPlot

import matplotlib.pyplot as plt



def save_grid_figure(grid, region, parameter, marker=None):
	""" Plot the finite element grid with highl-lighted bases """

	# Check whether needs to write results
	if not (parameter.get_write() and parameter.get_write_file()[1]):
		return

	# Parameters
	folder = parameter.get_folder()
	dataset = parameter.get_dataset()
	iteration = parameter.get_iteration()

	plt.clf()
	
	# Plot 1D grids
	if grid.get_dim() == 1:
		show_data = True
		tpsfemPlot1D.plot_tpsfem_solution_1D(grid, region, parameter, show_data)

	# Plot 2D grids
	elif grid.get_dim() == 2:
			
		femPlot2D.plot_fem_grid_2D(grid)

	# Save figure
	if marker is None:
		plt.savefig(folder+"grid"+"_"+str(iteration)+'.png')
	else:
		plt.savefig(folder+"grid"+"_"+str(iteration)+ "_" + str(marker) + '.png')


def save_error_figure(grid, parameter):
	""" Plot the error of the current TPSFEM solution """

	# Check whether needs to write results
	if not (parameter.get_write() and parameter.get_write_file()[2]):
		return

	# Parameters
	folder = parameter.get_folder()
	dataset = parameter.get_dataset()
	iteration = parameter.get_iteration()

	plt.clf()
	
	# Plot 1D grids
	if grid.get_dim() == 1:
		
		errorPlot.plot_error_1D(grid, parameter)

	# Plot 2D grids
	elif grid.get_dim() == 2:
			
		errorPlot.plot_error_contour_2D(grid, parameter)

	# Save figure
	plt.savefig(folder+"error_1_"+str(iteration)+'.png')
	
	"""
	if parameter.get_solution_type() == 2:

		plt.clf()
		
		# Plot 1D grids
		if grid.get_dim() == 1:
			
			errorPlot.plot_error_1D(grid, parameter, True)

		# Plot 2D grids
		elif grid.get_dim() == 2:
				
			errorPlot.plot_error_contour_2D(grid, parameter, True)

		# Save figure
		plt.savefig(folder+"error_2_"+str(iteration)+'.png')
	"""


def save_regression_figure(grid, region, parameter):
	""" Save regression error of the current approximation"""

	# Check whether needs to write results
	if not (parameter.get_write() and parameter.get_write_file()[5]):
		return

	# Parameters
	folder = parameter.get_folder()
	dataset = parameter.get_dataset()
	iteration = parameter.get_iteration()
	
	plt.clf()

	# Plot 1D grids
	if grid.get_dim() == 1:
		dataErrorPlot.plot_regression_error_2D(grid, region, parameter, False)

	# Plot 2D grids
	elif grid.get_dim() == 2:
		dataErrorPlot.plot_regression_error_2D(grid, region, parameter, False)

	# Save figure
	plt.savefig(folder+"data_error_"+str(iteration)+'.png')


def save_refinement_figure(grid, parameter, new_node_list):
	""" Plot the error of the current TPSFEM solution """

	# Check whether needs to write results
	if not (parameter.get_write() and parameter.get_write_file()[3]):
		return

	# Parameters
	folder = parameter.get_folder()
	dataset = parameter.get_dataset()
	iteration = parameter.get_iteration()

	plt.clf()
	
	# Plot 1D grids
	if grid.get_dim() == 1:

		refinementPlot.plot_refinement_1D(grid, parameter, new_node_list)

	# Plot 2D grids
	elif grid.get_dim() == 2:
			
		refinementPlot.plot_refinement_2D(grid, parameter, new_node_list)

	# Save figure
	plt.savefig(folder+"refinement"+"_"+str(iteration)+'.png')



def save_indicator_figure(grid, parameter):
	""" Plot the finite element grid with highl-lighted bases """

	# Check whether needs to write results
	if not (parameter.get_write() and parameter.get_write_file()[6]):
		return

	# Parameters
	folder = parameter.get_folder()
	dataset = parameter.get_dataset()
	iteration = parameter.get_iteration()

	plt.clf()
	
	fig = plt.figure(str(dataset)+"_indicator_"+str(grid.get_no_nodes()))

	# Plot 1D grids
	if grid.get_dim() == 1:
		indicatorPlot.plot_error_indicator_1D(grid, fig)

	# Plot 2D grids
	elif grid.get_dim() == 2:
		indicatorPlot.plot_error_indicator_2D(grid, fig)

	# Save figure
	plt.savefig(folder+"indicator"+"_"+str(iteration)+'.png')



def save_contour_figure(grid, region, parameter):
	""" Plot the coutour of the current TPSFEM solution """

	# Check whether needs to write results
	if not (parameter.get_write() and parameter.get_write_file()[4]):
		return

	# Parameters
	folder = parameter.get_folder()
	dataset = parameter.get_dataset()
	iteration = parameter.get_iteration()

	print_data=True
	solution=1
	true_soln=False
	show_plot=False

	plt.clf()

	# Plot 1D grids
	if grid.get_dim() == 1:
		# Duplicate
		pass		
#		tpsfemPlot1D.plot_tpsfem_solution_1D(grid, region, \
#			parameter, print_data, solution, true_soln, show_plot)

	# Plot 2D grids
	elif grid.get_dim() == 2:
			
		tpsfemPlot2D.plot_contour(grid, dataset)
#		tpsfemPlot2D.plot_contour_plotly(grid, dataset)
		# Save figure
		plt.savefig(folder+"TPSFEM"+"_"+str(iteration)+'.png')