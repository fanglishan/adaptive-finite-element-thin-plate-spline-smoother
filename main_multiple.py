#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-04-16 10:13:44
@Last Modified time: 2024-07-26 16:26:07

The TPSFEM main program builds a uniform grid and adaptively refine to obtain
a final solution. This version uses Python 3.7 syntax.

"""

# Import libraries
from tpsfem.main_init import main_init
from tpsfem.main_tpsfem import main_tpsfem
from grid.NodeTable import node_iterator
from grid.EdgeTable import endpt_iterator
from build.TrimElement import trim_elements
from parameter.ParameterInit import init_parameter
from parameter.Definition import IndicatorType
from data.ObtainData import obtain_data_all
from data.ObtainBndData import obtain_boundary_data

import plot.FemPlot2D as femPlot2D
import plot.FemPlot3D as femPlot3D
import plot.TpsfemPlot1D as tpsfemPlot1D
import plot.TpsfemPlot2D as tpsfemPlot2D
import plot.TpsfemGradientPlot2D as gradientPlot2D
import plot.RegionPlot as regionPlot
import plot.DataErrorPlot as dataErrorPlot
import plot.IndicatorPlot as indicatorPlot
import plot.EINormPlot as eiNormPlot
import plot.SurfaceTriangulation as trisurfPlot
import matplotlib.pyplot as plt



def set_indicator_param(indicator):
	""" Set Error indicator parameters """

	# Indicator parameters
	indicator_parameter = []

	# 0-boundary type, 1-number of layers, 2-refine choice, 3-number of uniform refinement, 4-error estimate form, 5-run gcv, 6-subset data, 7-min data
	# Refine choice 2 and 3 leads to the same result
	if indicator == IndicatorType.auxiliary:
		indicator_parameter = [1,2,2,0,2, False, False,-1]
	# 0-boundary type, 1-number of layers, 2-refine choice, 3-source of Dx, 4-ways to improve Dx
	elif indicator == IndicatorType.recovery:
		indicator_parameter = [1,2,0,1,1]
	# 0-number of layers, 1-refine choice, 2-source of Dx, 
	# 3-quadrature rule
	elif indicator == IndicatorType.norm:
		indicator_parameter = [2,0,1,1]
	# 0-boundary type, 1-number of layers, 2-refine choice, 3-ratio, 4-run gcv(EIRecovery1D)
	elif indicator == IndicatorType.residual:
		indicator_parameter = [1,2,1,0.0, False]

	# 0-number of layers, 1-refine choice                 
	elif indicator == IndicatorType.GCV:              ### Depreciated
		indicator_parameter = [2, 1]

	# 0-number of layers, 1-refine choice, 2-error type, 3-smoother type
	elif indicator == IndicatorType.regression:       ### Depreciated
		indicator_parameter = [1, 0, 2, 2]

	return indicator_parameter


def main(dataset,grid_type,max_sweep,indicator, boundary):
	# Basis function: 1-linear, 2-quadratic
	basis = 1

	# Tolerance for the solver
	solver_tol = 1.0E-12

	# Error tolerance
	error_tol = 1e-8
	error_tol = 0.0

	# Refinement approach
	# 1: refine all above error tol
	# 2: refine high indicator iteratively
	refine = 2

	# Maximum number of refinement in one iteration
	max_no = 1

	# Norm type
	# 0-max norm, 1-one norm, 2-two norm
	norm = 1

	# Min data points for refinement
	min_data = 0

	# Whether to run GCV
	run_gcv = True
	#run_gcv = False

	# GCV parameters: 0-whether to use apprximation, 1-max iteration, 2-start bound, 3-end bound (start should be smaller)
	#gcv_parameter = [True, 5, 1e-8, 1e-4]    # 1D
	gcv_parameter = [True, 6, 1e-6, 1e-4]    # 2D


	# Whether to store info: 0-whether to write anything, 1-write grid figure, 2-write error figure, 3-write refine figure
	# 4-write contour figure, 5-write regression error figure, 6-indicator figure
	write_file = [True, True, True, True, True, True, True, True, True]
#	write_file = [False, False, False, False, False, False, False, False]
#	write_file = [True, False, False, False, False, False, False, False]

	# Smoothing parameter
	alpha = 1e-9

	# Whether stop refine based on RMSE
	stop_criteria = False

	# Use only a subset of real-world data
	subset = False

	# Number of uniform refinement before adaptive refinement
	uni_refine = 0

	# Number of nodes (in each direction)
	n = 10

	trim_grid = False

	# Boundary type: 1-Dirichlet, 2-Neumann
#	boundary = 1

	# Type of weights for error indicators
	# 0-no weight, 1-data, 2-A, 3-dX&alpha, 4-activation
	weight = 0

	# Initialisation of boundary node values during adaptive refinement
	# 1-bnd function, 2-average, 3-calculage g using c
	bnd_init = 2

	indicator_parameter = set_indicator_param(indicator)

	###### Start the program #####
	parameter = init_parameter(boundary, basis, alpha, solver_tol, dataset, subset, error_tol, refine, uni_refine, \
			max_no, max_sweep, indicator, indicator_parameter, norm, min_data, \
			run_gcv, gcv_parameter, write_file, weight, bnd_init)

	# Build a initial grid and data region
	grid, region = main_init(parameter, n, grid_type)


	print(parameter)
	print("")

	from timeit import default_timer
	t = default_timer()
	# Adaptively refine grid
	grid = main_tpsfem(grid, region, parameter, stop_criteria)
	
	from fileio.WriteResult import write_line
	write_line(parameter, "iterative algorithm takes"+str(default_timer()-t)+"seconds", True)
#	print("iterative algorithm takes", default_timer()-t, "seconds")


### Real-world data sets
# 1-magnetic data, 2-bone data, 3-lake data, 4-lena data, 5-crater data, 6-iron mine, 
# 7-iron mountain, 8-Aeromagnetic Burney, 9-Grapevine Canyon, 10-river mouth, 11-black beach
# 12-coastal region
dataset = 7

# Real-world data: 1-square domain, 2-use flexible domain, 3-use trimmed domain
grid_type = 1

# Maximum number of refinement sweeps allowed
max_sweep = 8

# 1-Inf, 2-True, 3-Recovery, 4-Auxiliary, 5-Norm, 6-Residual, 7-GCV, 8-regression
indicator = 1

# Boundary type: 1-Dirichlet, 2-Neumann
boundary = 1

#from time import sleep
#sleep(8500)

# Full tests of 1D examples
data_list = [1081203, 1081223, 1101203, 1101223, 1111203, 1111223, 1121203, 1121223, 1131203,1131223, 1141203, 1141243, 1151203, 1151243, 1161203, 1161243, 1171203, 1171223, 1181203, 1181243]


#indicator_list = [3,4]
indicator_list = [1]

for data_item in data_list:
	for indicator_item in indicator_list:
		main(data_item,1,6,indicator_item,1)

