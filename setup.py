# -*- coding: utf-8 -*-
"""
@Author: Fang
@Date:   2022-04-17 10:07:37
@Last Modified by:   Fang
@Last Modified time: 2022-04-17 10:08:31

"""


from setuptools import setup
import sys

setup(
    name='tpsfem',
#    py_modules=['ir_sim'],
    version= '1.0',
    install_requires=[
        'matplotlib',
        'numpy',
        'scipy'
    ],
    description="A TPSFEM program with adaptive refinement features",
    author="Lishan Fang",
)