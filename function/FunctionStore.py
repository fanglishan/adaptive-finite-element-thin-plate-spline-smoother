#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-04-16 10:13:44
@Last Modified time: 2022-11-05 11:17:01

All the model problem functions should be included in the FunctionStore
class. This allows workers to access to the functions.

Adapted from Stals' FEM code.

"""

# Import libraries
import function.Function1D as function1D
import function.Function2D as function2D
import function.Function3D as function3D
import function.ConstantFunction as constFunction
import function.FunctionTPS as functionTPS
from function.ConstantFunction import zero

class FunctionStore:
	""" Store the functions used in the test problems """
	
	# Create an empty list
	_func_list = list()
	
	def __init__(self):
		"""Initialise the function store
		
		The functions used in the test problems should be appended here"""
		
		self._func_list = [zero]

		self._func_list.append(constFunction.magnet)
		self._func_list.append(constFunction.bone)
		self._func_list.append(constFunction.lake)
		self._func_list.append(constFunction.lena)
		self._func_list.append(constFunction.crater)
		self._func_list.append(constFunction.ironmine)
		self._func_list.append(constFunction.mountain)
		self._func_list.append(constFunction.burney)
		self._func_list.append(constFunction.canyon)
		self._func_list.append(constFunction.river)
		self._func_list.append(constFunction.beach)
		self._func_list.append(constFunction.coast)

		self._func_list.append(functionTPS.magnet)
		self._func_list.append(functionTPS.bone)
		self._func_list.append(functionTPS.lake)
		self._func_list.append(functionTPS.lena)
		self._func_list.append(functionTPS.crater)
		self._func_list.append(functionTPS.ironmine)
		self._func_list.append(functionTPS.mountain)
		self._func_list.append(functionTPS.burney)
		self._func_list.append(functionTPS.canyon)
		self._func_list.append(functionTPS.river)
		self._func_list.append(functionTPS.beach)
		self._func_list.append(functionTPS.coast)

		self._func_list.append(function1D.lin_u)
		self._func_list.append(function1D.sqr_u)
		self._func_list.append(function1D.cub_u)
		self._func_list.append(function1D.exp_u)
		self._func_list.append(function1D.sin_u)
		self._func_list.append(function1D.oscisin_u)
		self._func_list.append(function1D.gradexp_u)
		self._func_list.append(function1D.stexp_u)
		self._func_list.append(function1D.exp_fluc_u)
		self._func_list.append(function1D.exp_mul_u)
		self._func_list.append(function1D.frac_u)
		self._func_list.append(function1D.shift_u)
		self._func_list.append(function1D.runge_u)
		self._func_list.append(function1D.blocks_u)
		self._func_list.append(function1D.bumps_u)
		self._func_list.append(function1D.heavisine_u)
		self._func_list.append(function1D.doppler_u)
		self._func_list.append(function1D.hat_u)
		
		
		self._func_list.append(function2D.lin_u)
		self._func_list.append(function2D.sqr_u)
		self._func_list.append(function2D.cub_u)
		self._func_list.append(function2D.exp_u)
		self._func_list.append(function2D.sin_u)
		self._func_list.append(function2D.oscisin_u)
		self._func_list.append(function2D.gradexp_u)
		self._func_list.append(function2D.stexp_u)
		self._func_list.append(function2D.exp_fluc_u)
		self._func_list.append(function2D.exp_mul_u)
		self._func_list.append(function2D.sin_mul_u)
		self._func_list.append(function2D.hole_u)
		self._func_list.append(function2D.franke_u)
		self._func_list.append(function2D.tanh_u)
		self._func_list.append(function2D.cone_u)
		self._func_list.append(function2D.peak_u)
		self._func_list.append(function2D.nonhomo1_u)
		self._func_list.append(function2D.nonhomo2_u)


	def is_in_store(self, func):
		"""Is a given function in the store?"""
		return func in self._func_list
		
	def get_index(self, func):
		"""Get the position of the function in the store"""
		assert self.is_in_store(func), \
			"function not in store "
		return self._func_list.index(func)
		
	def get_function(self, index):
		"""Get the function that is in the given position in the store"""
		assert index >= 0 and index < len(self._func_list), \
			"index out of range "
		return self._func_list[index]