#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-10-14 20:25:53
@Last Modified time: 2022-11-05 10:59:49

This class stores functions that returns constant values.

"""


""" Zero function """
def zero(coord): return 0

""" One function """
def ones(coord): return 1.0


""" Obagoola magnetic data (average value) """
def magnet(coord): return 1978.348552991792

""" Bone data (value near boundary) """
def bone(coord): return 1.0

""" Lake depth data (depth of land is 0) """
def lake(coord): return 0.0

""" Lena image data (average value) """
def lena(coord): return 0.44748539658602493

""" Crater lake data (value near boundary) """
def crater(coord): return 1529.8713664942306

""" Iron mine data (value near boundary) """
def ironmine(coord): return 52553.67531555776

""" Iron mountain data (value near boundary) """
def mountain(coord): return 55537.68658595088

""" Burney data (value near boundary) """
def burney(coord): return 50333.07516862595

""" Canyon data (value near boundary) """
def canyon(coord): return 925.6532456896534

""" River mouth data (value near boundary) """
def river(coord): return 7.256306732186705

""" Black Beach data (value near boundary) """
def beach(coord): return -25.49431732283463

""" Coastal region data (value near boundary) """
def coast(coord): return 0