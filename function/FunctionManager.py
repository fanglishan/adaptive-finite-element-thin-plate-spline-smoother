#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-04-16 13:22:29
@Last Modified time: 2022-11-05 11:00:37

This class manages the functions of the program. The functions will 
take function parameters from the main routine and return corresponding
functions.

"""

# Import libaries
import function.Function1D as f1D
import function.Function2D as f2D
import function.Function3D as f3D
from function.ConstantFunction import zero, ones, magnet, bone, lake, lena, crater, \
	ironmine, mountain, burney, canyon, river, beach
#import function.ConstantFunction as constFunction
import function.FunctionTPS as functionTPS
from parameter.Definition import ModelProblem1D, ModelProblem2D, ModelProblem3D



def obtain_model_function(model_problem, d):
	""" Obtain the model problem functions """

	# For 1D model problems
	if d == 1:

		# Zero
		if model_problem == ModelProblem1D.zero:
			return [zero, zero, zero]

		# Linear
		elif model_problem == ModelProblem1D.linear:
			return [f1D.lin_u, f1D.lin_ux, f1D.lin_uxx]

		# Quadratic
		elif model_problem == ModelProblem1D.quadratic:
			return [f1D.sqr_u, f1D.sqr_ux, f1D.sqr_uxx]

		# Cubic
		elif model_problem == ModelProblem1D.cubic:
			return [f1D.cub_u, f1D.cub_ux, f1D.cub_uxx]

		# Exponential
		elif model_problem == ModelProblem1D.exp:
			return [f1D.exp_u, f1D.exp_ux, f1D.exp_uxx]

		# Sine
		elif model_problem == ModelProblem1D.sine:
			return [f1D.sin_u, f1D.sin_ux, f1D.sin_uxx]

		# Oscillatory sine
		elif model_problem == ModelProblem1D.osci_sine:
			return [f1D.oscisin_u, f1D.oscisin_ux, f1D.oscisin_uxx]

		# Gradual exponential
		elif model_problem == ModelProblem1D.gradual_exp:
			return [f1D.gradexp_u, f1D.gradexp_ux, f1D.gradexp_uxx]

		# Steep exponential
		elif model_problem == ModelProblem1D.steep_exp:
			return [f1D.stexp_u, f1D.stexp_ux, f1D.stexp_uxx]

		# Gradual exponential with some fluctuation
		elif model_problem == ModelProblem1D.exp_fluc:
			return [f1D.exp_fluc_u, f1D.exp_fluc_ux, f1D.exp_fluc_uxx]

		# Multiple exponential functions
		elif model_problem == ModelProblem1D.exp_mul:
			return [f1D.exp_mul_u, f1D.exp_mul_ux, f1D.exp_mul_uxx]

		# Fraction function
		elif model_problem == ModelProblem1D.frac:
			return [f1D.frac_u, f1D.frac_ux, f1D.frac_uxx]

		# Shifted absolute function
		elif model_problem == ModelProblem1D.shift:
			return [f1D.shift_u, f1D.shift_ux, f1D.shift_uxx]

		# The Runge function
		elif model_problem == ModelProblem1D.runge:
			return [f1D.runge_u, f1D.runge_ux, f1D.runge_uxx]

		# The Blocks function
		elif model_problem == ModelProblem1D.blocks:
			return [f1D.blocks_u, f1D.blocks_ux, f1D.blocks_uxx]
		
		# The Bumps function
		elif model_problem == ModelProblem1D.bumps:
			return [f1D.bumps_u, f1D.bumps_ux, f1D.bumps_uxx]

		# The HeaviSine function
		elif model_problem == ModelProblem1D.heavisine:
			return [f1D.heavisine_u, f1D.heavisine_ux, f1D.heavisine_uxx]
		
		# The Doppler function
		elif model_problem == ModelProblem1D.doppler:
			return [f1D.doppler_u, f1D.doppler_ux, f1D.doppler_uxx]
		
		# The Mexican Hat function
		elif model_problem == ModelProblem1D.hat:
			return [f1D.hat_u, f1D.hat_ux, f1D.hat_uxx]
	
	
	# For 2D model problems
	elif d == 2:
		# Special problem index is set in ParameterFunction

		# Lake data model problem
		if model_problem == ModelProblem2D.magnet:
			return [magnet, zero, zero, zero]

		# Bone data model problem
		elif model_problem == ModelProblem2D.bone:
			return [bone, zero, zero, zero]

		# Medicine lake data model problem
		elif model_problem == ModelProblem2D.lake:
			return [lake, zero, zero, zero]

		# Lena image data model problem
		elif model_problem == ModelProblem2D.lena:
			return [lena, zero, zero, zero]

		# Crater lake data model problem
		elif model_problem == ModelProblem2D.crater:
			return [crater, zero, zero, zero]

		# Iron mine data model problem
		elif model_problem == ModelProblem2D.ironmine:
			return [functionTPS.ironmine, functionTPS.ironmine_x, functionTPS.ironmine_y, zero]
#			return [ironmine, zero, zero, zero]

		# Iron mountain data model problem
		elif model_problem == ModelProblem2D.mountain:
			return [functionTPS.mountain, functionTPS.mountain_x, functionTPS.mountain_y, functionTPS.mountain_xxyy]
#			return [mountain, zero, zero, zero]

		# Burney data model problem
		elif model_problem == ModelProblem2D.burney:
			return [functionTPS.burney, functionTPS.burney_x, functionTPS.burney_y, zero]
#			return [burney, zero, zero, zero]

		# Canyon data model problem
		elif model_problem == ModelProblem2D.canyon:
			return [functionTPS.canyon, functionTPS.canyon_x, functionTPS.canyon_y, functionTPS.canyon_xxyy]
#			return [canyon, zero, zero, zero]

		# River mouth data model problem
		elif model_problem == ModelProblem2D.river:
			return [functionTPS.river, functionTPS.river_x, functionTPS.river_y, functionTPS.river_xxyy]
#			return [river, zero, zero, zero]

		# Black beach data model problem
		elif model_problem == ModelProblem2D.beach:
			return [functionTPS.beach, functionTPS.beach_x, functionTPS.beach_y, zero]
#			return [beach, zero, zero, zero]

		# Black beach data model problem
		elif model_problem == ModelProblem2D.coast:
			return [functionTPS.coast, functionTPS.coast_x, functionTPS.coast_y, zero]
#			return [beach, zero, zero, zero]

		# Zero
		elif model_problem == ModelProblem2D.zero:
			return [zero, zero, zero, zero]

		# Linear
		elif model_problem == ModelProblem2D.linear:
			return [f2D.lin_u, f2D.lin_ux, f2D.lin_uy, f2D.lin_uxxyy]

		# Quadratic
		elif model_problem == ModelProblem2D.quadratic:
			return [f2D.sqr_u, f2D.sqr_ux, f2D.sqr_uy, f2D.sqr_uxxyy]

		# Cubic
		elif model_problem == ModelProblem2D.cubic:
			return [f2D.cub_u, f2D.cub_ux, f2D.cub_uy, f2D.cub_uxxyy]

		# Exponential
		elif model_problem == ModelProblem2D.exp:
			return [f2D.exp_u, f2D.exp_ux, f2D.exp_uy, f2D.exp_uxxyy]

		# Sine
		elif model_problem == ModelProblem2D.sine:
			return [f2D.sin_u, f2D.sin_ux, f2D.sin_uy, f2D.sin_uxxyy]

		# Oscillatory sine
		elif model_problem == ModelProblem2D.osci_sine:
			return [f2D.oscisin_u, f2D.oscisin_ux, f2D.oscisin_uy, f2D.oscisin_uxxyy]

		# Gradual exponential
		elif model_problem == ModelProblem2D.gradual_exp:
			return [f2D.gradexp_u, f2D.gradexp_ux, f2D.gradexp_uy, f2D.gradexp_uxxyy]

		# Steep exponential
		elif model_problem == ModelProblem2D.steep_exp:
			return [f2D.stexp_u, f2D.stexp_ux, f2D.stexp_uy, f2D.stexp_uxxyy]

		# Gradual exponential with some fluctuation
		elif model_problem == ModelProblem2D.exp_fluc:
			return [f2D.exp_fluc_u, f2D.exp_fluc_ux, f2D.exp_fluc_uy, f2D.exp_fluc_uxxyy]

		# Multiple exponential functions
		elif model_problem == ModelProblem2D.exp_mul:
			return [f2D.exp_mul_u, f2D.exp_mul_ux, f2D.exp_mul_uy, f2D.exp_mul_uxxyy]

		# Multiple exponential functions
		elif model_problem == ModelProblem2D.exp_mul:
			return [f2D.exp_mul_u, f2D.exp_mul_ux, f2D.exp_mul_uy, f2D.exp_mul_uxxyy]

		# functions
		elif model_problem == ModelProblem2D.sin_mul:
			return [f2D.sin_mul_u, f2D.sin_mul_ux, f2D.sin_mul_uy, f2D.sin_mul_uxxyy]

		# functions
		elif model_problem == ModelProblem2D.hole:
			return [f2D.hole_u, f2D.hole_ux, f2D.hole_uy, f2D.hole_uxxyy]

		# The franke function
		elif model_problem == ModelProblem2D.franke:
			return [f2D.franke_u, f2D.franke_ux, f2D.franke_uy, f2D.franke_uxxyy]

		# Tanh functions
		elif model_problem == ModelProblem2D.tanh:
			return [f2D.tanh_u, f2D.tanh_ux, f2D.tanh_uy, f2D.tanh_uxxyy]

		# Cone function
		elif model_problem == ModelProblem2D.cone:
			return [f2D.cone_u, f2D.cone_ux, f2D.cone_uy, f2D.cone_uxxyy]

		# Peak function
		elif model_problem == ModelProblem2D.peak:
			return [f2D.peak_u, f2D.peak_ux, f2D.peak_uy, f2D.peak_uxxyy]

		# Large peak function
		elif model_problem == ModelProblem2D.lpeak:
			return [f2D.peak_u, f2D.peak_ux, f2D.peak_uy, f2D.peak_uxxyy]

		# Nonhomogeneous test function 1
		elif model_problem == ModelProblem2D.nonhomo1:
			return [f2D.nonhomo1_u, f2D.nonhomo1_ux, f2D.nonhomo1_uy, f2D.nonhomo1_uxxyy]
		
		# Nonhomogeneous test function 2
		elif model_problem == ModelProblem2D.nonhomo2:
			return [f2D.nonhomo2_u, f2D.nonhomo2_ux, f2D.nonhomo2_uy, f2D.nonhomo2_uxxyy]
		
	# For 3D model problems
	elif d == 3:
		return [ones, ones, ones]
		# Zero
		if model_problem == ModelProblem1D.zero:
			return [zero, zero, zero]

	else:
		print("function/FunctionManager - Invalid dimension", d, "or model problem", model_problem)