# -*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang
@Date:   2022-03-24 00:15:46
@Last Modified by:   Fang
@Last Modified time: 2022-11-05 11:02:06

This class contains modules to calculate Dirichlet boundary conditions, 
including c and g values for the TPSFEM. Currently we chose to use the 
TPS to estimate this information.

"""

# Import libraries
from rbf.Kernel import kernel
from rbf.CalcRbf import rbf_calculate_deri
import parameter.InfoBoundary as info


""" Obagoola magnetic data (average value) """
def magnet(coord): return 1970

""" Bone data (value near boundary) """
def bone(coord): return 1.0

""" Lake depth data (depth of land is 0) """
def lake(coord): return 0.0

""" Lena image data (average value) """
def lena(coord): return 0.5#124.0

""" Crater lake data (value near boundary) """
def crater(coord): return 2100.0 #1882.6


""" Iron mine data """
def ironmine(coord, b=1):
	data = info.ironmine_data
	rbf = info.ironmine_tps
	return rbf_calculate_deri(data, rbf, coord, 2, b, 0)

def ironmine_x(coord, b=1):
	data = info.ironmine_data
	rbf = info.ironmine_tps
	return rbf_calculate_deri(data, rbf, coord, 2, b, 1)

def ironmine_y(coord, b=1):
	data = info.ironmine_data
	rbf = info.ironmine_tps
	return rbf_calculate_deri(data, rbf, coord, 2, b, 2)


""" Iron mountain data """
def mountain(coord, b=1):
	data = info.mountain_data
	rbf = info.mountain_tps
	return rbf_calculate_deri(data, rbf, coord, 2, b, 0)

def mountain_x(coord, b=1):
	data = info.mountain_data
	rbf = info.mountain_tps
	return rbf_calculate_deri(data, rbf, coord, 2, b, 1)

def mountain_y(coord, b=1):
	data = info.mountain_data
	rbf = info.mountain_tps
	return rbf_calculate_deri(data, rbf, coord, 2, b, 2)

def mountain_xxyy(coord, b=1):
	data = info.mountain_data
	rbf = info.mountain_tps
	return rbf_calculate_deri(data, rbf, coord, 2, b, 3)


""" Burney data """
def burney(coord, b=1):
	data = info.burney_data
	rbf = info.burney_tps
	return rbf_calculate_deri(data, rbf, coord, 2, b, 0)

def burney_x(coord, b=1):
	data = info.burney_data
	rbf = info.burney_tps
	return rbf_calculate_deri(data, rbf, coord, 2, b, 1)

def burney_y(coord, b=1):
	data = info.burney_data
	rbf = info.burney_tps
	return rbf_calculate_deri(data, rbf, coord, 2, b, 2)


""" Canyon survey data """
def canyon(coord, b=1):
#	return 925.6532456896534
	data = info.canyon_data
	rbf = info.canyon_tps
	return rbf_calculate_deri(data, rbf, coord, 2, b, 0)

def canyon_x(coord, b=1):
	data = info.canyon_data
	rbf = info.canyon_tps
	return rbf_calculate_deri(data, rbf, coord, 2, b, 1)

def canyon_y(coord, b=1):
	data = info.canyon_data
	rbf = info.canyon_tps
	return rbf_calculate_deri(data, rbf, coord, 2, b, 2)

def canyon_xxyy(coord, b=1):
	data = info.canyon_data
	rbf = info.canyon_tps
	return rbf_calculate_deri(data, rbf, coord, 2, b, 3)


""" River mouth data """
def river(coord, b=1):
	data = info.river_data
	rbf = info.river_tps
	return rbf_calculate_deri(data, rbf, coord, 2, b, 0)

def river_x(coord, b=1):
	data = info.river_data
	rbf = info.river_tps
	return rbf_calculate_deri(data, rbf, coord, 2, b, 1)

def river_y(coord, b=1):
	data = info.river_data
	rbf = info.river_tps
	return rbf_calculate_deri(data, rbf, coord, 2, b, 2)

def river_xxyy(coord, b=1):
	data = info.river_data
	rbf = info.river_tps
	return rbf_calculate_deri(data, rbf, coord, 2, b, 3)


""" Black Beach data """
def beach(coord, b=1):
	data = info.beach_data
	rbf = info.beach_tps
	return rbf_calculate_deri(data, rbf, coord, 2, b, 0)

def beach_x(coord, b=1):
	data = info.beach_data
	rbf = info.beach_tps
	return rbf_calculate_deri(data, rbf, coord, 2, b, 1)

def beach_y(coord, b=1):
	data = info.beach_data
	rbf = info.beach_tps
	return rbf_calculate_deri(data, rbf, coord, 2, b, 2)


""" Coastal region data """
def coast(coord, b=1):
	data = info.coast_data
	rbf = info.coast_tps
	return rbf_calculate_deri(data, rbf, coord, 2, b, 0)

def coast_x(coord, b=1):
	data = info.coast_data
	rbf = info.coast_tps
	return rbf_calculate_deri(data, rbf, coord, 2, b, 1)

def coast_y(coord, b=1):
	data = info.coast_data
	rbf = info.coast_tps
	return rbf_calculate_deri(data, rbf, coord, 2, b, 2)