#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2021-07-04 23:42:01
@Last Modified time: 2022-03-29 11:08:59

This class contains a list of 3D model problem functions.

"""