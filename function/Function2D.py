#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-04-16 13:22:29
@Last Modified time: 2022-03-29 11:08:56

This class contains a list of 2D model problem functions.

Adapted from Stals' FEM code.

"""

# Import libraries
from numpy import sqrt
from math import pi, sin, exp, cos, tanh



# An exponential function with squares of two variables
def exp_squ_f(x, height, steep1, centre1, steep2, centre2):
	return height*exp(steep1*(x[0]+centre1)**2+steep2*(x[1]+centre2)**2)

def exp_squ_fx(x, height, steep1, centre1, steep2, centre2):
	return 2.0*steep1*(x[0]+centre1)*exp_squ_f(x, height, steep1, centre1, steep2, centre2)

def exp_squ_fy(x, height, steep1, centre1, steep2, centre2):	
	return 2.0*steep2*(x[1]+centre2)*exp_squ_f(x, height, steep1, centre1, steep2, centre2)

def exp_squ_fxxyy(x, height, steep1, centre1, steep2, centre2):
	u = exp_squ_f(x, height, steep1, centre1, steep2, centre2)
	return 2.0*steep1*u*(1.0+2.0*steep1*(x[0]+centre1)**2) + 2.0*steep2*u*(1.0+2.0*steep2*(x[1]+centre2)**2)


# A product of sine functions
def sin_f(x, height, steep1, steep2):
	return height*sin(steep1*pi*x[0])*sin(steep2*pi*x[1])

def sin_fx(x, height, steep1, steep2):
	return height*steep1*pi*cos(steep1*pi*x[0])*sin(steep2*pi*x[1])

def sin_fy(x, height, steep1, steep2):
	return height*steep2*pi*sin(steep1*pi*x[0])*cos(steep2*pi*x[1])

def sin_fxxyy(x, height, steep1, steep2):
	return -height*pi*pi*(steep1**2+steep2**2)*sin(steep1*pi*x[0])*sin(steep2*pi*x[1])



""" Linear function x0+x1 """
def lin_u(x): 
	return x[0]+x[1]

def lin_ux(x): 
	return 1

def lin_uy(x): 
	return 1

def lin_uxxyy(x): 
	return 0


""" Square function x_0^2+x_1^2 """
def sqr_u(x): 
	return x[0]*x[0]+x[1]*x[1]
 
def sqr_ux(x): 
	return 2*x[0]

def sqr_uy(x): 
	return 2*x[1]
			
def sqr_uxxyy(x): 
	return 4


""" Cubic function x_0^3+x_1^3 """
def cub_u(x):
	return x[0]**3+x[1]**3
 
def cub_ux(x):
	return 3*(x[0]**2)

def cub_uy(x):
	return 3*(x[1]**2)

def cub_uxxyy(x):
	return 6*x[0]+6*x[1]


""" Exponential function exp(x_0)exp(x_1) """
def exp_u(x):
	return exp(x[0])*exp(x[1])

def exp_ux(x):
	return exp(x[0])*exp(x[1])
   
def exp_uy(x):
	return exp(x[0])*exp(x[1])
	
def exp_uxxyy(x):
	return 2.0*exp(x[0])*exp(x[1])


""" Sine function sin(pi*x_0)sin(pi*x_1) """
def sin_u(x):
	return sin(pi*x[0])*sin(pi*x[1])
	
def sin_ux(x):
	return pi*cos(pi*x[0])*sin(pi*x[1])

def sin_uy(x):
	return pi*sin(pi*x[0])*cos(pi*x[1])
	
def sin_uxxyy(x):
	return -2.0*pi*pi*sin(pi*x[0])*sin(pi*x[1])


""" Oscillatory sine function a*sin(b*pi*x_0)sin(c*pi*x_1) """
c = [1.0, 4.0, 4.0]
def oscisin_u(x):
	return sin_f(x,c[0],c[1],c[2])
	
def oscisin_ux(x):
	return sin_fx(x,c[0],c[1],c[2])

def oscisin_uy(x):
	return sin_fy(x,c[0],c[1],c[2])
	
def oscisin_uxxyy(x):
	return sin_fxxyy(x,c[0],c[1],c[2])


""" Gradual exponential function exp(-a*(x_0-b)**2)exp(-a*(x_0-b)**2) """
a = 30    # size (how steep is the slope)
b = 0.5   # center of dataset
def gradexp_u(x):
	return exp(-a*(x[0]-b)**2)*exp(-a*(x[1]-b)**2)

def gradexp_ux(x):
	return (-2*a*x[0]+2*a*b)*gradexp_u(x)

def gradexp_uy(x):
	return (-2*a*x[1]+2*a*b)*gradexp_u(x)

def gradexp_uxxyy(x):
	u = gradexp_u(x)
	return (-2*a*u+((-2*a*x[0]+2*a*b)**2)*u) + \
		   (-2*a*u+((-2*a*x[1]+2*a*b)**2)*u)


""" Steep exponential function exp(-d*(x_0-e)**2)exp(-d*(x_0-e)**2) """
d = 50    # size (how steep is the slope)
e = 0.5   # center of dataset
def stexp_u(x):
	return exp(-d*(x[0]-e)**2)*exp(-d*(x[1]-e)**2)

def stexp_ux(x):
	return (-2*d*x[0]+2*d*e)*stexp_u(x)

def stexp_uy(x):
	return (-2*d*x[1]+2*d*e)*stexp_u(x)

def stexp_uxxyy(x):
	return (-2*d*stexp_u(x)+((-2*d*x[0]+2*d*e)**2)*stexp_u(x)) + \
		   (-2*d*stexp_u(x)+((-2*d*x[1]+2*d*e)**2)*stexp_u(x))


""" Steep exponential function exp(-d*(x_0-e)**2)exp(-d*(x_0-e)**2) """
n = 100    # size (how steep is the slope)
o = 0.5   # center of dataset
def exp_fluc_u(x):
	return exp(-n*(x[0]-o)**2)*exp(-n*(x[1]-o)**2)

def exp_fluc_ux(x):
	return (-2*n*x[0]+2*n*o)*exp_fluc_u(x)

def exp_fluc_uy(x):
	return (-2*n*x[1]+2*n*o)*exp_fluc_u(x)

def exp_fluc_uxxyy(x):
	u = exp_fluc_u(x)
	return (-2*n*u+((-2*n*x[0]+2*n*o)**2)*u) + \
		   (-2*n*u+((-2*n*x[1]+2*n*o)**2)*u)



""" Multiple exponential function exp(-a*(x_0-b)**2)exp(-a*(x_0-b)**2) """
f = 30    # size (how steep is the slope)
g = [0.7,0.7]   # center of dataset
h = 100
i = [0.2,0.2]
j = 0.2
k = 100
l = [0.2,0.4]
m = -0.2

def exp_mul_u(x):
	return exp(-f*(x[0]-g[0])**2)*exp(-f*(x[1]-g[1])**2) + \
			j*exp(-h*(x[0]-i[0])**2)*exp(-h*(x[1]-i[1])**2) + \
			m*exp(-k*(x[0]-l[0])**2)*exp(-k*(x[1]-l[1])**2)

def exp_mul_ux(x):
	u = exp_mul_u(x)
	return (-2*f*x[0]+2*f*g[0])*u + \
			j*(-2*h*x[0]+2*h*i[0])*u + \
			m*(-2*k*x[0]+2*k*l[0])*u

def exp_mul_uy(x):
	u = exp_mul_u(x)
	return (-2*f*x[1]+2*f*g[1])*u + \
			j*(-2*h*x[1]+2*h*i[1])*u + \
			m*(-2*k*x[1]+2*k*l[1])*u

def exp_mul_uxxyy(x):
	u = exp_mul_u(x)
	return (-2*f*u+((-2*f*x[0]+2*f*g[0])**2)*u) + \
		   (-2*f*u+((-2*f*x[1]+2*f*g[1])**2)*u) + \
			j*(-2*h*u+((-2*h*x[0]+2*h*i[0])**2)*u) + \
		   j*(-2*h*u+((-2*h*x[1]+2*h*i[1])**2)*u) + \
			m*(-2*k*u+((-2*k*x[0]+2*k*l[0])**2)*u) + \
		   m*(-2*k*u+((-2*k*x[1]+2*k*l[1])**2)*u)



""" Multiple oscillatory sine function """
p = [0.4, 1.0, 1.0]
q = [0.1, 4.0, 4.0]
r = [0.05, 15.0, 15.0]
def sin_mul_u(x):
	return 0.5+sin_f(x,p[0],p[1],p[2])+sin_f(x,q[0],q[1],q[2])+sin_f(x,r[0],r[1],r[2])
	
def sin_mul_ux(x):
	return sin_fx(x,p[0],p[1],p[2])+sin_fx(x,q[0],q[1],q[2])+sin_fx(x,r[0],r[1],r[2])

def sin_mul_uy(x):
	return sin_fy(x,p[0],p[1],p[2])+sin_fy(x,q[0],q[1],q[2])+sin_fy(x,r[0],r[1],r[2])
	
def sin_mul_uxxyy(x):
	return sin_fxxyy(x,p[0],p[1],p[2])+sin_fxxyy(x,q[0],q[1],q[2])+sin_fxxyy(x,r[0],r[1],r[2])


""" Hole function in a diamond domain """
def hole_u(x):
	return (0.25-(abs(x[0]-0.5)+abs(x[1]-0.5)))*(0.5-(abs(x[0]-0.5)+abs(x[1]-0.5)))

def hole_ux(x):
	if x[0] >= 0.5 and x[1] >= 0.5:
		return 2*x[0]+2*x[1]-2.75
	elif x[0] >= 0.5 and x[1] < 0.5:
		return 2*x[0]-2*x[1]-0.75
	elif x[0] < 0.5 and x[1] >= 0.5:
		return 2*x[0]-2*x[1]+0.75
	elif x[0] < 0.5 and x[1] < 0.5:	
		return 2*x[0]+2*x[1]-1.25

def hole_uy(x):
	if x[0] >= 0.5 and x[1] >= 0.5:
		return 2*x[1]+2*x[0]-2.75
	elif x[0] >= 0.5 and x[1] < 0.5:
		return 2*x[1]-2*x[0]+0.75
	elif x[0] < 0.5 and x[1] >= 0.5:
		return 2*x[1]-2*x[0]-0.75
	elif x[0] < 0.5 and x[1] < 0.5:	
		return 2*x[1]+2*x[0]-1.25

def hole_uxxyy(x):
	return 4.0

# The Franke function
s = [1.0, -0.1, 0.0, -0.1, 0.0]
t = [1.0, -5.0, -0.5, -5.0, -0.5]
u = [1.0, -15.0, 0.2, -15.0, 0.4]
v = [1.0, -9.0, 0.8, -9.0, -0.8]
	
def franke_u(x):
	return exp_squ_f(x, s[0], s[1], s[2], s[3], s[4])+exp_squ_f(x, t[0], t[1], t[2], t[3], t[4])+\
			exp_squ_f(x, u[0], u[1], u[2], u[3], u[4])+exp_squ_f(x, v[0], v[1], v[2], v[3], v[4])

def franke_ux(x):
	return exp_squ_fx(x, s[0], s[1], s[2], s[3], s[4])+exp_squ_fx(x, t[0], t[1], t[2], t[3], t[4])+\
			exp_squ_fx(x, u[0], u[1], u[2], u[3], u[4])+exp_squ_fx(x, v[0], v[1], v[2], v[3], v[4])

def franke_uy(x):
	return exp_squ_fy(x, s[0], s[1], s[2], s[3], s[4])+exp_squ_fy(x, t[0], t[1], t[2], t[3], t[4])+\
			exp_squ_fy(x, u[0], u[1], u[2], u[3], u[4])+exp_squ_fy(x, v[0], v[1], v[2], v[3], v[4])

def franke_uxxyy(x):
	return exp_squ_fxxyy(x, s[0], s[1], s[2], s[3], s[4])+exp_squ_fxxyy(x, t[0], t[1], t[2], t[3], t[4])+\
			exp_squ_fxxyy(x, u[0], u[1], u[2], u[3], u[4])+exp_squ_fxxyy(x, v[0], v[1], v[2], v[3], v[4])


# The Tanh function
def tanh_u(x):
	return -0.4*tanh(20.0*x[0]*x[1])+0.6

def tanh_ux(x):
	return -8.0*x[1]*(1.0-tanh(20.0*x[0]*x[1])**2)

def tanh_uy(x):
	return -8.0*x[0]*(1.0-tanh(20.0*x[0]*x[1])**2)

def tanh_uxxyy(x):
	u = tanh(20*x[0]*x[1])
	return 320.0*(x[0]**2+x[1]**2)*(u-u**3)


# The cone function
def cone_u(x):
	return sqrt(x[0]**2+x[1]**2)+0.2

def cone_ux(x):
	return x[0]/sqrt(x[0]**2+x[1]**2)

def cone_uy(x):
	return x[1]/sqrt(x[0]**2+x[1]**2)

def cone_uxxyy(x):
	return 2.0/sqrt(x[0]**2+x[1]**2)-(x[0]**2+x[1]**2)*(1.0/sqrt(x[0]**2+x[1]**2))**3


# The peak function
def peak_u(x):
	return 3*(1-x[0])**2*exp(-x[0]**2-(x[1]+1)**2)-10.0*(x[0]/5.0-x[0]**3-x[1]**5)*exp(-x[0]**2-x[1]**2)-1.0/3*exp(-(x[0]+1)**2-x[1]**2)

def peak_ux(x):
	return -2.0/3*exp(-x[0]**2-2*x[0]-(x[1]+1)**2)*(exp(2*x[0]+2*x[1]+1)*(30*x[0]**4-51*x[0]**2+30*x[0]*x[1]**5+3)+9*exp(2*x[0])*(x[0]**3-2*x[0]**2+1)+(x[0]+1)*(-exp(2*x[0])))

def peak_uy(x):
	return 1.0/3*exp(-x[0]**2-2*x[0]-(x[1]+1)**2)*(-6*x[1]*exp(2*x[0]+2*x[1]+1)*(10*x[0]**3-2*x[0]+5*x[1]**3*(2*x[1]**2-5))-18*exp(2*x[0])*(1-x[0])**2*(x[1]+1)+2*exp(2*x[1])*x[1])

def peak_uxxyy(x):
	dxx=2.0/3*exp(-x[0]**2-2*x[0]-(x[1]+1)**2)*((2*x[0]**2+4*x[0]+1)*(-exp(2*x[1]))+9*exp(2*x[0])*x[0]*(2*x[0]**3-4*x[0]**2-3*x[0]+6)+6*exp(2*x[0]+2*x[1]+1)*(10*x[0]**5-37*x[0]**3+10*x[0]**2*x[1]**5+18*x[0]-5*x[1]**5))
	dyy=2.0/3*exp(-x[0]**2-2*x[0]-(x[1]+1)**2)*(6*exp(2*x[0]+2*x[1]+1)*(5*x[0]**3*(2*x[1]**2-1)-2*x[0]*x[1]**2+x[0]+5*x[1]**3*(2*x[1]**4-11*x[1]**2+10))+9*exp(2*x[0])*(1-x[0])**2*(2*x[1]**2+4*x[1]+1)+exp(2*x[1])*(1-2*x[1]**2))
	return dxx+dyy


# The nonhomogeneous test function 1
exp2 = 2**(-7/5.0)
def nonhomo1_u(x):
	return sqrt(x[0]*(1-x[0]))*sin(2*pi*(1+exp2)/(x[0]+exp2))+sqrt(x[1]*(1-x[1]))*sin(2*pi*(1+exp2)/(x[1]+exp2))

def nonhomo1_ux(x):
	return 0

def nonhomo1_uy(x):
	return 0
		
def nonhomo1_uxxyy(x):
	dxx = 0
	dyy = 0
	return dxx+dyy


# The nonhomogeneous test function 2
def nonhomo2_u(x):
	return 1.9*(1.2*sqrt(x[0])+exp(x[0])*sin(20*(x[0]-0.7)**2))*(exp(-2*x[1])*sin(6*x[1]**2))/(x[0]+0.1)

def nonhomo2_ux(x):
	return 0

def nonhomo2_uy(x):
	return 0
		
def nonhomo2_uxxyy(x):
	dxx = 0
	dyy = 0
	return dxx+dyy