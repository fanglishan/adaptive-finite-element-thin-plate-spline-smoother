#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-04-16 13:22:29
@Last Modified time: 2022-03-29 11:08:52

This class contains a list of 1D model problem functions.

Adapted from Stals' FEM code.

"""

# Import libraries
from math import pi, sin, exp, cos
from numpy import sign
from scipy.stats import norm


# A standard exponential function
def exp_f(x, height, steep, centre):
	return height*exp(-steep*(x[0]-centre)**2)

def exp_fx(x, height, steep, centre):
	return height*exp(-steep*(x[0]-centre)**2)*(-2*steep*(x[0]-centre))

def exp_fxx(x, height, steep, centre):
	f = exp_f(x, height, steep, centre)
	return height*(f*4*steep*steep*(x[0]-centre)**2-2*steep*f)


	
""" Linear function x_0 """
def lin_u(x):
	return x[0]
 
def lin_ux(x):
	return 1

def lin_uxx(x):
	return 0


""" Square function x_0^2 """
def sqr_u(x):
	return x[0]*x[0]
 
def sqr_ux(x):
	return 2*x[0]

def sqr_uxx(x):
	return 2


""" Cubic function x_0^3 """
def cub_u(x):
	return x[0]**3
 
def cub_ux(x):
	return 3*x[0]*x[0]

def cub_uxx(x):
	return 6*x[0]


""" Exponential function exp(x_0) """
def exp_u(x):
	return exp(x[0])

def exp_ux(x):
	return exp(x[0])

def exp_uxx(x):
	return exp(x[0])


""" Sine function sin(pi*x_0) """
def sin_u(x):
	return sin(pi*x[0])
	
def sin_ux(x):
	return pi*cos(pi*x[0])

def sin_uxx(x):
	return -pi*pi*sin(pi*x[0])


""" Oscillatory sine function sin(b*pi*x_0) """
b = 2
def oscisin_u(x):
	return sin(b*pi*x[0])
	
def oscisin_ux(x):
	return b*pi*cos(b*pi*x[0])

def oscisin_uxx(x):
	return -b*b*pi*pi*sin(b*pi*x[0])



""" Gradual exponential function exp(-a*x_0*x0)exp(-a*x_1*x[0]) """
a = [1,30,0.5]
def gradexp_u(x):
#	return exp(-a*(x[0]-0.5)**2)
	return exp_f(x,a[0],a[1],a[2])

def gradexp_ux(x):
#	return (-2*a*x[0]+a)*exp(-a*(x[0]-0.5)**2)
	return exp_fx(x,a[0],a[1],a[2])

def gradexp_uxx(x):
#	return 2*a*exp(-a*(x[0]-0.5)**2)-(-2*a*x[0]+a)**2*exp(-a*(x[0]-0.5)**2)
	return exp_fxx(x,a[0],a[1],a[2])
"""
def gradexp_u(x):
	return exp_f(x, 1.0, a, 0.5)

def gradexp_ux(x):
	return exp_fx(x, 1.0, a, 0.5)

def gradexp_uxx(x):
	return exp_fxx(x, 1.0, a, 0.5)
"""


""" Steep exponential function exp(-c*x_0*x0)exp(-c*x_1*x1) """
c = [1,50,0.5]
def stexp_u(x):
#	return exp(-c*(x[0]-0.5)**2)
	return exp_f(x,c[0],c[1],c[2])

def stexp_ux(x):
#	return (-2*c*x[0]+c)*exp(-c*(x[0]-0.5)**2)
	return exp_fx(x,c[0],c[1],c[2])

def stexp_uxx(x):
#	return 2*c*exp(-c*(x[0]-0.5)**2)-(-2*c*x[0]+c)**2*exp(-c*(x[0]-0.5)**2)
	return exp_fxx(x,c[0],c[1],c[2])


def fluctuation(x):
	if x[0] > 0.12 and x[0] < 0.16:
		return 0.05*sin(50*pi*x[0])
	else:
		return 0

""" Exponential function exp(-d*x_0*x0)exp(-d*x_1*x1) with some fluctuation """

d = [1,50,0.5]
# x start, x end, magnitude, frequency
#e = [[0.06, 0.08, 0.03, 100],[0.12, 0.14, 0.03, 200], [0.86, 0.88, 0.05, 100], [0.92, 0.94, 0.05, 200]]
#e = [[0.04, 0.06, 0.01, 100], [0.12, 0.13, 0.01, 200], [0.87, 0.88, 0.03, 200], [0.94, 0.96, 0.03, 100]]
e = [[-0.3, -0.26, 0.1, 50]]#, [0.87, 0.88, 0.01, 200]]
#e = [[0.22, 0.28, 0.05, 50]]

def fluctuation(x):
	for i in range(0, len(e)):
		if x[0] > e[i][0] and x[0] < e[i][1]:
			return e[i][2]*sin(e[i][3]*pi*x[0])
	else:
		return 0

def exp_fluc_u(x):
	return exp_f(x,d[0],d[1],d[2]) + fluctuation(x)

def exp_fluc_ux(x):
	return exp_fx(x,d[0],d[1],d[2])

def exp_fluc_uxx(x):
	return exp_fxx(x,d[0],d[1],d[2])


""" Multiple exponential function exp(-c*x_0*x0)exp(-c*x_1*x1) """
f = [1, 50, 0.8]
g = [0.2, 100, 0.1]
h = [-0.2, 100, 0.2]

def exp_mul_u(x):
	return exp_f(x, f[0], f[1], f[2])+exp_f(x, g[0], g[1], g[2])+exp_f(x, h[0], h[1], h[2])
#	return exp(-f*(x[0]-g)**2)+j*exp(-h*(x[0]-i)**2)+m*exp(-k*(x[0]-l)**2)

def exp_mul_ux(x):
	return exp_fx(x, f[0], f[1], f[2])+exp_fx(x, g[0], g[1], g[2])+exp_fx(x, h[0], h[1], h[2])
#	return (-2*f*x[0]+f)*exp(-f*(x[0]-g)**2)+j*(-2*h*x[0]+h)*exp(-h*(x[0]-i)**2)+\
#			m*(-2*k*x[0]+k)*exp(-k*(x[0]-l)**2)

def exp_mul_uxx(x):
	return exp_fxx(x, f[0], f[1], f[2])+exp_fxx(x, g[0], g[1], g[2])+exp_fxx(x, h[0], h[1], h[2])
#	return 2*f*exp(-f*(x[0]-g)**2)-(-2*f*x[0]+f)**2*exp(-f*(x[0]-g)**2) + \
#		j*(2*h*exp(-h*(x[0]-i)**2)-(-2*h*x[0]+h)**2*exp(-h*(x[0]-i)**2)) + \
#		m*(2*k*exp(-k*(x[0]-l)**2)-(-2*k*x[0]+k)**2*exp(-k*(x[0]-l)**2))



""" Fraction function 10.0*x[0]/(1+100*x[0]**2) """
def frac_u(x):
	return 10.0*x[0]/(1+100*x[0]**2)

def frac_ux(x):
	return (10.0-1000.0*x[0]**2)/(1+100*x[0]**2)**2

def frac_uxx(x):
	v1 = (1+100*x[0]**2)**2
	return (v1*-2000.0*x[0]-(10.0-1000*x[0]**2)*400.0*x[0]*(1+400*x[0]**2))/v1**2



""" Shifted absolute function abs(x[0]-0.04) """
def shift_u(x):
	return abs(x[0]-0.04)

def shift_ux(x):
	if x[0] > 0.04:
		return 1.0
	elif x[0] < 0.04:
		return -1.0
	else:
		return 0.0

def shift_uxx(x):
	return 0.0



""" The runge function """
def runge_u(x):
	return 1.0/(1+25*x[0]**2)

def runge_ux(x):
	return -50.0*x[0]/(1+25.0*x[0]**2)**2

def runge_uxx(x):
	v2 = 1+25.0*x[0]**2
	return (-50.0*v2**2+5000.0*x[0]**2*v2)/v2**4



""" Blocks function """
t_list = [0.1, 0.13, 0.15, 0.23, 0.25, 0.4, 0.44, 0.65, 0.76, 0.78, 0.81]
h_list = [4, -5, 3, -4, 5, -4.2, 2.1, 4.3, -3.1, 2.1, -4.2]

def blocks_u(x):
	value = 0
	for i in range(len(t_list)):
		value += h_list[i]*(1+sign(x[0]-t_list[i]))/2.0
	return value

def blocks_ux(x): # Derivatives near boundaries
	return 0.0

def blocks_uxx(x):
	return 0.0


""" Bumps function """
h_bump = [4, 5, 3, 4, 5, 4.2, 2.1, 4.3, 3.1, 5.1, 4.2]
w_bump = [0.005, 0.005, 0.006, 0.01, 0.01, 0.03, 0.01, 0.01, 0.005, 0.008, 0.005]
def bumps_u(x):
	value = 0
	for i in range(len(t_list)):
		value += h_bump[i]*(1+abs((x[0]-t_list[i])/w_bump[i]))**(-4)
	return value

def bumps_ux(x):
	return 0.0

def bumps_uxx(x):
	return 0.0


""" Heavisine function """
def heavisine_u(x):
	return 4*sin(4*pi*x[0])-sign(x[0]-0.3)-sign(0.72-x[0])

def heavisine_ux(x):
	return 16*pi*cos(4*pi*x[0])

def heavisine_uxx(x):
	return -64*pi*pi*sin(4*pi*x[0])


""" Doppler function """
def doppler_u(x):
	e = 0.05
	return (x[0]*(1-x[0]))**0.5*sin(2*pi*(1+e)/(x[0]+e))

def doppler_ux(x):
	# Assume e = 0.05
	return -2.1*pi*(x[0]*(1 - x[0]))**0.5*cos(2.1*pi/(x[0] + 0.05))/(x[0] + 0.05)**2 + (x[0]*(1 - x[0]))**0.5*(0.5 - 1.0*x[0])*sin(2.1*pi/(x[0] + 0.05))/(x[0]*(1 - x[0]))

def doppler_uxx(x): 
	# Assume e = 0.05
	return 4.2*pi*(x[0]*(1 - x[0]))**0.5*cos(2.1*pi/(x[0] + 0.05))/(x[0] + 0.05)**3 - 4.41*pi**2*(x[0]*(1 - x[0]))**0.5*sin(2.1*pi/(x[0] + 0.05))/(x[0] + 0.05)**4 - 4.2*pi*(x[0]*(1 - x[0]))**0.5*(0.5 - 1.0*x[0])*cos(2.1*pi/(x[0] + 0.05))/(x[0]*(1 - x[0])*(x[0] + 0.05)**2) + (x[0]*(1 - x[0]))**0.5*(0.5 - 1.0*x[0])*sin(2.1*pi/(x[0] + 0.05))/(x[0]*(1 - x[0])**2) - 1.0*(x[0]*(1 - x[0]))**0.5*sin(2.1*pi/(x[0] + 0.05))/(x[0]*(1 - x[0])) + (x[0]*(1 - x[0]))**0.5*(0.5 - 1.0*x[0])**2*sin(2.1*pi/(x[0] + 0.05))/(x[0]**2*(1 - x[0])**2) - (x[0]*(1 - x[0]))**0.5*(0.5 - 1.0*x[0])*sin(2.1*pi/(x[0] + 0.05))/(x[0]**2*(1 - x[0]))


# Examples 6 and 7 from luo1997hybrid
#	return sin(2(4x - 2)) + 2exp(-16x2)
#	return 4x - 2) + 2exp(-16x2)
	
""" Mexican Hat function """
def hat_u(x):
	return -1 + 1.5*x[0] + 0.2*norm.pdf(x[0]-0.6, 0.0, 0.02)

def hat_ux(x):
	return 1.5

def hat_uxx(x):
	return 0

