#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-04-16 10:13:44
@Last Modified time: 2024-10-13 22:48:55

The TPSFEM main program builds a uniform grid and adaptively refine to obtain
a final solution. This version uses Python 3.7 syntax.

"""

# Import libraries
from tpsfem.main_init import main_init
from tpsfem.main_tpsfem import main_tpsfem
from grid.NodeTable import node_iterator
from grid.EdgeTable import endpt_iterator
from build.TrimElement import trim_elements
from parameter.ParameterInit import init_parameter
from parameter.Definition import IndicatorType
from data.ObtainData import obtain_data_all
from data.ObtainBndData import obtain_boundary_data
from fileio.GridFunction import read_grid
from indicator.EIParameter import get_indicator_param
import plot.FemPlot2D as femPlot2D
import plot.FemPlot3D as femPlot3D
import plot.TpsfemPlot1D as tpsfemPlot1D
import plot.TpsfemPlot2D as tpsfemPlot2D
import plot.TpsfemGradientPlot2D as gradientPlot2D
import plot.RegionPlot as regionPlot
import plot.DataErrorPlot as dataErrorPlot
import plot.IndicatorPlot as indicatorPlot
import plot.EINormPlot as eiNormPlot
import plot.SurfaceTriangulation as trisurfPlot
import matplotlib.pyplot as plt



# Basis function: 1-linear, 2-quadratic
basis = 1

# Tolerance for the solver
solver_tol = 1.0E-12

# Error tolerance
error_tol = 1e-4
#error_tol = 0.0

# Maximum number of refinement in one iteration
max_no = 1

# Norm type
# 0-max norm, 1-one norm, 2-two norm
norm = 1

# Min data points for refinement
min_data = 0

# Whether to run GCV
#run_gcv = False
run_gcv = True

# GCV parameters: 0-whether to use apprximation, 1-max iteration, 2-start bound, 3-end bound (start should be smaller)
#gcv_parameter = [True, 5, 1e-8, 1e-4]    # 1D
gcv_parameter = [True, 6, 1e-6, 1e-4]    # 2D


# Whether to store info: 0-whether to write anything, 1-write grid figure, 2-write error figure, 3-write refine figure
# 4-write contour figure, 5-write regression error figure, 6-indicator figure
#write_file = [True, True, True, True, True, True, True, True, True]

# Write nothing
write_file = [False, False, False, False, False, False, False, False]

# Write txt file only
#write_file = [True, False, False, False, False, False, False, False]

# Write txt file and grids into csv files
#write_file = [True, False, False, False, False, False, False, True]


# Smoothing parameter
alpha = 1e-7

# Whether stop refine based on RMSE
stop_criteria = False


### Dataset
# 1:dimension, 2-3:model, 4:distribution, 5:noise distribution, 6:noise level, 7:data size
### 1D Model problem
# zero=0, linear=1, quadratic=2, cubic=3, exp=4, sine=5, osci_sine=6, 
# grad_exp=7, steep_exp=8, exp_fluc=9, exp_mul=10, frac=11, shift=12, runge=13
# blocks=14, bumps=15, Heavisine=16, Doppler=17, Mexican_hat=18
### 2D Model problem
# zero=0, linear=1, quadratic=2, cubic=3, exp=4, sine=5, osci_sine=6, grad_exp=7, steep_exp=8
# exp_fluc=9, exp_mul=10, sin_mul=11, hole=12, franke=13, tanh=14, cone=15, peak=16
# peak(large domain)=17, nonhomosmooth1 = 18, nonhomosmooth2 = 19
### Real-world data sets
# 1-magnetic data, 2-bone data, 3-lake data, 4-lena data, 5-crater data, 6-iron mine, 
# 7-iron mountain, 8-Aeromagnetic Burney, 9-Grapevine Canyon, 10-river mouth, 11-black beach
# 12-coastal region
### Data distribution
# 1-uniform, 2-uniform with holes, 3-uniformly random, 4-normally random, 5-line data, 6-L-shaped
### Noise distribution pattern
# 1-uniform, 2-normal
### Noise level
# uniform:     0-0.0, 1-0.001, 2-0.005, 3-0.01, 4-0.03, 5-0.05, 6-0.06, 7-0.08, 8-0.09, 9-0.1
# normal (sd): 0-0.0, 1-0.005, 2-0.01, 3-0.02, 4-0.05, 5-0.07, 6-0.1, 7-0.15, 8-0.2, 9-0.25
### Data size
# 0-3, 1-10, 2-20, 3-50, 4-100, 5-250, 6-500, 7-750, 8-1000, 9-1500, 10-2000


dataset = 1181281
#dataset = 1181201
dataset = 2081202

# Use only a subset of real-world data
subset = False

# Number of uniform refinement before adaptive refinement
uni_refine = 0

# Maximum number of refinement sweeps allowed
max_sweep = 3

# Number of nodes (in each direction)
n = 10

# Real-world data: 1-square domain, 2-use irregular domain, 3-use trimmed domain
grid_type = 1

# Whether to trim elements without data points from the square grid
trim_grid = False

# Boundary type: 1-Dirichlet, 2-Neumann
boundary = 1

# Marking strategy
# 1: refine all above preset error tol
# 2: refine 10% edges with highest error indicator till # nodes double
mark = 1

# 1-Inf, 2-True, 3-Recovery, 4-Auxiliary, 5-Norm, 6-Residual, 7-GCV, 8-regression
indicator = 3

# Type of weights for error indicators
# 0-no weight, 1-data, 2-A, 3-dX&alpha, 4-activation
weight = 0

# Initialisation of boundary node values during adaptive refinement
# 1-bnd function, 2-average, 3-calculage g using c
bnd_init = 1

indicator_parameter = get_indicator_param(indicator)

###### Start the program #####
parameter = init_parameter(boundary, basis, alpha, solver_tol, dataset, subset, error_tol, mark, uni_refine, \
		max_no, max_sweep, indicator, indicator_parameter, norm, min_data, \
		run_gcv, gcv_parameter, write_file, weight, bnd_init)


#print(parameter.get_weight())
# Build a initial grid and data region
grid, region = main_init(parameter, n, grid_type)

#grid = read_grid("9_6608", "result/")
#grid = read_grid("9_7294", "result/")

"""
data = obtain_data_all(region)
print(len(data))
no_sample = 10000
chosen_data = obtain_boundary_data(data, grid, no_sample)
print(chosen_data)
#femPlot2D.plot_fem_grid_data_2D(grid, chosen_data, parameter=None)
from sys import exit
exit()
"""


#if grid_type == 1 and trim_grid:
#	grid = trim_elements(grid, region, parameter, trim_grid)

"""
#femPlot2D.plot_fem_grid_2D(grid, parameter, False)
data = obtain_data_all(region)
femPlot2D.plot_fem_grid_data_2D(grid, data)
plt.show()
from sys import exit
exit()
"""

print(parameter)
print("")

from timeit import default_timer
t = default_timer()
# Adaptively refine grid
grid = main_tpsfem(grid, region, parameter, stop_criteria)

from fileio.WriteResult import write_line
write_line(parameter, "Iterative adaptive refinement process takes "+str(default_timer()-t)+" seconds", True)


#from adaptive.tetrahedron.UniformRefinement import uniform_refinement

#grid = uniform_refinement(grid, parameter)

show_plot = True

if write_file[0]:
	show_plot = False


if show_plot:

	min_value = 100000000
	max_value = -100000000
	for node in node_iterator(grid):
		min_value = min(node.get_value(), min_value)
		max_value = max(node.get_value(), max_value)
	print("Range:", min_value, "to", max_value)


	# Plot resulting grids
	if parameter.get_dim() == 1:
		tpsfemPlot1D.plot_tpsfem_solution_1D(grid, region, parameter, True, 1, True)
#		dataErrorPlot.plot_data_error_1D(grid, region, parameter)
#		dataErrorPlot.plot_data_error_hist_1D(grid, region, parameter)
#		indicatorPlot.plot_error_indicator(grid, parameter)
#		eiNormPlot.plot_Dx_approximation_1D(grid, parameter)
#		eiNormPlot.plot_Dxx_approximation_1D(grid, parameter)
		pass

	elif parameter.get_dim() == 2:
#		tpsfemPlot2D.plot_tpsfem_solution_data_2D(grid, region, False, False)
		femPlot2D.plot_fem_grid_2D(grid, parameter, False)
#		data = obtain_data_all(region)
#		femPlot2D.plot_fem_grid_data_2D(grid, data, parameter=None)
#		tpsfemPlot2D.plot_tpsfem_solution_2D(grid)
#		tpsfemPlot2D.plot_tpsfem_solution_2D(grid, True)
#		gradientPlot2D.plot_gradient_contour(grid,1)
#		gradientPlot2D.plot_tpsfem_solution_2D(grid, True, 2)
#		gradientPlot2D.plot_tpsfem_solution_2D(grid, True, 3)
#		indicatorPlot.plot_error_indicator(grid, parameter)
#		tpsfemPlot2D.plot_tpsfem_solution_2D(grid)
#		tpsfemPlot2D.plot_contour_plotly(grid)
#		tpsfemPlot2D.plot_contour(grid)
#		trisurfPlot.plot_surface_triangulation(grid)
#		dataErrorPlot.plot_regression_error_2D(grid, region, parameter, True)
		pass

	elif parameter.get_dim() == 3:
#		femPlot3D.plot_fem_grid_3D(grid)
#		femPlot3D.plot_fem_grid_3D_line(grid)
		pass
#	regionPlot.plot_region(region, grid)

	plt.show()