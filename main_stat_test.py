
#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-06-13 10:13:44
@Last Modified time: 2024-10-04 21:20:15

This class plots a TPSFEM solution from a stored grid in a file.

"""

# Import libraries
from grid.NodeTable import node_iterator
from grid.EdgeTable import endpt_iterator
from build.CopyGrid import copy_grid
from function.FunctionManager import obtain_model_function
from parameter.Parameter import Parameter
from parameter.ModelProblemFunction import find_model_problem
from estimator.Error import Error
from fileio.GridFunction import read_grid
from grid.NodeTable import node_iterator
from grid.EdgeTable import endpt_iterator
from adaptive.triangle.Triangle import triangle_iterator, triangle_iterator_ref
from adaptive.triangle.GridValue import cal_value_2d, cal_smoother_2d
from adaptive.line.GridValue import cal_value_1d, cal_smoother_1d
from data.ObtainData import obtain_data, obtain_data_all
from data.RegionFunction import build_region
from tpsfem.main_init import set_region_no, obtain_dataset

import plot.FemPlot2D as femPlot2D
import plot.TpsfemPlot1D as tpsfemPlot1D
import plot.TpsfemPlot2D as tpsfemPlot2D
import plot.TpsfemPlotly2D as tpsfemPlotly2D
import plot.IndicatorPlot as indicatorPlot
import plot.ErrorPlot as errorPlot
import plot.SurfaceTriangulation as stPlot
import matplotlib.pyplot as plt

from numpy import sqrt
import numpy as np





#	plot_data_hist(mesh_sizes)

"""
# Obtain grid for error estimation
#path = "result/" + "result_1_6_20_02_01_11_02" + "/"
#filename = "1_5677"#path[14:19]
#filename = "1_177"
path = "result/" 
#path = "result/" + "result_5_4_21_03_20_14_11" + "/"
#filename = path[14:21]


#filename_list = ["7_16641", "7_6948", "7_6564", "7_6923", "7_6656", "7_11969", "7_7576", "7_7600", "7_7634", "7_7560", "9_16641", "9_6612", "9_6528", "9_6608", "9_6534", "9_10241", "9_7238", "9_7265", "9_7294", "9_7204", "10_16641", "10_6740", "10_6642", "10_6828", "10_6688", "10_9488", "10_6880", "10_6792", "10_6928", "10_6816"]
#filename = "10_10193"
filename_list = ["10_6816"]
#tol_list = [0.05, 0.01, 0.005, 0.001]
tol_list = [0.005]


for filename in filename_list:

	print("===", filename)

	for tol in tol_list:

		d = 2
		grid = read_grid(filename, path)
		parameter = Parameter()

#		tol = 0.005

		calc_nearboundary_nodes(grid, tol)

		obtain_mesh_sizes(grid, tol)

		print("")
"""


#filename = "2081134_1521"
filename = "9_7204"

d = 2




grid = read_grid(filename, "result/")

#grid = copy_grid(grid)

dataset = 9

#dataset = int(filename[:7])

parameter = Parameter()


# Obtain data
path = "dataset/"+str(dataset)+".csv"
#data, data_size = obtain_data(parameter, dataset, path)
data = obtain_dataset(dataset)
data_size = len(data)

# Set number of data subdivision regions
region_no = set_region_no(parameter, d, dataset, data_size)

# Build a data region
region = build_region(data, d, region_no, grid)

region.init_usage()

parameter.set_dataset(dataset)

error_list = []

for tri in triangle_iterator_ref(grid):
	if tri[0].get_node_id() < tri[1].get_node_id() and \
		tri[1].get_node_id() < tri[2].get_node_id():

		data, u = obtain_data(region, tri)

		for data_point in data:
			predictor = cal_value_2d(tri[0], tri[1], tri[2], data_point[0:2])
			error = abs(predictor - data_point[-1])

			error_list.append(error)


print(len(error_list))
print(min(error_list), max(error_list))
from scipy.stats import kstest
from scipy import stats

print(filename)

print("normal distribution")
ksresult = kstest(error_list, stats.norm.cdf)
print(ksresult)

print("uniform distribution")
ksresult = kstest(error_list, stats.uniform.cdf)
print(ksresult)


import matplotlib.pyplot as plt
plt.hist(error_list, bins= 32, color='skyblue', edgecolor='black')
plt.show()
# Find model problems
#model_problem = find_model_problem(dataset)
#function = obtain_model_function(model_problem, d)
#parameter.set_function(function)


#print(dataset)
#print("grid node number", grid.get_no_nodes())


show_plot = False

if show_plot:

	# Plot resulting grids
	if grid.get_dim() == 1:
#		indicatorPlot.plot_error_indicator(grid, parameter)
#		tpsfemPlot1D.plot_tpsfem_solution_1D(grid, None, None, False)

		pass

	elif grid.get_dim() == 2:
#		stPlot.plot_surface_triangulation(grid)
#		femPlot2D.plot_fem_grid_2D(grid, None, False)
#		tpsfemPlot2D.plot_tpsfem_solution_2D(grid)
#		tpsfemPlot2D.plot_contour(grid)
#		tpsfemPlotly2D.plot_contour(grid)
#		errorPlot.plot_error_contour_2D(grid, parameter)
		pass
#	plt.xlim(-0.05,1.05)
#	plt.ylim(-0.05,1.05)
	plt.show()