#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-04-25 23:40:23
@Last Modified time: 2022-11-03 00:05:03

This class keeps a record a FEM node and provides some elementary 
operations on the node.

Adapted from Stals' FEM code.

"""

# Import libraries
from grid.NodeID import NodeID
from grid.GlobalNodeID import GlobalNodeID

from numpy import array
from copy import copy


class Node:
    """ A finite element node"""
    
    # Node dimension (should match up with the dimension of the
    # node table)
    _dim = 0
    
    # Local node id
    _node_id = NodeID()
    
    # Global node id
    _global_id = GlobalNodeID()
    
    # Is the node a slave node (i.e. does it sit on the boundary?)
    _slave = False
    
    # What is the value assigned to the node
    _value = 0.0
    
    # Load value for orientation of tetrahedral grid
    _orient_load = 0.0

    # Data load (d entry)
    _data_load = 0.0
    
    # K^T*u vector for GCV
    _KTu_load = 0.0

    # What are the node's coordinates (size should be same as dim)
    _coord = array([])
   
    # derivative values (dx, dy, dxxyy)
    _d_values = array([])

    # Smoothing parameter
    _alpha = 0.0
   
    # Other parameters
    _parameters = array([])


    def __init__(self, dim=2, coord=[0, 0], slave=False, value=0.0, \
            data_load=0.0, alpha=0.0, KTu_load=0.0, orient_load=0.0):
        """Initialise a node
        
        By default, the node is a two dimensional node with 
        coordinates (0,0), value 0 and load 0. It is also assumed
        to be an interior node (i.e. not a slave node)
        """
        self._dim = dim
        self._coord = coord
        self._slave = slave
        self._value = value
        self._data_load = data_load
        self._node_id = NodeID()
        self._global_id = GlobalNodeID()
        self._alpha = alpha
        self._parameters = [0]*dim
        self._KTu_load = KTu_load
        self._orient_load = orient_load

    def __str__(self):
        """Convert a global node id into a string so it can be printed
        
        The format is the local id (global id) value
        
        """
        return str(self._node_id) + \
            "  (" + str(self._global_id) + ") "+ \
            repr(self._value)
    
     
    def set_node_id(self, node_id):
        """Set the local node id
        
        The local id is copied across
        
        """
        self._node_id = copy(node_id)
    
    def set_global_id(self, global_id):
        """Set the global node id
        
        The global id is copied across
        
        """
        self._global_id = copy(global_id)
        
    def set_dim(self, dim):
        """Set the node dimension"""
        self._dim = dim

    def set_orient_load(self, orient_load):
        """ Set the orient load """
        self._orient_load = orient_load   

    def set_data_load(self, data_load):
        """Set the data load (d value)"""
        self._data_load = data_load   

    def set_KTu_load(self, KTu_load):
        """Set the KTu load (K^T*u value)"""
        self._KTu_load = KTu_load  

    def set_value(self, value):
        """Set current value"""
        self._value = value
    
    def set_slave(self, slave):
        """Is the node a slave node (T/F)"""
        self._slave = slave
        
    def set_coord(self, coord):
        """Set the coordinates
        
        The length of the coordinates should agree with the
        node dimension
        
        """
        assert self._dim == len(coord), \
            "coordinates should be of length %d"% self._dim 
        self._coord = coord[:]


    def set_d_values(self, d_values):
        """Set the derivative values"""
        self._d_values = d_values

    def set_parameters(self, parameters):
        """ Set parameters """
        self._parameters = parameters

    def set_alpha(self, alpha):
        """ Set smoothing parameter """
        self._alpha = alpha

    def set_parameters_entry(self, entry_value, index):
        """ Set one entry of the parameters """
        self._parameters[index] = entry_value

    def add_parameters_entry(self, entry_value, index):
        """ Add value to one entry of the parameters """
        self._parameters[index].append(entry_value)

    def get_node_id(self):
        """Get the local node id"""
        node_id = copy(self._node_id)
        return node_id
        
    def get_global_id(self):
        """Get the global node id"""
        global_id = copy(self._global_id)
        return global_id 
    
    def get_dim(self):
        """Get the node dimension"""
        return self._dim 

    def get_orient_load(self):
        """Get the orient load """
        return self._orient_load  

    def get_data_load(self):
        """Get the data load (d value)"""
        return self._data_load  

    def get_KTu_load(self):
        """Get the KTu load (K^T*u value)"""
        return self._KTu_load  

    def get_value(self):
        """Get the node value"""
        return self._value
        
    def get_slave(self):
        """Is the node a slave node (T/F)"""
        return self._slave
    
    def get_coord(self):
        """Get the node coordinates"""
        coord = self._coord[:]
        return coord
    
    def get_d_values(self):
        """Get the derivative values"""
        return self._d_values

    def get_alpha(self):
        """ Get smoothing parameter """
        return self._alpha

    def get_parameters(self):
        """ Get the the parameters """
        return self._parameters

    def get_parameters_entry(self, index):
        """ Get the the parameters entry """
        return self._parameters[index]   