#-*- coding: utf-8 -*-
"""
The adaptive TPSFEM program interpolates and smooths large data sets.
Copyright (C) 2022 Fang

@Author: Fang

@Date:   2019-04-25 23:40:23
@Last Modified time: 2022-03-29 11:16:05

This class stores a finite element grid. It currently inherits most of the
operations from the NodeTable and EdgeTable classes.

Adapted from Stals' FEM code.

"""

# Import libraries
from grid.EdgeTable import EdgeTable
from grid.NodeTable import NodeTable
from grid.ConnectTable import ConnectTable


class Grid(NodeTable, EdgeTable, ConnectTable):
	""" A finite element grid"""

	# Smoothing parameter
	_alpha = 0.0

	def __init__(self, dim = 2):
		"""Initialise a edge table
		
		By default, creates an empty table
		"""
		self._alpha = 0.0

		NodeTable.__init__(self, dim)
		EdgeTable.__init__(self)
		ConnectTable.__init__(self)

	def set_alpha(self, alpha):
		""" Set smoothing parameter alpha """
		self._alpha = alpha

	def get_alpha(self):
		""" Get smoothing parameter alpha """
		return self._alpha